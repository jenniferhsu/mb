/*
  ==============================================================================

  This is an automatically generated GUI class created by the Introjucer!

  Be careful when adding custom code to these files, as only the code within
  the "//[xyz]" and "//[/xyz]" sections will be retained when the file is loaded
  and re-saved.

  Created with Introjucer version: 3.1.0

  ------------------------------------------------------------------------------

  The Introjucer is part of the JUCE library - "Jules' Utility Class Extensions"
  Copyright 2004-13 by Raw Material Software Ltd.

  ==============================================================================
*/

//[Headers] You can add your own extra header files here...
//[/Headers]

#include "PluginEditor.h"


//[MiscUserDefs] You can add your own user definitions and misc code here...
//[/MiscUserDefs]

//==============================================================================
Mb1AudioProcessorEditor::Mb1AudioProcessorEditor (Mb1AudioProcessor* ownerFilter)
    : AudioProcessorEditor(ownerFilter)
{
    addAndMakeVisible (lowFreqSld = new Slider ("lowFreqSld"));
    lowFreqSld->setRange (0, 1, 0.001);
    lowFreqSld->setSliderStyle (Slider::TwoValueHorizontal);
    lowFreqSld->setTextBoxStyle (Slider::NoTextBox, false, 30, 20);
    lowFreqSld->setColour (Slider::thumbColourId, Colour (0x54ffffff));
    lowFreqSld->setColour (Slider::trackColourId, Colour (0x7fffffff));
    lowFreqSld->setColour (Slider::rotarySliderFillColourId, Colour (0x61ffffff));
    lowFreqSld->setColour (Slider::textBoxTextColourId, Colour (0x75ffffff));
    lowFreqSld->setColour (Slider::textBoxBackgroundColourId, Colour (0x00ffffff));
    lowFreqSld->setColour (Slider::textBoxHighlightColourId, Colour (0x40fffcfc));
    lowFreqSld->setColour (Slider::textBoxOutlineColourId, Colour (0x00ffffff));
    lowFreqSld->addListener (this);

    addAndMakeVisible (freqCutoffLbl = new Label ("freq cutoff lablel",
                                                  "FREQUENCY CUTOFFS"));
    freqCutoffLbl->setFont (Font (15.00f, Font::plain));
    freqCutoffLbl->setJustificationType (Justification::centredLeft);
    freqCutoffLbl->setEditable (false, false, false);
    freqCutoffLbl->setColour (Label::textColourId, Colours::white);
    freqCutoffLbl->setColour (TextEditor::textColourId, Colours::black);
    freqCutoffLbl->setColour (TextEditor::backgroundColourId, Colour (0x00000000));

    addAndMakeVisible (delTimeSld = new Slider ("delay time slider"));
    delTimeSld->setRange (0, 1, 0.01);
    delTimeSld->setSliderStyle (Slider::RotaryHorizontalVerticalDrag);
    delTimeSld->setTextBoxStyle (Slider::NoTextBox, false, 50, 20);
    delTimeSld->setColour (Slider::backgroundColourId, Colour (0x00000000));
    delTimeSld->setColour (Slider::thumbColourId, Colours::black);
    delTimeSld->setColour (Slider::trackColourId, Colour (0x75ffffff));
    delTimeSld->setColour (Slider::rotarySliderFillColourId, Colour (0x7fffffff));
    delTimeSld->setColour (Slider::rotarySliderOutlineColourId, Colour (0xbaffffff));
    delTimeSld->setColour (Slider::textBoxTextColourId, Colour (0x75ffffff));
    delTimeSld->setColour (Slider::textBoxBackgroundColourId, Colours::black);
    delTimeSld->setColour (Slider::textBoxHighlightColourId, Colours::white);
    delTimeSld->setColour (Slider::textBoxOutlineColourId, Colour (0x00ffffff));
    delTimeSld->addListener (this);

    addAndMakeVisible (delayTimeLbl = new Label ("delay time lbl",
                                                 "DELAY TIME"));
    delayTimeLbl->setFont (Font (15.00f, Font::plain));
    delayTimeLbl->setJustificationType (Justification::centredLeft);
    delayTimeLbl->setEditable (false, false, false);
    delayTimeLbl->setColour (Label::textColourId, Colours::white);
    delayTimeLbl->setColour (TextEditor::textColourId, Colours::black);
    delayTimeLbl->setColour (TextEditor::backgroundColourId, Colour (0x00000000));

    addAndMakeVisible (delTimeSld2 = new Slider ("delay time slider"));
    delTimeSld2->setRange (0, 1, 0.01);
    delTimeSld2->setSliderStyle (Slider::RotaryHorizontalVerticalDrag);
    delTimeSld2->setTextBoxStyle (Slider::NoTextBox, false, 50, 20);
    delTimeSld2->setColour (Slider::thumbColourId, Colours::black);
    delTimeSld2->setColour (Slider::trackColourId, Colour (0x75ffffff));
    delTimeSld2->setColour (Slider::rotarySliderFillColourId, Colour (0x7fffffff));
    delTimeSld2->setColour (Slider::rotarySliderOutlineColourId, Colour (0xbaffffff));
    delTimeSld2->setColour (Slider::textBoxTextColourId, Colour (0x75ffffff));
    delTimeSld2->setColour (Slider::textBoxBackgroundColourId, Colours::black);
    delTimeSld2->setColour (Slider::textBoxHighlightColourId, Colours::white);
    delTimeSld2->setColour (Slider::textBoxOutlineColourId, Colour (0x00000000));
    delTimeSld2->addListener (this);

    addAndMakeVisible (lowFreqLbl = new Label ("low frequency label",
                                               "LOW"));
    lowFreqLbl->setFont (Font (15.00f, Font::plain));
    lowFreqLbl->setJustificationType (Justification::centredLeft);
    lowFreqLbl->setEditable (false, false, false);
    lowFreqLbl->setColour (Label::textColourId, Colours::white);
    lowFreqLbl->setColour (TextEditor::textColourId, Colours::black);
    lowFreqLbl->setColour (TextEditor::backgroundColourId, Colour (0x00000000));

    addAndMakeVisible (delTimeSld3 = new Slider ("delay time slider"));
    delTimeSld3->setRange (0, 1, 0.01);
    delTimeSld3->setSliderStyle (Slider::RotaryHorizontalVerticalDrag);
    delTimeSld3->setTextBoxStyle (Slider::NoTextBox, false, 50, 20);
    delTimeSld3->setColour (Slider::thumbColourId, Colours::black);
    delTimeSld3->setColour (Slider::trackColourId, Colour (0x75ffffff));
    delTimeSld3->setColour (Slider::rotarySliderFillColourId, Colour (0x7fffffff));
    delTimeSld3->setColour (Slider::rotarySliderOutlineColourId, Colour (0xbaffffff));
    delTimeSld3->setColour (Slider::textBoxTextColourId, Colour (0x75ffffff));
    delTimeSld3->setColour (Slider::textBoxBackgroundColourId, Colour (0x00ffffff));
    delTimeSld3->setColour (Slider::textBoxHighlightColourId, Colours::white);
    delTimeSld3->setColour (Slider::textBoxOutlineColourId, Colour (0x00ffffff));
    delTimeSld3->addListener (this);

    addAndMakeVisible (midFreqLbl = new Label ("mid frequency label",
                                               "MID"));
    midFreqLbl->setFont (Font (15.00f, Font::plain));
    midFreqLbl->setJustificationType (Justification::centredLeft);
    midFreqLbl->setEditable (false, false, false);
    midFreqLbl->setColour (Label::textColourId, Colours::white);
    midFreqLbl->setColour (TextEditor::textColourId, Colours::black);
    midFreqLbl->setColour (TextEditor::backgroundColourId, Colour (0x00000000));

    addAndMakeVisible (ampLowSld = new Slider ("low freq amp slider"));
    ampLowSld->setRange (0, 1, 0.001);
    ampLowSld->setSliderStyle (Slider::RotaryHorizontalVerticalDrag);
    ampLowSld->setTextBoxStyle (Slider::NoTextBox, false, 50, 20);
    ampLowSld->setColour (Slider::thumbColourId, Colour (0x75ffffff));
    ampLowSld->setColour (Slider::trackColourId, Colour (0x75ffffff));
    ampLowSld->setColour (Slider::rotarySliderFillColourId, Colour (0x7fffffff));
    ampLowSld->setColour (Slider::rotarySliderOutlineColourId, Colour (0xbaffffff));
    ampLowSld->setColour (Slider::textBoxTextColourId, Colour (0x75ffffff));
    ampLowSld->setColour (Slider::textBoxBackgroundColourId, Colour (0x00ffffff));
    ampLowSld->setColour (Slider::textBoxHighlightColourId, Colours::white);
    ampLowSld->setColour (Slider::textBoxOutlineColourId, Colour (0x00ffffff));
    ampLowSld->addListener (this);

    addAndMakeVisible (ampMidSld = new Slider ("mid freq amp slider"));
    ampMidSld->setRange (0, 1, 0.001);
    ampMidSld->setSliderStyle (Slider::RotaryHorizontalVerticalDrag);
    ampMidSld->setTextBoxStyle (Slider::NoTextBox, false, 50, 20);
    ampMidSld->setColour (Slider::thumbColourId, Colours::white);
    ampMidSld->setColour (Slider::rotarySliderFillColourId, Colour (0x7fffffff));
    ampMidSld->setColour (Slider::rotarySliderOutlineColourId, Colour (0xbaffffff));
    ampMidSld->setColour (Slider::textBoxTextColourId, Colour (0x75ffffff));
    ampMidSld->setColour (Slider::textBoxBackgroundColourId, Colour (0x00ffffff));
    ampMidSld->setColour (Slider::textBoxHighlightColourId, Colours::white);
    ampMidSld->setColour (Slider::textBoxOutlineColourId, Colour (0x00ffffff));
    ampMidSld->addListener (this);

    addAndMakeVisible (ampHighSld = new Slider ("high freq amp slider"));
    ampHighSld->setRange (0, 1, 0.001);
    ampHighSld->setSliderStyle (Slider::RotaryHorizontalVerticalDrag);
    ampHighSld->setTextBoxStyle (Slider::NoTextBox, false, 50, 20);
    ampHighSld->setColour (Slider::thumbColourId, Colours::white);
    ampHighSld->setColour (Slider::rotarySliderFillColourId, Colour (0x7fffffff));
    ampHighSld->setColour (Slider::rotarySliderOutlineColourId, Colour (0xbaffffff));
    ampHighSld->setColour (Slider::textBoxTextColourId, Colour (0x75ffffff));
    ampHighSld->setColour (Slider::textBoxBackgroundColourId, Colour (0x00ffffff));
    ampHighSld->setColour (Slider::textBoxHighlightColourId, Colours::white);
    ampHighSld->setColour (Slider::textBoxOutlineColourId, Colour (0x00ffffff));
    ampHighSld->addListener (this);

    addAndMakeVisible (ampLbl = new Label ("amplitude label",
                                           "AMPLITUDE"));
    ampLbl->setFont (Font (15.00f, Font::plain));
    ampLbl->setJustificationType (Justification::centredLeft);
    ampLbl->setEditable (false, false, false);
    ampLbl->setColour (Label::textColourId, Colours::white);
    ampLbl->setColour (TextEditor::textColourId, Colours::black);
    ampLbl->setColour (TextEditor::backgroundColourId, Colour (0x00000000));

    addAndMakeVisible (highFreqLbl2 = new Label ("high frequency label",
                                                 "HIGH"));
    highFreqLbl2->setFont (Font (15.00f, Font::plain));
    highFreqLbl2->setJustificationType (Justification::centredLeft);
    highFreqLbl2->setEditable (false, false, false);
    highFreqLbl2->setColour (Label::textColourId, Colours::white);
    highFreqLbl2->setColour (TextEditor::textColourId, Colours::white);
    highFreqLbl2->setColour (TextEditor::backgroundColourId, Colour (0x00000000));

    addAndMakeVisible (fbLowSld = new Slider ("low freq fb slider"));
    fbLowSld->setRange (0, 0.99, 0.01);
    fbLowSld->setSliderStyle (Slider::RotaryHorizontalVerticalDrag);
    fbLowSld->setTextBoxStyle (Slider::NoTextBox, false, 50, 20);
    fbLowSld->setColour (Slider::thumbColourId, Colours::white);
    fbLowSld->setColour (Slider::rotarySliderFillColourId, Colour (0x80ffffff));
    fbLowSld->setColour (Slider::rotarySliderOutlineColourId, Colour (0xbaffffff));
    fbLowSld->setColour (Slider::textBoxTextColourId, Colour (0x75ffffff));
    fbLowSld->setColour (Slider::textBoxBackgroundColourId, Colour (0x00ffffff));
    fbLowSld->setColour (Slider::textBoxHighlightColourId, Colours::white);
    fbLowSld->setColour (Slider::textBoxOutlineColourId, Colour (0x00ffffff));
    fbLowSld->addListener (this);

    addAndMakeVisible (fbLbl = new Label ("feedback label",
                                          "FEEDBACK"));
    fbLbl->setFont (Font (15.00f, Font::plain));
    fbLbl->setJustificationType (Justification::centredLeft);
    fbLbl->setEditable (false, false, false);
    fbLbl->setColour (Label::textColourId, Colours::white);
    fbLbl->setColour (TextEditor::textColourId, Colours::black);
    fbLbl->setColour (TextEditor::backgroundColourId, Colour (0x00000000));

    addAndMakeVisible (fbMidSld = new Slider ("mid freq fb slider"));
    fbMidSld->setRange (0, 0.99, 0.01);
    fbMidSld->setSliderStyle (Slider::RotaryHorizontalVerticalDrag);
    fbMidSld->setTextBoxStyle (Slider::NoTextBox, false, 50, 20);
    fbMidSld->setColour (Slider::thumbColourId, Colours::white);
    fbMidSld->setColour (Slider::trackColourId, Colour (0x75ffffff));
    fbMidSld->setColour (Slider::rotarySliderFillColourId, Colour (0x79ffffff));
    fbMidSld->setColour (Slider::rotarySliderOutlineColourId, Colour (0xbaffffff));
    fbMidSld->setColour (Slider::textBoxTextColourId, Colour (0x75ffffff));
    fbMidSld->setColour (Slider::textBoxBackgroundColourId, Colour (0x00ffffff));
    fbMidSld->setColour (Slider::textBoxHighlightColourId, Colours::white);
    fbMidSld->setColour (Slider::textBoxOutlineColourId, Colour (0x00ffffff));
    fbMidSld->addListener (this);

    addAndMakeVisible (fbHighSld = new Slider ("high freq fb slider"));
    fbHighSld->setRange (0, 0.99, 0.01);
    fbHighSld->setSliderStyle (Slider::RotaryHorizontalVerticalDrag);
    fbHighSld->setTextBoxStyle (Slider::NoTextBox, false, 50, 20);
    fbHighSld->setColour (Slider::thumbColourId, Colour (0x75ffffff));
    fbHighSld->setColour (Slider::rotarySliderFillColourId, Colour (0x7fffffff));
    fbHighSld->setColour (Slider::rotarySliderOutlineColourId, Colour (0xbaffffff));
    fbHighSld->setColour (Slider::textBoxTextColourId, Colour (0x75ffffff));
    fbHighSld->setColour (Slider::textBoxBackgroundColourId, Colour (0x00ffffff));
    fbHighSld->setColour (Slider::textBoxHighlightColourId, Colours::white);
    fbHighSld->setColour (Slider::textBoxOutlineColourId, Colour (0x00ffffff));
    fbHighSld->addListener (this);

    addAndMakeVisible (lowFreqValLbl = new Label ("low frequency value label",
                                                  "100 Hz\n"));
    lowFreqValLbl->setFont (Font ("Osaka", 15.00f, Font::plain));
    lowFreqValLbl->setJustificationType (Justification::centredLeft);
    lowFreqValLbl->setEditable (false, false, false);
    lowFreqValLbl->setColour (Label::textColourId, Colour (0x75ffffff));
    lowFreqValLbl->setColour (TextEditor::textColourId, Colours::black);
    lowFreqValLbl->setColour (TextEditor::backgroundColourId, Colour (0x00000000));

    addAndMakeVisible (lowDelTimeLbl = new Label ("low frequency delay time label",
                                                  "0 sec"));
    lowDelTimeLbl->setFont (Font (15.00f, Font::plain));
    lowDelTimeLbl->setJustificationType (Justification::centred);
    lowDelTimeLbl->setEditable (false, false, false);
    lowDelTimeLbl->setColour (Label::textColourId, Colour (0x75ffffff));
    lowDelTimeLbl->setColour (TextEditor::textColourId, Colours::black);
    lowDelTimeLbl->setColour (TextEditor::backgroundColourId, Colour (0x00000000));

    addAndMakeVisible (midDelTimeLbl = new Label ("mid frequency delay time label",
                                                  "0 sec"));
    midDelTimeLbl->setFont (Font (15.00f, Font::plain));
    midDelTimeLbl->setJustificationType (Justification::centred);
    midDelTimeLbl->setEditable (false, false, false);
    midDelTimeLbl->setColour (Label::textColourId, Colour (0x75ffffff));
    midDelTimeLbl->setColour (TextEditor::textColourId, Colours::black);
    midDelTimeLbl->setColour (TextEditor::backgroundColourId, Colour (0x00000000));

    addAndMakeVisible (highDelTimeLbl = new Label ("high frequency delay time label",
                                                   "0 sec"));
    highDelTimeLbl->setFont (Font (15.00f, Font::plain));
    highDelTimeLbl->setJustificationType (Justification::centred);
    highDelTimeLbl->setEditable (false, false, false);
    highDelTimeLbl->setColour (Label::textColourId, Colour (0x75ffffff));
    highDelTimeLbl->setColour (TextEditor::textColourId, Colours::black);
    highDelTimeLbl->setColour (TextEditor::backgroundColourId, Colour (0x00000000));

    addAndMakeVisible (highFreqValLbl = new Label ("high frequency value label",
                                                   "300 Hz"));
    highFreqValLbl->setFont (Font ("Osaka", 15.00f, Font::plain));
    highFreqValLbl->setJustificationType (Justification::centredLeft);
    highFreqValLbl->setEditable (false, false, false);
    highFreqValLbl->setColour (Label::textColourId, Colour (0x75ffffff));
    highFreqValLbl->setColour (TextEditor::textColourId, Colours::black);
    highFreqValLbl->setColour (TextEditor::backgroundColourId, Colour (0x00000000));

    addAndMakeVisible (lowHzLbl = new Label ("low Hz Label",
                                             "LOW:"));
    lowHzLbl->setFont (Font ("Osaka", 15.00f, Font::plain));
    lowHzLbl->setJustificationType (Justification::centredLeft);
    lowHzLbl->setEditable (false, false, false);
    lowHzLbl->setColour (Label::textColourId, Colour (0x75ffffff));
    lowHzLbl->setColour (TextEditor::textColourId, Colours::black);
    lowHzLbl->setColour (TextEditor::backgroundColourId, Colour (0x00000000));

    addAndMakeVisible (highHzLbl = new Label ("high Hz Label",
                                              "HIGH:"));
    highHzLbl->setFont (Font ("Osaka", 15.00f, Font::plain));
    highHzLbl->setJustificationType (Justification::centredLeft);
    highHzLbl->setEditable (false, false, false);
    highHzLbl->setColour (Label::textColourId, Colour (0x75ffffff));
    highHzLbl->setColour (TextEditor::textColourId, Colours::black);
    highHzLbl->setColour (TextEditor::backgroundColourId, Colour (0x00000000));

    addAndMakeVisible (syncOnHighTglBtn = new ToggleButton ("sync on high toggle button"));
    syncOnHighTglBtn->setButtonText ("sync");
    syncOnHighTglBtn->addListener (this);
    syncOnHighTglBtn->setColour (ToggleButton::textColourId, Colours::white);

    addAndMakeVisible (syncOnMidTglBtn = new ToggleButton ("sync on mid toggle button"));
    syncOnMidTglBtn->setButtonText ("sync");
    syncOnMidTglBtn->addListener (this);
    syncOnMidTglBtn->setColour (ToggleButton::textColourId, Colours::white);

    addAndMakeVisible (syncOnLowTglBtn = new ToggleButton ("sync on low toggle button"));
    syncOnLowTglBtn->setButtonText ("sync");
    syncOnLowTglBtn->addListener (this);
    syncOnLowTglBtn->setColour (ToggleButton::textColourId, Colours::white);

    addAndMakeVisible (lowAmpLbl = new Label ("low amplitude label",
                                              "0 dB"));
    lowAmpLbl->setFont (Font (15.00f, Font::plain));
    lowAmpLbl->setJustificationType (Justification::centred);
    lowAmpLbl->setEditable (false, false, false);
    lowAmpLbl->setColour (Label::textColourId, Colour (0x75ffffff));
    lowAmpLbl->setColour (TextEditor::textColourId, Colours::black);
    lowAmpLbl->setColour (TextEditor::backgroundColourId, Colour (0x00000000));

    addAndMakeVisible (midAmpLbl = new Label ("mid amplitude label",
                                              "0 dB"));
    midAmpLbl->setFont (Font (15.00f, Font::plain));
    midAmpLbl->setJustificationType (Justification::centred);
    midAmpLbl->setEditable (false, false, false);
    midAmpLbl->setColour (Label::textColourId, Colour (0x75ffffff));
    midAmpLbl->setColour (TextEditor::textColourId, Colours::black);
    midAmpLbl->setColour (TextEditor::backgroundColourId, Colour (0x00000000));

    addAndMakeVisible (highAmpLbl = new Label ("high amplitude label",
                                               "0 dB"));
    highAmpLbl->setFont (Font (15.00f, Font::plain));
    highAmpLbl->setJustificationType (Justification::centred);
    highAmpLbl->setEditable (false, false, false);
    highAmpLbl->setColour (Label::textColourId, Colour (0x75ffffff));
    highAmpLbl->setColour (TextEditor::textColourId, Colours::black);
    highAmpLbl->setColour (TextEditor::backgroundColourId, Colour (0x00000000));

    addAndMakeVisible (lowFbLbl = new Label ("low feedback label",
                                             "0"));
    lowFbLbl->setFont (Font (15.00f, Font::plain));
    lowFbLbl->setJustificationType (Justification::centred);
    lowFbLbl->setEditable (false, false, false);
    lowFbLbl->setColour (Label::textColourId, Colour (0x75ffffff));
    lowFbLbl->setColour (TextEditor::textColourId, Colours::black);
    lowFbLbl->setColour (TextEditor::backgroundColourId, Colour (0x00000000));

    addAndMakeVisible (midFbLbl = new Label ("mid frequency label",
                                             "0"));
    midFbLbl->setFont (Font (15.00f, Font::plain));
    midFbLbl->setJustificationType (Justification::centred);
    midFbLbl->setEditable (false, false, false);
    midFbLbl->setColour (Label::textColourId, Colour (0x75ffffff));
    midFbLbl->setColour (TextEditor::textColourId, Colours::black);
    midFbLbl->setColour (TextEditor::backgroundColourId, Colour (0x00000000));

    addAndMakeVisible (highFbLbl = new Label ("high feedback label",
                                              "0"));
    highFbLbl->setFont (Font (15.00f, Font::plain));
    highFbLbl->setJustificationType (Justification::centred);
    highFbLbl->setEditable (false, false, false);
    highFbLbl->setColour (Label::textColourId, Colour (0x75ffffff));
    highFbLbl->setColour (TextEditor::textColourId, Colours::black);
    highFbLbl->setColour (TextEditor::backgroundColourId, Colour (0x00000000));

    cachedImage_woodtablebw_vsmall_jpg = ImageCache::getFromMemory (woodtablebw_vsmall_jpg, woodtablebw_vsmall_jpgSize);

    //[UserPreSize]
    //[/UserPreSize]

    setSize (410, 500);


    //[Constructor] You can add your own custom stuff here..
    getProcessor()->RequestUIUpdate();// UI update must be done each time a new editor is constructed
    startTimer(200);    // start timer with interval of 200ms
    setLookAndFeel(&myLookAndFeel);
    //[/Constructor]
}

Mb1AudioProcessorEditor::~Mb1AudioProcessorEditor()
{
    //[Destructor_pre]. You can add your own custom destruction code here..
    //[/Destructor_pre]

    lowFreqSld = nullptr;
    freqCutoffLbl = nullptr;
    delTimeSld = nullptr;
    delayTimeLbl = nullptr;
    delTimeSld2 = nullptr;
    lowFreqLbl = nullptr;
    delTimeSld3 = nullptr;
    midFreqLbl = nullptr;
    ampLowSld = nullptr;
    ampMidSld = nullptr;
    ampHighSld = nullptr;
    ampLbl = nullptr;
    highFreqLbl2 = nullptr;
    fbLowSld = nullptr;
    fbLbl = nullptr;
    fbMidSld = nullptr;
    fbHighSld = nullptr;
    lowFreqValLbl = nullptr;
    lowDelTimeLbl = nullptr;
    midDelTimeLbl = nullptr;
    highDelTimeLbl = nullptr;
    highFreqValLbl = nullptr;
    lowHzLbl = nullptr;
    highHzLbl = nullptr;
    syncOnHighTglBtn = nullptr;
    syncOnMidTglBtn = nullptr;
    syncOnLowTglBtn = nullptr;
    lowAmpLbl = nullptr;
    midAmpLbl = nullptr;
    highAmpLbl = nullptr;
    lowFbLbl = nullptr;
    midFbLbl = nullptr;
    highFbLbl = nullptr;


    //[Destructor]. You can add your own custom destruction code here..
    //[/Destructor]
}

//==============================================================================
void Mb1AudioProcessorEditor::paint (Graphics& g)
{
    //[UserPrePaint] Add your own custom painting code here..
    //[/UserPrePaint]

    g.fillAll (Colours::black);

    g.setColour (Colours::black.withAlpha (0.371f));
    g.drawImage (cachedImage_woodtablebw_vsmall_jpg,
                 4, 4, 588, 508,
                 0, 0, cachedImage_woodtablebw_vsmall_jpg.getWidth(), cachedImage_woodtablebw_vsmall_jpg.getHeight());

    //[UserPaint] Add your own custom painting code here..
    //[/UserPaint]
}

void Mb1AudioProcessorEditor::resized()
{
    lowFreqSld->setBounds (32, 76, 336, 24);
    freqCutoffLbl->setBounds (25, 53, 143, 15);
    delTimeSld->setBounds (80, 173, 71, 71);
    delayTimeLbl->setBounds (76, 152, 76, 10);
    delTimeSld2->setBounds (80, 269, 71, 71);
    lowFreqLbl->setBounds (28, 192, 40, 21);
    delTimeSld3->setBounds (80, 365, 71, 71);
    midFreqLbl->setBounds (28, 288, 36, 21);
    ampLowSld->setBounds (185, 173, 71, 71);
    ampMidSld->setBounds (185, 269, 71, 71);
    ampHighSld->setBounds (185, 365, 71, 71);
    ampLbl->setBounds (181, 152, 83, 10);
    highFreqLbl2->setBounds (28, 384, 44, 21);
    fbLowSld->setBounds (290, 173, 71, 71);
    fbLbl->setBounds (288, 152, 76, 10);
    fbMidSld->setBounds (290, 269, 71, 71);
    fbHighSld->setBounds (290, 365, 71, 71);
    lowFreqValLbl->setBounds (64, 112, 80, 16);
    lowDelTimeLbl->setBounds (79, 242, 76, 16);
    midDelTimeLbl->setBounds (79, 338, 76, 16);
    highDelTimeLbl->setBounds (79, 434, 76, 16);
    highFreqValLbl->setBounds (270, 112, 80, 16);
    lowHzLbl->setBounds (30, 107, 48, 24);
    highHzLbl->setBounds (238, 107, 40, 24);
    syncOnHighTglBtn->setBounds (29, 404, 40, 15);
    syncOnMidTglBtn->setBounds (29, 308, 40, 15);
    syncOnLowTglBtn->setBounds (29, 212, 40, 15);
    lowAmpLbl->setBounds (184, 242, 76, 16);
    midAmpLbl->setBounds (184, 338, 76, 16);
    highAmpLbl->setBounds (184, 434, 76, 16);
    lowFbLbl->setBounds (288, 242, 76, 16);
    midFbLbl->setBounds (288, 338, 76, 16);
    highFbLbl->setBounds (288, 434, 76, 16);
    //[UserResized] Add your own custom resize handling here..
    //[/UserResized]
}

void Mb1AudioProcessorEditor::sliderValueChanged (Slider* sliderThatWasMoved)
{
    //[UsersliderValueChanged_Pre]
    Mb1AudioProcessor * ourProcessor = getProcessor( );
    //[/UsersliderValueChanged_Pre]

    if (sliderThatWasMoved == lowFreqSld)
    {
        //[UserSliderCode_lowFreqSld] -- add your slider handling code here..
        //ourProcessor->setParameterNotifyingHost(Mb1AudioProcessor::freqCutoff0, ( float )lowFreqSld->getValue( ));
        ourProcessor->setParameterNotifyingHost(Mb1AudioProcessor::freqCutoff0, ( float )lowFreqSld->getMinValue( ));
        ourProcessor->setParameterNotifyingHost(Mb1AudioProcessor::freqCutoff1, ( float )lowFreqSld->getMaxValue( ));
        //[/UserSliderCode_lowFreqSld]
    }
    else if (sliderThatWasMoved == delTimeSld)
    {
        //[UserSliderCode_delTimeSld] -- add your slider handling code here..
        ourProcessor->setParameterNotifyingHost(Mb1AudioProcessor::delTime0, ( float )delTimeSld->getValue( ));
        //[/UserSliderCode_delTimeSld]
    }
    else if (sliderThatWasMoved == delTimeSld2)
    {
        //[UserSliderCode_delTimeSld2] -- add your slider handling code here..
        ourProcessor->setParameterNotifyingHost(Mb1AudioProcessor::delTime1, ( float )delTimeSld2->getValue( ));
        //[/UserSliderCode_delTimeSld2]
    }
    else if (sliderThatWasMoved == delTimeSld3)
    {
        //[UserSliderCode_delTimeSld3] -- add your slider handling code here..
        ourProcessor->setParameterNotifyingHost(Mb1AudioProcessor::delTime2, ( float )delTimeSld3->getValue( ));
        //[/UserSliderCode_delTimeSld3]
    }
    else if (sliderThatWasMoved == ampLowSld)
    {
        //[UserSliderCode_ampLowSld] -- add your slider handling code here..
        ourProcessor->setParameterNotifyingHost(Mb1AudioProcessor::amp0, ( float )ampLowSld->getValue( ));
        //[/UserSliderCode_ampLowSld]
    }
    else if (sliderThatWasMoved == ampMidSld)
    {
        //[UserSliderCode_ampMidSld] -- add your slider handling code here..
        ourProcessor->setParameterNotifyingHost(Mb1AudioProcessor::amp1, ( float )ampMidSld->getValue( ));
        //[/UserSliderCode_ampMidSld]
    }
    else if (sliderThatWasMoved == ampHighSld)
    {
        //[UserSliderCode_ampHighSld] -- add your slider handling code here..
        ourProcessor->setParameterNotifyingHost(Mb1AudioProcessor::amp2, ( float )ampHighSld->getValue( ));
        //[/UserSliderCode_ampHighSld]
    }
    else if (sliderThatWasMoved == fbLowSld)
    {
        //[UserSliderCode_fbLowSld] -- add your slider handling code here..
        ourProcessor->setParameterNotifyingHost(Mb1AudioProcessor::fb0, ( float )fbLowSld->getValue( ));
        //[/UserSliderCode_fbLowSld]
    }
    else if (sliderThatWasMoved == fbMidSld)
    {
        //[UserSliderCode_fbMidSld] -- add your slider handling code here..
        ourProcessor->setParameterNotifyingHost(Mb1AudioProcessor::fb1, ( float )fbMidSld->getValue( ));
        //[/UserSliderCode_fbMidSld]
    }
    else if (sliderThatWasMoved == fbHighSld)
    {
        //[UserSliderCode_fbHighSld] -- add your slider handling code here..
        ourProcessor->setParameterNotifyingHost(Mb1AudioProcessor::fb2, ( float )fbHighSld->getValue( ));
        //[/UserSliderCode_fbHighSld]
    }

    //[UsersliderValueChanged_Post]
    //[/UsersliderValueChanged_Post]
}

void Mb1AudioProcessorEditor::buttonClicked (Button* buttonThatWasClicked)
{
    //[UserbuttonClicked_Pre]
    Mb1AudioProcessor * ourProcessor = getProcessor( );
    //[/UserbuttonClicked_Pre]

    if (buttonThatWasClicked == syncOnHighTglBtn)
    {
        //[UserButtonCode_syncOnHighTglBtn] -- add your button handler code here..
        ourProcessor->setParameter(Mb1AudioProcessor::syncOnHigh, syncOnHighTglBtn->getToggleState( ));
        if( syncOnHighTglBtn->getToggleState( ) == true )
        {
            delTimeSld3->setRange(delTimeSld3->getMinimum( ), delTimeSld3->getMaximum( ), 1.0f/11.0f);
        } else
        {
            delTimeSld3->setRange(delTimeSld3->getMinimum( ), delTimeSld3->getMaximum( ), .01f);
        }
        //[/UserButtonCode_syncOnHighTglBtn]
    }
    else if (buttonThatWasClicked == syncOnMidTglBtn)
    {
        //[UserButtonCode_syncOnMidTglBtn] -- add your button handler code here..
        ourProcessor->setParameter(Mb1AudioProcessor::syncOnMid, syncOnMidTglBtn->getToggleState( ));
        if( syncOnMidTglBtn->getToggleState( ) == true )
        {
            delTimeSld2->setRange(delTimeSld2->getMinimum( ), delTimeSld2->getMaximum( ), 1.0f/11.0f);
        } else
        {
            delTimeSld2->setRange(delTimeSld2->getMinimum( ), delTimeSld2->getMaximum( ), .01f);
        }
        //[/UserButtonCode_syncOnMidTglBtn]
    }
    else if (buttonThatWasClicked == syncOnLowTglBtn)
    {
        //[UserButtonCode_syncOnLowTglBtn] -- add your button handler code here..
        ourProcessor->setParameter(Mb1AudioProcessor::syncOnLow, syncOnLowTglBtn->getToggleState( ));
        if( syncOnLowTglBtn->getToggleState( ) == true )
        {
            delTimeSld->setRange(delTimeSld->getMinimum( ), delTimeSld->getMaximum( ), 1.0f/11.0f);
        } else
        {
            delTimeSld->setRange(delTimeSld->getMinimum( ), delTimeSld->getMaximum( ), 0.01f);
        }
        //[/UserButtonCode_syncOnLowTglBtn]
    }

    //[UserbuttonClicked_Post]
    //[/UserbuttonClicked_Post]
}



//[MiscUserCode] You can add your own definitions of your custom methods or any other code here...

// timer callback
void Mb1AudioProcessorEditor::timerCallback( )
{
    Mb1AudioProcessor * ourProcessor = getProcessor( );
    // exchange data between UI elements and plugin 'ourProcessor'
    if(ourProcessor->NeedsUIUpdate())
    {
        //amp_indicator->setText(String(ourProcessor->getParameter(Mb1AudioProcessor::ampCurrent)), juce::dontSendNotification);

        // low frequency slider value
        float lowFreqVal01 = ourProcessor->getParameter(Mb1AudioProcessor::freqCutoff0);
        float lowFreqValHz = ourProcessor->getFreqCutoff(0);
        String lowFreqValHzString = String(lowFreqValHz, 2);
        lowFreqValHzString.append(" Hz", 3);
        lowFreqSld->setMinValue(lowFreqVal01, juce::dontSendNotification);
        lowFreqValLbl->setText(lowFreqValHzString, juce::dontSendNotification);
        // high frequency slider value
        float highFreqVal01 = ourProcessor->getParameter(Mb1AudioProcessor::freqCutoff1);
        float highFreqValHz = ourProcessor->getFreqCutoff(1);
        String highFreqValHzString = String(highFreqValHz, 2);
        highFreqValHzString.append(" Hz", 3);
        lowFreqSld->setMaxValue(highFreqVal01, juce::dontSendNotification);
        highFreqValLbl->setText(highFreqValHzString, juce::dontSendNotification);

        // delay time slider values
        float lowDelTime01 = ourProcessor->getParameter(Mb1AudioProcessor::delTime0);
        delTimeSld->setValue(lowDelTime01, juce::dontSendNotification);
        // adjust label
        if( !ourProcessor->getParameter(Mb1AudioProcessor::syncOnLow) )
        {
            float lowDelTimeSec = 3.0f*ourProcessor->getParameter(Mb1AudioProcessor::delTime0);
            String lowDelTimeSecString = String(lowDelTimeSec, 2);
            lowDelTimeSecString.append(" sec", 4);
            lowDelTimeLbl->setText(lowDelTimeSecString, juce::dontSendNotification);
        } else
        {
            // display quantization values
            int lowDelTimeStringInd = ( int )floor(lowDelTime01*11.0f);
            String lowDelTimeString = ourProcessor->quantValStrings[lowDelTimeStringInd];
            lowDelTimeLbl->setText(lowDelTimeString, juce::dontSendNotification);
        }

        float midDelTime01 = ourProcessor->getParameter(Mb1AudioProcessor::delTime1);
        delTimeSld2->setValue(midDelTime01, juce::dontSendNotification);
        // adjust label
        if( !ourProcessor->getParameter(Mb1AudioProcessor::syncOnMid) )
        {
            float midDelTimeSec = 3.0f*ourProcessor->getParameter(Mb1AudioProcessor::delTime1);
            String midDelTimeSecString = String(midDelTimeSec, 2);
            midDelTimeSecString.append(" sec", 4);
            midDelTimeLbl->setText(midDelTimeSecString, juce::dontSendNotification);
        } else
        {
            // display quantization values
            int midDelTimeStringInd = ( int )floor(midDelTime01*11.0f);
            String midDelTimeString = ourProcessor->quantValStrings[midDelTimeStringInd];
            midDelTimeLbl->setText(midDelTimeString, juce::dontSendNotification);
        }


        float highDelTime01 = ourProcessor->getParameter(Mb1AudioProcessor::delTime2);
        delTimeSld3->setValue(highDelTime01, juce::dontSendNotification);
        // adjust label
        if( !ourProcessor->getParameter(Mb1AudioProcessor::syncOnHigh))
        {
            float highDelTimeSec = 3.0f*ourProcessor->getParameter(Mb1AudioProcessor::delTime2);
            String highDelTimeSecString = String(highDelTimeSec, 2);
            highDelTimeSecString.append(" sec", 4);
            highDelTimeLbl->setText(highDelTimeSecString, juce::dontSendNotification);
        } else
        {
            // display quantization values
            int highDelTimeStringInd = ( int )floor(highDelTime01*11.0f);
            String highDelTimeString = ourProcessor->quantValStrings[highDelTimeStringInd];
            highDelTimeLbl->setText(highDelTimeString, juce::dontSendNotification);
        }


        // amplitude values
        float lowAmpVal = ourProcessor->getParameter(Mb1AudioProcessor::amp0);
        ampLowSld->setValue(lowAmpVal, juce::dontSendNotification);
        String lowAmpValDBString;
        if( lowAmpVal > 0.0f )
            lowAmpValDBString = String(20.0f*log10(lowAmpVal), 1);
        else
            lowAmpValDBString = "-60.0";
        lowAmpValDBString.append(" dB", 3);
        lowAmpLbl->setText(lowAmpValDBString, juce::dontSendNotification);

        float midAmpVal = ourProcessor->getParameter(Mb1AudioProcessor::amp1);
        ampMidSld->setValue(midAmpVal, juce::dontSendNotification);
        String midAmpValDBString;
        if( midAmpVal > 0.0f )
            midAmpValDBString = String(20.0f*log10(midAmpVal), 1);
        else
            midAmpValDBString = "-60.0";
        midAmpValDBString.append(" dB", 3);
        midAmpLbl->setText(midAmpValDBString, juce::dontSendNotification);
        
        float highAmpVal = ourProcessor->getParameter(Mb1AudioProcessor::amp2);
        ampHighSld->setValue(ourProcessor->getParameter(Mb1AudioProcessor::amp2), juce::dontSendNotification);
        String highAmpValDBString;
        if( highAmpVal > 0.0f )
            highAmpValDBString = String(20.0f*log10(highAmpVal), 1);
        else
            highAmpValDBString = "-60.0";
        highAmpValDBString.append(" dB", 3);
        highAmpLbl->setText(highAmpValDBString, juce::dontSendNotification);


        // feedback values
        float lowFbVal = ourProcessor->getParameter(Mb1AudioProcessor::fb0);
        fbLowSld->setValue(lowFbVal, juce::dontSendNotification);
        String lowFbValString = String(lowFbVal*100.0f, 0);
        lowFbValString.append(" %", 2);
        lowFbLbl->setText(lowFbValString, juce::dontSendNotification);
        
        float midFbVal = ourProcessor->getParameter(Mb1AudioProcessor::fb1);
        fbMidSld->setValue(midFbVal, juce::dontSendNotification);
        String midFbValString = String(midFbVal*100.0f, 0);
        midFbValString.append(" %", 2);
        midFbLbl->setText(midFbValString, juce::dontSendNotification);
        
        float highFbVal = ourProcessor->getParameter(Mb1AudioProcessor::fb2);
        fbHighSld->setValue(highFbVal, juce::dontSendNotification);
        String highFbValString = String(highFbVal*100.0f, 0);
        highFbValString.append(" %", 2);
        highFbLbl->setText(highFbValString, juce::dontSendNotification);
        
        
        //freqWindowToggle->setToggleState(ourProcessor->getParameter(Mb1AudioProcessor::freqWind), juce::dontSendNotification);

        syncOnLowTglBtn->setToggleState(ourProcessor->getParameter(Mb1AudioProcessor::syncOnLow), juce::dontSendNotification);
        syncOnMidTglBtn->setToggleState(ourProcessor->getParameter(Mb1AudioProcessor::syncOnMid), juce::dontSendNotification);
        syncOnHighTglBtn->setToggleState(ourProcessor->getParameter(Mb1AudioProcessor::syncOnHigh), juce::dontSendNotification);

        ourProcessor->ClearUIUpdateFlag();
    }
}

//[/MiscUserCode]


//==============================================================================
#if 0
/*  -- Introjucer information section --

    This is where the Introjucer stores the metadata that describe this GUI layout, so
    make changes in here at your peril!

BEGIN_JUCER_METADATA

<JUCER_COMPONENT documentType="Component" className="Mb1AudioProcessorEditor"
                 componentName="" parentClasses="public AudioProcessorEditor, public Timer"
                 constructorParams="Mb1AudioProcessor* ownerFilter" variableInitialisers="AudioProcessorEditor(ownerFilter)"
                 snapPixels="8" snapActive="1" snapShown="1" overlayOpacity="0.330"
                 fixedSize="0" initialWidth="410" initialHeight="500">
  <BACKGROUND backgroundColour="ff000000">
    <IMAGE pos="4 4 588 508" resource="woodtablebw_vsmall_jpg" opacity="0.37099999999999999645"
           mode="0"/>
  </BACKGROUND>
  <SLIDER name="lowFreqSld" id="9fc546f8de352917" memberName="lowFreqSld"
          virtualName="" explicitFocusOrder="0" pos="32 76 336 24" thumbcol="54ffffff"
          trackcol="7fffffff" rotarysliderfill="61ffffff" textboxtext="75ffffff"
          textboxbkgd="ffffff" textboxhighlight="40fffcfc" textboxoutline="ffffff"
          min="0" max="1" int="0.0010000000000000000208" style="TwoValueHorizontal"
          textBoxPos="NoTextBox" textBoxEditable="1" textBoxWidth="30"
          textBoxHeight="20" skewFactor="1"/>
  <LABEL name="freq cutoff lablel" id="4a49630282497a3f" memberName="freqCutoffLbl"
         virtualName="" explicitFocusOrder="0" pos="25 53 143 15" textCol="ffffffff"
         edTextCol="ff000000" edBkgCol="0" labelText="FREQUENCY CUTOFFS"
         editableSingleClick="0" editableDoubleClick="0" focusDiscardsChanges="0"
         fontname="Default font" fontsize="15" bold="0" italic="0" justification="33"/>
  <SLIDER name="delay time slider" id="7d4d8e29f2c4d161" memberName="delTimeSld"
          virtualName="" explicitFocusOrder="0" pos="80 173 71 71" bkgcol="0"
          thumbcol="ff000000" trackcol="75ffffff" rotarysliderfill="7fffffff"
          rotaryslideroutline="baffffff" textboxtext="75ffffff" textboxbkgd="ff000000"
          textboxhighlight="ffffffff" textboxoutline="ffffff" min="0" max="1"
          int="0.010000000000000000208" style="RotaryHorizontalVerticalDrag"
          textBoxPos="NoTextBox" textBoxEditable="1" textBoxWidth="50"
          textBoxHeight="20" skewFactor="1"/>
  <LABEL name="delay time lbl" id="5de90a9f076e66d" memberName="delayTimeLbl"
         virtualName="" explicitFocusOrder="0" pos="76 152 76 10" textCol="ffffffff"
         edTextCol="ff000000" edBkgCol="0" labelText="DELAY TIME" editableSingleClick="0"
         editableDoubleClick="0" focusDiscardsChanges="0" fontname="Default font"
         fontsize="15" bold="0" italic="0" justification="33"/>
  <SLIDER name="delay time slider" id="54367530a80f088b" memberName="delTimeSld2"
          virtualName="" explicitFocusOrder="0" pos="80 269 71 71" thumbcol="ff000000"
          trackcol="75ffffff" rotarysliderfill="7fffffff" rotaryslideroutline="baffffff"
          textboxtext="75ffffff" textboxbkgd="ff000000" textboxhighlight="ffffffff"
          textboxoutline="0" min="0" max="1" int="0.010000000000000000208"
          style="RotaryHorizontalVerticalDrag" textBoxPos="NoTextBox" textBoxEditable="1"
          textBoxWidth="50" textBoxHeight="20" skewFactor="1"/>
  <LABEL name="low frequency label" id="fb880a058eab124d" memberName="lowFreqLbl"
         virtualName="" explicitFocusOrder="0" pos="28 192 40 21" textCol="ffffffff"
         edTextCol="ff000000" edBkgCol="0" labelText="LOW" editableSingleClick="0"
         editableDoubleClick="0" focusDiscardsChanges="0" fontname="Default font"
         fontsize="15" bold="0" italic="0" justification="33"/>
  <SLIDER name="delay time slider" id="c32839b4d14346c1" memberName="delTimeSld3"
          virtualName="" explicitFocusOrder="0" pos="80 365 71 71" thumbcol="ff000000"
          trackcol="75ffffff" rotarysliderfill="7fffffff" rotaryslideroutline="baffffff"
          textboxtext="75ffffff" textboxbkgd="ffffff" textboxhighlight="ffffffff"
          textboxoutline="ffffff" min="0" max="1" int="0.010000000000000000208"
          style="RotaryHorizontalVerticalDrag" textBoxPos="NoTextBox" textBoxEditable="1"
          textBoxWidth="50" textBoxHeight="20" skewFactor="1"/>
  <LABEL name="mid frequency label" id="5d7f3f93985001ea" memberName="midFreqLbl"
         virtualName="" explicitFocusOrder="0" pos="28 288 36 21" textCol="ffffffff"
         edTextCol="ff000000" edBkgCol="0" labelText="MID" editableSingleClick="0"
         editableDoubleClick="0" focusDiscardsChanges="0" fontname="Default font"
         fontsize="15" bold="0" italic="0" justification="33"/>
  <SLIDER name="low freq amp slider" id="844271891846d475" memberName="ampLowSld"
          virtualName="" explicitFocusOrder="0" pos="185 173 71 71" thumbcol="75ffffff"
          trackcol="75ffffff" rotarysliderfill="7fffffff" rotaryslideroutline="baffffff"
          textboxtext="75ffffff" textboxbkgd="ffffff" textboxhighlight="ffffffff"
          textboxoutline="ffffff" min="0" max="1" int="0.0010000000000000000208"
          style="RotaryHorizontalVerticalDrag" textBoxPos="NoTextBox" textBoxEditable="1"
          textBoxWidth="50" textBoxHeight="20" skewFactor="1"/>
  <SLIDER name="mid freq amp slider" id="acd69adc979a4ff6" memberName="ampMidSld"
          virtualName="" explicitFocusOrder="0" pos="185 269 71 71" thumbcol="ffffffff"
          rotarysliderfill="7fffffff" rotaryslideroutline="baffffff" textboxtext="75ffffff"
          textboxbkgd="ffffff" textboxhighlight="ffffffff" textboxoutline="ffffff"
          min="0" max="1" int="0.0010000000000000000208" style="RotaryHorizontalVerticalDrag"
          textBoxPos="NoTextBox" textBoxEditable="1" textBoxWidth="50"
          textBoxHeight="20" skewFactor="1"/>
  <SLIDER name="high freq amp slider" id="334c15d400c60c1c" memberName="ampHighSld"
          virtualName="" explicitFocusOrder="0" pos="185 365 71 71" thumbcol="ffffffff"
          rotarysliderfill="7fffffff" rotaryslideroutline="baffffff" textboxtext="75ffffff"
          textboxbkgd="ffffff" textboxhighlight="ffffffff" textboxoutline="ffffff"
          min="0" max="1" int="0.0010000000000000000208" style="RotaryHorizontalVerticalDrag"
          textBoxPos="NoTextBox" textBoxEditable="1" textBoxWidth="50"
          textBoxHeight="20" skewFactor="1"/>
  <LABEL name="amplitude label" id="4cede39823b62d6a" memberName="ampLbl"
         virtualName="" explicitFocusOrder="0" pos="181 152 83 10" textCol="ffffffff"
         edTextCol="ff000000" edBkgCol="0" labelText="AMPLITUDE" editableSingleClick="0"
         editableDoubleClick="0" focusDiscardsChanges="0" fontname="Default font"
         fontsize="15" bold="0" italic="0" justification="33"/>
  <LABEL name="high frequency label" id="8bd552cc67f7e513" memberName="highFreqLbl2"
         virtualName="" explicitFocusOrder="0" pos="28 384 44 21" textCol="ffffffff"
         edTextCol="ffffffff" edBkgCol="0" labelText="HIGH" editableSingleClick="0"
         editableDoubleClick="0" focusDiscardsChanges="0" fontname="Default font"
         fontsize="15" bold="0" italic="0" justification="33"/>
  <SLIDER name="low freq fb slider" id="a2d834ded6d401c9" memberName="fbLowSld"
          virtualName="" explicitFocusOrder="0" pos="290 173 71 71" thumbcol="ffffffff"
          rotarysliderfill="80ffffff" rotaryslideroutline="baffffff" textboxtext="75ffffff"
          textboxbkgd="ffffff" textboxhighlight="ffffffff" textboxoutline="ffffff"
          min="0" max="0.98999999999999999112" int="0.010000000000000000208"
          style="RotaryHorizontalVerticalDrag" textBoxPos="NoTextBox" textBoxEditable="1"
          textBoxWidth="50" textBoxHeight="20" skewFactor="1"/>
  <LABEL name="feedback label" id="885a35b96de2967b" memberName="fbLbl"
         virtualName="" explicitFocusOrder="0" pos="288 152 76 10" textCol="ffffffff"
         edTextCol="ff000000" edBkgCol="0" labelText="FEEDBACK" editableSingleClick="0"
         editableDoubleClick="0" focusDiscardsChanges="0" fontname="Default font"
         fontsize="15" bold="0" italic="0" justification="33"/>
  <SLIDER name="mid freq fb slider" id="1febaa3e923e2b7" memberName="fbMidSld"
          virtualName="" explicitFocusOrder="0" pos="290 269 71 71" thumbcol="ffffffff"
          trackcol="75ffffff" rotarysliderfill="79ffffff" rotaryslideroutline="baffffff"
          textboxtext="75ffffff" textboxbkgd="ffffff" textboxhighlight="ffffffff"
          textboxoutline="ffffff" min="0" max="0.98999999999999999112"
          int="0.010000000000000000208" style="RotaryHorizontalVerticalDrag"
          textBoxPos="NoTextBox" textBoxEditable="1" textBoxWidth="50"
          textBoxHeight="20" skewFactor="1"/>
  <SLIDER name="high freq fb slider" id="2462a1401ef07528" memberName="fbHighSld"
          virtualName="" explicitFocusOrder="0" pos="290 365 71 71" thumbcol="75ffffff"
          rotarysliderfill="7fffffff" rotaryslideroutline="baffffff" textboxtext="75ffffff"
          textboxbkgd="ffffff" textboxhighlight="ffffffff" textboxoutline="ffffff"
          min="0" max="0.98999999999999999112" int="0.010000000000000000208"
          style="RotaryHorizontalVerticalDrag" textBoxPos="NoTextBox" textBoxEditable="1"
          textBoxWidth="50" textBoxHeight="20" skewFactor="1"/>
  <LABEL name="low frequency value label" id="a1d6dc2ff9736343" memberName="lowFreqValLbl"
         virtualName="" explicitFocusOrder="0" pos="64 112 80 16" textCol="75ffffff"
         edTextCol="ff000000" edBkgCol="0" labelText="100 Hz&#10;" editableSingleClick="0"
         editableDoubleClick="0" focusDiscardsChanges="0" fontname="Osaka"
         fontsize="15" bold="0" italic="0" justification="33"/>
  <LABEL name="low frequency delay time label" id="46de8c2d8bf34ccc" memberName="lowDelTimeLbl"
         virtualName="" explicitFocusOrder="0" pos="79 242 76 16" textCol="75ffffff"
         edTextCol="ff000000" edBkgCol="0" labelText="0 sec" editableSingleClick="0"
         editableDoubleClick="0" focusDiscardsChanges="0" fontname="Default font"
         fontsize="15" bold="0" italic="0" justification="36"/>
  <LABEL name="mid frequency delay time label" id="4fc130c91559eb3" memberName="midDelTimeLbl"
         virtualName="" explicitFocusOrder="0" pos="79 338 76 16" textCol="75ffffff"
         edTextCol="ff000000" edBkgCol="0" labelText="0 sec" editableSingleClick="0"
         editableDoubleClick="0" focusDiscardsChanges="0" fontname="Default font"
         fontsize="15" bold="0" italic="0" justification="36"/>
  <LABEL name="high frequency delay time label" id="5702d8c46707a49b"
         memberName="highDelTimeLbl" virtualName="" explicitFocusOrder="0"
         pos="79 434 76 16" textCol="75ffffff" edTextCol="ff000000" edBkgCol="0"
         labelText="0 sec" editableSingleClick="0" editableDoubleClick="0"
         focusDiscardsChanges="0" fontname="Default font" fontsize="15"
         bold="0" italic="0" justification="36"/>
  <LABEL name="high frequency value label" id="15852869207575fa" memberName="highFreqValLbl"
         virtualName="" explicitFocusOrder="0" pos="270 112 80 16" textCol="75ffffff"
         edTextCol="ff000000" edBkgCol="0" labelText="300 Hz" editableSingleClick="0"
         editableDoubleClick="0" focusDiscardsChanges="0" fontname="Osaka"
         fontsize="15" bold="0" italic="0" justification="33"/>
  <LABEL name="low Hz Label" id="88d0d2a7d8b1a978" memberName="lowHzLbl"
         virtualName="" explicitFocusOrder="0" pos="30 107 48 24" textCol="75ffffff"
         edTextCol="ff000000" edBkgCol="0" labelText="LOW:" editableSingleClick="0"
         editableDoubleClick="0" focusDiscardsChanges="0" fontname="Osaka"
         fontsize="15" bold="0" italic="0" justification="33"/>
  <LABEL name="high Hz Label" id="3abea1f9bd0bf6e5" memberName="highHzLbl"
         virtualName="" explicitFocusOrder="0" pos="238 107 40 24" textCol="75ffffff"
         edTextCol="ff000000" edBkgCol="0" labelText="HIGH:" editableSingleClick="0"
         editableDoubleClick="0" focusDiscardsChanges="0" fontname="Osaka"
         fontsize="15" bold="0" italic="0" justification="33"/>
  <TOGGLEBUTTON name="sync on high toggle button" id="c246aacd3c59e494" memberName="syncOnHighTglBtn"
                virtualName="" explicitFocusOrder="0" pos="29 404 40 15" txtcol="ffffffff"
                buttonText="sync" connectedEdges="0" needsCallback="1" radioGroupId="0"
                state="0"/>
  <TOGGLEBUTTON name="sync on mid toggle button" id="4b92d929f6a8cb86" memberName="syncOnMidTglBtn"
                virtualName="" explicitFocusOrder="0" pos="29 308 40 15" txtcol="ffffffff"
                buttonText="sync" connectedEdges="0" needsCallback="1" radioGroupId="0"
                state="0"/>
  <TOGGLEBUTTON name="sync on low toggle button" id="c3987846fac3e909" memberName="syncOnLowTglBtn"
                virtualName="" explicitFocusOrder="0" pos="29 212 40 15" txtcol="ffffffff"
                buttonText="sync" connectedEdges="0" needsCallback="1" radioGroupId="0"
                state="0"/>
  <LABEL name="low amplitude label" id="4e5d65b00dfe8dd9" memberName="lowAmpLbl"
         virtualName="" explicitFocusOrder="0" pos="184 242 76 16" textCol="75ffffff"
         edTextCol="ff000000" edBkgCol="0" labelText="0 dB" editableSingleClick="0"
         editableDoubleClick="0" focusDiscardsChanges="0" fontname="Default font"
         fontsize="15" bold="0" italic="0" justification="36"/>
  <LABEL name="mid amplitude label" id="7fa570d515d62543" memberName="midAmpLbl"
         virtualName="" explicitFocusOrder="0" pos="184 338 76 16" textCol="75ffffff"
         edTextCol="ff000000" edBkgCol="0" labelText="0 dB" editableSingleClick="0"
         editableDoubleClick="0" focusDiscardsChanges="0" fontname="Default font"
         fontsize="15" bold="0" italic="0" justification="36"/>
  <LABEL name="high amplitude label" id="ee817bc2bdaeab0" memberName="highAmpLbl"
         virtualName="" explicitFocusOrder="0" pos="184 434 76 16" textCol="75ffffff"
         edTextCol="ff000000" edBkgCol="0" labelText="0 dB" editableSingleClick="0"
         editableDoubleClick="0" focusDiscardsChanges="0" fontname="Default font"
         fontsize="15" bold="0" italic="0" justification="36"/>
  <LABEL name="low feedback label" id="55c00fbde961be9d" memberName="lowFbLbl"
         virtualName="" explicitFocusOrder="0" pos="288 242 76 16" textCol="75ffffff"
         edTextCol="ff000000" edBkgCol="0" labelText="0" editableSingleClick="0"
         editableDoubleClick="0" focusDiscardsChanges="0" fontname="Default font"
         fontsize="15" bold="0" italic="0" justification="36"/>
  <LABEL name="mid frequency label" id="68ef5e85de46a5ea" memberName="midFbLbl"
         virtualName="" explicitFocusOrder="0" pos="288 338 76 16" textCol="75ffffff"
         edTextCol="ff000000" edBkgCol="0" labelText="0" editableSingleClick="0"
         editableDoubleClick="0" focusDiscardsChanges="0" fontname="Default font"
         fontsize="15" bold="0" italic="0" justification="36"/>
  <LABEL name="high feedback label" id="e8e81c3eb3cf7e42" memberName="highFbLbl"
         virtualName="" explicitFocusOrder="0" pos="288 434 76 16" textCol="75ffffff"
         edTextCol="ff000000" edBkgCol="0" labelText="0" editableSingleClick="0"
         editableDoubleClick="0" focusDiscardsChanges="0" fontname="Default font"
         fontsize="15" bold="0" italic="0" justification="36"/>
</JUCER_COMPONENT>

END_JUCER_METADATA
*/
#endif

//==============================================================================
// Binary resources - be careful not to edit any of these sections!

// JUCER_RESOURCE: woodtablebw_vsmall_jpg, 159460, "../woodtablebw_vsmall.jpg"
static const unsigned char resource_Mb1AudioProcessorEditor_woodtablebw_vsmall_jpg[] = { 255,216,255,224,0,16,74,70,73,70,0,1,1,1,0,72,0,72,0,0,255,225,9,144,69,120,105,102,0,0,77,77,0,42,0,0,0,8,0,10,
1,15,0,2,0,0,0,6,0,0,0,134,1,16,0,2,0,0,0,10,0,0,0,140,1,18,0,3,0,0,0,1,0,1,0,0,1,26,0,5,0,0,0,1,0,0,0,150,1,27,0,5,0,0,0,1,0,0,0,158,1,40,0,3,0,0,0,1,0,2,0,0,1,49,0,2,0,0,0,12,0,0,0,166,1,50,0,2,0,0,
0,20,0,0,0,178,2,19,0,3,0,0,0,1,0,1,0,0,135,105,0,4,0,0,0,1,0,0,0,198,0,0,3,44,65,112,112,108,101,0,105,80,104,111,110,101,32,52,83,0,0,0,0,72,0,0,0,1,0,0,0,72,0,0,0,1,71,73,77,80,32,50,46,56,46,49,48,
0,50,48,49,52,58,48,54,58,48,50,32,48,49,58,49,57,58,53,57,0,0,27,130,154,0,5,0,0,0,1,0,0,2,16,130,157,0,5,0,0,0,1,0,0,2,24,136,34,0,3,0,0,0,1,0,2,0,0,136,39,0,3,0,0,0,1,0,250,0,0,144,0,0,7,0,0,0,4,48,
50,50,49,144,3,0,2,0,0,0,20,0,0,2,32,144,4,0,2,0,0,0,20,0,0,2,52,145,1,0,7,0,0,0,4,1,2,3,0,146,1,0,10,0,0,0,1,0,0,2,72,146,2,0,5,0,0,0,1,0,0,2,80,146,3,0,10,0,0,0,1,0,0,2,88,146,7,0,3,0,0,0,1,0,3,0,0,
146,9,0,3,0,0,0,1,0,16,0,0,146,10,0,5,0,0,0,1,0,0,2,96,146,124,0,7,0,0,0,196,0,0,2,104,146,145,0,2,0,0,0,4,49,57,54,0,146,146,0,2,0,0,0,4,49,57,54,0,160,0,0,7,0,0,0,4,48,49,48,48,160,1,0,3,0,0,0,1,0,1,
0,0,160,2,0,4,0,0,0,1,0,0,12,192,160,3,0,4,0,0,0,1,0,0,9,144,162,23,0,3,0,0,0,1,0,2,0,0,163,1,0,7,0,0,0,1,1,0,0,0,164,2,0,3,0,0,0,1,0,0,0,0,164,3,0,3,0,0,0,1,0,0,0,0,164,5,0,3,0,0,0,1,0,35,0,0,164,6,0,
3,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,20,0,0,0,12,0,0,0,5,50,48,49,52,58,48,49,58,48,50,32,49,54,58,49,52,58,51,57,0,50,48,49,52,58,48,49,58,48,50,32,49,54,58,49,52,58,51,57,0,0,0,10,219,0,0,2,131,0,
0,18,237,0,0,7,126,0,0,12,167,0,0,11,22,0,0,0,107,0,0,0,25,65,112,112,108,101,32,105,79,83,0,0,1,77,77,0,6,0,1,0,9,0,0,0,1,0,0,0,0,0,3,0,7,0,0,0,104,0,0,0,92,0,4,0,9,0,0,0,1,0,0,0,1,0,5,0,9,0,0,0,1,0,
0,0,178,0,6,0,9,0,0,0,1,0,0,0,184,0,7,0,9,0,0,0,1,0,0,0,1,0,0,0,0,98,112,108,105,115,116,48,48,212,1,2,3,4,5,6,7,8,89,116,105,109,101,115,99,97,108,101,85,101,112,111,99,104,85,118,97,108,117,101,85,102,
108,97,103,115,18,59,154,202,0,16,0,19,0,1,41,149,1,191,210,238,16,1,8,17,27,33,39,45,50,52,61,0,0,0,0,0,0,1,1,0,0,0,0,0,0,0,9,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,63,0,6,1,3,0,3,0,0,0,1,0,6,0,0,1,26,0,5,0,0,
0,1,0,0,3,122,1,27,0,5,0,0,0,1,0,0,3,130,1,40,0,3,0,0,0,1,0,2,0,0,2,1,0,4,0,0,0,1,0,0,3,138,2,2,0,4,0,0,0,1,0,0,5,254,0,0,0,0,0,0,0,72,0,0,0,1,0,0,0,72,0,0,0,1,255,216,255,224,0,16,74,70,73,70,0,1,1,0,
0,1,0,1,0,0,255,219,0,67,0,114,79,86,100,86,71,114,100,93,100,129,121,114,136,171,255,186,171,157,157,171,255,250,255,207,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,219,0,67,1,121,129,129,171,150,171,255,186,186,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,192,0,17,8,0,147,0,196,3,1,34,0,2,17,1,3,17,1,255,196,
0,31,0,0,1,5,1,1,1,1,1,1,0,0,0,0,0,0,0,0,1,2,3,4,5,6,7,8,9,10,11,255,196,0,181,16,0,2,1,3,3,2,4,3,5,5,4,4,0,0,1,125,1,2,3,0,4,17,5,18,33,49,65,6,19,81,97,7,34,113,20,50,129,145,161,8,35,66,177,193,21,
82,209,240,36,51,98,114,130,9,10,22,23,24,25,26,37,38,39,40,41,42,52,53,54,55,56,57,58,67,68,69,70,71,72,73,74,83,84,85,86,87,88,89,90,99,100,101,102,103,104,105,106,115,116,117,118,119,120,121,122,131,
132,133,134,135,136,137,138,146,147,148,149,150,151,152,153,154,162,163,164,165,166,167,168,169,170,178,179,180,181,182,183,184,185,186,194,195,196,197,198,199,200,201,202,210,211,212,213,214,215,216,
217,218,225,226,227,228,229,230,231,232,233,234,241,242,243,244,245,246,247,248,249,250,255,196,0,31,1,0,3,1,1,1,1,1,1,1,1,1,0,0,0,0,0,0,1,2,3,4,5,6,7,8,9,10,11,255,196,0,181,17,0,2,1,2,4,4,3,4,7,5,4,
4,0,1,2,119,0,1,2,3,17,4,5,33,49,6,18,65,81,7,97,113,19,34,50,129,8,20,66,145,161,177,193,9,35,51,82,240,21,98,114,209,10,22,36,52,225,37,241,23,24,25,26,38,39,40,41,42,53,54,55,56,57,58,67,68,69,70,71,
72,73,74,83,84,85,86,87,88,89,90,99,100,101,102,103,104,105,106,115,116,117,118,119,120,121,122,130,131,132,133,134,135,136,137,138,146,147,148,149,150,151,152,153,154,162,163,164,165,166,167,168,169,
170,178,179,180,181,182,183,184,185,186,194,195,196,197,198,199,200,201,202,210,211,212,213,214,215,216,217,218,226,227,228,229,230,231,232,233,234,242,243,244,245,246,247,248,249,250,255,218,0,12,3,1,
0,2,17,3,17,0,63,0,138,140,82,226,142,40,1,49,69,40,162,128,14,244,82,209,138,0,74,90,49,69,0,37,20,82,208,2,82,15,165,45,24,160,4,230,138,118,41,40,1,41,104,163,20,0,81,69,20,0,81,70,125,232,160,3,25,
164,197,40,230,138,0,78,148,26,90,67,64,12,162,138,40,2,74,90,74,40,0,165,162,129,64,5,20,180,80,2,81,71,214,140,243,64,5,3,222,130,104,160,3,20,98,138,40,0,164,52,18,61,104,200,245,160,4,165,164,220,
61,13,38,79,165,0,58,146,147,38,147,241,160,7,240,5,38,71,173,55,140,81,143,106,0,80,195,222,130,254,130,144,209,64,11,146,105,57,239,69,33,160,2,138,40,160,9,41,70,41,9,25,164,221,237,64,14,160,115,77,
220,123,10,76,159,90,0,127,52,103,20,195,158,230,147,138,0,121,97,235,73,184,250,26,74,40,1,114,125,40,201,246,164,162,128,14,125,105,63,26,90,74,0,40,162,138,0,40,162,138,0,40,162,138,0,40,162,138,0,
40,164,162,128,22,138,74,114,251,140,138,0,105,162,148,245,162,128,10,40,165,160,2,138,40,160,2,146,150,138,0,74,90,40,160,2,138,41,40,1,104,197,20,80,1,69,20,80,2,81,75,73,64,5,20,81,197,0,20,148,180,
80,2,81,75,70,104,1,41,65,62,244,127,158,180,148,0,126,116,81,159,106,40,1,104,162,138,0,90,41,40,160,5,162,138,63,42,0,40,205,33,35,210,147,119,181,0,58,147,52,153,52,100,208,3,168,164,162,128,22,138,
41,40,1,104,205,37,25,160,3,35,210,138,40,160,2,138,13,31,231,173,0,37,20,180,148,0,102,138,40,197,0,20,81,249,81,64,11,69,38,105,115,64,5,20,102,138,0,90,74,40,160,2,140,81,69,0,20,81,75,64,9,69,45,20,
0,80,104,160,154,0,76,81,138,51,70,104,0,197,24,52,81,154,0,40,162,138,0,41,51,75,73,64,5,25,247,164,162,128,10,41,120,162,128,10,40,200,163,34,128,10,90,76,138,92,138,0,40,163,34,140,143,67,64,7,225,
69,46,239,106,76,143,74,0,40,197,25,30,134,140,251,80,1,248,81,248,81,184,122,81,184,122,80,1,248,81,143,106,50,61,13,25,30,134,128,19,20,103,218,151,35,208,209,145,232,104,1,40,165,200,244,52,100,123,
208,2,126,20,82,228,123,209,145,239,64,9,197,20,185,20,100,122,154,0,74,40,200,165,200,244,160,4,162,140,143,74,40,2,80,162,141,171,71,225,74,57,237,64,13,218,180,108,21,39,62,148,131,216,80,3,54,82,237,
245,167,156,227,181,38,51,222,128,27,183,142,148,108,52,252,123,210,113,64,13,217,239,70,209,78,56,163,143,90,0,110,207,106,93,148,113,239,75,197,0,52,175,174,41,12,99,60,26,127,203,233,70,71,247,104,
2,61,158,166,141,130,156,122,244,160,113,218,128,16,32,163,104,167,123,226,140,251,80,3,118,143,106,48,41,104,237,220,80,3,118,138,48,41,217,164,160,6,237,30,148,96,83,255,0,10,74,0,110,7,165,20,239,194,
138,0,51,199,90,81,245,164,101,35,181,11,131,64,15,31,90,92,123,154,111,20,163,20,0,184,245,163,0,80,0,163,20,0,1,71,25,160,224,119,160,1,64,6,40,224,82,209,218,128,27,145,75,145,69,38,70,104,1,104,163,
62,148,153,52,0,100,81,245,165,164,227,214,128,3,138,78,40,226,142,40,1,114,59,210,82,228,122,138,76,14,198,128,10,66,105,78,40,160,4,205,28,26,41,113,154,0,141,179,158,13,20,236,81,64,18,212,120,27,141,
20,80,3,128,20,184,162,138,0,5,20,81,64,6,5,0,81,69,0,24,226,150,138,40,0,164,239,69,20,0,184,160,10,40,160,3,189,33,3,52,81,64,1,20,96,122,81,69,0,24,30,148,132,15,74,40,160,0,129,142,148,218,40,160,
2,147,181,20,80,2,17,69,20,80,7,255,217,255,225,12,105,104,116,116,112,58,47,47,110,115,46,97,100,111,98,101,46,99,111,109,47,120,97,112,47,49,46,48,47,0,60,63,120,112,97,99,107,101,116,32,98,101,103,
105,110,61,39,239,187,191,39,32,105,100,61,39,87,53,77,48,77,112,67,101,104,105,72,122,114,101,83,122,78,84,99,122,107,99,57,100,39,63,62,10,60,120,58,120,109,112,109,101,116,97,32,120,109,108,110,115,
58,120,61,39,97,100,111,98,101,58,110,115,58,109,101,116,97,47,39,62,10,60,114,100,102,58,82,68,70,32,120,109,108,110,115,58,114,100,102,61,39,104,116,116,112,58,47,47,119,119,119,46,119,51,46,111,114,
103,47,49,57,57,57,47,48,50,47,50,50,45,114,100,102,45,115,121,110,116,97,120,45,110,115,35,39,62,10,10,32,60,114,100,102,58,68,101,115,99,114,105,112,116,105,111,110,32,120,109,108,110,115,58,101,120,
105,102,61,39,104,116,116,112,58,47,47,110,115,46,97,100,111,98,101,46,99,111,109,47,101,120,105,102,47,49,46,48,47,39,62,10,32,32,60,101,120,105,102,58,77,97,107,101,62,65,112,112,108,101,60,47,101,120,
105,102,58,77,97,107,101,62,10,32,32,60,101,120,105,102,58,77,111,100,101,108,62,105,80,104,111,110,101,32,52,83,60,47,101,120,105,102,58,77,111,100,101,108,62,10,32,32,60,101,120,105,102,58,79,114,105,
101,110,116,97,116,105,111,110,62,82,105,103,104,116,45,116,111,112,60,47,101,120,105,102,58,79,114,105,101,110,116,97,116,105,111,110,62,10,32,32,60,101,120,105,102,58,88,82,101,115,111,108,117,116,105,
111,110,62,55,50,60,47,101,120,105,102,58,88,82,101,115,111,108,117,116,105,111,110,62,10,32,32,60,101,120,105,102,58,89,82,101,115,111,108,117,116,105,111,110,62,55,50,60,47,101,120,105,102,58,89,82,
101,115,111,108,117,116,105,111,110,62,10,32,32,60,101,120,105,102,58,82,101,115,111,108,117,116,105,111,110,85,110,105,116,62,73,110,99,104,60,47,101,120,105,102,58,82,101,115,111,108,117,116,105,111,
110,85,110,105,116,62,10,32,32,60,101,120,105,102,58,83,111,102,116,119,97,114,101,62,55,46,48,46,52,60,47,101,120,105,102,58,83,111,102,116,119,97,114,101,62,10,32,32,60,101,120,105,102,58,68,97,116,
101,84,105,109,101,62,50,48,49,52,58,48,49,58,48,50,32,49,54,58,49,52,58,51,57,60,47,101,120,105,102,58,68,97,116,101,84,105,109,101,62,10,32,32,60,101,120,105,102,58,89,67,98,67,114,80,111,115,105,116,
105,111,110,105,110,103,62,67,101,110,116,101,114,101,100,60,47,101,120,105,102,58,89,67,98,67,114,80,111,115,105,116,105,111,110,105,110,103,62,10,32,32,60,101,120,105,102,58,67,111,109,112,114,101,115,
115,105,111,110,62,74,80,69,71,32,99,111,109,112,114,101,115,115,105,111,110,60,47,101,120,105,102,58,67,111,109,112,114,101,115,115,105,111,110,62,10,32,32,60,101,120,105,102,58,88,82,101,115,111,108,
117,116,105,111,110,62,55,50,60,47,101,120,105,102,58,88,82,101,115,111,108,117,116,105,111,110,62,10,32,32,60,101,120,105,102,58,89,82,101,115,111,108,117,116,105,111,110,62,55,50,60,47,101,120,105,102,
58,89,82,101,115,111,108,117,116,105,111,110,62,10,32,32,60,101,120,105,102,58,82,101,115,111,108,117,116,105,111,110,85,110,105,116,62,73,110,99,104,60,47,101,120,105,102,58,82,101,115,111,108,117,116,
105,111,110,85,110,105,116,62,10,32,32,60,101,120,105,102,58,77,97,107,101,62,65,112,112,108,101,60,47,101,120,105,102,58,77,97,107,101,62,10,32,32,60,101,120,105,102,58,77,111,100,101,108,62,105,80,104,
111,110,101,32,52,83,60,47,101,120,105,102,58,77,111,100,101,108,62,10,32,32,60,101,120,105,102,58,79,114,105,101,110,116,97,116,105,111,110,62,84,111,112,45,108,101,102,116,60,47,101,120,105,102,58,79,
114,105,101,110,116,97,116,105,111,110,62,10,32,32,60,101,120,105,102,58,88,82,101,115,111,108,117,116,105,111,110,62,55,50,60,47,101,120,105,102,58,88,82,101,115,111,108,117,116,105,111,110,62,10,32,
32,60,101,120,105,102,58,89,82,101,115,111,108,117,116,105,111,110,62,55,50,60,47,101,120,105,102,58,89,82,101,115,111,108,117,116,105,111,110,62,10,32,32,60,101,120,105,102,58,82,101,115,111,108,117,
116,105,111,110,85,110,105,116,62,73,110,99,104,60,47,101,120,105,102,58,82,101,115,111,108,117,116,105,111,110,85,110,105,116,62,10,32,32,60,101,120,105,102,58,83,111,102,116,119,97,114,101,62,71,73,
77,80,32,50,46,56,46,49,48,60,47,101,120,105,102,58,83,111,102,116,119,97,114,101,62,10,32,32,60,101,120,105,102,58,68,97,116,101,84,105,109,101,62,50,48,49,52,58,48,49,58,48,50,32,49,54,58,49,56,58,53,
48,60,47,101,120,105,102,58,68,97,116,101,84,105,109,101,62,10,32,32,60,101,120,105,102,58,89,67,98,67,114,80,111,115,105,116,105,111,110,105,110,103,62,67,101,110,116,101,114,101,100,60,47,101,120,105,
102,58,89,67,98,67,114,80,111,115,105,116,105,111,110,105,110,103,62,10,32,32,60,101,120,105,102,58,67,111,109,112,114,101,115,115,105,111,110,62,74,80,69,71,32,99,111,109,112,114,101,115,115,105,111,
110,60,47,101,120,105,102,58,67,111,109,112,114,101,115,115,105,111,110,62,10,32,32,60,101,120,105,102,58,88,82,101,115,111,108,117,116,105,111,110,62,55,50,60,47,101,120,105,102,58,88,82,101,115,111,
108,117,116,105,111,110,62,10,32,32,60,101,120,105,102,58,89,82,101,115,111,108,117,116,105,111,110,62,55,50,60,47,101,120,105,102,58,89,82,101,115,111,108,117,116,105,111,110,62,10,32,32,60,101,120,105,
102,58,82,101,115,111,108,117,116,105,111,110,85,110,105,116,62,73,110,99,104,60,47,101,120,105,102,58,82,101,115,111,108,117,116,105,111,110,85,110,105,116,62,10,32,32,60,101,120,105,102,58,69,120,112,
111,115,117,114,101,84,105,109,101,62,49,47,50,48,32,115,101,99,46,60,47,101,120,105,102,58,69,120,112,111,115,117,114,101,84,105,109,101,62,10,32,32,60,101,120,105,102,58,70,78,117,109,98,101,114,62,
102,47,50,46,52,60,47,101,120,105,102,58,70,78,117,109,98,101,114,62,10,32,32,60,101,120,105,102,58,69,120,112,111,115,117,114,101,80,114,111,103,114,97,109,62,78,111,114,109,97,108,32,112,114,111,103,
114,97,109,60,47,101,120,105,102,58,69,120,112,111,115,117,114,101,80,114,111,103,114,97,109,62,10,32,32,60,101,120,105,102,58,73,83,79,83,112,101,101,100,82,97,116,105,110,103,115,62,10,32,32,32,60,114,
100,102,58,83,101,113,62,10,32,32,32,32,60,114,100,102,58,108,105,62,50,53,48,60,47,114,100,102,58,108,105,62,10,32,32,32,60,47,114,100,102,58,83,101,113,62,10,32,32,60,47,101,120,105,102,58,73,83,79,
83,112,101,101,100,82,97,116,105,110,103,115,62,10,32,32,60,101,120,105,102,58,69,120,105,102,86,101,114,115,105,111,110,62,69,120,105,102,32,86,101,114,115,105,111,110,32,50,46,50,49,60,47,101,120,105,
102,58,69,120,105,102,86,101,114,115,105,111,110,62,10,32,32,60,101,120,105,102,58,68,97,116,101,84,105,109,101,79,114,105,103,105,110,97,108,62,50,48,49,52,58,48,49,58,48,50,32,49,54,58,49,52,58,51,57,
60,47,101,120,105,102,58,68,97,116,101,84,105,109,101,79,114,105,103,105,110,97,108,62,10,32,32,60,101,120,105,102,58,68,97,116,101,84,105,109,101,68,105,103,105,116,105,122,101,100,62,50,48,49,52,58,
48,49,58,48,50,32,49,54,58,49,52,58,51,57,60,47,101,120,105,102,58,68,97,116,101,84,105,109,101,68,105,103,105,116,105,122,101,100,62,10,32,32,60,101,120,105,102,58,67,111,109,112,111,110,101,110,116,
115,67,111,110,102,105,103,117,114,97,116,105,111,110,62,10,32,32,32,60,114,100,102,58,83,101,113,62,10,32,32,32,32,60,114,100,102,58,108,105,62,89,32,67,98,32,67,114,32,45,60,47,114,100,102,58,108,105,
62,10,32,32,32,60,47,114,100,102,58,83,101,113,62,10,32,32,60,47,101,120,105,102,58,67,111,109,112,111,110,101,110,116,115,67,111,110,102,105,103,117,114,97,116,105,111,110,62,10,32,32,60,101,120,105,
102,58,83,104,117,116,116,101,114,83,112,101,101,100,86,97,108,117,101,62,52,46,51,50,32,69,86,32,40,49,47,50,48,32,115,101,99,46,41,60,47,101,120,105,102,58,83,104,117,116,116,101,114,83,112,101,101,
100,86,97,108,117,101,62,10,32,32,60,101,120,105,102,58,65,112,101,114,116,117,114,101,86,97,108,117,101,62,50,46,53,51,32,69,86,32,40,102,47,50,46,52,41,60,47,101,120,105,102,58,65,112,101,114,116,117,
114,101,86,97,108,117,101,62,10,32,32,60,101,120,105,102,58,66,114,105,103,104,116,110,101,115,115,86,97,108,117,101,62,49,46,49,52,32,69,86,32,40,55,46,53,54,32,99,100,47,109,94,50,41,60,47,101,120,105,
102,58,66,114,105,103,104,116,110,101,115,115,86,97,108,117,101,62,10,32,32,60,101,120,105,102,58,77,101,116,101,114,105,110,103,77,111,100,101,62,83,112,111,116,60,47,101,120,105,102,58,77,101,116,101,
114,105,110,103,77,111,100,101,62,10,32,32,60,101,120,105,102,58,70,108,97,115,104,32,114,100,102,58,112,97,114,115,101,84,121,112,101,61,39,82,101,115,111,117,114,99,101,39,62,10,32,32,60,47,101,120,
105,102,58,70,108,97,115,104,62,10,32,32,60,101,120,105,102,58,70,111,99,97,108,76,101,110,103,116,104,62,52,46,51,32,109,109,60,47,101,120,105,102,58,70,111,99,97,108,76,101,110,103,116,104,62,10,32,
32,60,101,120,105,102,58,77,97,107,101,114,78,111,116,101,62,49,57,54,32,98,121,116,101,115,32,117,110,100,101,102,105,110,101,100,32,100,97,116,97,60,47,101,120,105,102,58,77,97,107,101,114,78,111,116,
101,62,10,32,32,60,101,120,105,102,58,83,117,98,83,101,99,84,105,109,101,79,114,105,103,105,110,97,108,62,49,57,54,60,47,101,120,105,102,58,83,117,98,83,101,99,84,105,109,101,79,114,105,103,105,110,97,
108,62,10,32,32,60,101,120,105,102,58,83,117,98,83,101,99,84,105,109,101,68,105,103,105,116,105,122,101,100,62,49,57,54,60,47,101,120,105,102,58,83,117,98,83,101,99,84,105,109,101,68,105,103,105,116,105,
122,101,100,62,10,32,32,60,101,120,105,102,58,70,108,97,115,104,80,105,120,86,101,114,115,105,111,110,62,70,108,97,115,104,80,105,120,32,86,101,114,115,105,111,110,32,49,46,48,60,47,101,120,105,102,58,
70,108,97,115,104,80,105,120,86,101,114,115,105,111,110,62,10,32,32,60,101,120,105,102,58,67,111,108,111,114,83,112,97,99,101,62,115,82,71,66,60,47,101,120,105,102,58,67,111,108,111,114,83,112,97,99,101,
62,10,32,32,60,101,120,105,102,58,80,105,120,101,108,88,68,105,109,101,110,115,105,111,110,62,51,50,54,52,60,47,101,120,105,102,58,80,105,120,101,108,88,68,105,109,101,110,115,105,111,110,62,10,32,32,
60,101,120,105,102,58,80,105,120,101,108,89,68,105,109,101,110,115,105,111,110,62,50,52,52,56,60,47,101,120,105,102,58,80,105,120,101,108,89,68,105,109,101,110,115,105,111,110,62,10,32,32,60,101,120,105,
102,58,83,101,110,115,105,110,103,77,101,116,104,111,100,62,79,110,101,45,99,104,105,112,32,99,111,108,111,114,32,97,114,101,97,32,115,101,110,115,111,114,60,47,101,120,105,102,58,83,101,110,115,105,110,
103,77,101,116,104,111,100,62,10,32,32,60,101,120,105,102,58,83,99,101,110,101,84,121,112,101,62,68,105,114,101,99,116,108,121,32,112,104,111,116,111,103,114,97,112,104,101,100,60,47,101,120,105,102,58,
83,99,101,110,101,84,121,112,101,62,10,32,32,60,101,120,105,102,58,69,120,112,111,115,117,114,101,77,111,100,101,62,65,117,116,111,32,101,120,112,111,115,117,114,101,60,47,101,120,105,102,58,69,120,112,
111,115,117,114,101,77,111,100,101,62,10,32,32,60,101,120,105,102,58,87,104,105,116,101,66,97,108,97,110,99,101,62,65,117,116,111,32,119,104,105,116,101,32,98,97,108,97,110,99,101,60,47,101,120,105,102,
58,87,104,105,116,101,66,97,108,97,110,99,101,62,10,32,32,60,101,120,105,102,58,70,111,99,97,108,76,101,110,103,116,104,73,110,51,53,109,109,70,105,108,109,62,51,53,60,47,101,120,105,102,58,70,111,99,
97,108,76,101,110,103,116,104,73,110,51,53,109,109,70,105,108,109,62,10,32,32,60,101,120,105,102,58,83,99,101,110,101,67,97,112,116,117,114,101,84,121,112,101,62,83,116,97,110,100,97,114,100,60,47,101,
120,105,102,58,83,99,101,110,101,67,97,112,116,117,114,101,84,121,112,101,62,10,32,60,47,114,100,102,58,68,101,115,99,114,105,112,116,105,111,110,62,10,10,60,47,114,100,102,58,82,68,70,62,10,60,47,120,
58,120,109,112,109,101,116,97,62,10,60,63,120,112,97,99,107,101,116,32,101,110,100,61,39,114,39,63,62,10,255,219,0,67,0,114,79,86,100,86,71,114,100,93,100,129,121,114,136,171,255,186,171,157,157,171,255,
250,255,207,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,219,0,67,1,121,129,129,171,150,
171,255,186,186,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
255,255,255,255,255,255,255,255,255,255,194,0,17,8,9,144,12,192,3,1,17,0,2,17,1,3,17,1,255,196,0,23,0,1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,1,2,3,255,196,0,20,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,255,218,0,
12,3,1,0,2,16,3,16,0,0,1,192,0,0,0,33,64,0,0,0,40,32,0,133,0,0,0,0,2,2,128,0,0,20,128,2,144,20,128,0,80,64,82,20,16,0,0,0,0,80,8,10,0,32,5,5,0,128,0,1,72,0,0,0,66,148,16,2,0,10,1,0,0,0,0,0,20,16,2,128,
1,1,72,1,64,4,0,20,128,0,0,0,20,16,0,1,72,1,64,4,0,160,0,0,0,0,8,0,0,160,0,0,0,16,20,0,64,0,0,160,2,20,16,160,16,0,1,72,1,72,10,0,32,0,0,80,0,0,2,130,0,1,72,0,0,0,0,0,0,0,0,2,0,80,0,0,0,0,33,72,10,64,
82,20,0,8,0,4,5,0,133,0,0,0,40,0,128,0,10,64,0,40,4,0,16,0,0,0,3,64,128,0,0,0,0,0,0,20,16,0,8,80,0,0,0,64,0,40,0,0,0,0,20,128,20,16,0,0,0,20,2,0,10,0,4,0,160,128,0,10,80,64,82,20,0,64,80,64,0,0,164,4,
5,41,0,33,65,10,8,10,66,144,20,0,1,0,40,0,2,0,10,0,0,0,1,1,72,0,40,4,0,2,130,2,26,0,16,133,40,0,0,10,1,0,4,40,4,0,2,128,66,144,0,0,0,0,10,0,0,128,0,0,0,0,0,0,160,2,20,128,0,0,40,0,128,0,10,1,64,4,0,0,
0,0,0,8,10,0,0,128,160,133,0,128,160,0,0,33,64,0,0,0,0,133,4,0,0,8,0,0,164,40,0,0,10,8,0,5,4,0,16,165,4,0,128,0,0,0,2,128,0,0,0,0,0,0,0,0,2,144,0,0,32,0,2,128,1,72,0,0,0,0,4,41,72,0,0,160,128,0,0,0,160,
0,8,10,8,80,64,82,144,2,20,160,132,0,20,16,160,0,10,8,64,10,0,0,2,2,128,0,0,0,0,0,0,0,0,2,2,128,67,68,0,16,2,128,0,4,0,128,166,136,8,1,65,0,5,0,160,16,16,2,144,0,0,0,0,10,0,32,0,0,0,5,4,0,0,0,0,20,133,
0,128,160,16,0,0,0,2,128,0,0,164,4,40,0,160,128,0,0,0,0,0,0,16,160,0,0,0,0,0,0,16,0,0,40,4,0,160,16,20,128,0,0,0,2,20,2,0,80,82,0,1,72,0,0,0,64,1,64,40,33,0,0,0,0,52,64,0,0,0,0,0,0,0,0,0,0,0,16,0,80,0,
0,160,128,0,10,0,32,0,2,130,0,1,65,0,0,0,1,64,0,0,0,0,20,128,0,1,1,72,10,80,64,0,0,0,66,128,10,64,8,10,64,80,82,0,0,0,0,0,0,0,0,0,0,66,128,8,0,5,0,16,160,0,8,82,144,0,0,0,0,8,10,8,0,0,133,0,20,0,0,0,16,
0,0,0,0,0,40,4,0,0,0,40,0,128,0,0,5,4,5,0,0,0,0,0,0,4,40,33,72,80,8,10,8,80,8,0,40,0,20,2,0,0,33,64,32,40,0,0,64,0,0,164,41,10,1,10,64,0,0,0,0,32,40,5,0,2,20,128,160,0,1,0,0,0,80,66,0,0,0,2,148,16,0,1,
72,0,0,16,20,0,1,72,0,33,64,5,0,128,20,0,0,0,128,164,0,0,8,80,0,40,32,0,0,10,64,80,80,0,33,10,0,0,0,0,0,0,0,33,160,64,8,1,64,0,0,0,41,0,0,133,5,32,0,0,0,40,33,65,0,0,0,0,0,0,0,4,5,4,5,0,0,0,5,32,0,0,1,
1,64,32,0,0,66,128,10,0,0,0,64,0,0,0,0,0,0,0,0,164,0,20,2,0,80,0,0,16,0,1,64,5,32,33,160,8,104,132,41,0,0,0,64,0,0,160,133,0,0,0,0,128,0,1,74,8,0,4,0,20,128,0,1,72,0,0,160,128,0,0,0,0,0,0,2,130,20,128,
160,0,0,32,0,0,1,0,0,0,0,41,64,32,0,0,0,0,0,66,128,0,0,128,160,0,0,40,41,1,10,0,4,41,1,64,4,5,32,0,0,1,64,32,0,0,82,128,1,8,10,64,80,80,66,2,130,128,66,0,0,40,0,128,20,128,0,80,10,0,4,0,133,5,32,0,20,
0,66,130,0,0,5,4,0,0,10,64,82,16,0,80,0,41,8,0,40,4,0,160,2,0,80,0,32,0,0,66,144,160,160,2,2,128,64,1,65,0,0,0,0,0,20,128,164,5,32,5,0,2,0,0,0,20,0,10,8,1,10,80,8,1,72,10,64,0,33,72,80,0,0,0,0,4,0,20,
0,0,0,0,0,0,0,8,1,72,0,0,0,80,64,0,41,0,5,32,0,2,20,2,130,20,128,160,128,0,0,33,74,1,1,1,72,0,0,2,130,144,0,0,0,133,0,20,128,0,0,0,0,0,0,160,0,8,80,8,10,0,0,2,144,128,0,10,0,0,0,64,10,0,0,0,64,0,0,160,
0,0,4,4,0,20,2,148,128,0,64,1,65,64,0,16,128,160,20,133,4,0,0,80,64,0,0,0,0,0,2,20,2,20,2,0,10,0,4,0,0,82,20,16,20,128,0,0,41,8,80,0,0,133,0,2,128,66,128,8,0,0,0,0,0,0,82,0,0,5,32,0,160,0,0,0,0,0,4,0,
20,128,160,160,133,4,0,0,0,0,0,10,1,1,0,40,0,0,0,0,2,20,0,8,80,0,0,133,0,133,32,0,0,0,40,0,128,160,16,0,0,0,2,130,0,0,0,2,128,8,0,0,16,26,6,64,0,2,128,0,0,0,0,0,2,144,0,0,0,160,133,0,16,160,0,8,80,8,1,
64,0,0,0,32,40,5,4,0,128,160,128,160,16,0,0,40,0,128,164,5,0,16,16,160,20,0,64,82,144,0,1,10,10,0,32,32,5,41,8,1,74,64,0,33,64,0,0,0,41,0,0,0,67,68,33,64,0,128,20,0,1,0,0,2,130,144,128,0,0,41,146,144,
20,160,16,0,82,2,128,0,33,72,0,5,32,4,40,0,0,0,0,0,0,5,0,133,0,0,0,4,40,0,128,160,160,0,0,0,2,2,144,0,0,0,0,0,0,0,0,0,0,1,72,1,1,72,82,20,0,0,4,0,2,144,20,0,0,0,16,20,128,0,0,0,128,160,0,1,64,0,0,8,0,
0,128,0,0,0,2,128,0,33,64,0,0,0,0,0,1,10,80,0,0,128,160,0,64,10,0,4,5,0,0,82,2,2,148,128,16,0,1,72,0,0,2,130,20,0,1,10,64,80,64,0,41,0,41,10,0,0,20,0,0,0,128,0,0,41,0,32,52,8,66,130,2,130,20,0,1,72,1,
1,64,0,0,0,0,16,2,128,1,8,10,0,5,0,2,20,0,1,10,64,1,72,0,0,0,80,8,0,40,32,41,0,0,0,0,0,0,1,65,0,0,160,0,82,0,8,82,20,16,160,16,160,0,0,0,164,0,0,8,80,0,0,0,0,0,0,0,0,0,1,0,0,0,80,1,65,1,0,5,0,133,4,40,
0,0,0,32,41,0,0,0,0,41,0,0,20,0,0,0,128,16,20,164,33,10,0,0,2,128,0,0,0,0,0,0,82,0,82,0,80,1,10,0,0,0,1,10,1,1,64,0,0,64,0,41,72,64,82,144,128,20,2,2,130,2,128,0,0,16,20,16,160,128,160,133,33,65,0,5,0,
20,0,66,2,148,16,0,82,0,0,0,0,64,0,40,0,0,0,0,0,82,0,0,0,0,0,32,40,4,32,0,20,0,0,5,0,16,160,128,0,0,0,20,128,0,10,8,0,5,0,0,1,0,0,0,1,65,0,0,160,133,0,0,1,10,8,0,40,0,2,0,0,40,0,0,8,1,64,0,0,0,0,0,0,0,
0,0,32,0,0,0,0,0,0,0,2,26,0,0,0,0,0,1,0,0,0,0,5,0,128,0,0,40,0,0,64,0,6,138,96,132,40,0,0,10,0,0,0,10,8,0,0,20,128,0,0,0,2,128,0,0,0,64,0,0,20,0,64,10,1,10,8,80,0,4,5,0,133,0,128,20,0,8,0,0,160,0,8,1,
64,0,20,2,16,160,0,0,0,164,0,20,128,133,41,8,1,65,0,40,33,65,0,40,4,41,10,0,0,0,0,0,0,1,1,65,8,1,64,32,0,165,0,0,0,4,0,0,0,0,0,0,5,0,0,0,0,128,20,2,0,80,0,0,128,160,133,0,16,0,1,72,80,1,10,0,0,128,0,10,
0,0,128,2,148,2,0,0,0,0,0,0,0,0,0,0,0,0,16,0,0,0,0,10,0,0,0,1,0,0,160,133,4,0,0,0,5,4,41,8,80,1,64,0,128,0,1,178,25,33,10,0,0,2,128,0,0,2,128,64,0,40,0,0,66,130,2,128,8,80,1,72,8,0,40,32,5,33,64,33,64,
4,0,20,0,8,0,40,32,40,0,0,1,1,64,4,40,0,2,20,2,128,0,4,4,6,129,10,64,82,0,0,0,16,0,10,0,0,0,64,82,0,1,64,0,0,0,0,0,0,0,16,0,10,66,0,0,0,0,83,68,0,0,0,0,16,0,0,0,0,66,148,16,2,130,0,10,1,0,0,2,130,0,1,
64,0,0,0,33,64,0,0,0,0,0,8,0,0,16,160,0,0,41,65,0,0,20,128,0,0,0,0,0,5,32,0,0,0,0,128,20,0,8,66,148,160,16,128,2,128,0,32,0,0,0,41,1,65,0,0,0,10,64,1,10,0,33,72,1,1,64,0,2,130,20,0,0,0,0,0,0,160,128,0,
0,40,0,2,144,160,128,0,66,144,0,10,1,1,64,0,128,20,160,128,128,0,80,0,0,128,164,41,0,40,4,5,0,0,1,65,1,1,65,10,64,80,10,64,0,40,33,0,40,0,2,2,144,20,0,66,144,0,10,0,0,0,0,0,0,0,0,164,4,0,160,128,0,0,4,
40,40,0,0,0,4,0,2,128,66,128,8,66,130,144,0,1,72,82,0,0,0,0,10,1,10,10,64,1,0,40,0,0,0,0,0,82,2,0,0,0,0,0,0,0,0,40,0,0,0,0,0,0,0,0,0,5,32,0,0,8,80,1,76,128,82,130,130,16,0,64,80,0,0,128,0,0,40,0,2,0,10,
64,80,66,20,16,160,2,0,1,10,0,0,2,128,0,0,0,1,64,0,128,20,16,0,64,80,80,0,0,0,0,4,0,0,1,65,10,0,4,0,160,2,144,16,0,82,131,37,4,41,72,8,0,5,0,0,0,0,0,0,0,0,66,130,144,20,128,0,0,0,2,20,2,20,2,20,0,66,144,
0,80,0,0,0,0,0,0,0,0,164,4,0,20,0,0,0,128,0,104,2,0,0,32,0,20,2,20,16,0,0,5,0,16,0,0,0,2,128,0,0,128,0,1,1,64,40,0,0,0,0,128,16,160,2,128,1,0,0,133,40,32,5,0,0,0,0,0,0,0,20,128,0,10,0,4,4,32,40,0,0,10,
80,10,80,8,100,0,0,0,0,64,0,0,20,0,0,33,1,74,8,8,10,1,10,0,32,0,0,0,0,2,128,1,10,0,5,0,0,8,80,82,2,0,0,40,5,32,0,16,2,144,0,0,0,20,2,144,2,20,0,10,64,0,0,0,66,144,160,0,0,0,0,8,0,5,5,4,0,164,40,32,0,2,
130,0,0,40,32,4,5,32,0,20,133,33,74,64,0,0,0,0,0,0,0,0,0,0,4,41,0,0,2,128,0,0,16,0,80,0,0,2,2,128,1,0,33,64,0,20,133,0,128,0,10,8,10,64,1,64,0,16,164,0,0,80,66,128,0,32,32,5,5,32,0,2,128,0,4,0,160,16,
20,128,160,16,160,0,0,0,0,0,0,20,128,0,0,0,0,0,5,0,16,3,68,40,50,100,20,160,0,0,32,5,4,40,0,0,0,33,1,64,4,40,0,16,160,0,0,32,0,0,0,40,0,0,82,20,0,64,10,0,5,4,5,4,0,0,8,82,20,128,160,0,1,0,0,3,68,0,2,2,
128,0,40,4,0,0,1,1,65,72,0,0,0,0,32,41,64,4,0,160,16,0,0,5,4,0,0,0,0,128,0,1,64,4,41,72,0,0,0,0,0,164,0,0,0,0,16,160,2,20,0,1,65,8,0,40,0,0,82,0,0,0,128,0,10,66,144,2,128,0,0,133,0,0,0,0,164,0,0,0,32,
0,20,0,8,8,1,65,64,0,2,2,128,0,0,2,0,80,10,8,0,0,16,160,0,0,0,2,144,2,130,0,0,0,0,1,0,40,0,128,160,208,33,144,8,10,80,0,0,16,2,128,0,0,0,0,32,0,0,0,5,4,0,0,82,148,193,0,0,0,10,82,0,0,40,4,0,20,20,2,0,
0,0,16,20,128,164,5,0,2,130,16,0,80,64,10,1,10,8,0,40,40,33,72,0,0,128,160,133,40,0,0,64,10,64,1,10,10,1,0,0,0,0,0,20,16,0,0,0,2,20,0,66,128,1,1,64,4,40,0,0,0,41,0,0,164,0,16,160,0,0,0,128,0,10,66,128,
0,0,20,16,0,0,0,128,0,10,66,128,0,32,40,0,0,0,0,0,66,130,0,0,0,0,0,0,0,80,0,0,0,1,10,0,4,5,0,160,0,64,0,33,65,72,66,128,0,0,0,1,65,1,1,64,41,0,0,128,0,0,0,20,160,160,201,0,0,160,0,64,80,64,0,40,0,2,20,
0,0,32,0,20,0,0,32,0,26,40,48,64,0,0,3,68,0,0,10,0,0,20,16,2,20,2,20,16,164,0,0,10,0,0,0,8,0,40,4,40,0,16,164,0,165,4,0,0,0,41,0,4,41,64,4,0,0,0,4,40,0,0,0,0,0,0,0,164,0,20,16,2,144,0,0,4,40,4,5,41,0,
0,0,8,80,0,0,0,0,0,0,1,1,64,4,5,0,0,0,33,10,82,20,0,0,4,40,4,0,2,128,66,128,1,10,0,0,0,0,0,133,32,0,160,128,0,10,0,4,0,160,16,160,0,0,4,5,0,0,0,0,20,128,160,160,128,16,2,130,0,82,0,0,0,0,1,0,41,0,40,4,
0,0,80,80,82,2,130,16,128,20,0,1,10,0,0,0,0,0,0,64,0,0,160,0,0,0,128,0,82,131,36,33,64,0,2,128,0,0,0,1,64,32,0,164,5,40,32,0,128,0,10,0,33,72,80,8,10,0,0,0,0,33,72,80,0,0,2,130,0,66,128,64,80,0,0,0,0,
0,2,20,133,4,5,0,0,1,65,1,72,0,0,0,0,0,0,8,80,64,10,82,0,82,20,128,16,160,133,0,2,144,0,8,82,20,0,0,32,0,0,82,0,0,0,164,52,64,0,4,0,0,1,64,0,0,1,1,64,41,0,0,0,8,1,72,0,0,2,144,160,2,0,0,5,4,0,160,0,0,
0,0,1,0,0,0,82,130,20,128,133,40,0,2,2,20,2,144,0,0,0,16,20,0,0,0,2,128,0,33,0,0,2,128,0,0,0,0,0,0,8,0,0,2,130,20,0,0,32,0,160,2,2,2,20,0,0,40,0,160,0,64,0,0,2,128,64,82,144,0,8,0,0,0,0,40,0,0,0,0,133,
0,2,2,148,0,0,4,0,16,160,133,0,2,144,0,0,0,2,0,80,8,10,0,0,164,0,0,82,20,16,0,80,64,0,32,40,4,41,10,1,1,74,8,0,0,16,2,128,0,4,40,0,0,0,0,2,0,10,8,0,40,0,128,160,20,16,0,1,1,64,0,0,0,32,0,164,40,0,0,0,
0,128,0,0,0,0,10,0,32,0,0,80,0,0,133,0,0,0,0,16,20,0,8,0,0,20,20,2,144,128,0,0,0,0,0,0,0,0,0,2,0,0,0,0,0,41,0,5,0,0,0,0,16,2,128,1,1,10,10,1,0,0,160,2,0,64,80,0,33,1,64,0,2,130,128,0,4,0,20,0,1,0,5,0,
164,0,16,0,1,64,0,0,10,64,0,0,0,0,4,40,0,0,0,0,0,0,5,33,10,0,0,2,130,0,8,10,8,10,0,0,160,128,0,10,67,70,64,0,0,0,0,0,1,10,0,33,74,64,1,72,1,1,64,0,0,0,0,0,0,0,0,8,82,20,2,20,0,64,0,40,0,0,0,0,0,0,4,0,
0,80,0,0,0,8,1,10,0,0,20,128,0,0,40,0,0,64,80,0,0,2,144,0,0,0,0,10,64,0,32,40,32,5,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,80,0,0,133,32,0,160,0,64,0,5,0,0,1,0,0,160,16,0,66,128,0,0,0,0,32,33,64,0,2,148,2,0,10,
8,80,8,0,0,20,0,0,0,16,0,10,0,0,0,0,0,128,164,0,20,0,64,0,0,160,0,0,40,32,0,0,10,8,0,41,0,0,0,0,0,0,1,65,0,0,0,0,0,20,128,0,1,1,64,33,65,0,41,72,0,41,0,0,0,0,0,0,0,0,0,1,72,64,10,80,64,0,32,40,0,0,10,
64,0,0,0,64,1,64,0,133,0,0,64,0,40,4,0,0,80,8,0,0,20,0,1,0,40,0,0,8,10,0,0,160,128,0,10,1,10,0,4,4,4,5,0,20,16,20,0,0,0,0,0,0,128,0,10,0,0,0,0,0,16,20,128,0,1,64,0,0,0,0,16,0,0,0,0,10,0,4,0,2,144,0,64,
0,0,2,128,10,0,0,133,4,0,20,20,16,133,5,33,64,32,4,0,20,0,0,4,5,0,16,160,164,32,40,4,0,160,20,164,4,0,160,128,0,0,0,20,132,40,5,32,0,16,20,0,0,32,5,41,0,4,40,5,4,0,0,8,10,64,10,0,33,64,0,0,0,0,0,64,80,
10,8,0,0,160,132,0,2,144,2,128,1,65,10,0,33,65,0,0,133,4,0,2,130,0,80,66,128,0,32,0,2,128,0,4,5,4,0,20,128,0,0,5,0,0,64,80,0,0,2,20,0,0,0,0,0,41,1,10,64,0,0,128,160,0,10,0,0,0,0,4,0,20,133,0,128,160,0,
8,0,0,160,0,66,20,0,10,10,64,80,0,4,32,0,160,0,8,1,64,4,40,4,40,4,0,0,66,20,0,0,40,0,160,16,2,128,0,5,0,16,20,160,128,0,64,8,10,0,0,128,20,0,1,10,10,8,64,82,144,128,160,160,16,20,20,128,128,0,0,0,2,130,
2,130,0,0,0,20,128,0,0,0,0,80,64,10,8,0,32,0,160,0,64,80,1,72,0,40,32,5,4,0,0,1,65,1,0,0,160,0,0,0,0,8,80,10,1,0,5,32,0,133,0,164,0,0,0,0,128,0,80,0,0,0,8,10,8,0,0,20,128,2,128,64,80,0,0,0,0,0,16,2,128,
0,0,0,64,0,0,0,66,128,82,2,0,82,128,8,80,0,4,5,0,0,0,0,16,0,0,40,0,20,16,128,0,0,40,40,0,128,128,2,128,1,1,64,0,128,2,128,8,80,66,128,64,0,0,128,0,0,52,0,0,0,0,0,0,80,66,20,20,0,1,8,80,8,80,8,0,40,0,0,
0,0,20,132,5,0,164,0,160,2,20,164,32,5,32,0,2,2,130,130,0,1,10,10,1,1,72,80,1,0,0,0,82,0,1,65,0,32,0,0,10,0,0,0,0,40,33,64,0,128,0,64,0,40,0,0,0,5,4,32,40,5,32,33,64,0,128,160,0,0,0,0,0,32,41,10,64,0,
5,32,5,32,40,32,0,20,16,2,128,1,0,0,2,128,0,32,5,0,0,66,128,64,1,64,0,0,0,0,16,0,10,0,0,0,10,8,0,0,0,0,0,0,0,0,16,0,80,0,0,128,20,2,144,2,2,128,8,0,40,0,2,0,0,40,0,133,33,64,0,0,8,80,66,20,0,8,0,0,3,64,
2,2,130,144,0,0,40,4,4,5,0,20,164,50,1,64,32,0,20,0,0,0,2,20,20,16,133,0,160,128,160,2,0,0,0,2,0,10,64,82,130,153,0,0,80,64,1,64,4,0,0,0,41,0,0,0,0,4,0,20,0,1,72,0,40,32,5,0,0,64,64,10,0,40,0,128,0,10,
64,8,66,128,80,8,82,20,0,0,33,64,4,41,65,0,0,0,0,0,128,0,0,0,0,80,8,80,0,0,0,0,0,128,20,0,0,0,133,0,2,20,2,2,144,2,128,0,0,0,0,4,0,164,41,76,130,128,0,0,0,0,0,0,0,0,128,160,128,0,0,33,65,64,40,0,16,0,
10,64,8,80,0,0,16,0,0,40,0,2,20,0,64,82,0,0,0,0,8,64,80,0,52,0,5,0,128,0,80,64,0,0,0,0,40,33,0,40,32,5,33,64,4,40,0,16,164,40,0,0,10,64,0,0,20,2,0,8,10,8,80,8,82,128,8,0,0,160,128,0,82,0,0,0,0,8,10,0,
0,0,0,0,0,0,0,16,165,4,0,160,16,133,0,160,128,0,0,0,2,144,0,8,10,0,0,0,66,128,0,0,0,0,40,32,0,0,0,0,16,0,0,40,4,5,0,0,0,0,0,0,0,16,160,0,0,0,2,0,80,0,4,0,20,0,0,0,164,0,128,2,128,82,16,160,0,1,72,0,0,
0,0,33,64,4,0,160,16,0,1,72,0,0,0,1,65,65,0,4,5,0,0,66,128,0,33,10,10,0,32,0,164,0,0,80,64,0,0,132,40,0,3,64,20,2,20,16,164,5,32,0,2,128,64,0,41,72,64,64,82,0,80,0,0,0,0,0,133,40,32,0,0,64,80,82,0,10,
64,0,5,32,0,0,1,65,0,40,32,40,41,8,80,66,144,0,0,4,40,0,128,165,4,5,32,0,0,80,64,8,10,80,8,0,0,160,16,0,0,0,2,130,0,0,4,5,41,0,0,2,2,128,80,64,80,8,82,16,208,32,5,4,0,0,1,0,0,0,80,0,0,0,0,0,0,0,0,0,0,
0,128,164,40,0,0,64,0,40,0,0,0,0,2,0,80,80,8,8,80,1,1,64,0,0,66,128,0,0,0,0,0,16,133,0,20,16,133,0,160,20,2,0,0,4,41,65,0,0,0,8,8,10,80,0,0,0,0,0,16,16,2,128,64,66,128,0,52,80,66,148,16,128,0,0,0,2,128,
66,20,2,148,132,32,5,33,1,74,10,0,32,5,32,0,133,5,32,0,0,64,10,0,5,0,16,0,0,0,0,82,0,0,40,33,64,0,128,160,0,64,0,0,133,0,2,20,0,0,0,160,2,0,0,0,2,0,104,0,0,32,0,20,2,20,16,0,0,0,2,144,0,64,1,72,82,128,
0,4,40,4,5,4,5,0,16,0,0,32,5,0,0,0,0,160,128,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,64,32,32,40,0,128,0,80,0,0,0,0,0,0,64,0,0,0,82,2,2,130,128,10,0,0,2,2,20,160,2,16,0,1,64,0,0,10,1,0,4,5,0,16,0,10,1,
8,0,0,3,64,2,130,0,1,64,4,0,2,130,0,0,41,72,66,0,82,16,160,20,20,16,2,20,0,0,0,0,0,32,0,160,20,16,0,0,0,0,1,64,0,16,2,128,1,0,0,160,0,1,0,5,33,72,0,0,0,10,0,0,0,66,144,0,1,65,10,8,80,0,33,65,1,72,0,0,
0,0,0,0,0,0,0,64,13,0,66,128,0,0,128,164,5,0,128,0,0,4,0,0,1,64,0,0,0,0,0,0,0,0,0,0,0,0,0,164,0,2,144,0,66,144,160,0,0,0,20,128,128,0,80,82,16,0,80,10,64,0,0,133,32,0,0,10,0,4,40,32,0,160,160,0,0,0,2,
0,10,10,1,8,1,10,0,0,0,0,4,40,0,2,144,0,0,32,0,25,40,0,3,64,2,0,10,10,8,1,10,0,40,32,32,5,0,0,8,10,64,1,72,10,8,80,1,0,0,160,0,64,1,64,0,20,128,2,0,80,1,64,33,72,10,1,0,0,0,82,0,1,72,0,40,0,16,2,128,64,
10,8,0,41,10,64,0,5,0,132,41,65,0,0,160,0,8,0,4,5,0,0,0,0,0,0,40,0,128,128,165,0,128,0,82,20,16,160,128,164,0,164,0,0,8,0,0,20,133,4,40,0,2,20,0,0,0,16,20,0,1,72,1,72,0,0,0,0,0,0,0,40,4,41,0,0,128,160,
128,164,5,32,0,160,0,8,0,0,20,0,0,0,0,64,0,40,0,0,0,0,20,16,128,2,148,16,160,16,133,0,0,0,4,40,0,160,16,160,16,0,1,1,72,64,66,128,0,52,64,0,0,160,0,80,64,0,41,1,0,0,3,68,4,41,10,8,1,74,66,130,0,64,0,0,
2,128,8,80,0,0,160,128,0,0,5,0,2,2,128,1,0,0,20,133,33,10,1,64,4,0,160,0,1,0,32,5,41,0,32,5,40,0,16,0,64,10,0,4,41,72,0,0,0,1,72,0,0,0,0,0,160,16,16,160,16,160,0,10,66,144,0,1,65,10,64,0,0,0,8,0,0,20,
2,0,80,66,128,66,128,64,80,0,0,0,10,64,0,0,0,0,0,0,0,0,16,160,0,1,72,0,0,133,32,40,32,32,41,64,32,0,20,133,0,0,1,0,4,5,0,20,16,20,0,0,40,33,0,0,0,80,64,80,0,0,20,0,66,2,128,0,0,160,128,0,64,1,72,8,0,0,
2,128,1,64,0,0,80,64,0,4,0,20,128,20,160,16,16,20,0,0,0,133,0,0,0,32,5,0,0,10,64,0,40,0,0,8,80,0,0,16,2,128,0,4,5,5,32,32,0,0,80,1,0,0,0,0,0,0,80,64,0,0,164,0,16,160,164,4,40,4,40,0,0,0,0,0,0,0,0,0,0,
0,8,10,10,66,144,0,66,130,0,80,0,0,160,133,32,0,0,64,0,0,133,5,0,128,0,10,0,0,0,1,0,0,160,0,0,0,0,0,0,0,8,80,0,0,0,0,0,0,64,100,165,0,20,0,8,1,64,0,0,8,0,0,0,0,0,160,0,1,10,0,0,0,80,66,20,160,16,2,128,
64,0,32,0,2,144,20,133,0,2,0,0,0,132,40,0,2,128,10,0,0,2,144,0,66,128,64,10,0,33,74,8,0,33,64,0,0,82,0,0,0,20,132,5,0,164,0,0,10,0,0,16,0,1,65,0,0,2,128,64,80,10,0,33,0,5,0,0,64,1,74,66,2,153,5,4,41,72,
0,0,20,0,64,0,0,0,67,68,5,0,128,0,0,0,160,16,0,0,0,0,0,0,0,80,64,0,0,0,0,0,2,128,0,32,0,0,64,0,5,32,0,2,130,0,80,1,10,0,0,16,2,128,0,0,0,0,0,2,144,164,41,0,33,64,5,0,0,64,8,8,0,41,10,10,66,128,0,32,0,
160,2,20,2,20,128,2,0,80,10,0,0,128,160,0,0,0,128,160,0,8,80,0,0,128,0,64,80,0,0,0,0,33,64,33,65,8,80,0,40,0,160,0,1,72,0,32,5,0,133,0,2,2,128,10,64,66,128,0,0,20,16,0,80,8,0,5,4,0,20,0,1,0,0,20,128,0,
0,0,2,128,64,10,0,32,40,0,133,0,16,0,80,66,130,2,0,1,64,41,0,40,0,16,0,0,33,64,4,41,10,80,1,1,10,0,0,0,0,5,32,0,0,0,4,40,4,40,4,5,0,20,2,0,1,64,4,0,20,2,0,64,1,72,0,0,160,0,0,0,0,66,128,0,0,16,2,128,0,
0,16,160,0,0,0,0,0,0,0,1,0,0,0,10,66,130,0,82,0,82,0,80,0,0,128,160,128,16,160,2,148,16,0,0,0,16,160,0,0,0,2,2,144,160,0,1,0,0,128,160,164,33,72,0,0,0,8,104,25,0,160,2,128,10,82,0,82,0,80,64,0,0,133,0,
0,0,5,4,0,0,8,10,0,40,0,0,64,0,0,0,0,0,0,0,0,0,0,0,160,16,0,0,41,0,40,40,4,32,33,74,0,32,0,16,2,128,0,33,74,64,0,0,20,16,0,0,0,0,0,5,0,2,0,1,72,82,0,0,0,164,0,0,0,0,133,33,64,0,16,160,16,20,20,128,20,
16,2,128,1,0,0,0,1,0,41,1,64,0,128,160,2,0,10,66,144,20,128,0,0,0,164,5,0,0,0,0,16,20,2,2,128,0,0,128,2,0,80,0,5,33,64,4,41,10,0,0,128,0,64,80,1,64,0,0,1,0,40,0,0,1,0,40,32,5,32,40,5,4,0,16,2,144,0,66,
0,0,0,0,10,64,80,0,52,64,10,82,0,0,0,164,32,0,160,16,160,0,0,0,0,64,80,0,0,20,16,164,5,0,2,0,0,0,133,4,0,160,0,66,128,0,5,0,16,0,10,8,10,0,0,128,0,8,80,0,0,128,160,0,10,64,10,64,0,0,0,0,0,0,64,104,16,
0,10,0,32,0,0,0,0,0,0,4,0,2,128,0,0,2,20,128,20,0,0,0,160,0,0,0,128,0,0,0,16,0,1,64,0,16,20,16,0,0,0,0,10,0,0,16,164,0,0,80,66,128,8,0,41,10,64,10,0,32,0,128,20,0,10,0,0,0,82,16,2,128,64,0,0,0,0,0,20,
0,1,1,64,0,164,0,164,4,0,20,160,0,0,4,0,16,0,0,32,32,0,0,0,0,0,80,0,52,8,10,0,0,0,0,32,0,20,0,0,32,0,0,80,0,5,0,16,0,0,5,0,2,0,10,8,8,80,0,0,0,1,162,0,8,80,8,8,82,128,64,10,64,0,33,74,8,0,41,10,8,0,6,
136,64,0,0,20,128,0,0,0,0,0,4,5,40,32,0,160,2,2,2,128,0,0,164,0,164,0,0,0,4,5,0,133,0,0,80,64,0,40,33,64,32,4,40,5,0,16,0,82,0,0,4,5,33,64,33,64,32,0,0,0,0,160,0,8,0,0,0,1,10,0,0,0,10,64,0,0,0,0,5,0,0,
10,0,4,4,33,65,72,82,0,80,64,0,0,2,144,0,1,65,0,40,40,0,128,164,0,0,10,8,0,0,128,0,8,0,32,0,0,0,0,0,0,5,0,3,64,133,0,0,0,4,0,0,82,26,4,0,0,0,0,160,16,20,0,8,66,148,0,64,80,8,0,0,0,0,0,0,0,0,20,0,0,4,0,
2,20,0,64,80,8,10,10,1,10,66,128,0,32,0,164,0,2,144,0,0,0,2,20,164,0,0,0,0,0,80,0,0,128,0,10,8,0,0,0,0,33,64,0,160,128,0,1,64,4,41,1,10,82,0,66,128,10,64,0,0,0,0,4,41,0,5,0,0,0,32,0,2,128,0,0,0,1,8,80,
0,0,0,0,0,0,80,8,64,10,10,0,0,0,0,33,64,0,128,0,80,8,0,0,0,0,0,0,0,5,0,2,144,0,0,0,0,1,65,0,0,16,2,0,8,1,65,0,0,0,0,5,0,133,0,133,0,3,64,133,0,0,0,4,0,164,0,20,160,0,0,0,2,130,20,128,16,0,1,65,0,5,4,32,
5,0,0,0,0,0,1,64,0,0,82,0,64,0,32,41,72,0,0,2,128,8,1,10,80,8,0,5,41,8,0,0,2,144,0,0,0,0,0,4,40,5,0,128,20,16,160,16,0,0,0,20,128,2,20,0,0,0,20,128,160,128,0,1,65,8,80,8,13,2,0,0,0,0,0,4,0,20,16,160,133,
0,0,0,4,0,0,80,0,0,2,2,144,0,82,0,0,0,20,2,0,0,0,20,0,8,1,64,5,0,16,164,0,16,0,0,0,0,0,0,2,130,0,0,40,0,0,80,1,0,40,0,128,16,164,4,0,2,0,0,5,41,72,100,2,130,0,1,72,104,16,133,4,5,0,3,64,164,32,40,0,0,
0,0,0,1,64,0,0,8,0,0,164,0,2,2,128,8,80,64,80,10,64,0,0,0,0,41,1,64,4,5,32,0,0,0,5,4,0,20,16,160,160,128,128,2,20,0,80,8,1,72,0,0,0,0,0,164,0,0,8,80,8,1,74,64,1,64,0,133,32,0,160,16,0,10,0,32,0,20,128,
2,0,80,0,5,0,25,5,0,128,160,0,0,0,16,20,133,0,0,1,10,0,0,0,0,0,16,0,80,0,0,0,0,0,16,0,80,1,0,0,2,128,0,0,16,0,0,41,64,0,133,0,3,32,160,0,64,0,0,0,1,64,32,40,0,0,0,40,0,2,144,16,128,2,0,64,0,0,0,0,6,129,
76,144,2,144,0,82,0,80,0,32,5,0,3,64,2,0,80,82,0,1,72,10,64,0,41,0,41,0,0,0,64,10,1,0,5,32,5,4,6,129,0,0,133,5,0,128,160,16,0,0,0,160,128,16,20,2,128,64,10,80,100,160,160,16,128,133,33,74,8,80,8,0,0,0,
0,40,32,0,0,0,0,2,20,0,0,0,20,128,160,2,0,0,5,32,0,0,0,0,0,10,64,80,64,8,1,162,2,2,130,0,1,74,8,0,0,0,0,0,0,0,0,16,2,130,0,10,0,32,5,0,0,0,0,0,1,0,0,2,130,0,0,0,0,80,0,4,0,0,10,0,5,4,4,5,33,72,0,0,0,0,
0,20,0,0,0,160,0,10,1,1,0,32,33,0,0,0,0,0,0,1,72,104,16,16,20,16,0,80,0,0,2,0,80,0,52,0,4,5,41,10,0,4,0,0,80,64,1,65,0,33,72,82,0,80,8,10,0,4,5,4,5,0,0,0,5,33,72,10,64,0,40,0,128,160,128,16,165,32,5,4,
0,165,33,1,64,32,0,0,0,0,20,16,0,0,0,0,0,0,20,2,0,1,1,64,32,5,0,2,144,160,0,64,0,0,20,2,0,0,0,2,128,64,0,0,2,27,50,64,80,66,128,8,13,2,20,128,133,0,0,0,0,0,0,32,5,0,2,0,82,0,80,66,128,0,0,128,2,130,0,
10,64,0,0,20,128,0,10,0,4,0,160,2,2,128,80,0,0,132,0,0,1,64,0,160,0,66,144,16,2,144,160,164,41,1,0,0,128,16,0,0,0,0,0,0,0,80,1,0,5,0,128,0,0,0,0,1,64,4,52,10,0,0,160,0,64,0,0,165,0,132,4,0,20,2,0,1,64,
4,40,0,0,0,0,0,0,0,0,0,5,0,16,0,10,0,4,32,40,33,64,40,32,40,0,128,20,2,0,0,5,4,5,4,0,160,133,32,0,0,0,0,0,1,72,1,0,0,2,128,10,0,0,16,0,10,0,0,16,0,0,41,0,40,0,0,0,50,66,130,128,8,80,8,82,144,0,0,0,0,0,
0,0,8,10,64,66,128,80,64,80,0,0,133,0,0,0,0,2,0,0,0,0,1,64,0,0,0,4,0,160,132,5,40,0,2,144,0,64,0,5,0,20,2,0,80,1,1,0,0,128,20,0,64,0,0,2,0,0,0,2,128,64,82,0,0,41,0,0,20,128,160,133,0,0,64,0,40,0,208,0,
0,10,64,0,33,64,0,20,0,64,0,0,2,2,128,10,64,0,0,0,0,0,0,1,0,40,0,164,5,0,128,20,128,0,0,0,0,66,128,0,0,164,41,10,1,0,5,0,128,160,133,4,0,20,16,0,0,0,0,0,0,0,0,33,65,1,74,1,1,65,1,64,0,0,0,0,133,0,2,20,
16,20,0,0,0,2,2,130,0,0,0,0,0,0,0,0,4,5,32,0,0,82,0,1,64,4,41,10,0,0,0,0,33,64,0,128,0,1,64,0,0,0,4,0,20,0,0,4,0,164,5,33,72,0,0,160,133,0,20,128,0,0,0,128,128,2,144,160,0,1,0,4,40,4,0,0,1,65,0,40,32,
0,160,0,0,0,0,0,0,0,0,0,128,2,128,13,2,2,128,0,0,128,2,128,10,0,32,0,0,0,0,2,2,128,0,0,0,1,65,0,32,0,2,130,128,64,10,0,32,0,0,0,5,0,2,0,8,10,80,10,66,20,0,64,10,66,144,0,0,0,0,0,33,65,64,0,128,0,0,0,2,
2,128,10,8,0,40,0,0,0,0,0,0,0,0,0,0,16,20,0,8,82,0,0,40,0,2,0,10,64,0,0,0,1,0,0,0,82,0,1,64,0,2,20,16,160,0,0,33,65,0,0,0,80,0,0,16,0,0,0,0,1,64,4,0,0,1,64,0,0,0,0,2,0,0,32,32,0,0,0,0,0,0,41,10,66,148,
132,0,0,1,65,10,0,0,0,0,0,0,10,0,4,5,0,16,160,128,16,2,128,13,2,2,128,1,0,5,0,20,0,1,0,0,0,0,0,0,0,4,5,0,0,1,1,64,5,4,0,160,2,0,1,72,80,8,80,8,1,72,80,64,66,144,20,0,10,10,66,0,10,1,0,0,2,130,0,0,0,0,
10,0,32,0,0,0,41,10,8,10,64,1,72,1,13,2,20,0,0,4,0,20,0,0,4,40,0,128,160,16,20,0,66,130,2,130,16,20,160,128,0,0,0,0,8,0,33,64,0,0,0,41,0,41,1,64,0,0,0,4,40,4,0,2,128,0,32,0,0,0,5,0,16,0,10,66,130,130,
20,0,1,0,0,16,0,64,64,0,0,0,1,72,0,0,0,80,66,144,2,130,2,128,0,0,0,0,0,160,133,0,0,0,0,0,0,4,0,20,128,16,2,128,13,0,0,0,0,0,33,64,33,10,104,16,0,0,0,20,128,0,0,0,0,1,10,82,0,10,64,0,5,0,133,4,0,20,0,0,
0,0,1,0,0,0,1,72,1,64,4,0,0,0,40,32,0,0,0,0,16,20,0,1,64,32,40,0,0,0,0,0,66,130,16,165,0,0,0,4,5,33,64,0,0,0,0,16,0,0,40,4,0,160,132,41,64,32,0,2,130,16,0,0,0,160,133,4,0,2,130,0,0,0,20,0,1,0,5,0,16,2,
128,0,0,128,164,0,20,0,0,0,2,0,80,0,0,160,133,32,0,128,2,2,0,0,0,0,0,0,0,0,0,0,0,40,0,0,10,8,0,0,0,0,40,0,0,0,0,0,0,0,0,0,32,0,2,2,128,10,1,65,13,16,0,0,33,64,32,41,64,32,0,0,1,65,0,0,0,8,10,0,0,0,0,0,
20,16,160,128,164,0,20,128,2,128,0,33,64,0,2,0,0,0,2,144,0,0,40,4,0,0,0,33,64,0,0,0,0,134,128,4,5,0,0,1,72,80,8,0,32,5,0,0,8,10,8,80,1,0,0,0,0,0,0,0,41,0,0,0,10,8,10,8,0,32,40,33,64,0,0,1,0,5,32,0,0,0,
5,0,128,20,0,66,128,0,0,128,0,10,0,0,0,8,0,0,0,0,0,160,0,0,32,0,128,2,0,0,0,0,10,64,0,41,0,0,0,80,1,72,82,0,0,0,0,0,0,0,1,72,80,1,10,64,80,66,128,66,128,1,0,40,32,0,2,2,128,10,0,40,0,0,0,0,0,1,65,0,5,
32,41,0,0,0,0,0,0,0,0,0,82,0,10,10,1,8,80,64,1,72,1,64,0,2,0,0,0,2,130,0,0,40,0,133,32,0,0,1,10,82,0,0,0,0,0,0,2,128,1,65,10,64,1,72,80,64,8,10,8,80,0,0,0,8,0,32,5,0,128,165,0,128,133,0,16,20,2,20,0,0,
0,160,16,128,160,0,0,0,0,0,0,16,0,0,0,20,128,0,80,0,0,2,0,80,0,0,0,0,0,16,0,0,5,0,0,0,0,2,16,0,64,0,0,0,0,0,20,128,160,128,0,82,0,80,0,0,0,1,0,5,0,2,128,0,0,133,4,41,10,64,0,0,2,128,0,0,16,0,0,40,0,132,
40,0,208,33,64,32,40,0,20,2,0,0,0,0,0,0,0,0,0,0,82,0,0,0,0,0,40,0,20,16,0,0,0,16,164,41,10,0,33,10,80,0,0,0,0,32,40,4,0,0,8,80,1,1,64,0,0,80,0,4,40,0,0,10,1,0,0,0,64,0,0,160,16,0,80,64,1,1,64,0,133,0,
160,16,0,8,1,64,0,160,16,0,10,8,10,66,20,20,133,32,0,0,0,0,128,0,0,0,16,160,0,80,8,80,0,0,128,20,0,0,4,40,33,65,0,0,20,0,10,64,82,0,0,33,0,32,40,33,74,8,64,0,0,2,144,0,82,2,128,0,0,0,8,80,0,0,0,1,65,0,
40,0,16,20,2,2,128,64,10,1,0,0,160,0,66,130,0,0,0,0,64,80,13,2,20,2,20,0,1,64,32,5,4,0,0,66,128,0,0,2,128,64,0,0,0,0,4,40,0,160,16,164,5,0,16,0,1,1,64,41,0,40,0,16,160,0,8,0,40,0,16,0,0,4,5,0,0,0,0,160,
160,133,4,4,40,0,16,20,16,128,160,0,0,41,10,64,8,10,1,1,64,0,0,0,0,160,16,160,132,5,0,160,128,160,128,16,209,8,0,5,0,20,128,0,0,4,0,0,1,64,4,0,2,130,0,1,65,1,64,32,40,0,16,0,1,65,0,0,20,133,0,160,128,
2,2,0,1,0,0,166,128,0,3,36,0,20,128,0,80,0,0,0,0,5,0,16,2,128,64,0,0,0,0,41,1,72,80,0,4,40,0,0,8,10,8,1,72,0,0,0,0,0,0,64,80,13,0,0,32,52,64,1,65,76,128,0,32,5,4,5,5,0,0,66,144,0,80,1,0,4,40,4,0,160,16,
160,2,128,0,32,0,16,160,0,0,41,1,64,33,65,0,40,4,0,2,144,0,1,65,0,0,2,128,10,0,0,0,64,1,72,8,64,0,5,5,32,0,2,144,20,128,0,0,0,0,0,41,10,0,4,40,0,128,0,0,0,0,0,0,0,10,0,33,0,40,0,16,0,80,0,0,0,1,0,0,0,
80,1,0,5,0,0,64,0,0,0,80,1,64,33,64,33,8,1,0,0,0,0,0,26,41,72,8,66,2,144,20,0,0,0,0,0,5,0,133,0,16,160,0,8,0,0,0,0,0,0,0,40,0,0,1,10,64,82,0,8,80,8,80,1,10,8,10,0,32,5,0,208,0,0,66,128,0,40,32,0,16,2,
128,0,0,0,0,4,40,41,72,64,10,8,0,0,0,0,0,2,130,0,0,0,133,0,0,0,0,133,0,0,80,0,4,0,0,0,0,160,2,0,10,0,0,160,16,0,0,0,0,8,10,66,128,0,32,5,32,41,1,65,0,0,164,0,0,0,0,20,0,64,80,64,66,128,0,0,160,16,0,80,
0,4,0,128,20,0,64,80,8,80,0,0,2,0,1,72,0,0,0,0,40,40,32,32,0,160,2,128,0,32,0,128,16,16,0,0,0,0,1,72,82,3,64,128,0,0,0,160,0,1,0,5,0,0,0,0,0,0,32,0,0,10,64,1,65,0,0,0,0,0,0,0,0,0,0,0,0,0,4,5,32,0,0,80,
13,0,8,80,64,104,128,0,82,2,20,128,2,128,82,0,0,0,0,0,5,4,0,20,128,0,1,72,0,40,0,2,0,1,64,32,5,4,5,0,16,0,10,1,1,72,1,65,0,0,2,128,8,80,0,0,0,82,16,2,128,0,0,0,64,80,64,80,1,10,8,0,0,0,0,0,160,128,164,
0,20,0,64,80,8,82,16,20,0,0,0,0,0,0,0,0,0,2,2,128,0,33,64,0,128,2,130,2,128,1,0,5,0,160,128,0,0,0,160,0,64,64,8,1,0,0,0,0,0,0,0,0,20,133,0,0,0,0,2,128,0,32,0,160,133,0,0,0,0,0,64,0,0,0,0,0,0,0,0,0,0,0,
0,0,0,20,0,8,0,0,0,64,0,5,4,5,0,208,0,128,0,80,0,0,16,160,0,0,0,0,0,0,0,0,0,20,16,20,128,0,0,0,0,80,1,0,0,164,40,32,40,0,2,144,2,0,0,33,74,0,33,160,66,0,0,5,0,0,0,0,16,0,1,64,0,0,0,33,65,0,5,0,16,20,132,
5,0,128,160,0,0,5,0,128,20,0,0,32,32,5,0,20,0,66,128,64,10,8,0,0,0,64,80,10,64,0,0,0,0,0,0,0,0,20,2,20,16,0,0,0,160,128,2,2,0,64,0,0,0,0,0,164,0,20,0,0,0,0,10,8,10,8,10,0,0,0,0,32,40,0,0,0,0,0,8,0,0,0,
0,0,0,0,0,0,0,0,0,0,0,20,0,64,0,0,0,0,0,0,64,80,13,0,0,32,5,0,16,2,128,82,0,0,0,2,20,0,0,0,0,0,5,4,5,32,0,2,128,64,82,130,16,164,0,2,130,0,80,0,0,0,0,32,0,20,128,20,2,0,80,0,0,2,2,128,64,0,0,2,128,0,4,
0,20,2,2,128,8,0,4,5,0,133,0,2,128,66,128,66,144,160,0,1,1,0,41,72,82,0,0,40,0,0,64,10,1,8,0,40,0,0,10,66,144,0,0,0,20,20,0,8,8,10,82,16,128,160,0,64,64,0,32,0,0,0,52,66,2,128,8,0,40,0,0,0,0,0,0,40,0,
0,0,0,0,0,0,0,0,0,0,0,0,128,16,20,2,144,0,0,0,0,0,0,0,0,0,0,0,5,0,0,8,0,0,133,0,0,8,10,13,0,0,4,5,0,0,0,0,20,132,0,20,0,0,0,0,0,0,0,1,64,32,0,2,148,16,133,0,0,1,0,41,72,64,80,10,64,1,65,1,72,0,33,72,0,
0,160,0,8,82,0,80,8,82,0,0,0,0,80,0,32,41,0,0,0,80,1,10,1,8,80,0,0,160,0,66,130,0,10,0,0,16,0,0,0,16,20,160,16,0,82,0,1,1,72,10,0,0,0,0,41,0,5,0,160,2,0,1,0,0,128,0,64,82,2,0,0,0,0,1,64,4,40,4,5,0,0,0,
0,2,128,0,0,0,0,0,0,0,0,0,1,10,0,4,41,1,72,0,0,0,0,5,4,0,0,80,8,0,0,0,0,0,0,0,0,0,80,64,80,8,0,0,0,0,4,0,164,54,8,0,0,0,80,0,0,0,80,66,2,128,0,5,33,72,0,0,2,130,0,10,64,0,40,0,2,2,144,20,0,8,10,0,33,64,
41,0,40,32,40,32,0,0,64,1,10,1,65,0,4,5,5,32,0,2,20,0,1,64,32,0,133,0,0,0,40,0,0,64,0,0,16,165,0,128,20,16,2,128,8,0,0,0,0,33,74,0,0,0,0,4,4,40,0,0,82,0,0,40,0,201,74,80,1,1,0,4,41,76,128,1,0,0,128,0,
0,41,72,82,0,64,80,64,10,0,0,0,0,0,2,128,0,0,0,0,0,0,0,0,133,4,0,164,0,0,0,0,0,0,0,133,0,2,128,8,0,0,0,0,0,0,0,41,0,0,20,0,1,1,65,0,33,64,0,164,0,16,2,131,64,16,20,0,8,80,0,0,0,8,0,40,0,0,0,0,0,82,0,82,
2,144,0,0,5,0,0,64,10,8,80,8,10,10,64,66,128,0,5,0,2,0,0,33,72,1,0,40,33,65,64,4,0,0,0,0,20,128,0,0,0,0,80,64,0,40,32,40,32,0,2,2,144,20,20,16,160,2,20,0,8,0,0,2,20,0,10,0,0,133,33,65,0,0,0,10,64,0,41,
0,40,4,5,40,32,4,32,0,0,0,4,41,8,0,0,0,1,74,8,0,0,2,2,130,20,0,0,40,0,0,0,0,0,0,0,164,0,2,0,0,0,0,0,0,0,0,0,0,1,72,0,0,0,1,10,0,0,0,0,0,20,128,0,82,0,80,8,80,0,0,128,2,20,0,64,80,1,10,0,52,0,0,0,8,80,
0,0,0,0,0,0,0,0,0,0,41,0,0,0,82,0,82,0,82,144,0,66,128,66,128,8,1,74,8,8,80,10,8,80,0,32,0,2,16,160,128,160,0,80,0,4,40,0,128,0,0,0,20,0,0,32,0,133,0,128,160,128,0,10,8,82,2,130,20,2,128,64,10,10,64,66,
130,0,64,1,65,65,10,0,0,2,0,0,41,0,0,160,128,20,0,66,144,2,130,2,0,0,0,16,0,64,13,131,36,0,164,0,160,0,0,0,0,0,0,0,1,64,0,2,2,128,0,0,0,1,0,0,0,1,64,4,0,2,130,0,0,0,2,144,0,0,0,128,2,128,10,0,0,133,4,
0,0,0,0,160,128,0,82,20,0,8,0,0,0,8,0,0,2,128,13,0,0,0,0,0,0,0,0,0,0,1,72,0,0,16,209,0,41,0,0,0,0,0,2,128,0,0,0,0,0,128,160,0,0,0,0,1,72,1,10,66,2,128,0,40,33,160,64,0,40,50,82,2,130,2,2,128,80,0,4,0,
20,16,0,0,0,133,32,33,65,64,0,133,5,0,16,133,40,41,1,1,65,0,41,1,72,0,0,0,66,148,128,0,10,8,82,0,0,5,0,0,64,8,0,4,41,0,0,128,0,80,66,148,128,0,0,5,0,128,0,0,0,0,0,0,20,0,0,32,5,0,0,1,0,0,0,0,0,160,128,
0,0,0,0,82,0,10,8,0,0,0,0,0,0,1,64,0,128,0,0,0,0,0,0,0,0,0,0,0,0,16,0,0,41,10,64,10,13,2,144,160,2,16,20,160,128,0,66,128,0,5,4,0,0,1,147,69,33,65,0,40,32,41,10,66,144,0,10,1,10,0,4,41,1,72,0,52,64,66,
128,0,0,2,144,2,0,80,1,1,64,5,4,40,4,32,33,65,10,8,1,64,40,0,133,32,5,0,0,64,1,10,64,82,16,160,20,2,144,0,80,0,0,20,16,0,64,80,8,10,8,0,0,0,66,148,128,160,2,20,128,16,160,2,128,66,0,0,33,72,1,1,72,80,
82,0,66,128,0,0,0,80,0,4,41,1,64,32,0,0,80,8,80,0,0,128,0,0,0,0,0,0,0,0,0,160,0,8,1,64,0,2,20,16,0,0,0,2,144,0,0,0,0,80,8,0,0,0,0,4,5,0,0,0,0,0,0,0,133,0,2,20,16,0,10,67,161,1,64,32,32,40,40,32,0,128,
166,136,64,1,72,0,0,0,0,5,32,0,20,16,164,0,160,128,0,80,1,1,65,0,40,32,5,41,0,33,64,0,0,0,0,2,128,64,66,144,20,20,134,74,1,0,5,4,0,20,20,133,32,40,32,5,0,16,133,0,0,0,4,0,0,80,82,2,128,1,1,64,5,4,0,160,
16,132,40,4,0,20,0,80,66,128,8,0,0,2,20,2,0,0,32,0,0,66,144,2,128,82,0,0,0,0,0,0,0,1,64,0,0,64,0,0,20,0,0,0,2,0,0,0,0,0,0,0,0,40,4,0,2,128,64,1,64,4,40,32,0,20,128,2,130,0,64,80,0,0,0,0,0,2,0,80,0,0,0,
0,0,0,0,0,0,8,0,0,0,0,0,232,64,1,0,5,0,0,8,0,41,162,25,5,0,2,20,0,1,65,10,8,1,72,10,8,82,20,0,8,1,64,0,0,0,0,128,2,128,82,16,160,2,2,128,0,0,2,144,0,0,0,200,0,2,128,1,0,40,0,0,0,0,160,0,8,0,32,40,5,4,
0,133,5,0,0,0,32,0,160,0,0,0,16,2,2,130,128,1,65,8,0,33,64,0,16,16,0,0,4,0,160,16,164,40,4,40,0,0,0,0,0,0,40,0,0,8,10,66,144,0,10,0,4,41,10,1,0,40,32,0,0,0,40,32,0,0,0,0,160,16,2,128,0,0,0,1,10,8,0,41,
1,72,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,32,41,10,8,0,40,0,128,20,208,0,16,160,0,0,33,64,32,41,72,64,82,2,128,8,10,0,0,20,128,20,128,160,0,1,10,1,0,5,0,0,0,33,64,32,5,0,16,160,16,160,16,20,133,0,0,1,
72,80,64,1,10,64,80,0,32,4,41,72,1,65,10,0,0,0,66,144,0,0,0,164,0,2,130,0,0,0,2,20,0,0,40,4,0,133,5,33,64,0,2,16,20,16,0,64,0,4,0,0,80,64,10,0,41,0,0,0,0,0,0,1,64,33,64,0,0,0,0,0,64,10,1,10,64,0,0,0,0,
0,0,80,66,144,0,0,0,20,133,0,16,164,40,0,0,0,0,0,64,0,0,0,0,0,2,130,0,0,0,0,0,0,164,0,0,0,0,0,0,0,0,0,4,40,0,0,0,0,128,20,208,0,0,0,0,2,2,130,20,20,0,64,0,33,64,0,0,80,8,0,0,160,0,0,33,65,1,64,32,40,0,
16,20,2,0,0,0,0,1,65,0,5,0,133,0,0,8,1,74,64,8,1,64,32,41,0,0,160,128,0,0,5,0,0,64,0,0,160,16,20,0,8,0,0,2,20,133,0,0,0,0,0,0,40,32,0,2,0,64,8,1,64,0,2,0,80,0,0,0,0,0,0,0,0,2,128,0,4,40,4,0,20,133,0,2,
0,0,0,0,0,33,64,0,0,1,65,0,0,20,2,2,128,64,10,1,0,0,20,16,160,128,0,0,0,0,0,0,0,0,0,0,0,0,0,66,128,0,0,0,0,0,0,0,0,2,144,0,0,0,2,130,0,1,1,77,0,0,0,2,2,130,2,128,66,128,0,41,0,0,0,8,80,1,65,0,5,0,16,20,
133,0,0,0,33,10,80,1,0,40,4,5,0,16,0,1,72,80,0,0,0,1,0,40,0,0,66,20,160,0,1,10,66,130,20,16,0,8,80,10,64,80,1,0,0,164,0,20,0,8,8,80,0,0,0,0,0,0,1,0,41,0,0,16,0,8,13,16,0,80,0,41,144,0,0,0,0,0,0,0,0,2,
128,0,33,64,0,128,0,10,1,0,4,40,32,40,4,40,0,0,0,0,164,0,0,0,0,0,82,20,0,64,82,0,0,0,0,0,0,0,0,0,0,0,0,0,10,0,32,0,0,0,0,0,0,0,0,0,0,0,0,40,4,0,0,0,5,32,0,0,1,10,13,0,0,0,0,1,10,0,32,40,41,10,80,66,20,
16,0,0,0,20,128,2,128,1,1,64,0,0,0,33,10,80,0,33,64,0,128,164,0,0,1,64,0,0,0,0,2,0,0,32,32,41,64,40,32,5,4,0,2,130,0,0,0,0,1,65,0,0,0,0,5,4,0,16,20,2,0,82,20,2,20,128,0,8,80,66,0,1,65,0,40,41,0,5,0,16,
0,8,80,66,128,0,0,160,0,0,0,0,0,0,128,0,8,82,20,0,0,4,40,33,64,0,0,0,0,20,2,20,16,20,133,33,65,0,0,2,144,0,0,0,164,0,0,0,0,0,0,0,0,0,0,0,10,66,130,20,128,20,133,0,16,0,82,0,82,20,16,0,0,0,0,0,0,0,0,0,
0,0,41,1,1,77,0,0,0,0,64,10,0,0,0,82,128,64,1,0,0,0,1,64,32,40,0,2,20,0,0,0,16,0,82,0,82,0,82,0,0,40,32,0,160,0,0,0,16,2,2,128,82,16,0,82,144,2,130,0,1,10,0,0,0,0,0,0,0,41,1,72,82,0,0,0,0,0,0,16,0,0,0,
0,1,10,64,0,33,65,10,82,16,2,128,0,0,0,0,0,0,0,0,2,128,0,0,2,20,0,66,128,1,0,0,0,0,0,0,0,0,0,0,0,0,0,41,10,64,10,83,37,0,16,0,0,0,0,0,40,0,0,1,10,8,0,40,32,0,0,82,0,0,0,0,10,0,0,0,8,10,64,80,8,0,0,0,0,
0,0,0,0,2,144,0,0,0,128,164,0,160,208,0,0,66,128,0,0,20,128,0,82,128,66,130,16,128,160,0,10,1,0,40,0,2,2,128,0,4,0,0,0,41,10,66,130,0,64,80,10,64,80,0,0,164,4,0,160,0,0,32,0,133,4,40,0,20,16,0,0,5,32,
0,0,0,0,2,128,1,10,8,0,0,16,0,0,0,16,0,0,0,0,64,1,72,82,20,0,0,0,0,0,0,0,0,5,33,65,10,8,1,64,0,2,20,0,8,0,0,0,0,0,0,0,0,0,0,0,0,0,40,32,40,32,0,0,1,65,1,72,0,0,0,0,0,20,16,160,128,164,0,0,0,5,0,133,4,
40,0,16,0,0,0,20,128,20,0,8,0,0,0,0,0,0,10,1,0,0,20,128,0,8,0,40,33,72,0,0,3,96,128,0,80,0,0,0,0,0,20,160,133,4,33,162,16,133,0,0,1,64,0,16,20,16,20,128,0,80,8,0,0,0,10,1,0,33,64,40,0,0,0,0,16,160,0,64,
10,66,130,0,64,80,0,0,0,0,0,133,0,0,1,64,0,16,160,2,0,0,0,16,0,0,0,133,32,4,0,3,70,65,64,0,0,0,0,0,0,0,0,0,0,160,16,20,164,4,5,0,2,2,128,1,0,0,0,0,5,32,5,32,0,0,0,0,0,1,65,0,5,32,0,0,1,72,0,0,0,1,65,0,
0,0,0,0,0,0,0,0,0,0,0,0,0,0,10,8,0,33,64,0,20,133,32,0,2,130,0,0,0,0,0,0,2,2,128,82,0,64,0,5,0,128,0,0,0,208,40,4,5,0,0,0,0,0,0,40,0,20,3,32,0,0,5,0,0,8,0,0,20,16,20,0,1,0,0,0,1,72,80,66,0,10,10,0,32,
5,0,128,20,128,0,0,0,0,66,128,0,0,0,1,10,0,5,0,0,0,32,0,0,0,0,128,0,64,0,4,0,165,33,76,128,82,144,0,0,0,0,1,65,0,0,160,0,0,0,0,0,0,0,64,10,0,32,40,32,0,0,0,0,20,2,0,0,0,0,0,0,160,2,20,128,0,0,0,0,0,0,
0,80,64,10,8,0,0,2,128,66,128,0,0,16,0,0,0,0,0,40,32,41,0,0,0,10,0,0,2,0,0,0,0,0,0,0,0,0,0,0,0,2,2,144,160,0,8,82,0,0,0,216,0,16,20,0,0,0,0,0,41,10,80,8,0,32,41,0,0,0,80,8,1,1,65,10,10,8,10,0,4,0,16,0,
10,1,72,0,0,0,82,0,1,64,0,128,0,0,0,2,20,0,0,0,0,0,0,0,1,65,0,0,16,0,0,0,2,0,0,32,5,32,0,20,0,0,0,0,0,0,0,0,0,0,10,0,5,32,0,2,144,0,0,33,72,0,5,4,0,0,0,0,0,10,1,0,0,0,0,40,33,72,0,0,0,0,0,0,0,0,0,0,40,
0,16,0,82,2,144,0,80,0,0,0,8,0,0,0,0,40,0,0,0,4,0,2,20,0,0,0,0,1,10,0,0,0,0,0,0,0,0,0,10,64,0,0,0,0,0,0,8,1,77,0,8,80,0,0,0,0,5,32,0,165,0,128,164,0,2,0,8,82,128,64,8,10,0,0,20,16,20,0,64,0,0,0,1,10,0,
0,0,8,10,10,0,32,4,40,0,0,0,0,2,144,0,0,0,0,1,1,65,0,0,16,160,2,0,10,64,0,4,5,4,40,0,0,0,0,0,0,0,0,0,0,20,2,0,80,0,0,0,0,0,2,2,144,20,133,32,0,0,0,0,20,128,20,0,8,0,0,0,0,5,0,128,0,66,130,0,80,0,0,0,0,
0,2,128,8,0,5,32,0,160,160,2,2,130,0,0,0,0,0,4,40,0,0,64,0,0,0,0,0,0,0,0,16,160,0,0,0,0,0,0,0,0,0,0,0,0,20,2,0,80,8,1,1,77,0,0,0,2,128,64,0,0,164,0,20,0,10,64,64,1,64,32,0,0,0,32,40,0,20,0,0,0,0,66,130,
0,8,80,10,64,0,33,74,8,80,8,0,0,0,0,0,0,64,10,66,128,0,32,4,41,1,64,32,0,160,2,2,20,128,20,209,8,0,0,0,0,0,0,0,0,0,10,0,0,0,1,0,5,0,0,0,0,0,82,0,8,0,5,32,0,0,0,41,0,0,0,82,0,80,8,0,0,0,0,0,209,0,0,0,64,
1,64,41,8,0,0,0,0,40,33,72,1,64,0,0,0,0,0,0,0,0,0,0,128,0,0,0,0,0,0,0,1,1,64,0,0,0,0,0,0,0,0,1,64,4,0,0,0,0,20,0,1,65,8,80,8,1,0,54,0,0,0,0,0,0,1,10,0,0,160,0,64,0,40,4,4,40,0,0,8,10,0,5,0,0,0,0,128,2,
20,16,20,0,0,0,0,82,20,0,64,0,5,32,0,0,64,80,64,0,0,128,0,0,41,1,72,82,0,8,80,0,0,0,82,0,0,0,20,0,0,0,0,0,5,32,0,0,0,0,0,0,0,0,0,0,2,2,144,16,160,26,50,0,0,0,0,0,0,10,64,0,41,0,0,0,0,0,20,160,16,2,130,
0,1,72,1,0,0,0,0,0,2,144,0,1,64,0,0,0,0,0,1,10,1,0,0,0,0,0,0,0,0,0,8,10,0,0,0,0,0,0,10,64,80,64,80,64,82,20,0,0,0,16,0,0,5,5,33,8,0,40,32,0,216,0,0,0,0,0,0,0,20,128,0,80,10,66,0,1,72,8,80,8,10,0,0,0,10,
64,10,0,41,0,32,0,2,144,2,144,0,10,64,10,64,1,72,80,0,0,128,0,0,0,16,0,64,0,0,2,2,128,82,20,128,0,0,0,20,0,0,0,0,0,0,160,0,64,0,0,0,0,0,0,66,148,128,16,20,16,20,133,0,128,0,10,66,20,20,20,200,41,0,40,
4,0,160,133,0,2,0,80,8,0,0,0,1,65,72,1,0,0,2,20,20,128,0,0,0,0,0,0,2,130,20,0,1,0,40,33,64,0,128,0,0,0,0,0,0,20,0,0,32,0,0,10,0,0,2,0,80,1,10,0,33,64,0,16,20,0,0,4,0,164,0,0,0,0,2,0,82,2,148,201,77,0,
0,0,0,0,0,0,0,0,2,128,0,4,0,0,1,10,0,0,0,0,5,32,40,0,2,144,2,0,0,33,74,0,32,41,10,64,0,40,0,0,8,1,1,64,0,0,64,0,32,0,16,0,10,0,0,0,10,1,10,0,0,164,0,0,0,0,0,80,0,0,0,66,128,8,80,66,144,0,0,4,0,0,0,0,0,
0,41,0,0,3,68,0,128,2,128,64,0,0,0,0,0,0,0,0,0,0,5,4,0,2,0,1,64,0,2,144,160,0,8,0,0,20,0,64,1,72,1,64,32,0,0,0,0,2,130,0,1,65,0,5,0,0,66,130,0,0,40,0,0,0,0,0,0,0,0,0,4,41,0,0,0,0,0,0,8,80,0,4,0,0,82,20,
208,0,0,0,0,0,0,0,0,0,41,10,0,32,0,0,0,0,2,130,2,2,128,80,64,80,0,0,0,1,8,1,74,0,0,0,64,80,0,41,8,1,0,5,0,0,64,1,0,4,0,2,128,0,40,0,0,66,128,0,0,0,0,0,133,0,0,0,40,33,64,0,0,0,0,128,0,0,0,128,0,0,0,0,
0,0,164,5,32,0,20,128,160,0,1,0,0,0,10,66,144,2,128,64,0,0,0,8,80,0,4,40,0,0,0,0,0,80,0,32,40,32,5,32,0,0,0,4,40,0,0,0,0,0,0,0,0,0,40,33,72,0,0,20,16,0,10,0,0,0,66,128,0,4,0,0,0,0,0,0,0,0,0,33,65,0,40,
0,0,64,1,65,72,13,0,0,0,0,0,0,0,0,0,2,144,20,16,0,0,0,0,1,74,64,64,0,0,0,80,0,0,0,8,8,82,144,164,0,165,32,4,0,2,128,64,0,0,16,16,0,8,1,65,10,0,5,5,0,0,0,0,128,0,0,0,2,128,1,0,40,0,16,160,0,0,0,16,164,
0,0,82,0,64,0,0,0,0,5,0,128,160,0,8,82,2,144,164,0,160,16,164,0,0,0,0,2,144,160,0,1,0,0,0,8,10,66,128,64,0,5,0,0,0,5,0,128,160,2,0,0,0,133,33,65,0,40,0,0,82,20,16,160,0,8,1,64,0,0,0,0,0,8,0,0,0,0,0,0,
0,0,2,20,0,0,0,0,0,0,164,0,0,0,41,0,40,32,4,40,0,160,132,54,10,64,0,0,0,8,80,0,0,133,0,0,82,16,160,0,0,4,5,5,0,2,0,64,80,0,40,0,0,0,33,65,0,0,133,41,72,8,0,40,4,0,128,2,2,20,16,0,0,5,40,41,0,41,64,0,2,
0,8,0,0,0,80,0,4,40,4,41,10,1,1,72,0,0,160,16,0,8,80,1,72,0,32,0,20,16,0,1,64,0,0,8,80,1,0,40,32,0,0,0,0,0,0,0,2,144,0,0,0,20,16,16,0,1,65,10,0,0,20,128,2,144,0,0,0,0,0,0,0,0,0,0,0,5,0,0,0,0,0,1,10,64,
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,10,1,0,0,20,2,0,10,8,10,64,0,40,0,132,40,52,0,0,133,0,0,64,82,2,128,0,0,0,0,0,164,0,0,0,5,0,2,0,8,80,80,0,0,0,1,0,40,33,64,0,2,0,8,80,8,64,0,0,16,26,41,146,0,83,70,
64,40,5,5,0,16,128,133,32,0,20,0,80,0,0,0,66,128,1,0,0,0,0,32,0,20,128,160,133,0,0,0,0,0,0,32,0,160,0,8,0,0,160,16,0,82,0,0,0,0,82,0,0,0,0,0,0,0,0,0,2,0,0,5,0,0,10,8,0,0,0,0,0,0,0,0,20,128,0,1,72,1,64,
0,0,0,0,0,64,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,64,0,128,0,0,0,0,10,0,32,0,20,208,50,100,160,128,216,0,16,2,144,20,16,0,0,0,20,2,20,0,0,0,0,0,0,0,0,5,4,5,0,128,20,0,0,0,0,0,0,16,0,0,32,33,0,41,0,5,
4,41,162,3,37,0,209,144,80,8,80,1,0,5,33,64,0,128,20,0,10,0,32,5,33,72,0,32,0,0,0,0,0,80,0,0,0,0,0,0,0,0,16,164,40,0,0,64,1,72,0,0,0,0,0,2,128,1,0,0,0,0,5,32,0,160,128,0,1,0,0,0,82,2,128,0,0,0,0,0,0,0,
0,0,1,72,0,41,1,72,80,66,128,0,32,0,0,0,0,0,0,0,0,0,0,0,0,0,20,2,0,1,64,33,65,10,64,0,40,32,0,0,0,0,0,0,40,4,4,5,5,52,12,16,0,13,128,8,0,0,0,0,5,4,5,33,64,0,0,0,0,0,8,80,0,5,32,0,0,10,64,10,0,0,0,0,0,
128,0,1,1,1,0,0,2,130,20,133,0,0,10,8,1,64,0,0,0,0,0,0,4,0,20,16,0,80,1,0,0,0,1,64,0,128,20,0,0,0,0,0,0,0,0,0,128,160,16,20,128,0,0,0,0,0,33,64,0,0,1,64,0,133,0,0,0,4,5,0,0,8,0,0,0,0,0,133,0,0,0,0,0,0,
0,0,0,5,4,0,0,10,0,0,16,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,2,144,20,0,8,80,8,0,0,0,0,0,0,0,0,0,1,10,0,41,1,72,0,40,0,132,0,164,54,1,0,0,0,1,64,0,0,1,10,0,0,0,80,64,0,0,0,0,0,0,0,5,0,128,0,0,0,2,0,64,64,0,0,
0,10,1,10,0,0,160,0,0,0,0,0,0,0,0,0,0,1,10,64,0,0,0,82,0,10,8,82,2,128,0,0,0,0,0,0,0,5,32,0,0,8,82,2,130,0,0,0,0,0,0,0,0,40,32,0,0,0,0,20,0,0,0,0,8,82,0,0,0,0,0,0,0,0,0,0,0,0,0,82,0,80,0,0,2,0,0,0,0,0,
33,64,0,0,0,0,20,128,0,0,0,0,0,0,20,0,1,1,72,80,64,0,0,0,0,0,2,20,0,0,0,0,0,0,0,10,10,80,64,1,146,130,2,20,16,232,64,8,10,64,80,0,0,0,64,80,8,80,66,128,0,0,0,0,0,2,144,0,0,0,0,8,0,32,0,0,8,1,65,10,64,
80,0,0,0,10,64,10,1,0,40,0,2,0,0,0,0,80,1,10,8,0,0,0,0,0,0,10,8,10,0,4,0,20,0,0,0,0,0,41,0,0,0,0,0,16,2,128,0,4,0,0,0,0,2,128,8,80,64,80,8,10,64,0,0,0,0,0,0,0,0,0,0,0,0,10,64,0,41,0,0,0,1,72,10,64,1,64,
4,0,0,66,128,0,0,20,0,64,0,40,0,2,0,80,8,10,8,0,0,0,0,0,0,0,0,0,0,0,0,0,0,133,0,0,0,0,0,10,8,0,5,41,160,8,8,100,20,16,20,132,58,16,0,0,0,0,1,1,64,0,2,20,0,0,0,0,8,80,1,65,0,0,0,83,32,128,0,64,10,80,66,
0,82,20,0,0,0,20,0,64,0,41,10,66,130,0,0,0,2,130,0,0,0,160,2,20,128,2,144,20,128,0,1,64,0,0,0,0,0,0,0,0,0,0,0,1,0,5,0,0,0,0,16,160,0,0,33,65,1,72,1,72,82,2,144,2,144,0,1,64,0,0,8,0,40,0,128,0,0,0,0,0,
0,0,0,5,32,0,0,0,0,0,0,0,0,0,33,72,80,0,0,0,10,64,0,0,160,2,0,0,40,0,2,0,0,32,0,160,0,66,128,0,0,0,66,128,0,0,0,0,0,164,0,0,1,72,80,8,1,74,104,0,66,16,133,4,0,128,217,72,0,0,0,0,0,0,0,4,0,160,0,0,0,160,
133,0,0,8,1,1,0,0,16,2,128,10,1,0,0,20,0,8,0,0,160,128,20,16,0,0,0,2,128,64,80,8,82,2,144,0,80,0,0,0,0,0,0,0,0,133,0,0,0,0,16,160,16,160,0,1,0,5,0,128,160,0,0,0,0,0,0,128,0,0,0,164,5,33,64,41,8,10,64,
10,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,5,32,0,0,0,0,133,0,0,0,0,0,0,41,10,0,32,0,0,0,0,0,0,0,0,0,0,0,64,80,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,40,0,128,2,130,148,160,3,36,4,41,0,4,54,82,0,0,0,0,0,0,0,0,0,0,
80,0,0,0,64,1,1,0,4,5,33,64,0,20,0,0,4,0,0,80,64,0,0,160,128,0,0,0,160,0,8,82,0,80,66,128,1,10,0,0,16,165,32,0,0,0,0,0,0,0,0,8,82,0,82,20,128,0,80,0,0,0,0,0,0,0,0,16,164,0,160,128,0,80,8,80,0,0,0,1,72,
1,0,0,0,0,0,164,0,2,144,0,10,64,0,0,0,0,0,0,0,0,0,0,32,0,20,0,0,0,2,144,0,0,0,20,16,164,0,0,0,0,0,0,0,0,64,10,1,0,0,160,0,0,0,0,0,0,0,0,0,0,0,4,40,5,0,0,1,0,0,0,66,148,26,40,0,132,6,65,10,64,83,96,128,
0,0,0,0,10,0,4,0,0,0,0,0,64,0,32,32,0,0,80,0,40,0,133,32,0,2,144,0,0,0,0,0,0,0,0,0,0,80,66,130,20,0,66,128,10,64,0,0,0,8,80,0,0,0,8,10,0,4,40,0,16,20,128,0,0,41,10,66,144,0,80,0,0,0,1,0,40,0,133,0,16,
0,10,0,0,0,0,4,40,32,33,65,10,10,64,10,64,80,8,10,64,0,0,0,0,0,0,0,0,128,2,144,160,0,0,0,0,0,0,0,0,0,0,0,41,0,40,32,0,0,0,0,0,0,0,0,0,0,0,0,0,160,16,0,0,0,0,0,4,40,0,160,0,64,0,0,2,128,0,4,0,0,8,1,65,
163,64,20,128,25,50,8,1,78,128,128,128,0,0,40,0,0,64,0,0,2,2,0,0,0,2,0,80,0,5,33,64,0,0,0,32,0,0,0,0,0,0,0,0,0,5,33,64,33,64,0,0,0,0,2,144,0,0,4,5,4,5,4,0,20,2,0,0,0,164,0,20,2,20,133,33,72,1,65,0,5,0,
0,0,0,0,0,0,0,0,4,0,2,144,160,0,1,10,8,0,0,0,0,0,164,0,0,1,64,0,16,0,0,5,32,0,16,20,128,20,0,0,0,20,133,32,0,0,0,5,32,0,2,128,64,0,0,0,1,64,0,128,0,0,0,0,0,0,0,0,0,0,0,0,0,80,8,0,40,32,0,133,0,0,1,72,
0,0,160,128,0,64,0,0,160,0,82,128,82,148,25,48,8,1,78,128,16,16,160,0,0,4,0,128,0,1,0,32,5,4,40,41,72,8,66,148,2,2,128,0,33,65,0,0,0,0,0,0,0,0,2,128,64,80,0,0,0,0,0,0,0,0,0,0,0,0,64,0,0,0,80,8,0,0,0,0,
0,20,0,1,0,5,0,0,0,0,0,0,0,0,1,10,0,0,2,0,0,0,0,0,0,2,144,160,128,0,80,66,128,0,0,0,0,4,0,0,0,0,128,2,128,82,0,0,0,0,0,5,4,5,32,0,0,82,0,0,0,0,0,0,0,0,0,0,82,0,0,0,2,128,64,80,8,1,0,0,2,128,1,72,0,0,0,
0,0,0,8,80,64,82,0,1,64,32,0,0,80,8,10,0,5,32,0,166,193,146,16,128,26,54,64,64,80,0,32,0,128,16,0,64,0,0,160,133,33,64,5,32,0,160,16,0,0,0,0,0,0,0,10,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,64,0,0,0,0,0,0,0,0,0,
0,0,160,2,0,0,41,1,64,0,0,0,0,0,0,0,2,144,0,0,0,16,160,128,0,0,0,0,0,0,2,128,0,0,0,0,0,0,0,32,5,32,0,16,20,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,2,130,2,144,160,0,8,0,5,0,2,0,0,0,0,0,0,0,0,4,
40,4,40,0,0,66,128,0,0,128,164,40,0,164,0,0,0,0,0,0,41,0,5,33,77,16,0,0,32,0,128,16,20,166,65,65,10,0,0,133,0,160,0,1,0,0,20,128,0,10,1,10,0,0,0,0,0,0,0,0,2,144,0,0,32,0,160,133,0,128,0,0,0,20,0,66,130,
20,2,2,128,0,0,0,66,128,8,0,40,0,0,0,4,0,160,20,16,0,0,0,128,160,2,0,0,0,0,0,0,0,0,0,20,2,20,0,0,0,133,32,0,0,0,0,0,0,0,0,0,0,0,0,0,2,144,2,144,0,0,0,0,8,1,64,0,0,0,0,0,0,41,10,0,0,0,66,128,0,0,0,0,0,
16,160,128,0,0,0,2,20,0,0,0,0,0,32,5,0,0,8,80,0,0,0,0,0,2,144,20,128,0,0,0,20,2,2,154,50,10,1,0,32,0,0,0,0,0,82,0,0,40,0,0,0,33,65,0,0,20,16,0,10,0,41,0,0,0,0,0,0,0,0,0,0,0,0,64,0,0,0,0,0,2,128,0,0,0,
0,0,16,20,133,0,0,64,80,64,10,0,0,0,0,33,64,4,5,32,0,0,0,0,20,133,32,0,164,0,160,16,20,16,2,128,0,32,0,20,16,20,2,2,144,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,20,16,0,0,41,1,64,0,0,8,80,66,128,
0,33,72,0,0,0,64,0,40,0,128,160,0,1,1,72,0,40,0,0,64,80,0,0,0,10,8,0,32,5,4,0,164,5,5,32,32,40,4,41,72,8,83,68,32,52,66,2,20,2,20,2,130,0,80,64,0,5,0,0,0,0,128,0,1,64,0,16,20,0,0,0,0,0,0,160,128,0,64,
80,66,128,8,80,64,0,5,0,0,64,0,0,160,0,0,0,0,0,0,16,0,82,0,0,5,0,133,0,0,1,1,72,0,0,0,0,0,0,82,2,144,0,0,40,0,0,0,0,0,0,0,0,0,0,128,0,0,0,0,104,200,0,20,128,0,0,0,0,0,0,0,0,33,72,80,0,0,20,128,0,0,5,0,
0,0,0,16,20,0,0,0,0,0,0,128,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,64,33,64,0,0,0,0,164,0,2,144,164,4,40,32,0,164,5,4,5,40,32,0,160,16,20,25,6,129,144,80,8,1,65,72,0,41,0,5,4,0,0,0,0,20,16,2,130,0,10,0,0,164,0,
16,160,0,0,0,0,0,0,16,0,0,0,0,0,0,20,16,20,16,164,0,0,80,8,1,64,0,0,66,130,20,0,8,82,20,0,66,128,0,32,0,0,0,0,0,0,0,20,0,8,80,0,33,64,0,0,8,0,40,0,128,160,0,0,0,16,0,0,0,0,10,0,0,0,8,10,1,0,0,0,0,0,0,
0,0,0,80,1,0,0,20,16,2,128,8,82,2,144,164,0,160,0,0,0,0,8,0,0,2,144,0,1,72,0,0,0,0,0,0,10,64,0,0,0,0,5,32,0,0,0,0,2,0,80,0,4,0,164,40,0,0,66,148,165,33,10,64,0,0,25,40,4,0,160,0,10,8,0,0,0,0,0,0,1,64,
0,2,0,80,66,128,0,0,0,64,10,0,4,0,0,80,0,4,0,0,10,8,0,0,0,8,80,8,82,3,161,130,128,0,4,41,10,0,4,0,0,0,5,4,41,0,0,0,0,0,0,0,0,0,10,0,4,5,4,5,0,0,0,0,0,0,0,0,0,0,128,0,0,0,0,10,1,10,8,80,0,0,128,160,128,
0,0,0,0,0,0,0,0,0,0,0,40,0,16,20,128,164,0,0,82,20,0,0,4,5,0,0,0,32,41,0,0,20,16,0,0,0,0,0,5,0,2,0,0,0,0,0,0,0,0,0,0,0,32,5,33,72,10,0,0,0,64,80,0,0,0,0,41,74,66,128,64,1,0,32,0,128,160,0,0,0,0,0,0,2,
144,0,1,64,0,0,0,0,0,0,0,2,0,0,5,4,0,2,128,1,0,0,0,10,8,10,64,0,0,0,64,82,3,102,74,8,82,0,10,1,1,64,0,16,20,128,0,0,0,0,10,64,0,0,20,128,0,0,0,0,80,0,0,0,0,0,0,0,0,16,0,0,33,72,80,0,0,0,80,1,10,0,0,2,
20,16,0,0,0,0,0,0,0,0,0,0,10,64,80,0,0,2,0,10,8,80,1,0,0,2,128,64,0,0,2,128,8,1,72,10,0,0,133,4,5,4,0,2,128,0,0,128,0,1,10,0,0,133,4,41,1,65,0,0,0,1,72,0,0,160,0,0,0,2,128,8,0,41,72,82,130,128,64,10,67,
32,128,128,160,0,0,0,0,0,0,0,0,0,2,128,0,32,0,0,80,0,4,0,20,128,2,130,0,10,64,10,8,10,82,0,8,10,8,0,0,0,0,0,2,0,10,0,0,0,0,0,0,0,0,0,1,64,33,65,0,0,0,0,0,0,82,20,128,160,0,8,1,64,0,16,0,0,0,0,0,0,0,0,
0,0,10,0,0,0,64,1,64,0,133,0,128,0,0,0,0,0,5,33,64,0,16,160,0,64,80,8,10,0,33,65,10,0,4,40,32,0,0,0,0,0,0,41,0,5,0,0,0,0,0,0,33,65,0,0,2,20,16,0,82,0,10,0,4,0,20,0,8,80,0,0,0,0,0,0,0,4,0,0,80,80,8,1,64,
0,20,209,1,8,83,70,76,128,1,0,40,0,0,0,0,0,0,5,0,16,160,0,0,4,0,0,0,40,32,0,160,0,0,4,0,160,0,0,33,72,0,0,160,2,20,128,0,0,41,0,0,128,20,0,80,83,32,2,144,2,130,2,128,0,0,0,0,0,2,2,144,2,144,160,0,66,144,
0,80,1,0,0,0,0,5,32,0,0,0,40,0,128,0,1,72,82,20,0,8,0,41,1,72,0,0,0,0,40,0,0,0,33,64,0,0,0,4,41,1,72,80,0,4,0,2,144,0,0,41,0,5,4,0,0,1,64,0,0,0,0,0,0,4,40,4,0,0,0,0,2,20,0,0,32,5,0,0,0,0,0,0,5,4,0,20,
2,0,0,0,0,0,0,133,0,0,80,0,32,5,52,1,8,0,4,4,40,4,40,0,0,10,0,0,2,0,80,0,33,64,0,128,160,128,20,0,0,0,0,1,64,32,0,0,0,4,5,4,0,20,0,13,152,0,16,160,133,0,128,2,0,80,0,0,0,10,1,10,0,33,64,0,0,0,0,16,20,
16,0,10,0,0,0,8,80,8,0,0,0,0,0,0,10,64,0,0,0,80,64,1,72,1,65,10,0,0,128,0,0,0,0,10,0,0,2,128,64,0,0,16,0,0,40,0,16,0,0,0,2,130,0,10,0,0,0,1,10,64,0,40,0,0,0,0,0,64,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,80,0,4,
40,32,5,4,0,0,0,0,0,0,0,0,0,0,133,0,0,0,40,6,129,146,0,64,0,40,32,40,0,20,128,20,0,0,4,0,0,0,0,160,0,80,8,0,0,2,144,0,10,8,0,0,0,0,0,0,0,0,0,1,0,0,20,133,32,0,2,144,0,80,0,4,0,2,144,20,0,1,0,0,160,0,64,
80,0,4,41,0,0,0,0,0,0,0,0,2,144,0,0,41,1,72,80,0,4,0,2,144,0,0,0,0,0,0,0,80,0,0,0,10,64,0,0,0,0,0,0,64,0,0,0,0,0,20,128,160,0,0,0,0,0,0,16,160,0,0,0,0,64,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,72,0,0,0,
0,0,0,0,0,133,0,0,0,0,0,8,80,64,10,0,0,20,128,128,0,0,0,3,68,0,0,0,41,0,5,32,0,0,1,64,0,0,0,0,0,0,0,0,10,0,4,0,0,64,0,0,160,2,0,80,64,0,0,160,128,0,0,41,0,5,0,16,0,10,0,4,0,2,130,0,0,0,0,0,5,32,0,0,1,
65,0,0,20,0,66,130,20,0,0,0,0,8,0,40,4,0,0,0,0,0,0,0,0,80,66,128,0,0,160,2,0,0,40,4,32,41,0,0,0,0,40,33,64,0,0,0,0,0,0,0,2,130,0,64,80,66,144,0,0,0,0,0,0,2,128,8,0,0,164,0,0,0,0,20,0,64,0,0,0,80,0,0,16,
0,10,8,10,64,0,5,33,65,8,0,0,20,2,0,10,66,128,66,128,0,0,128,0,0,0,160,128,2,128,0,0,0,1,64,32,40,0,128,160,0,8,80,64,0,0,160,0,0,0,0,82,2,144,2,20,0,64,0,5,32,0,0,10,8,0,0,0,0,0,0,1,72,0,0,0,0,0,160,
2,0,1,72,0,0,0,0,0,0,10,64,0,5,0,0,0,0,0,66,130,0,0,0,0,1,72,0,0,164,0,0,0,5,0,16,20,0,0,0,160,128,20,2,0,8,0,0,0,10,66,128,64,82,20,0,0,0,0,1,0,0,0,10,8,0,4,40,0,20,2,0,0,0,0,0,0,160,0,0,0,0,0,32,5,0,
0,8,0,0,0,0,0,2,144,0,0,0,0,0,0,0,0,0,0,1,0,0,160,0,66,128,0,0,0,0,0,2,144,0,1,64,0,0,0,0,0,0,0,128,20,16,160,0,8,0,5,0,133,4,0,160,0,80,66,0,0,0,0,0,5,0,0,0,0,0,64,0,5,4,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
0,2,128,66,130,20,0,0,0,0,0,4,5,0,0,64,0,5,0,0,0,0,0,0,0,0,0,0,0,0,0,2,128,8,8,1,72,82,2,128,1,0,40,0,2,20,0,64,0,5,4,0,0,0,0,0,0,0,2,20,0,80,0,4,0,160,0,1,1,64,0,0,0,0,2,144,0,8,0,0,0,0,0,0,0,0,0,0,0,
0,8,10,0,0,128,20,0,0,0,0,10,1,0,0,0,0,0,0,80,0,4,0,20,16,20,0,0,0,0,0,0,16,164,0,20,0,0,0,128,0,80,0,0,128,20,128,0,0,0,0,80,0,4,40,0,0,0,0,133,4,41,10,0,4,5,32,0,0,1,72,0,0,160,128,160,128,0,10,0,33,
64,0,0,0,4,5,0,2,0,82,20,128,0,10,0,40,32,0,0,0,0,0,10,64,82,0,0,0,16,160,0,0,0,0,0,0,128,0,0,5,0,0,64,0,41,10,64,0,0,0,0,0,0,66,128,0,0,0,1,65,10,66,144,160,0,0,0,0,0,0,0,8,80,0,0,0,8,80,0,4,0,0,0,0,
0,0,0,0,0,0,0,66,144,160,0,64,80,0,0,0,0,5,32,0,20,16,0,0,0,0,10,66,128,0,4,40,33,65,10,64,80,0,32,5,0,0,0,32,0,2,128,0,0,128,0,0,0,0,0,0,0,0,0,0,80,0,0,0,8,82,0,82,0,0,0,0,1,72,0,0,0,1,72,1,72,80,0,4,
5,0,0,0,0,0,0,32,0,160,128,160,128,0,10,8,1,64,0,0,0,0,0,0,0,0,1,72,0,0,0,0,0,0,0,0,16,0,0,40,0,0,0,0,0,64,0,0,0,0,0,2,144,0,0,5,32,0,0,80,0,0,16,2,144,20,133,33,64,0,0,0,0,2,2,128,8,10,1,0,0,0,0,0,0,
0,4,5,0,0,0,0,0,10,64,0,0,0,0,5,0,128,0,1,65,0,0,0,0,0,0,0,0,2,20,0,10,64,82,20,128,160,0,0,0,0,0,0,0,0,32,41,0,0,0,0,0,0,0,0,0,80,66,144,0,80,64,80,8,80,0,4,5,0,16,0,0,0,0,0,0,2,128,0,0,16,160,0,0,32,
40,4,0,164,0,160,0,0,32,0,0,0,0,0,1,72,0,5,0,133,0,0,0,0,0,0,0,0,0,0,16,2,128,66,144,0,10,8,80,0,0,0,82,0,0,4,0,0,1,72,0,5,0,16,0,0,5,32,0,160,128,0,80,0,0,16,160,0,8,80,0,0,0,1,0,41,0,40,4,0,0,0,0,0,
0,0,0,0,0,2,20,128,20,0,0,0,0,0,0,0,0,0,164,40,0,128,0,0,0,16,160,0,66,128,0,0,128,0,1,65,10,82,153,32,52,0,0,0,0,0,0,64,0,0,0,0,0,0,0,0,0,0,5,0,128,164,5,4,5,4,0,0,82,0,0,41,10,8,0,0,20,16,2,130,0,10,
0,0,2,0,80,1,1,74,66,2,128,66,130,20,128,0,80,0,0,16,0,0,41,72,0,4,5,4,0,160,0,10,8,0,33,64,0,16,164,40,32,0,20,2,0,80,8,0,0,160,128,160,0,0,0,0,0,32,0,0,10,8,0,40,32,0,0,1,10,0,0,0,10,1,0,0,160,16,20,
2,0,80,0,4,0,0,0,0,160,0,0,4,0,0,0,0,0,0,0,0,0,0,0,0,0,0,82,0,0,0,0,0,40,32,0,0,0,4,0,2,144,0,80,0,0,0,64,0,40,0,16,20,0,67,68,40,0,2,20,128,0,0,0,133,33,64,0,0,0,0,0,10,0,4,5,0,2,0,0,0,160,128,0,0,0,
0,10,0,0,128,160,128,160,133,33,64,32,5,33,65,10,64,82,144,128,160,16,2,128,0,0,16,160,0,0,0,0,0,0,0,0,0,0,64,10,0,0,164,4,41,1,72,0,0,164,40,0,0,0,0,0,8,80,0,0,0,1,10,64,0,0,2,144,0,0,0,164,0,2,128,0,
0,0,64,0,0,160,0,0,32,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,82,20,128,0,0,0,0,0,0,0,0,0,0,0,32,0,160,0,1,0,40,0,16,0,0,0,164,0,0,0,40,0,0,0,0,0,0,0,0,0,0,2,128,64,80,0,0,0,64,1,65,0,0,2,
128,64,0,0,20,0,0,0,0,82,16,160,128,0,1,64,32,0,0,0,5,0,0,0,0,0,1,64,32,0,20,128,0,0,0,0,0,33,72,80,8,0,0,20,16,2,130,2,144,164,0,20,128,160,0,64,80,66,128,0,0,0,0,0,16,0,0,0,0,10,0,0,2,2,128,0,0,0,0,
0,20,128,133,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,20,128,160,0,0,0,0,64,0,0,0,0,0,0,0,0,0,0,0,2,0,1,64,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,66,128,0,0,0,0,0,0,10,8,1,64,4,40,0,2,2,128,64,0,0,0,0,0,160,
0,0,33,72,80,82,0,0,0,16,20,128,0,1,65,10,8,0,0,0,0,0,20,0,0,0,0,0,0,164,4,40,0,0,64,80,0,4,0,0,0,0,0,0,0,0,80,64,80,0,0,0,0,0,0,10,64,10,64,1,0,0,0,0,5,0,0,0,0,160,128,0,0,0,0,0,4,40,0,0,64,0,0,0,0,0,
2,130,20,128,0,0,0,0,0,0,2,128,64,0,0,20,133,0,0,0,0,2,20,2,0,0,4,5,0,16,160,0,64,80,8,80,0,0,2,20,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,82,20,128,0,67,68,5,0,128,0,0,40,0,0,64,1,65,0,40,32,0,0,0,0,0,1,65,10,
64,80,0,0,16,20,128,160,2,0,10,8,0,0,0,80,8,0,0,0,0,5,0,0,0,0,0,0,0,0,0,0,0,66,130,0,0,0,0,0,0,2,128,0,0,2,144,2,20,0,0,0,0,0,0,0,1,0,0,164,0,0,82,20,0,0,0,0,0,5,32,4,40,0,2,20,128,0,0,0,0,0,0,0,0,5,0,
0,1,1,72,0,0,164,40,0,0,0,0,0,66,128,0,0,0,0,0,128,2,144,0,1,10,1,0,41,1,64,0,2,2,128,66,128,0,0,0,0,5,32,0,0,0,0,0,0,0,0,0,5,0,2,20,128,164,40,4,0,0,10,0,0,0,66,130,0,0,0,0,0,0,0,0,0,0,0,5,0,0,0,0,2,
0,0,0,2,128,0,0,0,0,33,64,0,0,1,0,0,160,128,2,128,0,0,0,0,0,16,2,144,164,40,32,5,32,0,0,0,0,2,128,0,0,160,128,160,16,164,4,0,0,0,0,0,0,0,164,0,0,0,0,0,10,82,0,0,0,0,10,64,0,0,16,160,128,0,0,33,64,0,0,
80,0,4,0,20,16,164,0,160,0,1,0,0,0,10,66,128,0,0,0,1,72,64,1,64,32,0,0,0,0,0,0,0,0,0,33,64,0,16,160,0,0,0,0,0,0,0,0,0,0,0,5,0,0,64,80,0,0,128,0,0,0,20,2,0,64,1,64,0,160,0,0,4,0,0,0,0,0,0,0,0,1,72,10,0,
0,0,0,0,0,1,0,40,0,0,0,0,0,0,0,2,20,0,8,80,0,0,0,8,1,64,0,133,32,0,0,10,8,1,65,10,66,144,20,0,0,0,0,0,0,0,0,0,0,0,32,0,0,0,0,0,1,65,0,0,164,5,33,64,0,0,0,0,0,0,0,16,0,80,0,0,128,0,0,0,0,0,40,0,133,0,0,
8,0,0,160,128,160,133,0,16,0,0,0,0,10,64,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,41,0,0,0,80,0,0,0,0,0,0,0,0,0,8,10,1,1,64,32,5,0,0,8,0,40,0,128,160,0,64,0,0,0,0,0,0,10,66,130,20,0,0,0,0,1,0,0,0,0,
0,160,0,0,0,0,1,0,40,0,0,0,33,64,0,128,2,128,8,10,66,130,0,0,0,20,16,0,80,0,0,0,80,64,82,0,0,0,128,0,10,0,4,0,0,0,40,32,0,0,80,0,0,0,0,0,0,0,0,16,160,0,0,0,2,20,0,64,82,0,1,64,4,0,0,80,8,0,40,0,16,0,0,
0,0,80,64,10,64,0,0,0,10,8,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,5,0,128,0,0,0,164,41,10,0,5,4,0,0,1,10,0,32,5,4,0,0,8,10,64,80,8,80,0,0,160,2,2,144,0,10,0,0,0,0,0,0,0,0,0,64,0,0,0,80,1,1,
72,80,0,0,0,0,0,2,2,128,0,0,0,8,80,64,0,5,33,65,0,0,160,133,32,40,0,0,0,0,0,0,32,0,160,16,0,1,72,0,0,164,0,0,10,0,0,0,82,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,16,0,0,0,2,128,0,0,16,0,1,64,0,0,0,33,72,8,80,0,0,
0,0,0,0,0,0,2,144,0,0,0,0,0,0,0,0,0,0,0,0,0,0,41,0,0,0,0,0,0,0,0,20,0,64,0,41,0,0,0,66,128,0,0,20,16,20,2,0,0,4,0,20,0,1,0,0,0,10,66,128,66,144,3,64,0,0,0,0,0,0,2,2,130,0,0,41,10,8,1,72,10,0,0,0,0,0,0,
0,4,0,164,0,0,0,0,20,16,160,133,32,5,33,64,32,40,0,0,0,0,0,0,0,2,20,2,0,0,41,0,41,0,0,0,0,0,0,80,0,0,0,64,80,64,10,0,0,0,0,5,32,0,0,0,0,0,0,0,2,0,0,0,20,16,20,128,160,16,20,0,0,0,2,20,128,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,5,0,0,8,10,8,0,0,160,2,0,0,0,20,128,160,0,0,5,32,0,128,0,82,2,144,160,0,1,1,72,0,0,20,128,0,0,0,20,16,20,128,0,82,0,1,0,0,164,0,160,0,1,0,0,0,80,0,0,20,2,144,0,64,0,0,0,1,64,32,5,0,164,
0,0,1,10,0,0,0,0,0,16,164,0,0,0,0,0,0,0,2,144,0,1,64,0,0,0,0,0,0,0,20,132,5,33,72,0,0,2,144,0,0,0,0,0,0,20,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,128,2,130,0,10,0,32,5,0,0,0,0,0,0,0,0,64,0,0,0,0,0,0,0,0,
0,0,0,0,1,64,0,0,8,80,0,0,0,8,10,0,0,0,0,32,40,5,32,0,0,10,64,1,10,0,32,0,20,128,0,0,0,0,0,0,0,10,64,80,64,0,32,0,0,10,8,10,0,33,64,41,0,0,0,0,0,0,8,80,80,0,0,0,1,0,0,0,0,0,0,80,0,0,0,10,0,4,0,0,0,32,
40,4,0,20,128,0,0,0,20,133,4,40,0,16,20,0,0,0,0,0,5,32,0,2,0,0,0,0,0,0,0,82,20,133,0,0,0,0,0,0,0,16,0,10,1,0,0,20,0,64,10,10,12,128,0,0,0,82,0,80,0,33,64,0,0,0,0,0,0,0,0,0,0,0,8,0,0,0,0,5,33,72,10,0,0,
0,66,128,0,0,0,0,0,0,1,10,1,1,64,0,0,0,0,0,0,4,5,0,0,0,0,0,0,0,2,2,0,80,64,0,5,0,0,0,0,0,1,0,40,5,32,0,0,0,0,0,0,0,0,0,0,164,0,0,0,0,0,80,80,64,0,0,0,64,0,0,160,0,0,0,0,0,0,0,0,0,0,8,0,40,0,128,160,0,
8,10,64,1,65,0,5,0,16,20,0,0,0,16,0,0,0,0,0,0,0,10,66,144,160,0,0,0,0,0,0,0,0,0,0,0,4,0,0,0,0,0,0,0,2,128,64,10,64,0,0,2,128,0,0,0,1,64,32,0,0,0,0,0,8,10,1,0,40,4,0,2,144,2,128,0,0,0,0,0,0,0,0,0,0,0,133,
0,0,0,0,0,1,72,0,0,0,0,4,4,0,160,160,128,0,1,72,0,0,16,2,128,8,80,8,80,0,0,0,0,5,0,16,2,154,33,13,16,133,4,33,74,8,1,0,0,0,0,40,0,2,0,0,0,20,133,0,164,0,0,0,0,0,0,0,2,144,0,0,0,0,8,0,0,0,80,0,0,0,0,0,
128,160,2,144,2,2,130,0,0,0,0,80,8,10,0,32,0,0,1,64,0,0,0,0,0,0,0,0,0,0,0,0,0,0,8,0,0,0,0,0,0,10,64,0,5,0,0,1,10,0,0,0,1,72,0,0,16,160,0,0,0,0,1,10,64,10,66,128,0,0,0,8,0,0,20,0,0,5,4,0,0,0,0,0,64,80,
0,0,133,0,0,0,0,0,0,0,128,2,144,0,0,4,5,0,0,0,0,0,0,0,133,0,0,0,0,0,0,0,0,10,0,0,16,2,20,160,0,64,1,65,1,64,32,0,0,0,5,0,16,0,10,8,80,0,4,41,10,10,64,0,0,0,0,0,0,0,4,0,0,0,0,160,0,8,80,0,0,16,0,0,0,0,
10,64,82,20,128,0,1,72,10,66,144,160,2,2,128,0,0,0,0,0,0,0,0,0,0,0,0,0,0,2,20,128,0,0,0,0,0,0,20,0,0,0,0,66,128,1,72,0,5,32,0,0,0,0,0,0,0,2,20,2,0,0,0,2,128,0,0,0,0,0,0,1,64,0,0,0,0,16,2,20,16,20,16,0,
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,20,0,8,10,0,4,0,0,0,0,160,0,1,0,33,72,80,64,80,64,0,41,64,4,0,0,0,0,0,0,0,0,0,0,128,3,64,0,8,0,0,0,0,0,0,0,0,20,0,0,0,0,1,0,0,0,0,0,164,5,4,5,4,40,0,128,0,
0,40,4,5,0,0,0,0,0,0,0,0,0,0,0,0,32,40,0,0,64,82,20,128,20,128,20,128,0,10,0,0,2,20,0,0,0,0,0,0,164,32,0,160,0,66,128,0,0,16,20,16,160,133,33,64,33,72,10,0,0,0,0,0,0,0,0,0,0,40,0,0,8,0,0,16,20,128,0,0,
0,0,66,128,1,10,0,0,0,10,1,0,0,164,0,0,0,0,0,0,5,0,128,0,1,65,1,64,0,160,2,0,1,0,0,0,1,64,0,0,66,0,0,0,0,0,40,0,0,1,1,65,0,0,0,0,0,0,0,0,2,128,0,0,16,160,0,0,0,160,0,0,0,0,0,0,128,0,0,0,0,1,64,0,16,0,
80,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,128,0,0,40,0,0,0,0,2,20,0,0,0,0,0,32,0,0,80,66,128,0,4,40,40,4,32,0,0,0,0,20,16,0,82,0,82,20,0,0,33,64,0,0,0,0,0,0,5,4,0,20,2,0,64,0,0,20,0,8,0,0,0,0,5,32,0,0,80,0,
0,0,0,0,0,1,10,0,0,0,0,0,0,0,0,2,0,0,40,4,41,10,82,0,0,0,128,0,0,0,0,0,0,16,0,0,0,0,0,0,0,80,0,0,16,0,0,0,0,0,0,164,0,2,128,0,0,0,10,0,0,0,8,0,0,0,0,0,20,128,0,0,0,20,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,
0,0,20,128,0,0,0,164,40,33,64,0,0,0,0,0,0,0,0,0,0,2,0,0,0,0,0,0,0,0,40,0,2,0,82,0,0,0,20,0,0,0,0,0,5,0,133,0,2,20,16,164,40,4,0,16,160,2,0,0,0,0,0,0,2,128,8,80,0,0,0,0,0,0,0,0,160,128,164,40,0,128,16,
20,0,0,0,0,0,0,0,0,0,0,8,80,0,32,32,0,160,16,160,0,8,0,0,0,0,0,0,0,41,0,41,0,40,0,0,0,0,0,0,0,0,8,10,64,10,64,80,10,8,80,1,0,0,164,0,0,0,0,0,0,0,164,40,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,5,32,0,2,20,16,164,
0,160,0,66,128,8,10,8,80,0,0,0,0,0,0,0,0,0,0,0,133,0,0,64,0,0,0,0,0,20,0,64,82,2,130,0,0,0,0,80,64,80,0,0,160,16,20,2,20,0,0,0,128,0,0,0,160,128,128,0,0,0,0,1,72,80,0,0,0,0,0,0,82,0,82,0,0,0,0,0,0,0,8,
80,1,0,40,32,0,160,16,0,0,0,0,0,0,0,0,4,5,32,0,0,0,0,0,1,64,0,128,20,0,8,80,0,0,0,1,64,32,40,0,16,0,0,0,0,0,0,0,0,32,40,0,0,0,0,0,0,0,0,10,0,0,0,0,0,16,160,128,20,0,0,33,64,0,0,0,32,40,0,2,0,0,0,0,80,
0,0,0,66,128,66,128,8,80,0,0,0,0,0,164,0,0,0,4,5,0,16,0,1,65,10,64,0,0,0,1,64,4,40,32,40,0,0,0,0,0,0,0,0,0,0,0,66,148,133,32,0,0,0,0,133,0,16,0,0,0,0,0,5,33,64,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,80,64,0,0,
0,0,4,40,0,0,0,0,128,160,133,0,164,32,5,0,133,33,72,10,64,0,0,20,0,0,0,0,0,0,160,2,0,0,0,0,80,10,0,4,0,128,0,0,0,0,10,64,0,0,133,32,40,4,40,0,0,0,0,0,80,0,0,16,20,0,0,0,16,0,0,0,0,0,0,0,0,5,32,41,10,0,
0,2,20,0,0,0,0,0,0,0,0,0,0,0,0,16,20,0,0,0,16,160,16,160,2,2,144,20,16,160,0,66,144,20,0,0,0,0,0,0,0,0,0,0,0,0,20,16,20,128,0,1,1,65,0,40,32,0,0,0,0,0,0,5,0,0,0,0,0,0,0,133,33,64,0,0,0,0,128,0,0,0,0,1,
0,40,0,133,0,0,1,10,8,1,72,80,1,10,0,0,0,0,0,16,0,80,0,0,0,0,0,0,0,0,160,0,8,80,64,0,0,2,144,0,0,0,0,10,8,0,33,65,0,0,0,0,41,10,64,0,0,2,128,0,0,2,128,8,0,5,32,5,0,128,0,80,64,0,0,0,0,0,0,0,0,0,0,40,0,
0,0,0,0,0,0,0,0,4,40,0,0,0,0,133,0,0,8,0,5,0,0,66,144,160,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,1,64,0,0,0,41,0,0,0,0,0,0,0,0,16,0,0,0,0,1,10,0,0,2,20,0,0,0,0,0,5,32,0,0,0,0,
2,128,8,0,0,0,0,0,0,0,5,4,5,0,0,64,0,0,0,82,0,0,0,16,160,2,0,0,0,20,0,64,1,72,0,0,0,0,0,2,130,0,10,66,128,67,64,16,160,2,20,0,1,0,40,32,0,0,0,0,0,10,64,80,0,0,2,20,16,164,0,160,0,0,32,40,0,0,0,0,2,0,80,
0,0,133,0,0,8,10,8,0,41,10,8,0,0,160,0,0,0,0,0,0,0,0,0,0,0,0,2,20,0,0,0,0,0,0,0,0,0,0,0,0,2,0,0,0,0,1,64,0,0,0,0,0,0,0,0,1,10,8,10,0,0,0,8,0,0,0,0,0,0,0,0,20,133,0,128,20,133,32,5,32,0,20,2,0,0,41,0,0,
20,128,20,128,20,2,20,0,0,32,5,0,16,0,0,5,32,0,0,0,4,40,0,2,0,0,0,164,0,0,0,41,0,0,0,1,65,0,0,160,0,0,0,0,0,0,160,0,0,32,0,0,0,0,0,0,0,20,128,2,128,8,80,10,1,0,0,16,20,0,1,10,0,0,133,0,0,0,0,0,1,1,64,
4,0,0,1,72,10,8,1,65,10,66,128,0,0,2,144,0,82,0,0,5,32,0,0,0,4,5,0,128,20,0,0,4,40,0,0,8,82,20,128,20,128,0,0,40,4,5,0,0,1,72,0,40,0,0,0,4,4,0,160,0,0,0,128,0,0,0,0,0,0,0,1,65,10,0,0,0,0,0,2,0,0,41,0,
0,0,82,20,0,0,4,0,0,0,0,0,0,5,32,0,0,0,0,0,0,0,0,0,0,0,0,0,2,20,0,0,0,0,0,0,0,0,0,0,80,0,0,2,0,0,0,0,0,4,41,65,0,0,0,0,0,0,0,40,0,0,66,128,8,80,0,0,0,0,0,0,0,0,16,0,0,0,0,0,0,2,144,160,0,0,32,0,0,10,0,
0,0,0,0,0,0,0,0,82,0,0,0,0,82,0,8,80,0,0,133,0,128,20,0,64,0,0,0,0,0,0,10,1,10,64,10,0,0,0,10,0,4,0,20,0,1,0,40,4,0,128,160,128,0,0,0,0,0,0,0,0,40,4,40,0,0,10,8,0,4,41,10,0,32,0,0,0,40,32,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,0,33,64,0,133,0,0,64,10,0,0,0,0,0,2,128,64,0,5,4,0,160,0,8,0,0,20,2,0,0,5,32,0,0,0,0,0,10,64,80,0,0,2,2,128,0,0,16,160,0,8,0,0,0,0,40,4,0,0,0,5,32,0,0,82,2,128,0,0,0,0,4,0,20,16,20,0,10,
64,80,0,0,128,20,128,0,0,0,0,66,128,0,4,0,0,0,0,0,10,0,32,0,2,128,0,4,5,0,133,0,0,1,72,82,0,0,0,0,1,64,32,5,4,0,0,0,32,0,0,0,0,0,0,40,32,40,0,0,0,0,2,20,0,0,0,2,0,0,0,0,0,0,0,0,0,0,0,0,0,8,80,64,80,0,
4,0,160,2,0,0,40,0,0,0,0,0,10,64,0,0,2,128,64,0,0,0,0,0,0,0,0,2,128,8,0,0,128,2,144,0,10,0,32,40,32,0,3,64,0,0,0,0,0,0,2,0,0,0,20,0,64,0,0,0,1,64,0,16,0,80,64,80,1,10,0,0,16,160,0,0,0,0,0,0,0,0,0,0,66,
128,0,4,0,0,80,0,0,0,0,4,41,10,66,144,0,0,0,2,144,0,10,0,0,0,10,64,0,0,0,0,0,0,0,0,133,0,0,0,33,72,10,64,0,0,0,0,0,2,128,0,0,0,0,0,0,0,0,128,0,0,0,0,0,0,2,144,0,1,72,10,1,0,0,2,130,0,0,5,4,0,0,1,64,32,
0,0,0,0,0,66,128,0,0,20,0,0,0,16,0,0,0,0,10,0,4,0,0,0,0,164,0,0,0,0,0,1,0,0,0,0,0,0,0,0,3,64,0,1,0,0,0,0,0,0,0,4,41,64,4,0,0,10,0,0,0,0,0,0,1,72,10,0,0,128,0,10,64,1,65,0,0,2,2,128,66,128,66,144,0,0,0,
0,0,0,2,128,0,0,16,160,133,0,0,0,0,2,20,0,0,4,5,0,0,66,128,0,0,0,8,0,0,0,0,0,0,10,64,1,64,0,0,0,0,0,0,0,0,0,0,0,0,0,0,8,0,0,0,0,0,0,0,5,0,0,82,2,144,164,0,20,16,0,1,0,0,0,80,0,4,5,0,0,0,41,0,0,0,1,0,0,
160,128,160,0,0,0,2,0,0,0,0,0,0,0,0,0,16,164,0,0,0,0,164,0,160,128,0,0,0,0,0,40,32,0,2,128,0,0,0,0,0,0,1,72,0,0,160,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,20,2,0,1,65,0,5,32,4,40,4,0,0,0,41,0,0,160,0,0,4,0,20,
0,0,0,0,0,0,0,0,0,0,0,0,0,64,10,64,0,0,0,0,0,2,128,0,0,0,8,82,2,128,0,0,0,10,0,33,72,0,0,0,0,0,2,0,0,0,0,0,5,0,0,1,10,66,130,20,20,0,8,0,0,0,80,10,8,64,10,64,1,65,0,33,64,0,0,0,0,0,0,0,0,0,0,0,64,10,8,
0,0,0,0,0,0,1,1,64,4,0,0,0,0,20,128,0,0,0,0,0,0,0,10,64,0,0,20,2,2,130,2,128,0,0,0,80,0,4,0,160,16,2,128,0,0,20,128,0,0,0,0,1,72,1,65,0,0,0,0,0,0,0,33,65,10,0,0,2,20,2,0,0,5,0,0,0,0,0,66,128,82,0,1,72,
0,40,0,16,0,0,0,164,0,20,128,16,160,16,20,128,160,133,4,40,4,40,32,0,20,128,160,0,8,80,0,33,64,0,0,1,65,1,65,0,0,0,0,32,0,0,1,72,10,64,0,0,164,0,0,82,144,2,130,20,2,20,128,128,20,160,2,144,0,0,32,40,4,
0,0,8,1,64,0,128,2,128,8,82,0,0,0,0,0,0,0,0,0,0,0,0,128,0,0,5,32,41,1,72,0,0,0,0,0,0,0,0,0,0,5,33,64,4,40,0,0,0,32,0,3,64,0,0,32,40,0,2,20,0,0,0,16,160,0,1,65,10,64,0,0,0,0,0,0,0,0,2,2,144,0,0,0,0,1,72,
80,1,0,0,20,128,160,0,0,40,0,16,0,0,33,65,0,41,10,64,10,0,0,0,0,4,0,0,0,0,0,1,72,10,0,0,0,0,0,0,66,130,0,1,72,80,0,0,0,0,0,0,0,0,16,164,0,160,2,0,82,20,0,0,5,0,133,4,4,0,160,2,20,20,2,20,2,0,0,0,160,128,
0,66,128,1,10,64,0,0,0,10,8,0,0,0,0,0,0,64,80,0,0,0,0,0,0,66,128,0,4,0,20,16,20,128,164,0,0,80,64,1,72,10,0,0,0,8,80,0,0,0,0,0,0,0,0,2,0,10,67,64,0,0,0,0,0,0,0,1,1,64,33,64,0,128,164,5,0,0,0,0,133,0,16,
0,82,0,0,0,160,0,0,0,16,20,0,0,0,0,0,0,0,0,5,0,2,0,0,0,0,1,0,0,164,0,2,130,0,80,64,80,0,0,0,0,32,40,0,16,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,40,0,0,0,0,0,80,0,5,0,0,1,0,0,2,2,0,0,0,2,128,0,4,0,0,0,40,32,
5,32,0,0,10,64,0,5,32,0,0,0,40,4,0,0,0,0,0,0,0,0,0,0,0,0,33,64,0,0,0,0,0,64,80,0,0,0,0,0,20,2,0,0,40,4,0,0,0,32,0,2,144,3,64,16,0,0,0,164,0,0,0,0,0,0,0,0,0,0,0,0,0,20,0,8,80,0,0,2,20,0,0,0,0,1,0,40,0,
0,0,0,0,0,0,128,2,144,2,128,64,0,0,0,10,66,144,0,1,64,0,0,8,10,0,4,0,0,0,0,0,80,64,10,8,1,72,0,0,0,1,65,0,0,2,128,0,0,0,0,0,0,0,0,0,0,0,2,130,130,0,1,0,0,0,64,0,4,5,33,65,1,64,0,0,0,0,0,0,0,0,0,5,0,0,
64,80,64,0,0,0,0,0,160,16,164,5,33,64,33,72,10,1,0,0,0,0,0,0,0,0,0,0,0,0,80,8,0,0,0,0,0,0,0,41,0,33,72,0,0,0,10,8,0,40,0,0,0,0,0,0,0,0,80,64,1,64,0,0,0,0,133,0,0,1,10,1,10,0,0,0,66,128,0,4,0,160,128,160,
133,0,0,0,32,40,0,128,0,0,0,0,0,0,20,0,0,0,0,0,0,0,64,0,0,0,0,0,0,0,0,0,80,0,32,0,0,80,0,0,133,0,0,1,72,80,64,1,72,0,0,0,0,0,0,0,0,2,20,16,0,0,0,0,0,0,0,8,80,0,0,0,0,0,0,0,0,0,0,0,160,128,160,128,0,80,
8,80,0,0,0,0,4,0,0,0,40,32,41,10,0,5,4,41,1,1,64,32,0,0,0,0,20,16,0,1,64,4,0,0,64,0,0,0,0,5,4,0,0,0,0,0,1,65,1,64,0,0,10,1,1,64,0,0,66,128,0,0,0,0,0,0,8,80,0,0,0,0,0,0,8,0,0,0,0,0,0,0,0,160,128,0,0,0,
164,0,0,0,5,4,5,0,0,66,128,66,128,0,0,2,0,0,0,0,0,0,0,0,0,0,0,41,0,40,33,64,0,0,0,0,0,1,64,0,16,0,0,0,0,0,0,0,0,0,0,8,0,0,20,128,0,0,0,0,0,0,0,0,0,2,128,64,1,65,0,5,32,5,0,0,8,1,72,0,0,164,40,0,0,0,0,
0,0,32,0,0,10,0,0,2,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,1,72,0,5,0,0,66,144,0,0,0,0,10,0,0,160,0,0,0,0,0,0,0,0,0,0,0,0,133,0,0,0,0,0,0,0,16,0,0,0,0,0,0,160,128,20,128,160,16,20,16,0,10,0,0,2,20,128,0,0,
5,0,16,209,0,0,2,20,128,0,0,0,20,128,0,0,40,33,72,0,0,0,0,0,0,80,8,80,0,0,0,0,5,4,4,0,0,0,40,4,40,0,16,0,10,64,1,72,0,0,0,0,5,32,0,20,0,0,0,2,0,0,0,160,0,8,80,1,0,5,32,40,32,0,0,0,0,164,0,0,0,41,0,0,0,
0,0,0,64,10,0,0,0,0,0,0,0,0,128,160,133,4,0,0,0,41,0,0,2,144,164,0,164,0,20,16,2,144,0,0,0,0,0,0,160,0,0,0,160,128,20,133,0,2,2,128,0,0,0,8,0,5,4,0,0,0,0,0,0,5,4,0,0,1,65,0,40,0,0,0,0,0,1,10,8,82,0,1,
65,0,0,0,0,0,0,0,0,0,0,0,0,0,41,0,0,20,128,0,82,2,130,0,0,0,0,0,0,0,82,0,0,5,0,128,160,128,20,16,0,0,0,2,144,0,0,0,20,133,0,0,8,1,72,10,0,0,0,0,0,0,0,0,16,164,0,2,130,2,128,8,1,64,32,0,16,160,0,0,32,5,
0,160,128,0,8,80,1,10,0,0,0,66,128,0,0,0,0,0,0,66,144,160,2,0,0,0,20,128,0,1,65,1,72,0,0,164,0,0,82,0,0,0,0,82,2,144,164,0,2,128,0,0,0,0,0,0,0,0,0,10,64,10,8,0,0,0,0,0,0,0,0,0,0,40,4,0,160,0,0,0,0,0,0,
0,1,1,64,0,2,0,0,0,0,0,0,0,0,0,164,0,0,0,0,20,16,0,1,72,0,0,160,128,0,0,0,0,0,0,0,8,80,66,128,0,40,32,0,0,0,0,164,5,0,0,1,0,0,160,0,0,0,0,0,4,5,0,0,0,0,0,0,0,0,8,82,144,0,0,32,0,0,0,0,0,0,0,0,0,0,0,1,
64,33,72,82,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,2,128,0,0,16,20,2,130,0,64,1,64,0,0,0,32,40,0,0,0,5,4,0,160,0,0,32,41,10,0,0,128,160,0,64,80,0,0,0,0,0,0,0,0,0,66,144,0,0,0,0,0,0,0,
0,0,2,128,64,0,40,0,133,0,2,20,0,0,0,0,66,144,0,0,0,0,0,0,0,0,0,0,0,0,0,80,1,0,5,32,0,0,0,0,0,0,5,4,0,20,0,0,0,0,0,0,164,0,160,133,0,128,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,64,40,0,128,20,16,0,0,
4,0,2,144,0,1,72,0,5,0,0,0,0,0,0,4,41,0,0,0,0,0,0,0,0,0,0,32,5,0,16,20,16,160,0,0,0,128,20,128,0,0,0,3,64,0,0,32,40,4,0,0,80,1,1,64,0,0,0,0,0,0,0,128,0,0,0,0,82,0,0,0,164,0,0,0,0,2,128,0,0,0,1,1,64,0,
0,1,1,64,0,0,8,10,0,4,0,0,0,0,0,10,64,0,0,20,0,64,1,162,152,0,2,128,0,0,2,20,16,0,0,0,0,0,0,0,82,20,0,0,0,0,0,4,5,0,0,8,0,0,164,0,0,0,41,0,5,0,128,160,16,0,0,0,0,0,0,0,10,0,0,164,40,0,0,66,0,80,0,0,0,
0,0,2,2,130,20,0,0,0,20,2,0,0,0,2,20,16,2,128,1,1,10,0,33,64,0,0,0,4,0,0,0,0,0,80,1,0,0,0,0,0,0,0,0,160,2,144,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,64,0,0,10,1,0,40,0,0,0,0,0,8,10,0,0,0,0,32,0,20,128,0,
0,0,0,82,0,0,0,0,0,40,32,5,0,0,10,64,10,64,10,0,0,166,64,0,133,5,0,16,164,0,128,0,0,41,0,0,160,2,0,10,0,33,64,4,0,0,0,0,16,20,133,0,0,0,41,1,64,0,0,1,0,0,0,10,1,10,66,144,160,128,0,82,2,130,0,0,0,0,8,
80,80,64,0,0,160,0,0,32,0,0,0,0,160,0,64,0,0,0,0,0,0,0,0,2,144,16,0,82,0,80,0,0,2,0,0,0,0,0,0,160,2,0,0,0,0,0,0,0,10,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,2,144,0,10,0,0,0,66,128,1,0,41,0,0,0,82,0,1,72,0,
0,0,0,0,0,0,0,0,0,0,0,0,0,0,10,82,0,1,72,0,0,164,4,0,0,80,0,0,0,0,33,64,40,33,72,64,0,0,164,0,0,0,41,0,0,0,1,72,80,64,0,0,164,0,0,0,0,0,0,0,20,0,8,1,64,0,0,10,8,8,10,82,0,1,0,0,0,80,8,1,1,64,32,0,20,128,
20,128,2,148,128,2,128,64,0,0,0,0,0,0,0,0,2,0,0,5,32,40,4,5,4,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,41,10,1,0,0,2,128,0,40,32,40,4,41,1,64,4,0,0,1,72,10,0,4,40,0,2,2,128,1,1,64,0,0,8,0,0,0,0,0,0,0,0,0,82,20,133,
0,0,0,32,0,0,0,0,2,130,2,130,0,10,1,1,64,0,20,132,0,2,128,0,0,0,1,1,72,1,72,80,66,130,20,0,8,80,8,0,5,0,0,0,0,128,0,0,41,10,64,10,64,80,0,0,16,0,10,64,80,0,4,5,33,64,33,64,0,0,0,5,4,0,0,10,8,0,0,133,33,
64,32,0,0,0,0,0,0,33,64,0,2,20,0,0,0,0,0,0,0,0,0,16,0,0,0,0,1,64,0,16,0,0,0,0,0,0,0,10,64,0,0,0,0,0,20,0,0,0,0,0,0,0,10,0,0,0,0,0,0,0,0,2,20,0,0,0,0,0,0,0,0,0,128,0,1,72,0,0,0,0,0,0,0,0,20,128,0,80,0,
0,2,130,0,0,33,64,0,0,0,0,0,0,0,0,0,40,32,0,128,0,80,0,0,128,16,0,82,20,2,20,0,1,64,32,0,0,0,41,0,0,0,0,0,20,16,20,128,164,40,0,0,0,4,40,32,0,20,16,2,144,0,0,0,2,2,128,10,64,82,20,0,0,0,16,0,0,32,40,0,
0,0,0,0,0,0,20,128,0,0,4,41,10,66,128,0,0,133,0,128,20,2,20,16,0,0,0,2,128,0,0,16,0,0,0,0,10,64,0,0,0,0,0,0,0,40,0,0,0,4,0,160,0,0,0,0,1,64,0,0,0,0,2,0,0,0,0,0,0,20,16,0,0,0,0,0,0,0,10,0,4,0,0,80,1,10,
0,0,2,128,8,10,0,0,164,41,0,4,40,33,64,32,0,16,160,0,0,40,0,128,0,64,0,40,0,0,8,8,0,0,0,82,0,0,0,164,0,160,128,0,80,1,65,0,40,32,0,0,0,0,160,16,20,0,80,64,0,32,0,0,10,1,1,72,80,64,0,0,0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,2,128,1,64,0,2,2,0,0,0,0,1,64,32,0,0,1,64,32,0,160,133,32,41,0,0,0,1,64,32,0,0,0,0,128,20,16,164,0,160,128,160,16,0,10,8,0,40,0,0,0,0,0,0,0,2,0,13,16,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
0,80,0,0,0,0,0,160,133,33,64,0,0,0,0,0,0,0,16,0,10,64,0,0,164,0,0,1,72,0,0,128,20,0,1,0,0,16,0,82,0,0,6,136,8,0,0,0,1,72,80,0,0,2,128,0,0,128,0,10,64,0,0,20,0,64,0,40,0,20,133,32,0,2,0,82,0,10,8,1,64,
0,0,64,0,0,0,0,6,136,0,0,0,10,8,0,5,0,16,0,0,0,0,0,0,0,80,8,1,65,0,0,0,0,33,64,0,160,0,0,4,5,32,0,128,0,0,0,0,0,0,0,0,0,133,0,0,0,0,128,2,128,0,0,0,8,0,5,32,41,0,0,160,0,0,0,0,8,80,0,0,0,0,0,0,0,0,0,0,
0,160,2,2,128,0,0,160,133,0,0,64,10,64,64,0,0,0,0,0,0,66,128,1,10,1,64,5,32,41,0,0,0,0,0,16,0,0,4,0,0,0,0,0,0,5,32,0,0,0,0,0,0,40,4,40,0,0,82,0,0,0,164,0,0,0,0,2,128,0,4,0,0,0,0,0,0,0,0,10,64,66,128,0,
40,4,0,160,0,64,0,33,65,64,0,2,0,8,80,1,65,0,0,2,128,0,41,10,0,33,64,4,0,0,8,0,40,5,32,5,41,10,0,4,4,0,2,20,0,64,0,0,0,0,0,133,0,0,0,40,4,0,0,66,128,8,0,0,3,68,32,0,0,0,0,0,13,0,0,0,0,8,0,0,160,133,0,
128,0,0,40,0,0,0,33,64,0,133,32,0,2,128,1,0,0,16,20,0,64,10,66,128,64,82,0,0,0,0,10,64,10,104,208,50,100,0,10,66,20,0,10,8,80,1,144,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,64,0,0,0,0,0,0,0,164,0,164,0,0,10,0,
0,128,2,130,0,0,0,0,10,0,0,16,0,80,8,80,64,82,0,0,0,20,0,66,144,16,20,16,2,144,20,133,32,5,0,2,144,0,1,64,0,133,0,0,1,0,0,0,0,41,1,65,1,64,4,0,2,128,64,10,0,0,128,0,0,0,0,0,0,0,0,0,0,0,32,0,0,0,40,4,0,
0,0,0,0,0,0,208,0,0,0,0,0,66,128,0,0,0,0,0,0,0,0,128,0,0,0,0,8,80,0,0,0,0,0,2,20,0,0,0,0,0,0,0,0,0,20,20,209,76,25,32,0,20,128,0,1,64,41,144,0,0,0,0,0,0,0,0,0,0,40,32,0,20,16,0,10,0,0,160,0,8,80,1,0,0,
0,0,0,0,0,0,20,0,0,0,128,20,0,0,33,74,64,0,0,0,0,0,0,10,66,20,128,160,133,0,16,164,4,0,0,0,5,32,0,0,80,0,4,41,0,5,0,0,0,0,0,0,32,5,0,0,0,0,2,0,0,40,0,2,128,1,0,0,0,0,0,0,0,0,2,0,0,0,0,0,0,0,0,0,0,0,0,
0,0,0,3,64,2,2,128,64,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,20,2,20,128,160,0,0,33,64,0,0,66,144,160,16,164,41,72,67,69,33,128,0,0,164,0,20,128,0,0,0,0,0,0,0,0,0,0,1,72,10,8,0,5,32,0,20,0,0,0,0,0,40,
0,164,0,128,0,0,40,0,0,1,64,0,16,2,16,165,33,64,5,33,64,4,32,0,20,2,0,80,0,0,128,133,0,0,0,0,16,0,0,5,32,41,0,0,20,133,0,0,0,33,64,0,0,0,0,133,32,0,0,0,5,4,0,0,0,0,0,1,72,80,8,0,40,0,128,0,0,0,0,0,0,0,
0,40,0,133,0,2,0,10,0,0,0,0,0,2,0,10,0,0,0,0,0,0,0,0,0,0,33,65,10,0,0,0,0,5,33,64,0,0,0,4,40,0,0,66,128,0,0,0,64,1,64,0,2,2,130,20,0,82,16,0,64,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,10,64,0,0,160,0,0,0,
20,0,64,1,64,40,50,1,64,0,0,10,64,0,0,2,0,1,1,74,64,0,5,32,0,0,0,4,0,20,0,0,0,128,0,10,8,0,0,0,82,20,16,160,0,0,0,0,0,0,0,10,64,0,0,0,0,0,160,128,0,66,128,0,4,0,160,0,0,4,0,0,10,64,0,0,0,0,0,2,128,64,
0,0,160,0,0,5,32,0,2,130,128,66,128,0,32,32,33,65,10,0,0,20,128,0,0,0,0,0,0,164,0,0,80,0,0,0,0,0,0,0,0,2,0,0,40,32,0,0,82,20,16,0,0,0,2,130,2,130,0,80,8,64,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,80,
1,1,64,0,160,0,0,0,16,0,1,64,0,0,8,0,0,0,0,0,0,0,0,0,0,0,0,0,0,2,2,128,1,1,64,0,0,0,0,0,0,0,0,1,65,0,0,0,10,0,0,16,0,80,1,1,64,33,65,0,0,160,128,164,5,0,0,64,0,5,32,0,0,0,0,16,160,128,0,10,64,1,64,0,0,
0,0,133,32,5,0,2,128,1,10,66,130,2,144,0,0,33,64,4,0,208,4,0,20,0,0,4,0,0,0,0,160,0,0,0,16,164,0,0,0,0,20,128,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,2,128,1,144,0,40,32,41,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
0,164,0,165,32,40,0,0,0,0,128,0,0,0,0,0,0,2,128,1,1,64,0,133,32,0,0,80,64,0,0,0,0,0,0,0,5,4,0,0,0,0,0,0,0,0,1,64,0,0,0,0,0,1,72,0,0,0,1,0,0,2,144,164,0,0,0,40,0,0,0,0,128,0,0,0,0,0,0,0,0,0,2,144,164,0,
2,144,2,20,0,1,10,64,0,5,0,0,0,0,160,128,128,160,0,0,0,0,0,0,0,1,72,0,0,2,130,0,0,0,0,0,0,0,0,0,0,66,128,0,0,0,10,64,0,0,0,0,0,0,1,72,0,41,144,0,41,10,64,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,82,130,
20,0,64,0,0,0,0,0,0,0,0,0,0,0,2,128,8,0,40,0,2,144,160,128,0,64,0,40,0,0,64,0,40,32,0,0,0,0,0,0,0,0,0,0,2,144,0,82,20,16,0,10,64,80,1,10,64,0,0,0,80,64,10,64,0,0,160,0,0,0,0,0,32,40,32,0,0,0,0,0,0,0,0,
0,0,0,66,128,1,10,64,80,0,33,64,0,0,0,0,0,0,32,40,0,16,0,80,8,80,1,72,0,0,0,0,0,0,0,0,20,133,32,41,0,0,0,0,0,0,0,0,0,80,0,0,0,1,0,5,32,0,0,64,10,1,10,8,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,80,0,0,
20,16,164,0,0,80,1,0,5,33,72,0,5,0,133,0,20,128,0,82,0,0,0,16,20,16,2,144,0,10,8,0,0,0,0,4,0,0,10,0,0,0,0,0,0,0,0,0,0,0,20,128,0,0,0,0,0,0,0,8,80,0,0,0,0,0,20,0,64,10,64,80,66,130,0,0,0,0,8,10,64,1,65,
0,0,0,0,0,0,0,0,0,80,1,0,5,0,16,0,10,0,0,0,0,0,0,0,41,1,64,4,0,0,0,0,160,128,20,0,0,33,64,0,0,0,0,0,0,0,0,0,0,0,0,0,128,160,133,4,0,2,0,0,0,0,82,0,10,64,0,0,0,0,0,0,0,0,0,1,72,10,1,1,65,10,8,80,66,128,
0,0,20,16,0,0,41,10,64,10,64,10,8,1,72,1,64,0,133,4,40,0,0,64,10,64,0,0,0,0,4,5,0,0,0,0,0,0,0,0,1,0,40,0,0,10,64,0,0,0,82,0,0,0,20,128,0,0,0,0,0,0,0,0,0,0,8,80,0,0,0,0,0,2,2,144,0,80,64,0,0,2,128,0,0,
128,2,130,0,0,5,32,0,20,0,13,2,20,16,2,16,0,0,0,165,0,0,0,0,0,66,128,0,0,0,0,0,0,0,0,0,0,0,2,20,2,20,128,20,0,0,0,2,144,0,0,0,0,0,32,0,0,0,0,2,0,0,0,0,0,0,0,0,0,0,10,8,1,65,10,0,0,0,0,5,4,0,0,1,65,1,64,
4,0,0,10,1,0,40,0,16,0,10,8,0,0,0,0,0,164,41,0,0,0,0,5,32,0,0,0,0,0,0,0,20,128,2,144,0,0,0,20,0,64,0,0,0,0,0,0,0,0,160,133,0,0,8,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,4,5,32,0,0,1,64,0,0,0,0,2,0,0,0,20,0,0,
41,64,0,0,0,32,50,0,0,0,13,2,2,128,0,4,40,4,40,0,128,0,1,65,0,0,0,0,0,0,0,0,0,0,0,0,1,64,0,16,0,0,0,0,0,0,0,0,0,0,1,10,0,0,0,0,0,0,0,0,0,0,0,20,0,0,41,10,64,0,0,0,0,32,0,20,128,20,0,0,0,0,8,1,72,0,41,
0,5,32,0,20,0,0,32,40,0,164,0,16,160,0,0,4,0,0,1,65,10,8,0,0,0,0,0,0,0,0,0,0,0,0,0,41,10,0,0,0,64,0,0,20,128,0,1,65,0,0,2,130,20,2,0,0,40,0,164,0,16,0,10,8,0,0,0,0,0,0,0,0,0,0,0,128,0,0,41,10,8,1,65,65,
65,10,66,130,16,133,32,0,0,0,0,160,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,128,160,0,0,0,0,8,1,64,0,0,0,0,0,0,0,0,0,0,0,1,64,4,0,0,1,65,0,0,0,82,0,0,0,0,10,0,0,0,0,5,32,0,0,64,0,0,0,0,0,0,0,0,0,0,0,0,0,41,0,5,0,
16,0,82,0,1,64,0,0,0,0,0,0,32,0,0,0,41,0,0,0,0,0,0,0,0,164,41,0,40,32,40,0,2,20,0,0,0,128,160,128,20,133,33,74,1,0,32,0,0,0,40,32,41,0,5,0,2,128,8,0,4,0,164,5,4,40,32,0,0,0,0,0,0,0,0,0,0,0,0,32,0,160,
160,128,2,0,0,0,0,0,0,160,0,0,0,0,0,5,0,0,0,32,41,0,0,20,16,0,0,0,164,0,2,128,1,0,0,0,10,64,10,64,0,40,0,0,0,0,0,0,41,0,4,0,0,10,64,0,0,0,0,0,0,0,0,133,0,16,160,16,160,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,160,133,0,0,0,0,0,0,32,0,2,144,164,0,0,0,0,0,82,2,144,0,80,64,80,64,0,0,0,80,1,10,8,80,64,0,0,2,144,2,128,1,0,0,0,0,0,160,0,1,0,0,2,128,10,8,10,1,0,4,5,4,40,33,65,0,0,0,80,64,0,0,2,20,
0,0,32,0,0,0,0,0,0,0,0,0,0,208,4,0,2,128,64,1,64,40,32,0,0,0,0,16,160,2,20,0,64,10,66,130,2,128,1,0,40,0,0,0,0,2,0,80,0,0,20,128,0,1,0,0,0,0,41,0,0,0,0,0,0,0,32,0,20,0,8,10,0,0,164,0,0,0,0,0,0,0,0,0,0,
0,0,0,0,10,8,1,65,10,0,0,0,66,128,66,128,0,0,160,2,2,130,20,0,64,10,64,1,0,33,64,0,0,0,0,0,0,0,0,0,0,0,0,0,2,144,0,0,0,16,160,0,0,41,10,8,1,72,82,0,0,0,160,0,8,80,66,130,0,10,0,0,2,144,20,132,0,0,10,0,
0,16,160,0,1,0,0,0,0,5,0,128,16,0,0,0,0,0,0,0,0,0,208,32,0,0,1,72,10,0,32,0,2,128,64,0,0,164,41,0,40,32,0,0,0,0,0,10,66,130,20,0,0,0,16,0,0,0,0,1,72,0,0,0,10,8,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,10,64,0,0,
0,0,0,0,0,0,0,10,8,10,0,32,5,32,0,2,128,64,80,0,5,32,0,0,1,65,0,0,160,0,64,1,0,0,0,0,0,0,0,0,0,0,0,0,0,4,40,0,2,0,10,0,0,2,0,1,72,80,64,80,66,144,20,0,0,0,0,0,0,0,0,0,2,128,1,0,0,0,80,64,0,0,0,0,0,20,
16,164,0,164,0,16,160,0,0,41,1,72,1,0,0,0,0,0,0,0,0,0,10,0,0,0,0,0,0,0,0,0,64,10,0,0,0,0,5,0,128,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,72,0,5,0,128,0,0,0,0,0,0,0,0,0,0,0,0,20,16,0,0,0,20,128,0,0,0,0,0,0,0,
0,40,4,41,1,65,0,0,2,128,0,0,0,0,32,41,1,64,4,0,0,0,0,0,0,0,20,16,160,2,0,0,0,2,144,0,0,0,160,128,20,128,160,128,0,0,0,0,0,0,0,0,0,0,0,0,0,80,64,0,0,0,8,80,0,0,0,0,0,0,66,144,20,0,0,0,0,0,0,0,0,0,0,80,
64,1,1,72,82,0,0,0,0,0,0,0,0,0,0,10,8,80,0,4,0,20,0,0,0,128,0,10,0,0,0,0,0,164,0,0,0,0,0,1,65,10,64,0,5,0,0,64,0,0,0,0,0,0,10,8,0,0,0,0,0,20,2,0,0,41,0,0,0,0,0,160,128,0,0,40,32,0,0,0,0,0,0,0,0,0,40,0,
133,4,0,20,2,0,10,0,32,0,0,0,0,0,0,0,0,0,0,20,0,0,0,0,0,5,32,0,0,0,0,133,0,16,0,1,64,0,128,160,0,0,32,0,0,0,0,0,0,0,0,0,5,0,128,0,80,8,0,0,0,8,80,0,4,0,0,0,5,33,72,0,0,2,144,0,0,5,0,16,2,144,0,0,0,0,0,
0,0,0,0,0,0,0,0,10,0,5,0,128,0,0,0,16,0,0,0,160,0,82,20,128,0,0,0,0,0,0,0,0,0,0,0,0,20,0,64,0,40,0,0,0,4,5,0,0,64,80,1,0,0,20,2,20,133,33,64,0,0,0,32,5,0,133,0,16,20,16,0,0,0,0,10,64,0,0,0,0,0,0,0,0,0,
0,0,20,128,0,0,0,0,0,0,0,1,64,0,0,0,0,0,0,0,128,0,80,1,10,8,80,0,0,0,0,0,0,64,10,64,0,40,0,128,0,82,0,0,40,0,16,0,0,0,0,0,0,0,10,66,128,8,80,64,0,0,0,0,33,64,0,16,160,0,66,144,0,0,5,4,5,0,0,64,10,1,1,
64,4,40,0,0,1,0,0,2,144,164,41,10,0,5,4,5,0,133,0,128,2,0,0,5,0,160,0,0,0,0,64,82,2,130,0,0,0,160,16,0,10,0,0,0,0,0,0,0,0,16,0,10,8,80,0,0,128,20,2,0,80,0,0,0,0,4,0,2,130,20,0,8,82,0,0,0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,0,0,0,0,0,5,32,5,0,0,0,0,0,0,4,0,2,128,0,0,128,0,10,0,33,65,0,5,4,40,0,0,1,0,0,20,128,0,0,0,164,0,2,128,0,0,0,8,0,0,0,1,64,32,0,0,0,0,0,0,0,0,0,4,5,0,0,8,82,2,128,8,10,0,0,0,0,4,40,0,2,
0,10,0,0,0,1,0,40,0,2,128,0,4,5,0,0,8,82,0,64,0,5,40,4,5,32,0,2,144,0,0,0,0,80,0,0,0,0,0,0,0,4,40,4,0,0,82,0,0,0,0,1,64,4,40,32,0,20,128,0,10,64,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,10,0,0,2,0,0,0,0,1,72,80,
0,4,0,0,10,0,0,0,0,0,0,1,64,0,0,8,80,64,8,1,64,33,64,4,0,160,128,20,16,164,0,0,0,0,0,0,0,0,0,0,20,16,160,16,2,130,0,0,0,0,0,40,33,72,80,8,0,0,0,0,0,0,0,0,0,80,64,0,0,128,160,2,20,16,0,0,0,2,130,0,1,64,
0,0,1,10,0,32,5,0,128,160,20,128,0,0,0,0,0,5,32,5,4,0,128,2,128,0,0,0,0,0,0,0,0,0,0,0,0,10,0,4,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,2,128,66,128,0,0,0,0,0,0,0,4,40,4,5,32,
0,164,41,10,0,0,0,0,0,128,160,2,0,0,40,32,0,2,130,20,16,160,2,0,1,65,0,0,0,1,72,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,2,130,20,16,0,0,0,0,0,5,0,0,0,0,16,0,8,1,64,4,0,160,128,2,144,164,41,10,64,80,64,1,72,
82,20,128,0,0,40,0,0,0,0,2,0,82,20,16,160,2,0,80,8,0,5,0,0,0,0,0,0,0,160,16,160,128,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,10,64,0,0,0,0,40,0,128,160,133,0,0,0,32,5,33,72,80,0,33,64,0,0,0,0,
0,0,0,128,164,0,164,40,0,133,0,0,0,0,0,0,0,2,0,0,0,20,2,0,80,1,0,0,0,0,0,2,128,0,4,40,0,164,0,0,0,0,0,0,0,16,20,128,2,128,66,144,0,0,0,0,0,0,0,0,0,0,0,0,0,0,5,33,65,0,0,0,1,0,0,0,0,0,20,2,0,0,0,0,0,0,
0,0,0,0,1,72,0,40,0,128,160,0,0,0,0,0,0,0,0,0,0,0,0,0,0,5,0,0,64,0,0,0,10,0,4,40,0,0,0,0,0,0,0,133,0,0,0,5,32,0,16,0,0,0,0,10,0,0,0,8,1,64,4,5,0,0,0,0,0,0,0,2,2,128,0,0,0,0,0,133,0,0,66,144,20,0,8,82,
20,133,40,33,72,0,0,0,0,0,128,0,0,0,160,0,0,4,40,0,0,1,0,40,0,0,0,0,128,160,0,0,0,2,128,66,144,160,2,20,128,0,0,0,0,8,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,128,160,0,0,4,5,4,41,1,72,10,0,0,16,0,0,0,0,0,0,
0,0,0,164,0,0,0,0,160,0,0,40,32,0,0,0,0,0,0,5,0,128,0,0,0,0,0,0,0,0,0,20,0,0,0,0,0,0,0,0,0,0,80,1,10,66,128,8,0,0,128,160,0,8,80,66,128,0,32,5,33,72,80,66,144,2,130,2,144,0,0,5,32,0,0,0,41,0,5,0,128,0,
0,0,0,80,66,128,0,33,64,0,16,0,0,0,0,10,0,0,0,0,0,0,0,0,0,0,5,4,0,0,1,10,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,10,8,0,0,128,20,0,8,0,0,20,128,0,82,0,0,0,0,0,0,0,0,0,0,1,1,64,0,16,20,16,2,130,0,10,64,0,0,20,128,
0,0,0,2,130,0,0,0,0,10,0,0,0,0,0,0,0,0,2,144,160,128,0,0,0,0,80,8,1,72,0,0,0,0,0,2,144,160,0,0,0,0,0,0,0,0,0,0,10,8,64,10,64,0,0,0,0,0,0,0,0,20,2,0,0,0,0,10,64,80,0,33,64,4,40,4,5,32,40,4,0,2,144,160,
128,160,0,0,0,0,64,0,0,0,0,5,4,5,0,0,82,20,128,20,2,0,0,0,0,1,10,80,8,0,4,40,0,0,0,32,0,20,0,80,64,1,10,1,10,66,128,0,0,2,128,0,0,2,0,82,0,8,80,0,0,0,0,0,0,0,32,0,0,0,0,0,0,0,16,160,16,160,0,0,0,0,64,
10,0,4,40,0,16,164,0,0,0,0,0,0,40,0,128,165,33,64,33,1,72,80,8,80,10,1,0,0,20,16,0,0,0,0,0,5,0,16,0,0,0,0,10,0,4,0,0,0,0,0,0,41,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,164,0,164,40,32,0,2,128,8,10,8,80,0,4,
0,0,10,0,32,0,0,0,0,0,0,0,2,130,2,128,1,1,64,0,0,0,40,0,16,20,0,0,32,5,32,0,2,0,10,0,4,5,0,0,0,32,5,33,65,10,0,0,0,0,0,2,2,128,1,64,0,0,8,0,33,65,0,5,0,0,0,0,0,0,0,0,0,0,0,8,80,80,66,0,0,0,0,1,1,64,0,
0,8,1,64,32,40,0,0,64,10,1,0,0,0,1,64,0,0,1,65,0,0,0,0,0,2,144,2,144,0,0,0,0,0,0,160,0,0,0,128,0,0,5,4,0,164,0,20,16,160,0,66,144,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,160,16,160,0,64,1,72,82,0,80,0,0,133,0,16,
160,2,2,128,0,0,0,0,0,0,0,0,128,0,1,64,4,5,0,0,0,0,133,0,2,128,0,0,0,64,0,0,0,64,10,1,0,0,0,0,0,0,0,0,20,2,0,0,0,2,128,8,0,0,160,0,0,0,0,8,0,0,0,80,0,0,0,0,32,0,160,0,0,0,0,0,0,164,0,0,64,0,0,0,0,0,0,
8,80,1,0,0,2,144,0,1,64,32,40,32,0,20,0,66,130,128,1,0,41,0,0,0,80,8,80,0,0,0,8,0,0,20,0,0,0,0,0,0,16,20,16,0,0,5,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,5,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,16,20,0,
0,0,2,20,0,0,0,0,0,0,0,8,82,144,0,0,0,0,8,0,0,0,0,0,0,0,0,2,130,20,128,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,20,133,0,16,160,128,0,0,0,0,0,0,2,128,0,0,16,0,0,0,0,0,0,0,0,0,128,0,80,0,32,0,2,128,64,0,5,0,
2,0,0,40,0,2,128,1,0,0,0,10,0,0,0,0,0,0,0,0,128,160,0,0,0,16,0,80,0,0,0,0,0,0,1,65,0,0,2,20,16,0,0,5,33,72,10,0,0,0,1,0,41,10,0,0,2,144,0,66,128,0,0,0,0,0,0,0,0,2,0,0,40,33,64,0,0,0,0,0,0,0,2,20,133,0,
128,2,144,164,0,164,0,160,2,0,0,0,0,10,0,0,0,64,0,0,0,0,0,160,0,0,4,0,0,0,0,0,0,0,0,0,0,128,2,128,0,0,0,0,0,0,0,0,0,8,0,0,20,0,0,0,0,0,0,0,66,128,0,0,133,0,0,0,0,0,1,10,64,1,64,0,128,0,0,0,0,0,0,0,0,0,
0,13,0,64,80,0,32,0,160,0,0,0,0,64,0,5,0,16,0,0,40,0,16,0,0,41,0,5,0,0,0,0,0,0,0,0,1,1,72,82,0,80,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,5,0,0,64,10,1,0,0,0,0,5,0,0,64,0,0,0,0,0,0,0,40,0,0,0,4,5,0,2,2,128,
0,41,0,0,0,0,4,0,20,2,0,0,40,0,0,0,0,16,0,10,1,0,41,10,0,0,2,0,0,0,0,0,0,0,0,0,2,2,128,1,0,5,4,5,32,5,0,133,32,41,10,0,32,0,2,130,0,82,0,0,5,4,40,32,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,41,1,72,10,0,5,0,2,16,
2,128,8,0,0,0,10,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,82,2,144,20,0,0,0,0,0,0,0,8,1,72,80,0,0,0,1,1,72,0,0,133,0,0,0,0,20,16,0,0,0,164,41,0,0,0,0,0,0,0,0,0,10,8,0,40,33,64,0,0,0,0,0,1,10,66,128,0,0,0,0,40,32,
0,0,0,0,0,0,32,5,0,0,0,0,0,0,0,0,8,0,0,20,0,10,8,0,0,0,8,82,20,0,64,80,64,0,0,0,0,0,0,8,80,0,0,0,0,4,0,160,128,20,128,0,82,0,1,65,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,164,40,0,2,0,1,64,4,0,0,0,0,0,10,0,0,
0,0,0,0,0,0,0,8,0,0,160,0,0,0,0,1,65,0,0,0,0,5,0,0,0,4,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,80,0,41,0,0,0,1,0,0,0,0,0,0,10,8,0,0,20,0,0,0,0,8,0,40,0,0,66,128,0,0,0,0,0,0,0,0,0,0,0,0,0,32,40,0,0,0,0,0,8,1,
72,0,0,20,0,0,0,0,1,64,0,2,0,0,0,128,160,133,0,128,20,133,32,0,160,16,0,0,0,164,0,0,0,4,5,0,0,1,0,0,20,128,0,0,0,0,0,0,0,0,0,0,0,0,0,10,66,128,8,82,20,0,0,4,5,32,40,0,0,0,0,0,0,0,0,0,0,0,82,0,0,0,0,0,
0,0,0,0,0,0,0,20,16,164,41,1,64,32,0,0,82,0,0,0,2,144,0,10,8,80,1,0,0,0,0,0,0,0,0,0,10,0,0,2,0,0,0,0,0,40,32,0,0,1,65,0,5,0,0,0,0,0,0,0,0,0,0,0,10,0,33,64,32,0,0,0,0,0,0,0,0,64,80,0,0,2,0,0,0,164,0,0,
0,0,2,130,20,0,8,10,1,10,0,0,164,0,0,0,0,0,0,0,16,2,130,20,0,0,0,0,0,0,2,0,0,0,2,0,10,64,0,0,0,0,5,32,0,20,128,0,0,0,0,0,0,0,0,40,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,2,128,8,0,0,0,10,64,0,0,0,0,0,0,80,0,4,
0,0,0,0,0,0,0,0,0,0,0,1,64,0,0,0,0,0,64,0,0,160,16,160,0,64,0,0,0,80,0,0,133,0,0,0,0,0,1,10,8,0,40,0,0,0,0,0,0,0,2,2,128,0,0,0,0,0,0,1,0,5,32,40,32,5,0,128,0,0,0,0,0,0,164,0,0,82,0,0,0,160,0,8,0,0,164,
0,2,144,20,128,2,128,8,80,64,0,0,0,0,40,32,0,160,16,0,82,20,0,64,0,0,16,0,0,0,164,0,160,133,0,2,0,10,1,0,0,0,1,64,4,0,0,10,82,0,0,0,0,0,0,0,0,0,2,130,2,144,0,0,0,0,0,0,0,0,0,0,80,64,0,0,0,1,74,64,0,0,
16,0,0,0,0,10,1,0,0,0,82,0,0,0,0,0,0,2,128,0,4,0,2,128,0,0,0,0,0,0,64,10,0,0,0,0,0,0,0,32,40,40,32,4,0,0,10,1,10,66,128,8,80,64,0,0,20,133,0,16,0,0,0,0,0,0,0,0,0,0,8,10,0,4,0,20,0,0,0,2,20,16,160,0,0,
0,0,0,0,0,0,0,2,2,128,0,0,0,0,0,0,0,5,32,0,0,0,0,0,0,0,128,20,16,0,0,40,4,41,0,0,160,2,20,128,160,0,8,80,64,0,40,0,0,0,41,64,50,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,128,2,148,128,0,0,5,32,0,0,
0,0,0,0,0,0,0,0,0,10,64,0,0,0,0,0,0,80,64,0,0,164,0,0,1,64,0,0,8,0,0,0,0,0,0,0,0,2,128,10,8,1,0,0,0,80,0,0,0,0,0,0,0,0,0,1,0,0,0,64,0,0,0,10,0,0,2,0,0,41,0,0,2,130,0,0,0,0,0,0,0,0,0,0,10,0,0,2,0,80,0,
0,0,0,0,0,66,128,0,4,0,0,80,0,4,0,0,0,0,2,130,0,82,2,128,1,0,5,33,65,10,0,0,2,2,128,0,0,20,128,0,10,12,128,1,65,0,41,0,40,0,0,1,0,0,2,128,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
0,2,144,0,80,8,0,0,2,144,0,0,0,0,0,0,20,16,0,0,0,0,0,0,0,80,0,0,0,0,0,0,0,0,0,0,32,0,0,80,0,0,2,0,82,0,0,0,133,0,128,0,0,40,0,0,8,0,0,20,16,160,0,64,10,0,0,0,0,0,133,0,0,0,0,0,1,10,0,0,0,0,0,0,0,0,0,0,
0,0,64,82,20,0,0,4,40,0,0,0,0,128,0,0,0,0,0,0,0,0,40,32,40,0,16,0,1,64,0,2,0,1,72,0,41,0,40,32,5,0,128,0,10,64,10,0,0,0,0,0,0,0,0,0,0,0,0,0,0,164,0,20,2,20,128,0,0,0,0,0,0,0,0,0,0,0,0,20,133,4,0,160,2,
0,0,0,0,0,0,0,0,0,0,0,0,0,1,65,0,0,0,0,40,0,2,130,16,20,16,164,5,0,128,0,10,0,4,40,0,0,0,5,32,0,2,0,0,0,2,144,0,1,10,0,0,0,0,4,40,0,0,0,0,0,10,0,33,64,0,0,0,0,128,0,0,41,0,40,4,40,4,40,33,64,0,128,160,
0,0,32,0,0,0,0,0,0,0,16,160,0,0,0,0,0,41,8,1,72,10,0,0,0,64,82,0,0,0,0,1,64,0,0,0,0,2,0,0,5,32,0,160,0,0,0,0,0,0,2,0,10,66,128,0,0,0,0,0,2,130,0,10,64,0,0,0,0,0,0,0,0,0,0,0,20,0,0,0,20,128,2,144,0,8,0,
0,0,0,0,0,10,66,130,20,0,1,1,65,10,64,80,1,1,72,80,1,0,0,20,0,64,1,72,0,0,164,5,32,0,0,0,0,2,128,1,10,66,128,8,0,0,0,0,40,32,0,0,0,0,0,0,0,0,0,0,0,0,4,40,40,4,0,0,0,0,0,0,5,0,16,0,0,0,0,0,0,0,0,0,0,0,
0,2,128,0,32,0,0,66,128,0,0,0,0,0,128,164,0,0,0,0,0,82,0,82,0,0,41,0,40,32,0,0,10,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,160,128,20,0,0,33,72,0,0,0,0,0,0,82,0,0,0,20,128,0,0,0,0,0,0,160,0,0,0,128,
0,0,0,160,160,0,0,32,0,128,2,128,0,0,133,0,0,0,0,0,1,10,0,0,0,0,0,16,0,1,64,0,2,0,0,40,0,0,1,0,0,0,0,0,0,0,0,0,0,5,32,0,128,160,0,10,64,0,0,0,0,0,0,0,0,0,10,8,8,80,0,0,0,0,0,0,0,0,0,0,0,0,82,20,128,0,
0,0,0,0,33,72,10,0,0,16,160,16,2,130,20,128,0,0,0,20,2,2,144,0,0,0,0,10,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,5,0,133,0,16,0,0,0,0,0,40,0,16,20,128,0,0,0,0,0,0,2,128,0,0,0,0,32,0,2,144,20,
164,40,0,128,0,0,0,0,0,0,0,0,0,20,0,0,32,0,2,144,0,0,0,0,8,10,0,0,0,8,10,66,128,1,1,72,0,0,0,0,4,0,160,0,0,0,0,0,4,0,160,133,0,0,10,8,0,0,0,0,0,0,0,0,133,0,0,80,8,0,5,0,16,0,80,0,0,20,2,0,0,4,0,0,0,0,
0,1,0,0,160,0,1,0,0,0,82,2,144,0,0,40,32,41,0,0,20,16,0,0,0,160,0,0,0,128,20,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,5,0,2,0,82,0,82,0,80,0,0,0,0,4,0,0,0,0,2,130,20,2,20,0,1,0,0,0,0,0,164,5,32,0,
20,128,0,1,64,0,0,80,64,64,0,5,0,0,1,65,1,64,4,0,16,0,80,8,10,0,0,0,8,0,0,2,20,0,0,32,5,4,0,164,40,0,0,0,32,0,0,1,72,0,0,2,130,20,0,0,0,0,8,10,1,10,0,0,0,0,0,0,80,8,0,0,0,0,0,2,128,0,0,0,64,0,0,0,0,4,
0,160,2,144,0,0,0,0,0,0,2,128,66,0,0,0,2,144,20,128,160,133,32,40,0,128,0,0,5,0,0,66,128,8,0,40,0,128,164,41,1,65,10,64,10,8,80,66,128,0,0,0,0,0,0,0,0,0,0,0,0,82,0,0,0,0,0,0,164,0,0,0,0,0,0,41,0,0,164,
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,82,2,144,20,128,164,41,0,40,32,0,0,0,0,2,128,1,0,0,0,0,32,0,0,0,5,32,41,0,0,0,0,0,0,82,0,0,0,2,144,0,1,72,0,0,0,10,64,0,0,0,80,0,0,0,0,0,0,1,1,64,4,40,0,133,0,0,
82,0,0,0,0,10,64,0,0,2,144,0,0,0,20,133,4,41,1,64,4,32,5,0,0,8,10,8,10,0,32,40,0,128,20,0,0,0,0,64,1,64,32,40,0,133,0,0,0,0,0,64,80,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,2,144,0,0,0,0,0,
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,20,16,0,0,0,0,0,0,2,128,0,0,16,0,1,0,0,160,0,0,32,0,0,0,0,0,0,0,160,128,20,133,4,40,32,0,0,0,5,4,40,32,5,0,128,0,0,0,20,128,0,0,0,20,128,164,0,0,0,0,0,0,0,20,16,0,0,40,
0,0,8,80,66,144,0,10,8,10,8,0,0,160,0,0,0,0,0,5,32,0,0,0,0,2,130,0,0,0,0,0,0,164,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,20,
128,0,0,0,0,1,72,0,0,0,0,0,0,0,0,164,0,0,1,0,0,0,0,0,0,0,0,0,0,0,2,128,0,6,138,64,1,0,0,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,8,80,8,0,41,0,0,0,1,72,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,20,0,8,10,10,64,0,40,
33,64,32,41,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,5,32,0,0,80,64,0,0,0,0,5,32,41,10,8,10,8,82,0,0,0,0,0,5,0,128,0,0,0,0,0,0,0,0,0,0,0,0,0,10,8,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
0,66,128,1,1,64,0,16,2,144,0,10,0,5,0,20,166,64,41,72,64,64,10,0,4,0,0,0,0,0,0,0,0,0,0,0,1,72,0,0,0,0,0,0,0,0,0,64,0,0,160,133,0,128,0,0,0,0,0,0,0,0,0,0,82,0,0,0,0,0,0,2,128,0,0,128,20,0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,80,66,144,0,1,64,4,0,0,10,0,0,0,64,10,1,0,0,20,0,1,10,1,0,0,0,10,8,0,0,0,1,72,1,65,0,0,0,0,0,0,0,0,0,0,0,0,82,20,128,0,10,64,0,0,0,0,41,10,1,0,5,32,0,160,0,66,144,0,0,
0,0,0,5,32,0,0,0,0,20,20,0,66,16,2,155,40,50,100,128,26,4,0,0,0,0,0,0,0,2,144,164,0,0,0,0,2,128,64,0,0,133,32,0,160,0,0,0,0,0,0,160,128,128,0,0,0,0,0,0,0,0,0,0,10,64,0,0,0,0,0,0,0,0,0,0,40,41,1,10,0,0,
0,0,0,128,0,1,64,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,20,0,1,1,65,0,0,165,4,0,0,8,1,65,10,64,10,82,0,8,10,8,0,0,160,0,8,0,0,0,0,40,32,0,0,0,0,0,0,0,0,0,5,0,16,0,80,8,0,41,0,0,0,0,0,2,128,8,0,0,2,128,0,0,20,
2,20,16,16,2,130,20,166,64,0,0,1,65,1,64,32,32,0,166,129,8,64,0,40,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,20,16,0,10,66,128,1,0,4,0,0,0,0,0,10,1,0,0,0,10,64,0,0,0,0,0,0,1,65,0,0,2,148,16,0,0,0,0,
0,0,2,0,10,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,2,130,2,144,20,0,8,80,0,40,32,0,16,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,80,0,0,164,0,0,0,0,0,10,64,8,0,0,20,2,0,0,0,0,0,0,20,128,160,0,1,0,0,0,
0,0,16,0,10,1,72,82,16,0,0,0,0,0,40,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,2,144,0,0,0,0,0,0,0,0,0,0,80,1,0,0,128,20,0,64,10,0,0,0,1,0,41,0,0,0,0,5,32,0,2,128,0,33,64,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,0,0,40,32,0,20,0,1,1,72,10,0,0,2,130,0,82,16,0,0,41,0,0,0,0,0,2,144,0,0,0,0,0,0,0,0,0,0,0,40,0,0,10,0,32,0,0,0,0,16,0,0,5,0,0,0,4,40,32,4,5,0,0,8,80,0,0,2,0,0,40,4,40,0,2,20,128,0,0,0,
0,0,5,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,20,128,0,0,0,0,80,0,0,16,0,0,0,16,0,80,8,80,66,144,0,1,72,10,0,0,128,0,0,0,20,2,20,20,128,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,16,160,0,0,0,0,82,
0,0,0,0,0,5,32,0,0,1,64,5,32,32,40,0,128,0,0,0,0,0,0,2,144,0,0,0,160,128,0,0,40,0,128,0,1,10,66,128,8,82,20,0,10,0,40,32,4,0,20,0,0,0,133,5,4,0,0,10,64,0,32,0,0,8,82,0,80,8,0,0,0,0,0,0,0,0,0,0,0,0,0,52,
1,0,32,0,2,144,160,0,0,0,0,66,128,0,0,0,0,0,0,0,0,0,0,0,160,133,0,128,2,20,0,0,40,0,16,164,0,0,0,32,0,20,128,2,128,64,10,1,1,64,0,128,164,0,20,0,0,0,0,0,0,0,0,0,164,5,32,0,0,0,0,0,0,0,0,0,5,0,128,0,0,
0,0,0,0,0,0,0,0,0,0,0,0,5,32,0,20,128,0,0,0,0,0,0,0,0,0,164,0,0,0,0,0,0,5,4,5,32,0,0,82,0,0,0,16,2,128,66,128,0,0,0,0,0,0,0,0,0,10,8,0,0,0,0,5,4,0,0,66,144,20,128,2,128,8,10,82,0,0,0,2,130,2,0,0,0,0,0,
0,0,83,70,64,0,2,0,0,0,0,0,0,0,1,65,1,72,82,0,10,64,80,1,1,64,0,0,0,0,0,0,4,40,0,0,80,8,0,4,40,0,2,0,0,0,20,0,8,10,1,10,0,0,128,20,0,66,130,20,0,0,0,0,0,0,0,80,8,0,0,0,0,0,0,0,0,0,0,5,4,0,0,0,0,0,0,0,
0,0,0,20,0,64,0,0,0,0,0,0,0,0,133,33,64,0,2,144,0,0,0,0,0,0,0,0,0,0,1,10,0,0,0,0,0,0,0,0,0,0,0,0,0,4,40,0,0,0,0,0,0,0,0,0,0,0,8,80,0,32,41,1,64,0,0,8,80,80,8,0,0,0,0,32,0,0,0,0,0,0,0,160,2,144,0,0,32,
0,0,0,40,32,0,0,0,0,0,0,0,0,0,0,0,0,5,5,32,0,0,8,80,82,0,0,4,0,164,5,0,16,164,40,32,0,0,0,0,164,0,160,0,1,1,64,0,0,64,80,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,80,64,0,5,0,133,0,128,0,10,0,0,16,20,0,0,0,
0,0,4,0,0,0,0,0,0,0,0,0,0,20,133,0,0,64,10,64,80,64,1,72,0,0,0,0,0,0,0,5,32,0,0,0,0,0,0,32,0,160,0,0,0,0,0,5,0,2,0,64,0,0,0,82,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,164,40,0,0,0,0,0,8,0,0,0,0,5,4,
0,164,5,4,0,160,0,0,40,0,0,80,64,0,0,0,64,0,4,0,0,1,64,32,5,32,0,0,0,0,20,128,160,128,160,0,0,5,32,0,2,20,0,0,0,0,0,0,0,0,0,0,0,0,0,0,32,41,0,40,0,0,0,0,0,10,8,80,0,0,160,128,0,64,80,0,0,164,40,32,0,2,
0,0,0,164,0,20,0,1,10,8,80,0,0,0,0,32,5,0,0,0,0,16,0,0,0,20,2,20,128,0,0,0,0,0,0,0,0,0,0,0,5,32,0,0,82,0,8,1,72,80,8,0,0,0,0,40,33,64,0,128,160,0,0,0,0,1,0,0,0,0,0,0,0,0,0,1,72,0,0,0,1,65,10,64,0,0,0,
80,0,0,0,0,0,0,0,5,0,0,64,0,4,40,5,0,0,1,0,0,128,0,0,0,0,0,0,0,0,0,0,80,8,0,0,0,10,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,5,0,20,128,0,0,0,0,0,0,0,82,0,10,0,0,0,0,0,0,0,4,0,2,128,8,82,0,0,0,160,0,82,
20,128,133,0,0,64,10,0,0,0,0,32,41,0,5,0,0,66,144,164,40,4,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,164,0,0,0,5,4,40,0,128,0,0,4,0,0,0,0,0,0,0,20,0,0,0,133,33,64,0,0,64,0,0,2,128,66,128,0,0,128,20,
0,0,32,0,20,16,0,82,20,16,0,0,0,20,160,2,2,0,0,0,0,0,0,0,0,0,0,0,0,160,16,0,10,0,0,164,0,0,0,0,0,0,0,0,0,41,0,0,0,0,0,0,0,0,0,10,8,0,0,0,82,0,0,0,0,0,0,0,10,64,0,5,32,5,0,2,20,0,8,0,0,20,2,2,144,2,144,
2,128,1,0,0,20,0,0,0,16,2,128,10,8,0,0,164,4,0,20,0,64,80,1,0,40,32,5,32,0,160,128,0,82,0,0,0,2,128,64,0,0,209,0,0,0,0,0,128,0,0,0,0,10,8,0,0,0,1,0,0,0,0,0,0,80,0,0,164,0,0,1,64,32,0,0,1,0,5,0,0,0,0,2,
2,144,0,0,5,0,0,1,10,1,0,0,0,0,0,2,130,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,64,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,10,1,10,8,0,0,0,0,0,0,0,0,0,0,0,20,16,0,0,0,0,0,5,0,0,0,0,16,2,130,0,0,0,0,0,0,0,0,0,2,128,
1,0,41,10,66,130,2,128,64,0,41,0,40,0,0,64,80,64,80,0,32,5,0,0,1,10,1,64,41,8,0,0,0,0,32,40,0,0,0,0,128,0,0,0,2,130,0,0,0,0,8,0,0,0,1,64,0,0,104,128,128,0,0,0,0,0,0,0,0,0,0,0,0,20,16,0,1,10,64,80,0,0,
0,0,0,0,0,33,72,80,0,0,0,0,0,0,0,0,0,0,0,128,0,0,0,0,1,65,0,0,160,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,72,82,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,41,0,40,0,0,8,0,0,0,0,0,0,0,0,0,1,72,0,40,4,40,32,40,0,
2,144,0,0,4,0,0,0,0,160,0,0,0,0,80,64,0,0,20,0,64,1,65,72,0,41,10,64,64,80,0,4,0,20,128,0,0,32,0,160,2,0,64,10,0,0,16,0,10,0,0,0,0,0,0,0,32,5,0,0,0,0,160,20,2,20,128,2,2,128,0,32,0,0,0,0,0,0,0,0,0,0,0,
0,0,2,148,128,160,16,0,0,0,2,0,1,0,0,20,133,32,0,0,0,0,2,128,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,82,0,10,64,82,0,10,64,80,64,0,0,2,128,0,32,41,0,0,0,0,0,0,0,40,32,41,0,0,0,82,0,0,0,0,0,0,0,0,0,0,0,0,0,
8,10,0,0,2,144,2,130,20,0,0,0,0,0,0,0,1,64,0,132,0,2,128,0,40,32,0,164,41,0,0,20,0,0,33,72,0,4,0,2,20,0,0,0,128,0,10,1,10,0,0,0,80,64,0,0,0,0,0,0,0,40,32,0,164,5,0,20,16,128,2,130,0,1,64,0,0,0,4,0,0,0,
0,160,133,0,0,66,128,0,0,16,0,0,0,0,0,0,0,0,0,0,0,0,0,66,128,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,2,128,0,0,133,0,0,8,0,40,0,0,0,0,0,0,0,0,0,33,64,0,2,0,0,0,0,80,66,144,0,0,0,0,0,0,0,10,0,0,128,0,80,
64,0,0,0,0,0,20,20,128,0,0,4,0,2,128,1,64,4,0,0,0,32,0,0,0,0,0,66,144,160,0,0,0,0,0,32,0,0,0,0,0,0,0,0,10,0,0,0,0,0,0,0,5,33,64,4,0,2,144,0,0,5,32,0,2,130,2,144,160,16,20,0,0,5,0,16,2,20,128,0,0,40,40,
0,128,128,160,2,0,0,0,0,0,0,2,144,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,160,0,0,0,0,0,4,40,0,0,0,0,0,0,0,2,128,0,0,128,0,0,0,0,0,0,2,2,128,1,0,40,33,64,0,0,0,0,2,0,0,5,4,0,
0,0,0,0,0,0,0,0,5,32,0,133,0,0,0,0,0,0,0,16,20,128,0,0,5,32,0,20,0,66,130,20,16,160,16,160,128,0,82,0,10,0,4,40,32,40,0,0,0,0,0,10,64,0,0,2,128,64,0,0,2,128,0,33,64,0,128,20,16,0,82,20,2,130,0,64,80,0,
33,64,4,0,20,0,64,0,0,0,0,0,0,0,0,0,1,72,0,5,4,0,20,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,64,33,64,4,5,0,0,8,80,8,80,0,0,0,80,80,8,0,5,4,0,0,8,82,20,133,4,41,0,5,0,128,0,10,8,10,64,0,0,
0,0,0,0,0,0,0,0,0,2,0,10,0,0,16,0,0,0,0,0,0,0,0,33,65,0,0,2,128,8,0,0,160,128,160,0,0,40,32,40,32,0,164,0,0,0,41,0,0,0,0,0,0,0,0,0,1,10,0,4,40,0,0,0,0,0,10,0,0,0,0,0,0,0,0,160,2,16,20,2,20,0,0,4,0,20,
0,64,1,72,0,0,0,0,0,160,128,2,128,0,32,40,41,0,33,64,32,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,10,64,82,0,80,66,144,0,82,20,16,20,0,1,65,65,13,16,200,40,0,0,1,0,0,0,0,5,4,0,160,16,0,80,64,
0,33,65,1,64,0,0,0,41,10,64,80,0,0,16,164,5,0,128,2,144,0,64,1,72,0,0,0,0,0,0,0,0,0,0,0,128,164,5,0,0,0,0,160,20,128,160,16,128,0,0,0,0,0,0,0,82,0,0,0,0,0,0,0,0,0,0,1,10,0,32,40,4,0,160,0,0,0,20,0,1,0,
0,20,20,0,1,144,0,0,0,10,64,0,5,32,5,4,0,0,0,5,0,2,20,0,8,0,40,0,0,8,82,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,2,128,8,1,64,4,0,2,128,64,1,72,0,41,0,0,0,10,0,0,0,1,74,64,1,0,0,2,128,8,0,0,133,33,65,1,64,
4,0,0,10,0,0,0,0,0,0,1,1,65,0,0,160,2,2,128,10,0,0,133,5,32,0,16,0,0,0,0,10,82,0,0,4,40,0,2,0,64,1,64,0,0,0,0,0,0,5,0,16,0,0,32,0,20,0,64,80,66,128,1,1,72,10,0,4,0,0,0,0,0,0,0,2,144,2,128,64,0,0,0,0,5,
0,2,130,20,132,40,0,2,0,0,0,0,80,8,10,8,0,0,0,1,64,4,5,0,2,128,66,128,64,1,65,0,33,72,10,66,144,2,144,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,10,0,0,20,2,0,10,0,0,128,0,1,0,40,0,16,20,0,1,72,0,5,0,128,2,128,0,
4,0,0,0,33,64,0,0,1,10,0,0,0,10,64,10,64,1,72,0,0,2,2,128,8,10,0,40,5,32,0,0,0,40,32,0,0,8,82,20,0,0,0,0,0,41,10,8,0,33,65,64,0,128,2,0,0,0,0,80,64,67,64,133,0,128,133,0,20,128,0,0,0,0,0,5,0,128,0,0,4,
5,0,0,0,32,0,0,80,1,0,0,0,0,0,0,0,0,16,164,40,0,2,128,66,128,64,1,64,0,128,0,0,0,0,0,0,2,144,0,80,10,80,1,0,32,32,0,0,10,0,0,16,20,0,8,0,0,0,0,0,0,1,72,0,0,0,10,0,0,2,0,80,0,0,0,82,0,1,72,10,1,1,64,4,
40,41,0,5,0,16,160,0,100,2,128,64,80,64,0,40,4,5,32,0,164,0,2,128,1,10,64,1,65,0,0,20,0,0,0,0,0,0,0,0,0,0,64,80,8,0,0,0,80,8,10,82,16,164,0,0,1,1,64,41,10,0,5,4,5,4,0,20,128,2,128,1,8,10,1,10,64,80,82,
0,0,0,0,66,128,0,0,0,64,0,0,133,5,33,64,0,0,1,0,33,64,0,0,0,0,0,0,0,0,66,144,0,10,0,0,2,20,0,0,4,0,0,10,8,0,0,0,0,0,0,82,148,128,16,2,130,128,1,0,0,0,64,0,0,128,160,0,0,0,133,0,16,0,82,0,0,0,0,0,0,0,80,
0,4,0,0,0,0,0,0,0,20,16,20,0,64,80,10,80,8,0,5,5,4,4,32,4,0,165,4,50,80,0,41,74,8,0,5,0,16,2,144,160,0,0,4,40,0,128,0,0,4,0,20,128,2,130,2,130,0,0,0,0,0,0,0,0,40,41,0,0,0,0,4,5,4,5,0,0,64,0,5,0,2,0,0,
40,4,0,160,128,160,0,64,82,128,0,0,0,8,0,40,0,0,64,80,66,130,0,10,80,64,0,0,2,0,1,72,1,72,82,2,144,0,82,0,0,0,2,20,133,0,20,16,160,0,0,0,0,0,0,0,0,4,0,2,20,16,0,80,10,8,0,0,0,66,130,2,144,0,0,33,72,82,
131,32,160,0,0,0,0,0,0,0,0,0,0,0,0,0,80,66,144,0,0,0,0,0,0,0,0,0,16,2,128,0,5,40,4,32,0,133,33,72,0,32,0,160,2,154,48,64,10,82,0,80,66,144,0,0,0,20,160,16,0,64,64,80,64,0,0,0,1,64,0,0,0,0,0,8,1,72,0,40,
0,0,82,0,80,64,0,5,32,0,0,0,0,0,80,1,72,1,64,4,40,32,0,2,20,128,0,8,10,1,1,64,5,0,2,0,1,65,10,0,0,133,0,0,10,0,40,0,133,0,2,144,2,130,0,0,5,32,33,162,0,64,80,0,33,64,33,64,0,0,0,5,32,40,0,128,128,160,
164,0,20,128,16,128,0,0,0,133,4,0,0,0,4,0,0,0,0,0,0,0,160,128,0,1,64,0,0,0,40,33,64,4,0,160,0,0,0,0,0,0,0,0,0,164,0,160,2,0,0,0,0,82,0,1,72,64,8,10,64,0,0,0,0,40,40,50,104,16,128,0,0,0,0,0,0,0,0,0,0,0,
0,0,80,1,0,0,0,0,0,0,0,0,0,0,0,20,128,0,0,0,0,0,0,0,0,0,0,0,0,0,1,65,0,0,0,82,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,72,0,5,4,5,0,0,66,128,0,0,160,0,1,0,0,0,0,0,26,0,16,16,128,20,2,148,0,66,2,20,0,66,
128,8,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,41,0,0,0,0,0,2,144,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,82,144,128,0,0,0,0,0,0,0,0,0,0,0,0,2,128,0,33,65,0,0,0,0,0,0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,0,0,0,1,65,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,10,64,0,0,0,80,8,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,20,2,0,0,5,40,32,4,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,5,0,128,0,0,0,0,0,0,20,128,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,10,8,0,41,0,0,0,0,0,0,0,0,0,0,0,
20,0,64,0,0,0,0,5,32,0,0,10,1,0,0,0,82,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,2,144,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,40,32,0,20,128,0,0,5,4,0,0,10,8,10,0,0,0,66,128,8,80,0,0,16,2,144,2,128,0,0,2,130,2,144,128,20,0,0,0,16,160,128,164,0,160,128,0,1,64,32,
0,0,0,40,4,0,0,0,0,0,0,40,0,16,0,0,0,0,0,0,0,1,64,0,0,8,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,2,144,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,160,0,8,0,0,0,0,0,0,0,0,0,
0,0,0,10,66,128,8,0,41,0,0,0,0,5,4,0,0,0,0,20,2,2,128,8,82,20,2,20,20,25,5,0,0,0,0,0,0,4,40,0,2,128,64,0,0,0,0,41,1,64,0,2,0,0,5,4,0,0,0,0,0,66,128,1,0,0,0,1,64,4,0,160,0,0,32,5,0,128,160,0,0,0,0,8,0,
0,0,0,0,2,128,0,0,0,64,0,0,0,82,0,0,40,0,128,0,0,0,0,0,5,0,16,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,10,8,0,0,0,0,0,0,0,0,0,0,0,0,0,0,164,0,0,0,0,0,0,0,160,0,64,10,64,0,0,0,0,0,160,0,1,0,0,0,80,0,0,0,1,
1,64,32,0,20,0,64,0,5,0,128,160,0,0,0,134,128,4,0,160,133,0,128,0,0,0,160,0,8,0,0,20,0,8,0,40,0,0,0,0,0,0,32,40,0,0,0,0,0,8,0,0,0,0,0,2,2,128,8,10,64,80,0,0,2,128,8,0,5,4,0,20,16,20,16,128,0,80,0,0,0,
0,0,0,0,0,0,0,0,0,0,0,0,0,0,133,0,16,160,133,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,80,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,41,0,41,10,64,10,0,0,2,20,0,0,0,0,0,0,128,2,128,64,82,0,1,64,0,133,33,
65,10,64,0,0,160,128,0,0,5,0,0,10,8,10,1,64,4,0,0,0,4,0,2,128,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,160,128,0,0,0,0,0,0,0,64,0,0,0,0,0,0,0,40,0,0,0,0,128,0,0,0,0,1,65,0,33,72,80,1,72,0,0,0,0,0,0,0,0,0,0,0,
2,2,144,0,10,0,0,16,0,80,0,0,128,0,0,0,0,0,0,160,0,0,41,8,10,0,32,40,0,128,0,10,0,5,32,0,0,0,0,0,0,0,133,0,0,8,10,66,144,0,0,0,2,144,160,0,0,0,0,64,10,0,0,0,0,0,20,0,8,0,40,32,32,40,0,0,64,1,64,0,133,
0,0,64,0,40,0,16,0,0,5,0,160,0,0,0,165,0,128,128,0,1,1,64,0,0,0,0,2,128,0,32,0,20,2,0,0,0,0,0,0,0,0,0,0,10,64,0,0,2,144,0,0,0,16,0,0,40,0,0,82,0,10,8,1,65,1,64,32,32,33,64,0,0,8,80,0,4,5,32,40,32,0,160,
2,20,0,0,0,0,0,33,72,0,0,0,1,72,1,64,0,128,160,128,20,0,8,80,0,0,0,0,4,40,0,160,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,66,144,164,40,0,0,0,0,0,8,13,2,2,128,0,0,16,160,2,2,128,0,4,0,2,128,
1,0,5,32,0,2,130,2,0,0,40,0,0,8,82,20,0,80,0,0,0,10,0,0,132,0,20,0,10,8,82,2,144,0,80,0,0,0,8,0,41,0,0,164,0,0,0,0,0,0,0,0,0,0,0,64,80,66,128,0,0,0,10,0,0,0,0,32,41,72,1,65,0,6,129,8,8,8,64,80,0,0,133,
0,0,0,0,164,33,72,1,64,0,0,64,80,0,0,16,160,16,160,0,0,0,0,0,0,0,0,0,0,1,72,1,64,0,0,0,0,128,0,0,0,0,80,0,0,0,1,1,10,0,0,20,0,8,0,0,0,0,0,2,144,0,0,0,0,0,0,0,0,0,0,0,0,0,0,40,0,0,10,64,1,10,64,64,80,80,
0,40,0,0,1,0,0,2,20,2,20,2,20,128,0,1,64,0,128,0,66,130,144,160,0,0,0,160,0,1,0,0,0,0,0,2,128,0,5,4,0,0,64,10,64,0,0,0,8,0,0,0,0,0,0,0,0,0,0,0,2,128,0,0,2,128,0,0,0,0,40,0,2,0,0,5,0,16,16,2,0,0,4,5,0,
0,0,0,0,64,0,0,0,10,64,0,40,0,0,0,0,160,0,0,0,128,0,82,0,1,64,4,40,0,0,0,0,0,0,4,0,0,1,64,0,0,0,0,0,0,32,0,0,0,0,20,128,164,40,4,0,20,133,0,0,1,0,0,0,1,65,0,0,0,0,0,0,0,0,2,148,0,0,0,2,0,0,0,0,82,128,
0,0,16,128,20,20,25,0,160,128,0,1,0,40,0,0,0,40,4,0,0,0,0,0,0,0,0,0,41,0,5,4,40,5,0,16,0,0,4,41,0,0,16,0,80,8,0,0,0,0,0,0,80,0,0,0,1,64,0,0,0,32,0,20,0,8,80,64,82,2,128,0,0,0,0,5,0,16,2,0,80,1,1,8,80,
0,0,0,0,0,0,0,4,40,0,0,0,5,4,0,0,10,0,0,0,0,0,16,160,16,160,0,0,0,0,0,0,0,10,8,64,80,0,0,2,128,0,4,40,32,0,16,2,128,0,4,40,0,0,0,0,0,0,0,0,0,32,40,0,0,0,32,0,0,82,0,0,0,160,0,10,0,0,2,128,0,0,16,160,2,
20,0,0,0,132,0,20,160,0,66,144,16,0,1,1,64,40,32,0,0,10,0,4,0,128,0,10,0,32,41,65,72,8,1,64,0,2,128,1,0,0,128,0,64,0,0,20,128,0,0,41,10,0,0,2,128,0,32,40,0,0,8,80,0,5,32,4,40,0,164,5,50,1,64,0,160,128,
0,10,64,64,80,10,0,32,4,40,41,0,6,64,0,160,16,0,1,10,8,10,0,0,2,128,66,128,0,0,0,0,0,0,0,0,0,0,0,20,133,0,16,0,80,64,0,5,4,0,164,0,0,10,0,0,160,0,8,0,4,0,164,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,128,
160,0,0,4,5,0,164,41,10,0,40,0,0,1,64,32,5,4,0,164,0,0,8,64,10,10,12,130,130,130,0,0,32,5,0,0,0,4,0,0,0,33,64,0,0,0,5,40,0,16,2,128,8,0,40,0,128,0,66,0,0,5,0,0,0,41,0,0,2,128,8,80,8,10,1,72,0,0,160,16,
133,0,160,0,1,1,64,4,41,0,0,160,16,160,0,1,10,1,1,64,0,16,0,66,130,128,64,66,0,80,0,0,16,0,0,0,0,80,80,8,10,0,0,0,1,0,0,0,1,72,10,1,72,1,64,0,0,0,32,0,0,0,32,5,0,2,0,80,82,144,2,0,8,0,40,0,2,2,130,20,
0,66,128,8,80,0,0,0,8,0,5,33,72,0,5,32,0,0,0,4,5,0,0,0,0,0,0,41,13,0,0,0,2,2,130,128,64,0,33,64,0,0,82,0,64,80,8,0,0,2,128,0,0,2,20,0,64,80,64,64,0,0,160,0,64,80,10,1,10,0,5,32,0,128,0,10,8,64,0,0,0,0,
5,4,0,20,0,64,10,80,8,10,8,0,0,165,32,32,52,0,4,0,128,160,160,16,16,0,0,0,2,148,2,0,8,10,0,0,0,0,0,128,2,130,2,128,1,8,64,80,0,0,0,64,10,0,40,0,2,128,1,0,40,4,0,0,64,82,20,2,0,80,82,144,20,132,0,2,20,
0,0,40,4,0,128,160,0,66,128,1,72,13,25,0,0,0,0,0,82,16,20,0,0,0,0,0,0,0,0,0,128,0,0,0,0,0,0,0,0,0,0,0,5,0,2,0,0,41,13,0,0,0,2,20,20,16,0,0,0,0,0,40,0,132,40,0,20,128,0,10,0,32,0,0,8,8,1,74,8,0,33,64,0,
160,0,8,10,0,32,0,160,2,20,2,0,0,32,5,0,0,1,1,64,0,2,20,0,80,64,10,0,0,132,5,0,160,20,16,160,0,64,8,80,1,72,1,0,40,0,0,1,72,82,20,2,2,20,0,66,128,1,0,0,165,4,32,5,5,32,33,0,0,0,0,5,0,16,160,160,0,10,64,
64,80,8,1,72,0,0,16,160,0,10,82,144,132,54,1,146,130,25,5,4,0,160,20,16,16,2,128,1,1,64,0,164,41,0,0,0,0,5,4,0,2,20,0,0,33,72,1,65,64,4,0,0,64,0,0,0,80,66,144,2,128,8,0,0,0,10,0,0,2,0,0,52,0,0,0,0,40,
32,0,16,2,128,0,5,33,76,148,16,20,0,1,65,0,41,1,64,4,0,16,128,2,148,0,66,128,1,1,64,4,41,0,0,2,128,0,0,0,64,0,0,0,1,0,40,0,0,1,72,0,40,0,2,20,160,128,2,0,8,80,82,128,0,32,4,0,20,2,20,128,0,82,130,20,0,
0,32,0,0,8,80,8,0,0,0,1,65,72,0,33,74,82,0,100,133,0,0,0,40,4,0,2,128,0,4,0,20,0,64,0,0,0,0,0,165,4,4,33,65,74,66,131,32,20,128,0,0,0,128,160,0,0,0,0,0,40,32,0,0,82,2,128,8,0,0,160,16,164,33,64,0,2,128,
1,0,33,65,1,64,0,0,0,0,2,0,0,0,0,1,72,80,8,0,33,72,83,68,5,0,0,0,40,32,0,2,20,0,0,0,0,0,41,1,65,0,0,20,128,128,20,160,128,0,8,10,1,0,0,160,133,0,0,64,0,5,0,164,0,16,164,0,0,0,0,0,1,10,1,10,80,66,128,66,
144,20,164,0,0,10,66,144,128,0,0,41,72,1,65,8,80,1,10,10,67,37,5,32,41,72,0,5,0,25,5,0,0,0,0,0,1,0,40,33,74,64,80,8,104,0,8,64,8,10,64,10,10,66,2,128,64,82,16,160,164,0,20,2,0,10,64,0,0,0,10,8,8,82,0,
80,10,1,1,64,0,128,0,64,80,1,64,32,0,2,131,37,0,0,0,5,32,0,164,0,0,0,0,20,128,0,0,0,20,0,0,0,128,0,64,0,0,0,0,0,0,0,0,0,0,0,133,32,0,217,10,0,33,72,80,0,0,0,0,0,0,0,33,74,0,32,5,4,0,2,0,10,64,1,64,0,2,
0,0,0,0,80,8,10,64,64,10,0,40,41,1,0,0,160,0,10,64,0,32,0,0,0,40,40,4,0,133,41,0,0,0,80,66,0,0,40,40,32,0,16,160,164,40,32,4,33,74,0,5,32,4,41,64,32,0,2,130,0,0,0,20,128,0,1,65,10,82,16,26,0,0,64,8,80,
0,33,10,0,0,133,32,4,5,5,0,0,8,0,40,0,2,0,0,40,32,5,32,0,20,2,0,0,0,0,0,0,20,2,128,64,80,8,100,20,160,128,0,0,0,0,0,32,40,0,0,0,0,0,0,4,0,160,128,0,0,0,0,0,0,0,0,0,0,0,0,16,0,0,0,3,64,20,128,20,16,160,
20,128,0,0,0,0,0,0,20,128,20,0,8,64,8,10,0,0,20,0,8,0,0,0,0,5,0,128,2,0,80,0,5,0,128,20,128,160,0,0,41,8,10,64,0,0,20,2,128,1,8,10,8,82,130,2,128,8,1,10,66,148,0,64,0,0,160,16,2,0,80,80,8,0,0,0,1,64,5,
32,0,0,0,0,133,0,20,3,37,5,4,5,0,133,32,0,0,82,2,0,0,0,16,0,0,40,41,0,0,0,1,0,0,0,8,80,10,0,0,0,1,0,0,0,66,144,20,0,1,64,0,0,0,4,0,16,0,80,0,4,41,0,0,2,128,1,10,0,5,33,72,0,0,0,64,0,0,164,0,0,1,1,64,0,
0,0,0,0,0,33,72,0,0,3,64,0,80,0,0,160,16,0,8,80,0,41,0,0,164,0,0,0,4,32,0,2,130,144,2,130,0,1,10,66,144,160,2,128,64,80,0,5,32,0,0,8,10,8,10,0,32,40,41,8,0,0,0,1,160,0,0,128,2,128,10,64,0,5,32,4,5,0,0,
66,2,128,1,72,10,66,2,130,144,0,82,0,80,64,0,40,40,33,64,32,41,0,0,2,144,20,3,32,160,164,0,128,160,0,64,80,1,1,64,0,16,164,0,16,160,16,0,0,40,0,128,0,10,8,0,5,40,32,0,0,1,0,0,0,64,1,74,80,1,0,0,0,0,4,
0,128,0,80,0,0,0,64,1,64,0,16,2,20,20,0,82,16,160,0,64,0,0,20,0,1,0,0,2,0,0,5,0,0,1,65,0,0,16,3,64,0,10,0,0,160,16,128,2,20,160,0,0,0,128,0,10,66,144,16,20,0,0,41,0,0,0,64,80,1,72,1,65,65,10,0,0,133,32,
4,41,72,8,82,131,32,2,128,0,40,33,0,41,0,33,77,0,80,0,4,33,74,64,10,66,16,165,0,0,0,5,33,0,0,20,128,20,16,16,160,20,0,0,0,0,66,128,10,66,0,82,144,20,128,0,10,64,10,64,66,2,130,130,16,160,20,128,0,10,8,
0,0,16,20,0,64,80,66,16,20,160,0,0,41,162,16,128,160,128,0,80,80,1,1,0,41,0,0,0,0,40,0,128,0,0,0,133,0,128,164,5,0,0,0,0,160,128,0,8,0,41,10,1,0,0,164,0,0,10,8,0,0,20,0,0,0,0,64,0,0,2,130,0,1,64,32,0,
0,10,0,0,160,133,40,0,132,32,0,2,128,0,5,33,1,64,5,32,0,16,160,20,2,0,66,128,80,8,80,1,64,0,0,0,0,0,8,10,64,10,66,20,160,128,133,5,0,0,0,0,0,64,1,65,64,4,40,32,0,0,64,82,0,82,20,0,64,0,0,0,0,0,0,1,65,
64,32,0,0,0,0,2,128,64,82,20,128,0,0,32,0,20,132,40,0,16,133,40,33,65,144,10,0,40,0,16,0,1,10,0,33,72,0,0,0,10,0,5,4,0,20,128,2,130,16,160,164,32,0,160,0,0,0,20,20,132,0,0,0,0,128,0,10,8,1,64,0,2,0,80,
0,32,0,20,0,0,33,0,5,0,0,80,8,80,0,0,0,0,40,32,0,0,64,0,40,32,0,160,16,0,0,0,160,0,0,5,41,1,8,80,0,0,160,128,0,1,10,0,40,0,128,0,66,148,128,0,0,40,0,0,0,40,0,0,0,0,2,16,20,160,2,0,80,64,66,128,0,41,0,
5,5,32,0,0,1,65,8,0,40,0,2,0,1,162,0,64,1,0,5,41,0,0,20,128,2,2,128,10,0,0,0,1,0,40,0,133,32,32,0,160,16,20,0,1,1,10,0,4,5,0,0,0,41,1,10,0,0,133,0,16,0,10,1,1,64,0,128,165,4,5,0,0,0,0,160,134,65,64,40,
4,5,33,65,1,64,0,0,64,10,0,32,41,0,0,0,0,0,2,144,0,0,40,4,0,0,0,0,0,0,4,40,0,0,1,72,0,0,0,0,41,0,33,64,4,5,4,5,0,0,0,32,0,2,144,164,40,0,0,0,4,5,5,4,0,0,8,80,0,41,72,1,72,0,0,2,144,0,1,72,0,4,40,5,32,
5,5,4,0,2,144,16,128,165,40,4,33,65,0,0,0,0,0,0,80,8,80,64,0,5,50,66,144,2,148,160,128,20,0,1,0,0,2,2,130,0,80,0,32,5,32,40,41,0,40,6,74,10,12,128,80,0,32,5,0,0,66,2,148,2,144,128,132,5,0,20,128,160,2,
144,20,160,132,0,2,130,20,16,16,0,10,8,0,40,0,128,20,160,133,0,0,0,0,2,0,0,5,0,0,0,41,0,41,0,4,0,20,128,0,0,40,0,16,128,2,130,144,160,0,0,0,0,1,10,0,33,64,33,0,40,5,4,0,164,41,0,0,0,0,0,0,0,4,0,20,0,80,
1,0,0,128,2,144,164,5,0,0,0,0,0,82,0,0,0,2,128,10,0,0,16,20,0,0,0,132,5,5,32,5,0,0,64,10,80,8,0,0,128,0,104,133,32,4,0,16,160,0,0,5,0,0,0,0,16,0,64,1,64,0,164,0,0,0,32,0,0,0,41,0,40,32,0,0,0,40,32,0,0,
64,1,72,10,80,0,0,2,130,2,16,133,41,72,1,0,4,40,5,0,2,128,82,0,82,144,128,0,10,8,64,8,0,0,0,0,40,32,6,128,0,0,0,0,164,4,0,2,144,20,133,0,2,128,0,0,128,2,0,1,72,0,0,160,2,130,25,0,160,160,133,0,133,4,40,
40,0,0,0,32,0,16,0,10,64,80,1,0,0,2,20,0,0,0,0,0,0,0,0,0,0,0,0,0,8,10,0,5,0,0,64,10,0,0,2,0,1,64,5,4,40,5,32,32,40,0,2,0,66,3,69,32,40,0,0,0,41,0,5,0,128,128,20,20,201,72,8,1,1,64,0,20,0,0,40,33,1,72,
10,0,0,160,128,20,16,0,0,4,0,0,0,0,0,64,80,0,0,0,64,80,1,8,10,0,0,2,128,10,1,0,41,64,4,33,10,10,12,20,209,10,82,0,1,65,8,13,0,10,64,80,64,0,0,0,66,16,0,0,0,20,128,164,0,26,5,0,2,20,0,1,8,8,80,10,64,80,
0,0,160,2,0,0,32,5,4,40,0,128,20,160,128,2,2,20,20,16,164,0,2,144,2,130,144,2,130,16,0,80,1,1,65,1,65,0,0,0,0,0,0,0,0,164,5,0,16,0,64,0,0,0,80,0,4,40,40,0,0,64,10,0,5,0,132,33,74,0,5,4,0,165,0,16,0,1,
64,32,32,0,160,0,0,0,2,130,0,0,5,4,0,2,2,130,2,0,0,0,2,20,2,144,20,16,0,0,0,2,148,0,8,1,0,40,0,133,5,4,0,0,1,10,0,32,0,0,82,0,80,1,8,1,64,40,0,0,0,0,133,5,0,2,0,10,1,64,0,0,1,0,32,40,0,164,0,160,128,0,
80,64,66,16,20,128,0,80,0,32,0,20,20,2,144,20,133,0,16,16,16,20,0,10,1,72,10,64,0,5,4,0,0,1,10,64,0,40,0,2,0,66,148,16,164,0,0,64,80,80,66,20,16,2,128,0,0,133,40,32,40,33,64,4,5,32,0,160,0,0,0,16,0,0,
0,128,0,1,64,4,40,0,2,128,1,0,40,0,0,0,32,5,0,0,64,80,80,80,1,144,80,1,64,0,128,0,0,41,0,0,2,130,0,0,5,0,132,32,5,0,0,0,0,128,0,0,0,133,4,0,160,0,80,10,10,8,0,32,5,0,16,160,0,64,0,0,20,16,2,0,80,1,8,82,
128,0,32,40,5,0,128,0,0,5,0,0,0,32,0,165,0,0,0,0,16,20,16,164,0,164,0,0,0,5,33,8,82,16,165,33,0,0,165,0,128,2,128,64,80,0,40,0,0,8,64,0,0,2,128,10,64,0,40,0,128,0,1,10,8,0,40,40,4,0,16,160,0,64,0,32,40,
0,160,133,32,0,0,0,32,5,0,2,130,0,10,1,0,0,0,10,0,0,128,0,0,0,0,0,0,0,0,0,2,130,16,160,2,128,64,80,0,32,40,0,0,0,40,5,32,33,1,74,0,40,0,133,32,4,41,72,0,0,0,0,4,0,0,1,1,64,32,0,0,80,64,64,10,10,8,10,0,
0,0,1,64,0,160,128,2,0,10,0,0,16,20,0,64,80,0,0,128,0,0,33,64,0,0,0,0,0,0,0,2,128,0,0,128,0,80,80,0,0,0,0,0,0,64,66,128,8,10,0,0,134,74,10,64,80,66,20,128,160,160,0,1,1,64,5,0,0,0,0,16,2,130,0,10,0,4,
0,20,16,160,128,0,0,0,128,20,2,128,80,64,64,82,0,8,0,0,0,0,40,0,128,2,144,0,0,4,5,0,0,0,5,32,41,0,0,0,80,8,0,0,0,0,4,5,0,0,0,0,0,8,80,10,0,32,5,4,0,20,0,0,0,2,128,0,32,5,0,164,41,0,0,0,66,128,1,0,40,0,
128,0,0,4,0,160,2,0,80,8,64,1,65,74,0,0,160,16,2,2,0,82,144,160,160,2,0,0,0,0,0,33,64,6,65,162,144,2,20,16,0,80,0,0,128,0,10,64,64,80,1,74,8,10,64,80,66,0,66,130,128,80,0,0,0,0,0,0,64,0,0,128,160,0,1,
0,4,0,208,32,4,52,66,20,0,0,0,160,128,0,80,64,10,8,1,65,0,0,2,128,0,0,0,8,0,0,0,64,1,72,10,80,64,10,0,0,128,2,16,160,0,1,64,41,0,0,16,0,0,32,5,5,32,0,0,10,0,0,0,0,4,0,0,0,32,40,32,5,0,0,0,4,52,64,67,69,
50,10,0,32,5,4,5,0,0,0,0,133,40,4,0,0,10,1,10,1,0,5,4,5,40,33,0,0,2,2,128,0,32,5,32,0,2,148,2,0,82,2,148,128,0,80,1,1,65,8,64,80,10,10,64,8,10,8,0,5,0,3,37,0,0,82,144,20,16,0,0,0,0,8,1,64,0,2,16,160,165,
0,16,20,0,66,0,1,65,72,10,0,0,0,0,0,0,0,33,10,0,0,20,128,2,2,2,148,16,133,0,16,133,52,0,4,0,166,72,82,130,20,20,128,0,66,130,2,128,66,20,20,160,0,8,0,4,5,0,0,64,10,66,0,80,1,65,64,0,133,0,16,0,0,5,32,
0,0,1,10,64,1,0,5,5,0,16,0,0,0,0,10,66,128,64,0,0,128,0,0,0,2,128,0,4,40,33,74,8,10,0,32,41,0,5,0,0,0,0,128,208,33,0,0,160,16,20,0,64,80,0,40,0,132,4,41,10,1,64,33,65,0,40,32,5,40,0,128,0,80,0,0,0,10,
8,66,128,8,0,40,5,33,10,1,1,0,52,1,64,4,32,33,64,5,0,160,16,2,20,2,20,0,0,0,0,0,32,5,40,0,16,2,128,66,0,10,80,1,8,10,10,0,0,0,0,0,128,160,16,164,5,4,4,0,132,5,0,16,0,82,128,8,0,41,0,0,20,0,0,4,40,32,41,
0,0,16,160,164,0,160,0,0,0,0,0,0,128,0,0,0,160,2,20,0,10,0,0,16,2,20,0,0,0,16,0,1,0,0,160,2,128,82,0,8,0,0,16,160,0,0,4,0,0,0,0,2,128,0,4,40,0,0,80,64,0,32,5,0,0,0,0,2,144,0,1,10,1,64,0,2,16,160,0,80,
64,1,0,5,0,160,2,148,128,25,0,160,160,0,66,128,0,0,0,0,0,3,37,41,72,8,1,74,8,66,2,128,1,1,74,80,8,80,66,0,0,4,41,64,40,32,33,74,64,0,32,40,0,2,2,128,1,72,0,4,0,160,0,8,10,82,0,8,10,64,10,82,20,133,32,
0,132,0,160,0,64,0,4,0,0,1,64,32,41,64,32,32,5,0,0,66,128,10,64,0,0,16,160,0,0,5,4,32,0,165,33,64,4,40,5,4,4,41,0,0,16,160,0,0,5,0,128,0,10,66,0,0,0,20,20,128,2,0,0,0,0,10,80,0,0,128,0,8,1,64,0,2,0,0,
0,0,13,16,128,160,16,2,128,0,0,128,0,0,40,4,0,0,82,20,20,128,0,64,80,10,64,0,32,0,20,0,80,0,33,72,10,80,1,1,74,0,4,33,10,13,16,0,0,0,0,0,0,0,0,40,0,2,2,128,8,64,0,41,0,5,40,32,33,65,8,80,0,4,5,41,64,0,
0,64,0,0,0,0,0,2,16,160,2,0,1,64,33,64,4,41,64,32,33,65,1,74,0,0,128,132,0,20,0,66,128,64,80,8,10,1,64,4,32,40,4,0,2,144,160,2,20,0,64,0,40,0,128,0,0,41,64,50,0,5,0,160,2,2,128,0,0,164,0,16,128,0,0,0,
0,80,0,0,128,0,0,0,3,64,128,0,8,10,64,0,0,133,40,0,20,128,0,8,0,0,0,0,0,0,0,0,209,146,20,0,1,10,0,0,0,0,0,2,130,0,10,0,40,41,0,0,16,160,0,8,80,10,1,8,80,10,1,1,64,0,0,82,0,80,66,130,2,2,128,0,4,0,20,0,
0,0,0,0,5,0,0,0,0,16,0,66,130,2,130,144,0,8,10,0,0,2,16,165,0,2,128,64,0,0,2,144,20,128,132,5,6,74,8,1,64,5,0,20,132,0,0,0,0,165,32,0,0,1,0,0,0,0,32,0,164,5,40,0,2,16,0,80,1,72,0,0,0,0,0,128,0,80,8,10,
0,4,40,4,0,2,128,0,0,0,0,0,0,1,72,64,0,0,2,130,128,8,0,0,2,2,130,2,128,64,1,65,1,72,80,0,0,16,128,160,0,0,0,20,128,0,66,128,0,0,16,2,128,1,0,5,0,16,160,20,0,0,5,0,0,8,0,40,40,0,0,0,4,40,0,0,1,64,4,50,
82,144,160,0,1,13,0,0,33,64,0,0,12,130,144,20,2,0,10,0,0,0,0,40,0,128,0,80,0,4,0,2,0,1,65,1,0,0,2,130,128,1,8,0,0,2,144,20,16,0,10,80,64,82,2,0,64,10,0,5,4,40,0,133,32,0,20,2,0,82,130,16,20,16,0,0,0,0,
1,10,0,41,0,0,0,66,20,16,20,0,0,41,10,66,128,80,66,130,16,3,64,0,1,65,8,0,0,2,128,8,0,0,0,0,32,5,4,0,2,128,8,80,10,1,10,64,80,8,80,66,20,0,8,0,0,0,0,0,0,82,0,10,12,128,10,0,0,0,0,0,0,0,0,0,1,64,32,0,0,
0,0,20,0,0,5,0,0,64,0,40,40,33,65,10,0,0,2,128,0,0,0,66,0,10,0,40,33,64,0,0,0,0,2,16,0,0,40,0,0,0,0,0,0,0,0,0,5,4,40,33,144,80,0,33,64,33,64,0,0,80,66,144,0,64,1,1,72,82,0,80,82,0,1,74,64,80,66,16,2,128,
82,0,82,144,128,0,0,0,160,0,0,33,64,4,0,0,8,80,82,2,144,20,164,0,0,66,128,64,8,64,104,2,20,16,160,0,1,64,0,0,64,80,0,5,32,5,32,32,41,10,0,0,16,0,0,0,0,0,40,32,0,0,80,8,80,0,0,0,64,0,0,2,0,0,0,160,0,0,
0,164,0,20,16,16,0,10,8,80,0,0,0,0,41,8,10,0,0,0,1,0,5,0,128,2,128,10,0,33,64,32,0,2,148,128,160,128,160,0,8,10,80,0,0,0,66,2,20,160,160,0,0,0,0,0,33,0,4,5,0,0,80,66,130,2,144,20,128,160,0,8,10,0,4,0,
160,132,0,0,1,64,33,74,0,0,2,0,1,1,1,65,72,64,1,64,0,2,128,64,10,1,0,5,0,0,64,80,1,10,0,0,0,64,10,0,0,0,0,0,160,16,0,0,0,20,16,164,0,160,128,133,33,10,104,166,72,104,128,16,160,0,0,0,0,0,0,160,0,66,20,
0,0,0,20,2,2,0,0,0,2,130,2,130,20,2,2,128,0,0,128,20,2,2,2,148,0,8,1,0,5,0,160,0,0,4,5,4,0,2,20,0,1,64,0,133,0,2,20,2,0,0,0,16,20,2,0,80,83,32,20,0,10,0,32,5,32,0,164,40,40,0,0,0,0,0,80,0,4,0,16,0,80,
80,8,82,2,128,0,0,16,128,0,82,0,0,0,2,130,0,1,1,74,1,72,8,80,64,82,16,20,0,1,0,0,20,16,20,0,8,10,0,4,5,0,0,64,10,66,130,144,133,32,41,72,64,10,0,0,20,16,2,20,164,41,8,82,128,0,4,32,41,1,64,41,0,5,0,0,
0,32,0,20,0,0,4,40,32,0,0,80,1,0,0,0,10,1,0,32,5,5,32,0,0,0,32,0,160,0,66,148,2,0,1,64,33,0,40,0,0,0,0,0,0,0,128,20,0,0,5,32,33,72,0,32,40,0,164,0,20,0,1,0,0,0,1,64,0,0,0,4,40,32,0,0,0,32,0,2,144,0,10,
8,10,10,0,0,0,1,0,0,20,128,20,0,0,0,2,148,0,0,32,0,16,3,64,0,0,0,0,8,10,66,0,80,66,130,0,8,10,0,0,0,8,80,10,0,32,32,0,165,32,0,160,0,64,64,0,0,20,20,128,2,128,8,0,40,6,65,64,0,165,32,41,8,82,0,0,0,164,
40,0,128,0,10,1,1,64,0,2,16,20,0,8,82,144,20,2,16,20,20,128,133,5,0,0,0,0,16,2,128,8,82,144,134,74,66,128,82,130,0,0,33,64,0,164,0,0,1,10,64,10,10,66,2,130,2,128,10,64,8,10,0,41,0,0,0,0,0,0,0,0,0,0,0,
128,2,128,64,0,40,4,41,10,0,0,16,0,0,0,20,0,1,0,0,2,130,16,2,130,144,0,1,65,0,0,160,128,133,0,160,0,0,0,128,20,128,2,2,130,130,20,2,2,130,128,0,5,32,0,0,80,1,0,0,160,2,16,165,4,4,0,128,20,16,2,20,128,
160,160,16,0,82,20,2,153,0,2,20,165,0,201,65,64,32,41,0,4,5,5,0,0,1,0,0,0,82,16,160,0,10,8,82,144,164,0,16,20,0,0,0,128,160,2,144,0,0,0,16,0,80,64,0,5,0,2,0,1,64,0,0,8,80,0,4,0,0,0,41,64,50,66,130,16,
160,208,32,0,16,20,0,10,64,1,72,0,0,2,144,2,128,8,10,66,0,8,80,10,1,64,0,128,0,0,32,40,0,2,130,16,160,0,1,0,40,4,5,5,32,0,0,0,0,2,0,0,5,4,40,4,41,0,0,0,10,66,2,128,0,41,10,0,0,0,8,64,1,64,40,4,0,2,2,128,
1,10,66,128,80,1,0,40,0,2,20,160,0,0,0,2,16,160,20,2,20,128,160,0,8,8,0,0,128,20,0,80,64,10,0,5,0,128,164,33,72,82,128,66,20,20,133,5,0,16,0,0,0,2,0,1,10,64,82,148,132,0,160,128,164,5,5,0,16,16,20,2,20,
0,8,10,1,72,0,0,0,82,0,0,0,16,20,0,8,1,64,0,0,0,0,164,0,2,2,128,64,0,41,72,8,10,8,64,10,82,0,0,4,40,0,0,0,0,164,0,0,82,20,0,64,8,10,8,0,40,0,20,0,0,4,0,0,64,10,1,72,8,10,8,1,64,4,0,2,128,0,0,0,0,4,40,
32,0,0,82,2,148,16,2,0,0,41,0,0,0,82,0,0,0,0,1,8,10,1,64,0,164,0,2,0,1,64,4,0,160,0,8,10,0,0,20,2,0,80,0,0,128,160,16,160,2,20,0,1,160,100,16,164,40,32,5,32,41,64,0,0,0,0,0,64,1,64,32,0,160,20,0,64,0,
4,40,40,32,0,0,64,1,64,0,0,8,0,40,0,20,0,8,0,0,0,66,128,1,0,5,0,0,0,0,0,0,5,32,32,5,33,64,4,5,0,0,1,0,0,20,133,0,0,0,0,2,0,0,40,33,10,0,4,40,5,4,40,32,0,25,41,64,5,0,164,32,0,0,8,1,64,5,0,2,2,130,0,0,
0,0,104,132,0,0,1,0,40,0,16,0,1,64,0,133,0,128,0,0,0,0,0,5,0,2,20,0,1,1,64,0,2,20,128,0,8,82,2,2,20,160,0,80,0,32,0,20,16,160,26,50,64,10,0,32,40,0,20,128,0,8,80,0,0,164,5,0,132,40,0,20,20,128,0,64,10,
66,128,80,8,10,0,0,0,0,0,2,2,128,8,0,5,0,20,16,16,20,2,0,82,130,16,160,133,32,0,20,2,20,0,0,0,16,2,148,0,8,0,0,0,1,8,0,40,5,0,160,128,0,1,64,0,128,16,133,5,0,16,2,148,128,2,20,2,0,80,0,0,0,64,80,0,32,
0,160,3,32,20,2,2,148,20,128,164,4,5,33,10,1,65,10,66,130,0,80,8,8,0,40,0,0,0,0,20,0,0,4,0,0,1,1,64,4,40,4,40,4,40,33,64,0,133,0,128,160,0,0,0,16,164,32,40,40,0,0,1,72,82,0,8,1,65,0,0,16,2,2,20,160,2,
128,1,0,0,0,0,5,32,0,165,32,0,0,0,0,0,66,128,0,0,2,128,64,0,0,160,0,8,0,40,40,0,0,1,10,0,0,0,0,32,0,164,40,0,2,20,0,10,8,1,1,65,10,1,72,83,38,74,1,65,10,1,72,10,0,0,128,160,2,128,0,6,65,10,1,64,4,0,2,
130,0,80,0,4,5,33,64,40,4,0,16,20,0,0,32,40,50,82,130,20,2,20,0,8,80,0,33,64,0,2,0,10,64,0,0,0,1,72,0,40,0,133,32,4,4,0,208,4,41,64,0,164,0,128,16,160,0,0,0,165,33,0,40,4,0,0,0,33,72,10,1,10,0,4,0,160,
0,0,0,128,20,128,160,2,20,128,2,0,10,80,0,32,41,72,0,0,2,2,144,164,4,0,16,2,3,96,0,0,0,2,2,128,8,0,40,0,160,16,128,160,0,0,0,160,16,2,128,0,5,0,2,0,8,80,0,4,41,64,0,0,0,0,0,0,0,0,64,0,0,160,2,130,2,128,
1,0,0,20,128,20,0,66,16,2,128,1,65,0,40,0,16,164,0,20,160,0,1,0,0,2,2,144,2,128,1,1,65,72,1,1,64,0,160,16,0,0,5,32,0,132,40,5,0,0,66,128,0,0,0,0,0,0,0,0,128,20,0,0,0,128,16,0,82,144,164,0,16,128,20,164,
4,41,65,65,10,0,4,0,0,0,0,2,148,132,4,6,128,32,0,0,0,0,0,0,0,2,2,128,0,0,2,16,20,160,164,0,0,64,8,80,1,1,64,0,20,16,160,160,2,16,2,144,16,0,64,8,13,130,128,64,0,0,2,2,128,0,0,20,133,4,0,160,0,1,64,0,16,
160,0,82,0,80,1,0,32,0,160,133,33,1,64,40,0,0,0,0,0,0,4,40,32,0,2,128,10,1,10,0,4,5,0,133,0,2,20,2,2,0,0,5,4,0,160,0,1,10,8,80,10,0,0,16,0,0,32,0,165,0,128,20,16,160,2,2,128,0,0,128,0,10,8,0,4,0,2,128,
0,0,0,80,1,10,64,0,0,128,160,0,0,0,0,66,128,64,0,40,0,16,133,32,0,165,4,0,160,128,164,0,164,5,0,16,0,0,0,165,0,24,5,40,32,0,133,0,0,0,0,133,0,0,8,82,2,128,0,33,0,41,64,0,16,0,66,128,1,0,4,52,0,0,2,128,
1,8,83,68,32,4,0,132,0,216,5,32,0,0,0,32,40,0,2,20,0,80,1,10,0,0,20,0,0,40,32,40,0,0,8,0,5,4,0,0,66,0,104,160,128,0,1,10,0,0,2,20,2,0,80,0,33,160,8,0,5,0,0,8,82,0,66,128,64,0,41,0,5,4,5,0,0,1,64,32,32,
52,0,0,0,8,0,32,0,165,0,0,8,1,64,0,0,0,4,32,5,0,0,0,4,0,2,130,128,0,4,0,0,0,0,16,0,10,0,50,0,0,165,4,40,0,0,0,0,2,0,0,41,1,64,40,32,0,128,2,128,1,0,0,0,80,80,1,128,82,130,2,0,10,0,0,0,64,0,40,4,5,32,5,
0,0,8,0,40,0,128,0,1,0,40,0,16,160,0,0,5,0,16,2,128,8,10,67,36,0,216,5,32,0,0,8,10,0,5,32,0,20,0,1,10,0,0,0,1,64,40,0,0,0,0,128,164,0,128,20,20,2,16,165,4,0,2,128,64,82,0,0,0,0,64,80,64,82,20,0,8,10,0,
0,0,8,1,64,33,65,10,8,0,5,4,0,160,2,128,66,144,0,1,64,0,2,0,1,0,0,165,0,0,8,0,5,0,2,20,2,2,16,208,4,0,160,2,0,0,0,164,41,72,8,1,72,10,0,0,16,0,1,72,12,130,128,1,74,1,1,64,0,0,0,0,16,160,201,10,82,128,
0,0,0,0,0,16,0,64,10,82,128,1,144,80,8,0,33,64,0,0,0,4,0,20,0,8,10,66,128,8,82,0,82,20,0,1,0,41,0,4,5,0,2,130,0,1,64,0,128,2,148,128,128,201,0,52,10,0,40,32,0,0,10,64,10,0,40,4,32,0,160,160,0,8,8,80,80,
1,64,0,0,1,1,8,80,80,1,65,0,0,20,128,16,208,0,0,1,0,0,0,8,0,5,0,128,20,133,0,160,128,20,16,2,128,64,80,1,0,0,128,0,1,64,41,1,65,0,0,20,128,0,1,10,1,10,1,64,32,0,16,2,130,16,209,10,0,4,5,32,0,160,0,0,0,
16,0,0,40,0,16,2,128,0,4,0,0,1,65,8,0,0,20,2,128,0,0,20,16,133,0,2,16,132,40,40,0,20,0,0,0,0,0,4,41,8,83,69,0,2,0,0,4,5,32,0,0,0,0,133,0,164,0,2,0,0,0,164,0,128,2,128,10,1,10,0,0,133,32,40,0,128,2,128,
0,32,5,32,0,134,64,52,1,65,72,0,0,0,0,40,0,2,128,64,64,10,80,0,4,4,5,5,0,128,160,20,16,0,8,1,64,0,160,2,0,64,0,40,40,0,0,0,4,0,0,0,33,64,0,0,1,72,10,0,0,16,2,128,0,33,64,0,2,2,2,144,2,128,10,8,1,10,0,
0,0,0,32,40,40,32,0,132,0,2,128,82,16,160,164,0,164,5,0,133,0,0,0,32,0,0,80,80,66,0,1,72,80,1,0,0,26,33,1,72,66,20,2,128,83,32,165,0,128,160,2,144,128,2,20,2,0,80,1,1,64,0,2,144,0,0,0,2,0,82,130,128,64,
8,0,0,0,0,0,0,1,64,0,2,0,1,0,0,164,0,16,0,80,0,40,0,0,64,1,10,10,8,0,5,0,2,0,0,0,25,4,52,1,64,0,0,0,0,0,0,0,160,0,64,10,1,64,4,0,2,128,0,40,0,128,0,0,0,0,1,65,0,4,0,20,2,128,0,32,0,0,8,10,64,10,0,0,0,
10,66,128,1,0,41,1,65,0,40,32,0,20,2,0,0,0,128,160,16,20,128,160,0,0,0,2,20,2,0,0,32,5,0,2,130,0,82,20,0,0,0,0,0,4,0,0,0,40,0,16,16,2,130,0,10,0,0,26,33,1,10,8,10,0,4,33,65,74,8,80,8,10,8,0,0,160,20,128,
128,0,1,76,130,128,0,0,0,0,4,40,5,41,1,1,72,0,0,0,0,32,40,0,160,128,0,0,0,0,0,4,0,16,0,1,65,74,8,64,0,0,16,2,128,80,0,4,41,0,0,128,0,8,10,0,40,0,16,164,0,2,128,8,10,10,0,4,0,165,32,5,0,0,1,64,0,0,8,82,
0,66,128,0,0,2,144,0,64,1,74,0,0,0,8,1,0,0,0,80,0,0,128,20,160,2,0,0,0,0,0,0,20,128,20,0,64,0,0,2,130,2,0,82,144,16,0,1,72,0,32,0,2,144,16,165,40,32,0,0,82,2,20,20,0,0,0,2,0,0,0,160,2,0,64,64,10,1,10,
0,0,2,128,0,0,2,144,0,1,72,66,128,8,1,64,0,2,20,2,0,0,40,0,164,0,128,0,80,0,0,160,0,0,4,0,0,0,0,0,0,0,0,10,8,66,128,64,1,64,0,128,0,66,130,0,0,40,40,32,0,2,0,1,10,0,0,20,0,82,131,32,16,0,8,10,0,40,0,128,
0,0,5,0,0,80,0,0,128,165,50,80,0,5,0,0,80,64,0,0,0,0,0,2,20,20,128,0,0,33,74,0,4,41,1,72,1,0,5,0,0,1,65,0,0,20,2,2,20,16,164,5,0,133,0,0,0,0,0,0,0,20,16,164,33,64,4,0,0,10,66,20,132,5,0,0,0,5,0,128,20,
16,0,0,40,5,32,0,2,20,0,10,0,0,0,64,1,1,74,64,8,66,128,0,0,20,0,10,64,64,0,40,5,50,1,72,80,64,82,20,0,66,20,128,160,0,0,5,32,0,0,80,0,0,20,133,4,4,0,0,1,64,0,0,0,0,0,8,82,0,0,0,2,128,8,0,0,0,64,0,0,20,
0,1,10,64,64,0,0,20,20,128,20,165,50,0,32,4,0,160,0,0,0,0,0,5,0,2,128,0,0,133,33,72,0,5,0,20,0,64,0,0,160,0,66,128,0,33,64,0,0,0,32,5,5,32,0,0,8,82,0,1,65,72,10,8,0,0,20,128,16,0,10,66,144,164,5,0,0,64,
0,40,0,0,66,128,0,32,5,32,0,0,80,64,64,1,64,32,0,0,0,5,0,128,16,164,40,0,20,0,0,0,20,0,0,32,5,0,0,10,0,0,132,33,64,0,0,0,0,16,2,128,0,33,72,66,148,0,64,0,41,10,64,0,0,164,0,160,0,64,0,0,0,80,8,0,0,16,
0,80,1,64,0,0,0,4,5,0,128,0,0,0,0,0,0,0,82,0,0,0,128,0,0,0,160,2,0,8,80,83,64,128,128,20,128,2,20,128,0,0,0,0,8,10,66,128,80,0,0,0,0,0,20,132,5,0,160,0,82,0,0,0,2,130,0,10,0,32,0,2,130,20,2,2,148,16,16,
20,0,0,0,128,20,0,1,1,64,4,0,20,128,0,0,4,33,65,65,64,4,5,32,40,0,0,8,10,8,0,5,0,16,160,0,8,0,0,164,0,2,2,144,20,2,144,2,0,80,1,64,0,0,0,5,4,5,4,32,0,160,20,0,0,0,16,128,133,5,32,41,0,40,4,0,160,164,33,
0,4,5,40,0,0,1,0,0,160,128,164,5,5,4,0,2,0,0,5,33,72,0,0,16,0,1,65,65,0,0,0,82,0,0,0,0,0,0,0,0,0,20,2,20,128,0,0,4,0,0,0,40,0,16,0,13,0,1,144,0,0,160,2,0,0,0,0,64,80,0,0,20,16,0,10,0,32,5,32,40,5,32,0,
160,128,2,128,0,40,0,128,20,128,0,0,0,0,0,5,4,0,160,0,0,4,0,0,66,128,66,128,0,40,0,128,133,32,40,0,201,74,10,0,0,134,136,0,40,0,16,20,16,133,0,160,128,133,0,164,0,0,0,0,2,2,144,160,0,0,32,32,41,65,10,
0,0,20,0,8,1,64,0,128,0,10,0,0,16,20,16,2,20,128,2,130,20,0,1,10,1,8,80,1,1,64,0,0,66,0,80,0,5,32,32,41,64,0,0,0,0,0,0,32,41,0,0,2,2,130,0,82,128,0,4,32,40,40,32,0,0,82,20,0,64,66,130,128,0,4,0,0,0,0,
0,0,40,0,128,128,16,20,0,0,0,20,0,64,0,0,0,64,82,0,80,0,0,0,0,0,0,10,0,40,32,0,164,0,20,0,10,64,10,64,10,64,0,0,16,160,0,0,0,0,1,64,4,0,128,2,128,64,10,1,64,0,0,66,2,128,82,0,1,64,0,2,0,80,1,64,32,41,
0,0,0,80,64,8,80,10,8,1,1,74,8,0,0,2,20,2,144,2,2,2,148,0,0,0,0,0,4,41,64,0,128,160,0,66,128,1,130,148,0,0,4,5,0,0,0,0,2,0,80,8,0,0,0,8,0,40,4,40,33,64,32,41,0,40,0,0,1,64,0,20,134,74,0,0,16,128,0,1,74,
80,8,82,16,2,128,8,0,0,0,0,0,0,10,80,8,0,0,128,0,80,1,0,0,160,2,0,1,0,0,160,0,1,0,0,0,10,8,0,41,0,0,0,0,0,164,5,4,41,0,40,40,0,128,0,80,66,128,0,0,0,8,10,0,0,0,8,0,4,40,40,0,128,160,0,0,0,20,16,16,160,
20,164,4,0,160,132,33,74,80,8,10,8,80,8,0,41,10,0,0,0,1,1,64,0,0,10,64,80,0,4,4,0,160,0,1,72,8,1,64,4,4,5,5,0,16,2,130,130,0,66,128,80,1,10,0,0,132,41,64,6,1,64,40,0,0,0,0,0,66,0,1,72,82,128,8,0,0,133,
0,0,0,0,0,64,0,40,0,16,0,80,80,80,0,33,1,64,0,132,33,72,10,1,64,0,160,2,0,0,32,0,0,0,0,2,128,64,80,1,10,8,0,0,0,1,72,10,8,0,5,32,4,40,0,2,144,133,0,128,2,128,0,0,128,0,10,1,0,5,0,0,80,64,1,72,80,0,0,128,
2,128,0,4,41,0,5,33,65,64,32,0,0,64,0,40,5,33,1,65,72,0,33,74,0,4,40,0,20,16,128,0,64,1,65,72,0,5,32,0,160,0,10,1,0,0,2,16,20,160,128,160,20,128,160,16,0,0,0,0,66,128,0,32,5,0,16,0,1,74,64,64,1,74,64,
82,16,20,160,0,0,0,132,40,0,20,132,32,0,160,160,0,0,33,65,1,72,0,0,0,0,0,20,128,0,10,64,10,66,20,0,8,0,5,4,0,160,0,80,100,166,128,0,16,0,1,8,1,64,0,16,160,2,130,0,10,64,8,0,0,0,1,64,0,128,0,0,0,0,0,0,
0,10,0,32,33,64,4,6,128,0,128,0,82,16,20,133,0,16,164,0,0,10,0,0,0,1,72,10,1,10,0,0,0,0,0,0,10,8,64,10,1,64,0,20,128,16,0,0,0,20,2,2,3,64,0,64,80,82,16,0,1,64,33,10,0,4,5,0,2,144,0,0,40,0,160,0,1,10,64,
1,0,0,160,133,0,164,5,0,128,128,160,2,128,8,0,0,133,0,0,1,0,0,20,2,0,1,160,1,0,0,160,16,0,66,144,128,165,5,33,65,8,1,64,0,2,0,0,0,160,128,0,0,0,16,20,2,144,160,2,0,0,0,0,1,0,0,20,0,0,0,2,20,160,160,16,
128,164,0,0,1,0,0,0,80,10,8,0,5,32,0,0,0,0,0,0,4,0,0,0,5,4,0,20,133,0,2,16,20,0,0,41,10,8,1,72,80,64,0,0,16,164,0,0,80,0,0,0,10,8,80,0,41,0,4,5,0,0,80,0,0,128,160,128,0,82,130,0,0,0,16,20,16,2,128,8,10,
0,4,40,40,50,10,66,128,0,50,10,1,72,0,0,2,144,2,128,1,64,0,16,2,130,0,0,0,0,0,41,0,0,2,144,0,0,41,0,0,20,16,128,2,128,1,0,0,160,0,66,128,82,20,2,2,130,2,0,66,128,64,10,10,10,0,0,0,8,0,4,0,2,128,8,82,0,
0,0,0,1,64,0,128,0,1,72,8,80,0,33,64,33,64,0,2,128,8,80,8,10,1,0,0,2,16,20,0,0,5,0,20,128,0,0,41,8,82,0,66,148,0,0,0,0,64,0,5,4,0,20,0,0,32,0,160,128,0,66,128,10,8,80,8,66,128,8,0,0,0,10,1,0,40,0,2,128,
0,0,16,2,128,0,40,0,0,0,0,128,164,5,0,0,0,0,16,20,2,20,20,0,1,0,0,0,8,82,2,128,0,32,0,20,2,2,130,2,128,0,0,160,2,20,16,0,0,0,160,0,64,0,40,4,41,0,4,5,0,0,66,2,128,80,0,0,0,8,0,40,0,0,1,64,50,1,72,10,82,
16,0,64,0,5,0,160,2,144,0,0,40,0,128,160,16,0,0,4,0,0,0,0,20,0,64,0,0,160,128,16,160,2,20,164,33,74,0,0,164,4,41,8,10,0,41,0,0,0,64,10,0,4,40,0,0,10,8,0,4,32,0,164,5,0,2,128,0,0,16,20,0,0,32,40,0,2,20,
160,200,40,4,4,40,5,0,0,64,0,4,0,0,0,0,0,8,80,80,0,40,0,164,0,0,80,1,0,41,10,0,0,0,1,0,5,0,128,20,0,82,0,0,0,20,0,1,0,0,160,128,0,1,72,0,0,16,160,2,20,16,2,128,0,0,2,128,0,32,41,0,5,4,5,4,4,40,40,4,0,
16,2,128,1,0,0,164,5,0,0,0,0,132,5,0,160,0,0,0,132,5,5,0,200,0,0,64,10,10,66,130,2,144,160,2,130,128,0,0,0,64,0,0,0,8,1,10,0,40,4,0,0,10,8,0,0,20,128,164,0,160,0,1,72,66,0,0,0,209,0,0,0,0,40,32,0,0,0,
41,0,0,0,8,10,66,144,2,130,2,128,0,0,0,64,0,41,0,41,0,0,20,160,200,40,0,128,0,0,41,1,1,72,1,65,0,0,0,1,0,40,40,0,0,82,20,0,80,64,10,1,0,0,160,0,0,33,65,0,0,164,0,2,128,1,64,0,16,160,0,66,2,144,160,2,20,
160,0,1,0,5,4,0,0,0,32,5,41,0,0,2,128,0,0,16,0,80,64,0,0,2,128,64,0,0,0,0,0,133,0,2,144,0,0,4,0,0,10,0,0,0,8,0,40,41,8,1,0,0,2,128,8,0,0,0,80,0,40,0,0,80,1,8,1,65,10,8,0,0,20,0,0,0,0,1,0,0,0,80,0,0,0,
0,41,146,0,1,64,0,164,0,2,144,160,0,64,0,5,32,0,0,0,0,20,0,0,0,0,8,66,128,1,0,0,0,1,65,10,64,1,74,100,20,16,20,0,8,8,0,33,74,8,1,65,0,0,0,0,0,2,128,0,0,20,0,8,80,10,8,64,1,65,64,0,128,20,2,0,80,0,0,20,
128,164,5,0,16,160,0,66,20,2,2,128,1,64,0,16,0,80,64,0,0,2,0,80,0,0,160,0,0,0,2,20,0,8,0,0,20,0,0,4,0,0,0,0,133,4,40,0,20,0,64,8,0,0,160,20,16,0,0,4,5,0,132,40,0,0,0,0,0,1,65,0,41,0,5,0,0,80,8,1,0,40,
0,164,0,201,64,40,0,0,82,0,0,32,0,0,80,8,1,64,0,0,1,0,40,0,0,0,0,0,0,0,16,160,16,0,0,0,2,128,8,10,0,0,16,0,1,0,0,20,0,8,1,64,33,64,0,25,6,138,64,66,128,0,33,0,0,160,160,128,128,0,0,4,0,20,2,128,0,0,20,
0,0,5,0,128,0,10,10,64,64,1,65,1,64,5,4,5,0,133,4,5,0,128,160,128,0,80,64,10,0,32,40,4,0,0,10,0,32,0,2,0,80,0,0,0,10,0,0,16,160,0,0,32,0,2,130,20,128,0,0,32,0,20,16,0,0,40,0,0,8,0,5,0,0,0,0,16,16,20,2,
0,13,16,0,0,0,0,0,5,32,41,0,0,160,20,128,2,16,160,128,160,160,164,0,128,0,0,0,0,0,0,0,1,10,0,0,20,0,66,128,8,82,2,128,0,0,0,1,10,8,1,64,0,0,8,82,0,10,0,4,32,0,0,80,0,0,0,0,0,2,128,64,64,0,41,72,64,8,104,
160,132,0,0,80,64,8,80,13,2,2,16,16,20,16,16,165,0,0,1,64,33,65,64,5,4,40,0,16,160,128,20,0,1,0,0,20,0,82,16,0,0,0,20,0,10,64,0,40,0,2,20,0,64,80,1,0,0,0,82,16,0,0,0,160,0,1,72,10,0,4,0,164,0,160,0,1,
1,65,10,64,64,10,0,0,0,0,32,0,20,2,0,80,1,0,0,20,0,8,80,8,82,0,10,8,80,66,144,2,128,64,0,0,20,0,8,0,0,134,138,64,1,0,5,0,0,80,0,32,0,0,0,4,33,64,5,32,5,32,0,0,0,0,2,130,131,32,165,32,5,4,0,0,0,0,0,66,
144,0,10,0,0,0,1,1,1,10,0,0,164,0,160,0,82,0,8,10,1,1,1,64,5,0,160,0,8,0,0,0,0,50,80,13,0,8,8,64,64,0,0,160,0,0,0,2,128,13,2,16,160,0,80,8,0,0,160,0,0,0,0,1,1,1,72,10,10,0,32,40,40,0,0,0,0,0,0,0,16,160,
128,0,8,0,40,0,16,160,0,0,0,2,128,64,10,64,0,0,0,10,1,10,8,1,65,0,0,20,0,64,0,40,32,32,0,0,0,0,2,130,0,0,0,0,80,0,4,0,2,128,0,0,0,0,0,128,0,64,80,80,0,32,5,0,0,0,0,128,20,16,160,0,0,0,0,0,4,40,0,0,64,
0,0,208,4,0,2,20,16,160,0,0,0,0,8,0,40,0,0,0,4,0,16,0,0,41,0,0,20,16,160,128,0,0,4,0,0,10,1,74,1,72,0,0,0,8,80,64,82,2,128,0,0,128,134,64,0,0,10,0,0,0,10,82,144,128,0,10,82,0,64,10,0,40,0,0,0,0,16,0,0,
5,33,64,4,5,0,160,2,0,80,0,0,0,8,80,64,0,0,2,2,144,160,16,20,0,0,0,0,0,0,0,0,0,0,80,0,0,16,0,0,0,20,128,2,0,80,1,8,0,0,160,0,0,0,0,0,0,2,130,20,16,0,0,40,33,64,0,0,0,0,16,2,20,0,10,0,33,0,0,160,0,64,0,
0,133,0,2,128,0,0,165,0,16,0,0,0,128,0,10,8,0,0,0,0,0,2,144,0,0,5,0,0,8,0,0,0,0,4,5,0,128,16,160,0,66,128,8,80,10,1,0,0,20,0,0,40,0,0,0,4,33,65,10,64,13,0,0,0,0,66,25,0,0,1,64,0,2,128,0,40,32,41,10,1,
0,0,0,1,64,0,2,128,8,0,0,0,1,74,8,1,10,0,0,2,128,8,0,0,0,66,130,0,8,10,64,10,8,82,2,130,128,0,0,0,64,10,0,0,20,128,0,10,0,32,0,0,64,80,0,0,128,0,0,41,0,32,5,0,20,128,0,80,64,0,0,0,8,0,40,32,40,4,0,20,
128,160,0,80,64,10,8,1,72,0,5,33,8,0,40,0,0,0,4,0,0,1,74,0,32,40,5,32,0,0,0,0,0,64,0,0,2,128,8,1,65,10,0,0,2,20,2,0,80,64,0,0,160,0,0,0,133,32,0,2,0,10,82,0,82,130,16,160,16,160,2,0,1,64,40,33,72,64,82,
0,0,32,54,0,0,0,0,32,33,1,0,0,20,0,10,0,0,160,0,1,0,0,2,144,0,0,0,160,2,144,0,8,1,65,64,0,0,0,0,0,0,0,16,133,40,32,0,0,64,1,10,64,10,10,0,5,0,160,128,160,128,2,20,20,0,66,128,64,83,32,165,0,0,64,0,41,
0,0,16,0,10,8,82,20,16,128,0,0,5,0,2,0,66,130,128,66,20,160,0,8,1,64,32,5,0,2,128,8,80,0,0,0,64,80,8,8,64,80,82,2,128,64,10,64,0,4,5,0,20,132,40,40,32,32,40,32,40,0,0,0,0,128,2,128,0,0,0,0,0,0,80,8,64,
80,0,4,0,160,20,0,0,4,40,32,0,0,10,0,0,2,0,10,64,0,0,2,2,130,148,128,16,0,0,0,16,216,0,0,0,41,0,32,32,33,0,0,2,130,128,1,64,0,0,66,153,40,0,164,0,20,133,4,0,160,0,0,0,0,80,0,32,0,133,5,32,40,0,0,8,0,0,
2,0,10,8,8,1,74,80,0,40,32,0,0,0,0,2,128,0,0,2,2,20,20,16,0,8,80,10,66,2,144,0,66,128,1,10,0,32,0,0,66,148,0,8,80,66,130,130,16,3,64,2,0,1,64,32,0,20,0,64,0,40,0,0,0,4,0,16,0,1,72,80,0,4,41,0,40,0,0,64,
0,33,64,32,0,0,0,5,32,40,4,0,0,0,0,164,41,0,0,160,0,0,0,2,0,0,0,0,0,40,41,0,0,20,128,20,0,1,0,0,0,0,0,16,20,0,0,0,0,0,0,16,0,1,77,0,0,0,0,0,0,16,2,16,128,20,0,0,5,5,0,0,64,0,0,0,80,0,32,40,0,0,0,41,0,
0,2,144,20,16,0,8,10,10,0,41,0,33,10,82,0,0,0,2,0,10,0,5,4,0,0,0,0,133,0,0,10,64,10,64,0,40,0,0,0,5,4,33,64,0,2,20,2,2,128,64,64,1,72,10,80,1,0,0,20,0,64,10,10,64,64,10,0,0,0,0,0,128,164,0,20,0,0,0,128,
0,64,1,64,0,160,128,0,0,0,0,8,80,64,64,1,64,0,0,1,1,65,64,4,0,128,0,80,0,32,5,4,5,0,0,0,32,0,16,160,16,20,2,128,0,0,0,80,0,0,16,128,0,80,0,32,41,0,40,0,16,0,0,0,0,0,0,3,96,0,0,0,0,0,0,2,16,128,0,0,0,0,
80,0,0,2,128,82,2,20,20,16,20,16,2,128,0,0,2,0,10,1,65,0,0,128,20,2,130,0,64,10,66,2,130,0,10,8,10,64,0,0,2,144,20,0,0,0,160,128,0,8,1,74,0,0,20,16,20,133,4,4,0,20,0,0,0,0,0,4,41,0,4,52,0,0,16,20,133,
41,10,8,66,148,2,2,0,10,0,5,0,128,0,0,0,0,8,1,64,32,0,0,8,0,5,0,0,0,0,0,0,0,20,132,33,74,10,66,2,128,1,1,65,72,80,1,8,80,0,0,0,8,80,64,0,32,5,0,164,41,8,10,1,10,0,0,0,80,0,0,0,8,1,10,1,0,0,0,1,72,1,64,
32,0,133,33,65,0,0,20,2,3,160,0,0,0,0,0,0,4,6,64,5,0,0,8,80,0,0,0,10,0,33,64,0,20,16,0,80,66,128,0,0,20,16,0,0,0,0,8,0,41,8,0,5,4,0,165,0,128,160,133,0,133,4,32,5,0,164,0,0,0,40,32,0,0,10,0,0,0,0,5,32,
0,0,0,0,160,128,0,0,0,0,10,66,144,128,20,0,0,40,0,2,16,26,0,132,0,20,0,1,64,0,16,20,0,64,0,32,5,0,0,66,128,8,66,128,0,32,41,65,0,0,0,0,41,72,64,10,80,8,8,0,40,33,64,0,164,0,0,1,0,0,20,201,74,64,8,8,1,
65,74,66,144,133,0,128,0,64,10,0,40,0,0,64,8,1,64,4,40,32,40,32,5,0,2,130,16,20,128,133,0,0,0,0,3,96,0,0,0,0,0,0,128,132,0,20,160,2,20,133,0,0,8,80,1,72,0,0,2,130,0,1,64,0,2,0,1,64,0,16,0,80,0,4,0,16,
2,128,0,5,0,160,128,0,1,1,65,8,80,0,0,164,0,160,0,0,0,2,144,2,16,165,0,0,10,64,0,5,4,40,0,16,2,20,0,0,4,40,41,1,1,72,10,0,5,0,0,66,153,5,41,0,0,0,82,0,80,0,4,40,0,128,133,0,128,160,20,2,0,80,8,66,128,
66,130,2,128,8,0,0,0,82,144,0,1,64,4,4,5,0,0,0,0,0,8,80,82,0,8,0,0,160,16,128,20,160,16,160,128,160,128,128,0,0,0,0,10,64,8,80,0,0,16,160,0,0,0,2,20,20,128,20,16,133,0,0,1,0,0,216,0,0,0,0,0,0,4,33,0,40,
41,10,0,0,2,128,8,10,0,0,0,0,0,16,160,0,0,0,16,0,1,64,0,0,8,0,40,0,128,160,0,8,80,80,64,66,128,0,32,0,2,144,2,128,0,0,2,2,128,10,0,4,0,133,0,160,0,8,80,0,0,128,160,0,64,64,80,0,0,0,0,50,10,80,64,80,80,
0,4,40,4,5,0,0,64,0,5,0,0,0,0,16,0,0,0,0,1,64,0,2,16,160,16,160,164,0,160,128,16,20,16,160,128,20,160,16,0,66,128,0,0,2,20,0,0,0,20,0,66,2,128,82,0,82,2,20,164,0,0,66,148,128,128,16,0,80,64,0,0,0,80,8,
0,0,0,82,0,0,0,133,0,133,40,4,0,133,0,20,16,16,20,208,0,0,0,0,0,0,0,132,33,74,0,0,20,2,2,130,0,10,0,0,0,8,10,0,0,0,0,0,0,0,0,20,0,8,1,0,40,0,2,128,1,10,0,0,132,52,8,10,64,64,0,40,0,20,16,160,16,0,1,64,
0,0,64,1,64,0,0,0,0,0,0,0,133,0,128,20,0,64,0,0,128,2,128,0,5,0,2,16,208,6,65,160,0,32,5,0,16,2,128,0,0,133,32,0,0,0,0,160,0,8,0,33,64,0,160,133,33,64,32,0,2,2,20,20,160,128,20,128,0,0,4,0,2,144,160,0,
0,0,20,16,16,0,64,80,0,41,10,66,20,2,148,128,2,2,0,0,0,0,1,65,72,1,0,0,20,0,66,144,0,0,0,133,5,0,16,0,10,8,0,5,63,255,196,0,33,16,1,0,1,2,5,5,0,0,0,0,0,0,0,0,0,0,17,0,1,208,16,32,96,160,192,64,80,112,
176,224,255,218,0,8,1,1,0,1,5,2,229,135,86,245,8,223,59,107,114,250,118,81,54,63,170,123,136,235,240,164,38,204,246,54,15,103,43,24,247,118,58,57,143,64,199,69,49,140,114,186,213,143,154,88,218,62,136,
67,2,28,178,10,222,136,127,255,196,0,20,17,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,224,255,218,0,8,1,3,1,1,63,1,127,254,255,0,253,255,0,250,123,63,255,196,0,20,17,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,224,255,218,0,
8,1,2,1,1,63,1,127,254,255,0,253,255,0,250,123,63,255,196,0,20,16,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,224,255,218,0,8,1,1,0,6,63,2,127,254,255,0,253,255,0,250,123,63,255,196,0,35,16,1,0,1,3,5,1,1,1,1,1,1,
0,0,0,0,0,17,0,1,64,80,16,32,48,96,112,128,33,65,49,144,160,255,218,0,8,1,1,0,1,63,33,237,167,82,49,141,201,184,135,163,215,12,112,22,140,115,180,235,167,180,49,233,229,195,213,159,24,51,6,227,167,182,
245,232,204,120,88,226,93,24,244,106,246,230,252,232,70,174,164,51,236,121,93,41,77,43,13,43,124,198,58,144,135,159,188,21,181,51,108,120,28,203,106,198,49,186,99,30,174,116,195,4,240,210,205,221,252,
239,103,51,194,92,186,157,106,149,149,194,24,211,22,90,211,193,205,134,214,201,140,124,206,151,132,33,132,167,104,99,63,116,47,216,216,59,11,134,61,34,144,196,177,232,239,17,120,199,176,55,13,243,169,
135,116,99,185,203,82,154,87,188,23,44,99,180,233,244,229,253,132,58,177,102,67,145,197,82,86,190,224,207,219,6,252,134,214,57,99,107,173,54,56,231,172,151,140,99,30,154,108,48,140,253,132,37,44,13,8,
111,99,104,198,53,208,216,112,152,250,145,212,148,164,33,12,51,218,171,138,58,211,24,207,221,132,49,204,116,253,208,231,53,113,228,33,190,177,235,7,154,177,140,253,201,49,140,107,161,11,71,67,12,67,113,
13,24,199,86,49,140,174,68,203,156,52,197,157,101,141,155,24,215,66,25,6,51,246,126,194,16,185,49,172,99,28,237,114,207,131,49,141,193,105,75,182,49,172,253,159,176,133,251,198,66,193,181,51,84,192,28,
185,217,94,138,109,33,12,11,108,97,235,147,99,43,131,175,137,61,29,140,253,199,177,140,118,211,115,27,35,153,140,99,30,193,92,153,165,48,148,238,236,103,238,56,135,75,123,165,113,199,70,253,200,49,159,
176,202,144,212,218,198,62,94,92,55,117,172,108,140,85,72,232,112,144,216,236,47,93,78,186,219,23,78,108,178,111,140,53,56,11,115,113,114,198,59,75,118,227,246,16,232,13,227,110,108,99,98,198,61,10,189,
230,150,108,99,26,207,221,12,1,14,70,61,49,222,109,50,70,198,59,8,67,57,92,89,200,240,56,167,10,199,82,16,179,110,93,135,78,33,13,166,60,214,155,158,163,90,96,171,192,218,87,160,186,126,194,16,240,87,
111,249,63,221,41,133,120,24,234,66,200,232,78,44,190,33,147,124,29,218,108,49,172,99,192,245,74,99,142,152,207,216,67,136,240,3,145,196,28,196,56,221,134,126,150,181,236,180,202,157,105,140,111,219,214,
59,76,61,121,219,90,105,93,213,222,198,245,232,230,134,166,142,195,194,139,166,54,100,44,13,245,167,59,29,196,33,203,94,86,221,142,50,156,238,61,142,194,27,105,163,31,22,118,18,183,206,231,145,219,77,
140,123,193,204,92,54,196,57,171,185,237,44,123,113,14,71,162,56,2,228,135,9,139,60,48,224,116,166,88,149,210,157,148,198,155,93,11,54,60,132,48,142,194,216,226,165,179,176,207,186,25,118,62,1,73,91,86,
58,156,172,118,49,242,119,152,204,189,174,184,231,145,141,163,194,99,72,112,49,159,189,45,183,112,175,102,122,51,27,2,241,233,14,195,160,31,153,186,217,16,134,231,123,30,250,240,27,11,231,136,198,27,72,
111,119,177,140,253,132,45,29,72,116,118,57,38,193,140,114,166,226,27,12,203,214,105,8,100,142,248,114,49,142,227,33,76,205,37,101,57,139,199,36,112,144,187,116,165,139,155,253,208,132,165,55,60,36,33,
8,67,10,114,59,88,198,49,227,57,233,126,114,49,220,231,141,105,200,235,76,81,177,188,118,28,102,234,87,137,142,134,83,242,52,140,99,91,70,58,177,140,99,24,232,225,200,106,238,99,103,94,165,91,55,82,22,
21,221,93,41,121,73,91,199,170,151,142,165,145,134,56,136,113,49,216,244,122,74,210,29,77,178,58,35,29,13,132,255,0,43,163,199,78,176,202,232,106,216,144,208,135,59,24,237,99,30,224,108,119,49,229,56,
30,10,236,166,210,205,230,99,138,119,60,181,212,234,103,53,107,135,99,214,217,92,3,196,115,57,146,202,156,12,112,132,55,26,16,184,99,179,243,90,99,30,98,253,140,99,163,183,242,126,110,118,157,36,134,29,
212,212,183,44,43,41,153,166,135,137,16,176,99,24,219,211,165,189,25,183,99,108,230,14,35,105,163,104,199,163,144,178,99,30,215,74,227,41,43,116,90,25,2,26,83,28,114,177,140,107,63,97,14,134,218,177,238,
20,191,120,203,215,141,141,209,98,114,49,185,99,27,39,153,172,253,192,185,118,49,140,124,221,182,174,4,176,174,199,6,239,54,60,31,152,3,18,115,58,49,181,167,158,210,232,133,139,24,217,55,4,56,200,105,
93,93,148,188,50,140,99,29,132,52,165,33,43,229,180,203,144,229,115,236,99,118,67,105,194,67,153,183,99,24,199,199,203,23,7,91,210,22,15,68,44,27,246,208,135,3,24,216,83,107,31,13,166,234,112,150,37,169,
130,108,13,78,128,198,221,142,0,134,159,154,177,140,99,24,232,109,88,249,3,108,109,52,49,12,108,158,146,216,16,216,113,16,134,159,156,236,99,24,198,50,190,40,244,118,253,232,228,33,112,234,67,90,90,144,
132,33,188,132,56,88,198,49,241,211,166,27,88,245,166,193,218,199,107,30,23,136,132,57,216,236,99,24,199,220,78,200,89,144,135,49,14,22,49,142,214,59,152,218,63,4,189,127,251,192,199,111,251,11,50,28,
46,97,182,60,164,182,33,14,148,113,255,0,55,214,82,156,238,218,126,202,243,86,187,41,212,24,198,62,76,112,16,235,5,177,163,29,27,10,127,154,87,169,157,193,233,166,56,229,167,33,112,114,177,140,120,222,
87,202,203,138,244,131,115,163,192,241,177,194,19,249,116,97,143,55,120,171,214,43,88,198,59,206,39,54,243,177,227,99,171,30,90,74,202,127,177,236,244,149,235,53,206,86,237,220,98,139,227,137,143,35,240,
187,143,49,143,192,199,109,119,61,16,208,132,62,47,118,177,199,16,133,227,24,198,49,222,112,210,191,22,188,71,43,210,217,95,105,58,89,12,51,226,117,175,144,190,10,199,87,71,193,223,129,30,35,243,37,252,
249,123,251,140,167,201,172,99,192,248,79,243,208,76,89,130,37,105,232,7,100,122,83,163,29,142,246,205,224,165,101,123,85,54,16,132,33,169,8,66,16,232,239,148,187,8,91,154,23,103,25,176,192,215,127,243,
17,78,54,49,209,140,175,123,122,209,150,127,52,121,75,35,123,28,37,118,255,0,37,41,28,173,43,28,67,216,43,215,72,67,115,24,243,215,173,211,129,196,190,201,75,39,153,209,143,124,46,235,226,6,9,197,82,241,
143,17,169,8,104,104,67,170,255,0,61,12,213,233,117,134,202,235,78,86,49,140,99,30,74,89,189,76,252,243,195,71,42,226,88,199,117,37,117,166,149,217,72,113,177,140,99,24,250,19,230,12,121,41,192,246,71,
152,135,153,215,171,49,140,115,12,99,212,159,43,58,179,24,199,194,171,128,115,164,58,177,8,116,183,115,31,15,167,154,83,146,153,151,99,24,240,87,228,90,113,215,44,199,71,136,135,138,127,124,214,156,78,
65,140,99,30,203,93,223,205,41,152,174,149,252,241,99,10,74,226,93,29,24,199,48,247,26,255,0,186,58,62,105,78,10,211,8,199,227,71,9,75,83,24,236,173,187,185,140,99,31,136,206,163,74,199,74,214,201,143,
85,166,36,248,21,140,174,164,33,41,244,129,41,43,173,63,52,172,166,247,205,14,197,83,185,214,187,41,9,88,202,104,245,87,193,223,3,50,21,167,230,180,172,123,11,180,237,79,68,165,219,131,174,98,189,211,
249,241,29,54,16,208,132,59,101,43,249,63,191,61,26,215,215,200,117,230,60,21,233,207,9,119,79,0,46,220,123,30,180,245,143,228,175,150,62,27,77,43,219,15,36,60,29,143,108,123,193,196,96,152,199,224,55,
184,49,143,206,5,217,171,24,199,221,31,28,99,28,193,8,120,21,125,77,197,144,241,230,63,24,157,146,154,87,179,57,2,16,208,135,65,119,87,228,199,229,98,237,241,71,71,217,223,10,62,2,115,237,185,13,13,12,
243,30,38,63,41,61,72,132,62,57,110,15,90,124,101,241,7,176,188,175,168,255,0,56,156,125,125,181,248,76,208,208,134,215,123,244,51,24,250,227,152,124,20,132,52,62,53,49,143,27,24,199,10,241,144,248,84,
133,195,216,152,198,58,49,208,199,4,199,38,252,176,234,199,205,171,177,190,254,95,49,222,225,205,134,14,159,0,189,65,234,14,134,213,143,252,45,60,201,199,190,186,231,94,140,198,49,143,252,1,99,24,198,
62,38,121,201,233,207,134,57,7,224,138,248,53,123,97,9,90,103,223,44,175,67,252,236,204,99,24,234,250,235,28,163,31,147,30,102,62,102,199,175,30,10,66,16,134,108,134,194,16,132,55,30,128,198,49,140,122,
201,143,113,191,158,214,249,177,193,88,198,56,103,87,71,123,241,75,24,221,144,227,103,251,14,87,115,163,163,24,199,99,238,15,122,56,205,140,99,181,182,126,103,99,29,88,198,49,140,116,99,24,198,49,249,
124,135,31,243,228,83,198,219,87,149,250,17,255,0,166,196,53,33,8,96,200,101,41,73,95,86,112,38,134,100,132,59,27,230,38,148,74,211,134,148,203,49,140,99,24,198,49,142,44,228,126,19,33,173,63,37,107,161,
211,72,88,16,218,198,49,216,109,252,211,246,28,15,35,230,15,85,118,177,140,99,163,171,181,234,95,147,242,126,104,236,253,132,56,8,120,81,229,4,58,153,8,66,16,248,161,183,99,211,216,198,50,181,140,99,24,
199,197,152,247,230,60,205,197,37,114,166,28,133,137,13,13,127,51,36,33,228,12,99,24,234,234,224,24,244,162,16,218,198,56,227,97,189,220,198,49,142,242,27,139,146,16,132,33,14,23,194,24,199,70,49,140,
121,30,86,224,213,142,140,114,110,124,134,194,26,156,44,99,163,24,241,24,247,194,88,198,49,142,198,56,58,66,16,149,213,213,213,216,234,234,202,215,32,91,55,15,9,8,67,128,159,155,63,103,236,253,159,188,
36,45,220,107,24,198,49,140,99,132,99,221,8,67,66,26,16,230,45,213,234,229,129,12,123,249,237,172,101,114,14,0,208,135,166,154,26,144,237,134,149,213,234,46,140,109,222,15,205,239,141,210,144,179,123,
11,219,158,182,90,61,61,143,191,27,136,116,114,208,135,185,59,24,217,23,164,44,12,83,242,73,102,198,193,209,249,201,197,27,136,67,194,8,66,16,241,210,27,136,67,83,152,233,174,37,211,243,228,18,29,64,179,
33,13,11,99,162,62,50,198,57,118,49,140,99,192,247,71,128,132,50,110,140,99,133,33,15,129,92,163,100,67,166,61,29,248,13,209,143,162,189,16,226,99,24,199,231,247,107,163,163,30,192,67,228,231,42,90,49,
143,202,206,140,127,224,37,41,43,79,162,15,135,156,137,243,9,184,248,128,237,76,99,24,198,49,140,126,62,99,141,111,156,89,8,114,57,50,16,248,141,140,112,7,59,24,244,39,231,23,46,245,227,228,119,226,103,
254,53,49,140,99,31,117,53,60,37,143,134,30,30,66,31,244,25,142,65,195,27,77,72,124,214,226,216,253,158,109,167,76,123,123,31,87,116,118,49,143,196,102,16,248,109,143,107,62,21,123,11,24,252,44,96,79,
156,223,167,88,199,70,233,140,124,132,135,210,204,125,237,186,126,200,16,225,33,11,67,231,67,99,30,22,49,194,212,58,195,24,232,250,163,149,99,182,146,154,214,87,164,155,8,122,109,49,148,196,16,227,173,
197,35,162,176,252,148,94,59,29,24,198,49,140,126,79,33,14,118,49,142,20,216,119,83,223,157,29,173,179,192,236,117,114,36,33,242,123,204,89,155,8,66,16,132,33,15,156,77,237,195,24,198,58,58,49,213,213,
181,117,33,169,149,33,8,120,233,169,220,72,67,188,155,8,66,28,68,33,8,124,14,198,49,193,154,16,216,66,29,73,227,99,24,198,49,218,66,29,245,234,231,80,99,220,24,198,63,39,31,36,144,176,33,180,135,136,16,
208,134,167,180,186,49,140,99,27,146,24,214,240,132,52,51,111,167,144,134,132,33,122,66,24,215,52,221,158,234,198,54,38,173,131,27,55,225,226,16,234,142,142,46,187,233,70,87,174,158,82,66,25,50,31,49,
49,148,149,140,99,30,102,205,142,175,19,215,72,120,121,220,24,235,90,234,202,244,39,5,77,164,33,161,169,13,78,2,16,132,33,106,67,132,208,178,52,46,200,66,23,76,99,24,198,192,133,241,219,206,156,66,253,
222,199,132,132,33,209,79,39,120,88,198,212,225,33,11,162,22,100,33,8,66,16,132,44,136,89,186,49,140,118,54,6,230,224,181,56,142,114,27,78,99,83,97,8,67,66,16,132,33,8,109,33,8,66,27,143,7,116,99,29,140,
99,41,194,199,107,29,29,148,142,149,213,187,56,24,198,49,140,99,177,224,118,49,227,120,141,133,231,238,167,3,24,198,49,140,99,24,224,200,106,66,27,14,83,67,67,83,82,244,222,66,16,134,132,33,8,66,16,132,
33,8,66,16,134,165,147,163,24,198,49,140,99,24,199,14,92,156,228,52,33,13,13,149,175,3,24,199,138,146,188,44,99,24,198,49,140,99,24,198,58,24,198,59,3,100,198,55,108,99,24,198,58,140,99,24,198,49,140,
99,24,198,49,140,99,24,198,49,140,99,24,198,58,183,44,99,24,198,49,140,99,24,198,49,140,99,168,198,49,140,99,24,198,49,142,142,163,24,198,49,216,199,156,209,141,147,24,198,49,224,99,24,198,58,49,140,99,
24,237,6,49,142,134,49,140,99,24,198,59,28,11,24,198,54,110,230,49,140,99,102,199,87,115,24,198,49,142,163,29,143,59,165,125,1,140,117,99,24,198,49,140,99,24,199,81,142,140,99,24,232,198,58,58,60,110,
214,49,220,248,253,61,181,140,127,242,182,67,117,122,33,255,0,56,8,67,0,110,167,66,33,237,71,158,16,245,166,62,42,226,107,121,95,148,203,179,223,141,75,178,16,241,3,211,139,55,52,100,136,112,151,102,202,
248,81,8,121,185,118,99,78,23,185,16,241,211,202,201,92,41,118,108,33,189,209,140,108,93,244,241,118,60,132,58,53,112,36,48,117,182,48,228,52,116,254,236,53,175,202,230,89,142,198,49,143,1,115,95,60,114,
212,229,33,206,198,204,132,33,195,90,237,167,1,226,78,143,144,152,119,90,195,97,188,226,44,205,43,182,178,157,116,132,33,165,114,79,93,120,155,98,16,228,47,91,50,212,232,166,210,16,232,142,140,123,133,
123,61,54,16,132,37,118,151,245,148,224,113,44,118,56,6,60,100,173,14,51,197,171,96,199,4,67,82,220,184,165,49,181,209,221,91,22,54,108,101,116,52,166,142,247,70,49,225,99,198,242,211,74,242,49,185,120,
92,75,171,208,41,181,235,229,203,198,198,49,143,17,122,113,49,140,99,173,37,117,120,220,75,98,223,157,178,154,49,233,38,234,114,49,141,139,24,198,193,226,112,44,99,24,198,201,141,171,24,226,235,210,206,
214,67,5,76,43,141,50,21,177,52,59,141,123,155,28,43,132,33,196,222,58,187,216,199,107,207,77,88,198,59,77,72,67,179,215,196,169,202,115,155,30,180,106,67,150,188,20,166,192,74,232,82,146,180,199,83,59,
91,166,61,216,188,99,112,199,18,89,215,128,184,33,205,88,124,30,199,8,97,14,164,216,154,153,226,16,132,60,125,216,232,199,157,225,33,8,67,162,61,84,246,151,105,8,109,33,11,242,194,151,134,116,222,109,
44,27,86,49,188,99,24,199,176,83,16,226,76,67,171,97,78,194,199,70,49,194,154,54,133,193,13,135,130,49,199,177,190,175,53,114,21,209,141,219,24,198,49,142,172,121,24,198,61,37,142,246,49,189,58,203,102,
67,132,218,66,224,148,167,1,178,188,84,224,173,141,101,48,204,167,19,198,198,49,140,99,24,199,70,60,164,55,177,229,99,104,105,92,67,185,143,66,164,175,69,33,13,11,26,118,7,70,49,142,214,60,78,250,96,8,
99,91,199,3,76,179,43,128,33,8,90,16,133,245,101,37,114,140,120,72,67,99,27,86,49,180,175,33,196,92,58,55,21,224,53,33,12,73,161,225,53,196,151,143,19,27,118,49,141,155,30,2,27,222,154,117,226,30,60,235,
91,67,16,116,50,26,144,246,150,49,181,108,217,93,141,139,27,186,219,155,8,89,144,208,185,172,33,173,125,169,208,196,83,82,28,173,129,194,67,14,227,169,186,181,197,59,207,9,113,46,13,228,165,137,176,229,
175,37,97,188,228,51,212,216,228,152,198,62,42,198,61,129,141,139,136,50,53,148,166,234,122,157,119,49,219,75,226,16,229,174,88,135,41,114,100,41,230,37,179,29,205,249,13,196,45,94,138,198,56,53,48,68,
53,175,25,195,95,28,174,69,142,142,105,140,99,27,114,29,154,150,76,124,81,202,16,135,90,114,108,173,113,236,99,24,241,49,218,198,50,151,53,209,140,124,20,132,54,154,144,225,58,61,97,128,58,201,128,37,
46,107,8,103,222,152,94,82,61,85,179,166,227,178,211,66,242,184,131,17,78,158,231,29,239,118,99,163,104,98,107,214,43,208,216,199,24,198,224,203,56,71,4,219,214,186,186,182,206,85,237,109,149,101,37,101,
50,228,48,167,3,24,230,206,135,78,189,90,117,58,118,38,49,232,53,216,241,60,181,225,33,8,94,190,95,78,190,238,174,77,227,165,137,220,107,234,12,121,152,221,23,174,13,140,99,128,99,210,222,3,182,29,214,
156,164,33,13,75,218,228,30,160,67,125,125,145,141,129,97,91,186,113,144,236,116,216,67,87,48,245,218,225,136,111,48,53,242,202,70,49,204,87,195,169,211,75,247,163,24,39,208,235,210,88,229,107,146,167,
194,148,235,6,61,239,167,136,60,69,251,136,101,43,214,105,217,41,107,90,244,83,6,110,99,163,24,242,144,232,133,155,210,94,134,94,16,132,58,203,229,117,209,140,117,112,71,177,86,49,237,47,21,105,241,29,
110,24,198,57,211,142,184,195,13,77,107,41,205,95,82,33,182,182,134,246,61,2,189,82,188,239,72,45,105,223,43,148,175,86,112,213,210,149,227,175,199,116,234,4,37,58,179,231,36,33,202,96,169,176,232,108,
99,27,50,16,135,17,184,132,60,45,238,172,99,24,199,197,72,66,210,158,38,218,22,238,29,226,112,77,235,163,28,33,14,2,27,91,22,49,230,33,231,108,99,213,88,199,70,61,137,192,16,240,154,89,182,143,180,144,
133,197,122,25,184,193,63,11,16,135,90,174,230,49,142,173,163,24,198,56,218,98,14,142,115,151,21,234,245,193,153,243,163,154,82,194,189,56,132,33,192,114,16,192,87,200,30,99,197,200,91,144,202,215,228,
242,23,180,181,56,200,116,42,247,179,203,28,73,244,243,30,161,255,218,0,12,3,1,0,2,0,3,0,0,0,16,16,0,0,2,0,32,2,73,0,144,8,0,128,0,32,2,65,0,0,72,36,16,8,32,2,1,0,128,73,36,146,73,32,146,64,0,146,0,0,
0,72,32,146,8,32,16,1,32,146,9,4,144,9,36,146,72,4,144,64,4,130,72,0,16,72,36,2,64,4,130,64,0,130,72,32,18,64,0,2,9,36,2,72,0,0,1,4,128,72,0,18,73,36,0,8,4,0,72,4,130,72,36,0,9,36,130,0,32,2,64,36,146,
8,36,146,73,36,144,1,36,0,9,32,146,0,0,2,8,36,16,8,0,2,73,4,128,8,0,2,72,4,2,1,36,18,73,0,18,64,4,146,64,32,2,8,0,128,0,32,0,72,0,2,8,0,0,0,4,130,65,4,130,73,0,2,8,4,144,73,36,128,9,36,144,73,32,2,73,
36,0,64,32,0,64,4,0,0,4,18,64,0,128,64,36,16,72,4,144,73,0,144,9,36,2,73,36,144,72,36,144,0,36,144,73,32,128,73,32,2,1,0,18,65,0,16,1,4,16,73,36,146,9,36,0,0,32,130,73,36,144,9,36,16,8,36,146,73,0,2,0,
36,2,0,4,0,73,4,2,0,36,0,0,36,2,0,36,0,9,0,0,0,0,0,0,32,16,1,0,146,73,4,2,73,36,0,73,32,18,1,0,16,72,0,128,72,32,146,73,0,0,0,4,128,9,32,16,64,0,2,73,36,0,8,36,144,64,32,16,72,36,146,64,32,146,73,36,0,
0,32,2,1,36,18,9,0,146,9,4,144,0,0,144,65,32,0,9,36,128,9,32,0,0,36,146,64,0,146,65,4,128,65,36,18,64,0,144,8,36,128,72,36,16,1,4,144,64,0,0,64,36,18,9,36,146,73,32,18,73,0,2,72,32,2,0,4,146,64,4,144,
73,32,16,73,0,0,9,0,0,0,0,0,0,4,0,8,0,130,9,32,16,73,36,144,1,36,0,72,36,146,73,0,2,0,36,146,9,36,128,0,32,128,73,32,146,64,0,0,73,0,18,9,32,18,72,4,128,0,36,18,64,0,18,73,32,0,64,36,146,0,0,16,65,0,146,
64,4,146,65,4,128,9,32,2,0,32,130,8,4,130,64,4,146,0,0,16,1,4,130,64,36,2,73,32,144,1,36,146,64,0,0,1,4,130,64,36,144,65,32,128,65,0,16,64,4,128,64,32,0,8,4,128,1,36,0,73,36,144,0,36,144,1,36,146,1,0,
2,9,0,18,1,32,0,0,4,146,0,0,16,8,32,2,64,32,2,73,0,2,0,36,146,8,0,144,0,0,16,72,4,16,64,4,18,73,32,0,0,36,2,0,0,128,64,0,128,64,0,146,64,0,2,65,4,144,73,36,130,72,4,146,0,0,146,64,32,144,0,32,128,0,0,
2,65,36,128,8,36,144,9,36,146,65,36,130,72,36,0,8,36,146,73,4,18,8,4,2,72,36,128,0,36,0,1,0,130,73,36,146,64,32,18,73,32,0,72,0,144,0,36,146,8,0,0,64,36,146,73,4,128,64,0,0,64,4,0,0,0,18,73,36,144,64,
36,146,73,32,18,72,0,130,0,0,128,65,36,128,72,32,130,72,4,18,73,36,18,73,36,2,0,0,0,72,36,146,72,0,0,65,36,130,72,0,130,73,36,2,72,32,0,73,32,0,73,4,130,65,0,128,8,4,146,73,0,146,64,0,0,9,36,128,0,4,128,
64,0,0,9,0,146,1,36,144,64,0,16,72,32,18,9,0,146,72,32,130,73,32,18,1,36,18,8,0,144,65,0,18,1,0,146,8,36,146,9,4,144,72,32,130,73,32,0,64,36,146,72,4,0,9,0,2,73,32,130,65,36,144,65,4,146,0,32,18,0,0,0,
65,4,16,9,0,18,72,4,0,8,32,18,72,36,18,8,32,0,9,0,130,72,4,144,8,4,0,72,0,144,72,0,130,0,0,18,8,0,18,72,36,144,72,36,0,0,36,128,8,36,146,73,32,18,73,0,146,9,36,130,72,0,130,64,36,146,72,32,0,64,4,146,
64,0,16,73,0,0,8,0,2,73,0,16,0,4,128,65,0,2,0,36,146,0,4,2,73,36,0,9,4,0,1,0,18,65,36,146,73,0,128,1,36,146,0,0,144,1,36,130,1,36,0,64,0,0,73,36,128,73,36,128,9,32,0,9,32,128,9,32,16,65,4,146,8,4,130,
73,36,144,8,0,0,8,36,144,1,4,2,73,36,146,72,0,18,9,36,146,8,36,0,0,0,18,9,36,18,8,36,16,8,4,146,73,36,130,73,0,16,73,0,0,0,4,144,8,4,130,0,36,130,0,32,144,73,36,144,0,36,0,73,36,144,73,0,146,1,36,144,
72,36,130,0,0,18,0,0,2,65,0,2,0,0,16,73,0,0,64,32,0,64,0,18,0,36,144,0,4,18,9,4,128,64,36,16,1,4,146,65,0,2,65,32,0,1,0,128,65,36,16,9,4,16,8,32,146,0,36,146,73,0,128,65,32,146,65,0,2,1,0,130,64,36,130,
64,4,146,73,0,18,73,36,144,65,36,16,64,32,144,64,0,130,73,32,146,64,0,18,73,36,144,1,32,2,0,36,146,1,0,2,73,0,128,0,0,2,64,4,146,8,0,0,64,0,0,0,0,18,9,4,130,8,4,146,65,36,144,72,0,0,0,4,130,73,36,144,
9,0,146,72,0,128,64,0,0,9,32,144,72,0,2,0,32,0,8,0,18,72,36,16,8,36,2,65,36,18,72,0,18,0,4,128,8,0,144,73,0,146,65,36,144,8,0,0,65,36,16,9,4,144,64,0,146,0,0,0,8,0,146,65,36,144,1,0,144,8,0,2,8,4,128,
8,36,2,64,4,146,0,4,144,9,32,144,8,0,0,65,36,130,73,4,146,0,36,144,9,4,16,73,36,130,72,36,128,73,36,2,72,32,2,8,4,0,64,0,2,73,4,144,1,32,0,1,36,144,73,0,2,73,0,0,64,4,146,0,32,16,64,32,130,1,0,0,0,0,144,
65,0,146,73,4,146,72,4,0,0,36,18,8,4,16,8,4,0,8,36,130,73,0,2,9,32,146,64,4,0,8,0,0,64,4,128,72,32,18,64,0,146,64,32,2,1,0,128,0,0,0,72,4,2,64,32,128,73,32,18,0,4,144,0,0,2,9,32,0,64,0,144,0,0,0,64,0,
2,0,36,128,64,0,144,0,32,2,73,32,18,0,4,2,73,36,18,72,32,2,72,36,130,64,0,0,73,0,18,72,4,146,64,32,2,64,4,2,73,0,0,8,0,16,1,36,130,0,36,146,73,36,0,73,36,146,73,32,146,73,36,18,72,0,0,72,32,0,1,0,0,64,
4,146,73,36,146,0,36,128,9,0,2,1,36,128,65,36,18,64,0,0,9,36,144,9,32,2,72,36,146,1,36,0,0,36,0,73,36,146,9,4,146,64,0,128,0,4,130,8,0,146,0,0,146,73,0,0,8,32,144,72,32,16,73,36,146,65,32,2,8,0,146,1,
36,2,1,36,0,65,0,146,64,0,144,8,0,18,0,32,128,9,36,0,0,0,0,73,36,18,73,32,18,9,0,128,72,0,130,0,32,128,1,32,144,8,4,18,73,32,128,65,0,2,1,0,16,64,0,144,64,0,146,0,36,18,73,36,146,73,32,0,1,32,16,0,0,2,
1,0,2,73,32,18,73,0,128,73,36,146,72,4,2,0,32,0,9,32,0,0,36,2,8,0,128,73,0,144,1,0,0,64,0,16,9,4,130,9,32,130,73,0,130,8,0,128,1,36,130,72,32,144,73,0,0,9,0,2,65,4,0,8,4,2,64,4,146,1,4,130,73,32,0,73,
36,0,8,0,18,9,36,128,0,4,146,1,0,16,9,4,144,1,32,146,64,0,144,64,0,16,0,4,146,73,36,146,0,4,144,8,4,16,1,0,16,9,4,128,9,36,128,8,0,18,72,32,16,0,0,2,1,4,146,73,32,18,73,32,144,1,36,146,0,36,130,73,32,
0,72,36,130,73,4,18,1,32,144,0,32,146,73,36,144,72,0,146,0,4,18,1,4,2,73,36,130,8,0,130,9,36,146,0,0,2,64,36,146,1,0,0,65,0,2,73,4,146,64,36,0,0,36,128,9,36,130,0,0,146,65,0,16,1,36,144,72,32,18,72,36,
18,73,4,144,64,4,146,64,0,144,72,0,16,73,36,0,1,4,146,0,0,16,1,4,16,0,32,18,0,4,0,65,32,144,1,36,144,1,0,0,73,0,0,72,32,146,0,4,146,64,4,16,72,4,144,8,0,146,9,36,130,8,0,0,8,0,0,0,36,144,73,32,0,0,0,2,
72,36,144,72,32,16,0,32,18,0,0,16,8,32,18,0,0,2,73,36,144,0,0,146,65,32,18,1,0,2,9,36,18,8,32,0,0,32,18,72,0,2,73,4,18,72,32,146,72,0,0,1,36,144,8,4,0,9,36,130,65,4,146,0,32,18,0,32,2,8,0,146,64,32,0,
9,4,16,65,32,146,1,36,16,8,0,146,9,0,16,73,32,128,1,36,18,73,32,2,1,4,16,64,32,130,0,32,2,8,36,144,9,32,128,8,4,144,0,0,0,73,36,146,73,0,0,0,36,0,65,32,144,0,4,146,64,0,16,73,0,0,64,32,144,9,0,146,0,0,
0,0,36,146,9,36,144,0,4,2,65,36,128,72,4,128,0,0,0,0,0,16,72,4,128,1,0,18,64,0,144,9,0,2,64,32,2,8,36,0,65,4,146,8,36,2,8,32,146,9,4,130,0,4,144,73,32,0,72,4,0,8,0,16,9,0,2,1,4,18,64,0,128,0,36,146,8,
4,144,64,36,144,0,4,2,64,36,18,9,0,130,9,4,0,0,0,0,9,36,146,73,36,0,1,4,18,73,32,146,72,4,146,9,0,0,9,0,144,0,36,144,8,36,130,64,36,2,65,36,146,0,4,16,65,32,16,73,36,130,72,32,146,73,0,128,1,36,0,64,4,
146,73,36,146,0,36,146,73,32,146,64,0,0,0,0,2,73,4,18,1,4,18,72,0,2,1,0,18,73,0,18,9,4,18,73,0,0,0,32,2,73,36,146,64,32,128,64,36,18,72,4,0,73,36,130,8,36,0,9,4,144,9,4,128,1,4,18,65,36,0,0,4,146,8,36,
0,1,0,146,0,4,144,65,36,146,64,4,18,64,32,0,73,32,128,1,32,128,0,36,0,1,4,146,1,0,0,64,4,128,64,0,2,73,4,128,8,36,146,64,36,2,65,4,144,9,4,18,0,0,0,1,4,144,0,0,146,72,4,130,9,32,128,73,0,144,0,0,144,9,
32,18,8,0,144,0,36,128,9,32,18,64,36,0,0,0,144,73,0,130,65,4,146,73,32,18,64,0,144,0,0,0,0,0,146,9,0,146,1,0,18,8,0,2,65,0,130,0,4,144,65,32,128,9,36,2,65,32,144,72,0,18,9,36,128,73,36,16,9,36,0,8,32,
2,9,0,144,0,0,0,1,32,18,64,0,16,9,36,146,64,4,18,64,0,130,0,4,2,65,36,0,64,0,0,9,0,0,65,32,16,8,0,144,9,32,144,64,0,0,73,4,128,0,0,146,73,4,0,0,36,18,72,0,0,64,4,2,0,36,146,64,0,2,72,0,0,0,4,16,0,4,128,
64,0,18,72,32,130,65,36,144,73,36,128,65,32,16,73,32,146,0,0,146,9,0,128,1,32,0,72,0,18,64,0,2,0,0,2,9,32,0,0,36,146,9,0,0,73,0,18,73,36,128,1,32,18,8,0,130,65,4,146,64,36,144,0,36,144,1,36,130,65,4,2,
65,32,2,64,32,16,0,4,16,73,0,16,1,32,0,9,32,146,72,36,146,65,4,0,0,0,16,1,32,16,0,36,130,0,32,2,72,0,0,8,4,128,9,32,144,0,0,146,8,36,146,65,0,16,9,0,16,0,4,16,0,36,146,64,32,146,64,32,18,65,0,18,64,4,
0,0,0,18,0,0,18,73,32,144,65,32,16,64,36,130,73,0,0,0,4,144,73,0,18,73,32,130,9,0,130,0,36,0,64,0,16,0,4,144,1,36,18,64,4,128,0,36,146,64,0,128,1,32,146,9,36,146,72,0,146,65,32,0,0,0,146,64,0,130,0,36,
2,1,0,146,8,32,2,73,36,130,65,0,18,73,0,0,0,0,0,72,32,18,1,36,0,1,0,146,9,0,146,72,0,2,1,36,128,72,32,128,64,32,146,65,32,2,8,0,16,0,36,146,8,4,144,65,4,128,1,36,146,72,0,18,64,4,130,65,4,144,0,36,130,
0,36,144,64,0,130,73,32,18,64,0,0,0,0,144,73,36,146,0,32,130,9,36,128,8,4,18,1,32,16,72,0,0,0,4,18,0,4,2,64,4,18,0,36,146,0,0,2,64,4,144,8,0,128,9,36,0,72,32,128,1,0,0,64,36,2,64,36,144,1,32,18,0,32,16,
73,36,144,73,36,128,8,32,128,9,32,0,73,4,0,64,32,16,64,0,18,0,0,146,0,36,2,1,4,128,0,0,146,64,4,130,9,0,16,64,4,144,9,36,16,73,0,146,65,0,0,0,4,16,9,36,2,73,0,2,64,0,16,64,4,128,0,0,18,9,36,16,73,0,144,
0,36,0,73,36,16,64,0,146,73,32,2,0,32,146,1,0,128,8,0,0,9,36,130,9,0,128,73,0,2,73,32,18,73,36,144,9,36,144,1,36,144,64,36,130,1,36,144,73,0,146,65,0,0,73,36,18,9,4,130,72,36,128,1,0,146,73,0,16,72,0,
144,73,32,0,64,0,128,72,4,130,1,4,0,8,0,16,65,4,146,64,32,2,0,0,128,64,36,18,0,32,2,72,36,0,72,0,128,9,4,18,9,0,0,0,0,0,64,0,128,65,32,128,72,0,2,0,36,2,72,4,18,64,32,128,8,0,18,9,4,144,0,36,146,9,0,18,
65,32,18,73,0,2,65,36,146,72,36,146,73,0,0,1,32,0,9,4,18,73,32,128,9,4,144,9,0,146,1,36,18,64,0,0,9,36,146,73,4,128,73,36,130,9,36,130,8,36,2,1,36,146,9,36,146,9,36,144,73,4,0,9,32,18,73,32,128,1,4,128,
9,36,2,8,36,146,72,36,18,64,4,0,73,32,18,72,0,2,65,36,128,1,36,146,65,36,0,64,32,18,65,0,130,65,32,0,1,0,0,64,32,0,9,36,18,1,32,0,0,4,128,8,4,146,9,36,2,0,0,0,9,36,2,0,0,128,1,36,0,72,0,144,73,32,144,
1,36,146,1,36,128,0,32,16,9,32,0,1,36,146,0,32,146,72,0,0,73,4,0,8,36,2,64,0,146,73,0,16,64,4,16,64,36,130,64,0,0,73,36,146,72,36,146,65,4,0,8,0,0,72,36,146,72,0,2,73,36,146,65,36,16,9,36,0,1,36,144,73,
0,0,9,0,18,64,0,146,1,36,146,1,32,146,0,32,16,8,36,128,1,32,144,64,0,144,1,36,16,1,4,144,0,4,18,64,32,146,0,36,144,72,0,0,9,36,0,1,0,0,0,4,2,65,36,16,8,0,0,65,0,2,1,32,0,65,36,144,1,36,144,73,0,0,1,4,
2,65,0,130,65,36,144,9,32,16,73,0,146,65,0,0,1,32,18,72,36,146,73,0,0,1,32,0,0,0,130,0,36,146,73,36,16,65,36,144,9,0,16,64,0,146,72,36,18,72,32,2,64,0,2,73,36,2,72,32,146,65,4,128,0,4,146,0,0,2,8,0,16,
1,0,18,73,0,128,9,0,146,64,4,144,1,36,128,1,36,18,0,4,130,1,0,2,8,36,146,65,0,18,65,36,16,1,32,2,9,36,2,64,36,0,64,36,128,9,0,146,73,4,144,0,36,144,8,0,146,64,0,18,0,36,144,0,36,128,72,0,2,1,4,130,64,
4,2,0,36,130,72,4,18,0,32,130,9,4,146,9,32,2,65,32,0,1,36,0,64,36,146,72,36,144,1,32,0,0,0,0,1,4,144,0,0,146,9,36,146,72,4,18,9,36,144,72,32,146,65,32,2,73,0,0,73,4,144,1,32,18,72,4,128,65,32,2,73,0,16,
72,0,2,9,0,2,1,0,18,9,0,0,1,0,0,64,0,18,1,36,128,8,32,128,1,32,146,8,36,144,9,36,146,64,32,146,73,0,16,72,36,2,72,0,0,9,4,0,1,36,0,64,0,128,1,0,0,8,36,146,0,0,146,73,36,0,0,4,0,0,32,2,9,4,18,9,4,146,73,
4,128,8,4,130,8,32,2,65,36,130,73,36,144,9,4,18,73,32,146,9,32,146,72,4,144,1,4,146,73,0,0,9,32,0,1,0,18,72,0,144,73,0,16,64,36,2,8,32,16,8,0,2,73,36,144,8,0,0,0,32,0,0,36,130,65,4,128,73,32,16,73,32,
146,1,0,2,8,4,128,0,32,2,9,0,0,9,0,0,9,4,16,1,36,0,65,32,130,64,36,146,72,36,128,72,0,2,8,36,2,9,32,16,72,36,144,73,32,146,65,36,16,73,0,0,73,0,128,64,4,16,73,4,144,8,32,16,72,0,0,8,4,2,64,0,2,65,36,16,
64,0,128,9,4,146,65,32,2,9,32,16,8,36,0,1,36,128,73,32,2,73,36,146,73,36,2,0,0,0,0,0,2,72,0,0,0,32,0,8,0,144,73,0,18,65,0,0,1,0,16,8,36,146,73,36,146,64,4,146,64,36,0,0,0,146,0,4,16,1,0,16,73,32,128,64,
4,146,73,0,144,8,0,18,9,4,128,1,0,18,1,32,0,8,0,0,9,36,144,72,0,128,73,4,144,9,32,130,65,0,144,65,4,0,9,4,130,73,32,144,0,36,2,0,36,2,9,4,16,9,36,146,9,36,146,0,36,0,8,0,146,72,36,130,73,4,128,65,0,146,
9,0,144,0,4,146,65,36,130,64,36,128,1,32,18,1,36,128,8,36,146,73,32,0,64,0,16,1,32,0,0,4,0,0,0,0,64,0,0,0,0,146,0,4,18,72,0,2,73,4,130,1,4,0,64,4,2,0,36,2,64,0,128,1,36,0,64,36,130,1,36,146,72,32,16,1,
4,128,1,0,146,9,36,18,9,36,18,73,32,18,64,0,144,9,32,2,0,32,16,72,32,0,65,36,0,0,4,18,8,0,0,8,36,144,9,32,0,0,36,18,8,36,16,73,32,130,1,36,146,1,36,2,64,0,128,65,36,128,1,0,2,72,4,130,73,4,128,72,36,0,
8,0,16,73,0,144,73,36,2,73,32,18,65,36,18,9,4,128,72,0,130,65,36,0,73,36,128,72,36,146,0,0,130,65,4,130,8,0,0,0,0,2,65,0,18,1,36,144,73,0,2,65,32,146,8,0,130,9,4,2,8,0,16,0,0,0,0,0,144,72,36,128,72,4,
2,0,0,16,73,0,16,65,36,2,9,0,16,0,0,18,72,4,146,65,4,16,0,32,128,64,4,146,65,36,130,64,0,128,1,0,146,1,32,16,72,36,128,0,32,130,64,32,16,64,36,146,1,36,146,1,36,130,72,36,2,8,32,144,0,4,146,64,32,144,
9,36,128,1,0,2,73,0,2,9,36,16,0,36,146,0,32,16,0,0,146,65,36,18,72,36,0,0,4,0,73,4,146,65,32,0,9,32,0,0,0,0,0,0,146,0,0,0,0,0,0,72,0,0,64,0,0,64,0,2,73,36,18,0,32,18,8,32,2,0,36,130,0,0,146,73,36,2,72,
32,2,65,0,16,65,32,2,8,0,2,65,36,128,0,0,16,1,36,18,0,4,146,65,4,128,1,0,0,1,32,2,0,0,128,64,32,0,1,4,146,9,4,130,9,0,130,9,0,18,64,0,146,64,36,144,0,32,2,0,36,128,1,4,0,1,32,0,1,32,18,72,32,144,0,36,
16,0,32,0,65,0,0,1,0,128,8,32,18,8,0,0,0,4,146,72,4,2,1,4,0,64,0,0,9,4,128,9,36,0,0,0,144,72,0,2,72,0,0,0,0,0,0,36,0,64,36,16,0,36,16,1,4,18,73,36,2,8,0,2,8,0,2,9,4,0,0,32,146,73,36,0,1,0,144,65,0,128,
1,36,2,72,4,18,8,4,0,0,0,18,65,4,0,65,36,146,8,0,16,9,32,2,9,36,16,9,4,0,64,0,146,9,32,130,64,36,130,64,32,0,73,36,0,1,36,128,1,32,146,9,32,0,0,36,128,1,4,144,72,32,2,9,36,0,1,36,0,72,0,144,65,4,146,8,
32,18,72,0,144,1,0,144,1,32,0,9,4,0,1,36,0,8,36,128,9,36,130,1,0,144,65,4,128,0,4,0,8,32,16,64,0,130,64,32,0,73,32,130,9,32,146,0,36,146,73,36,18,64,4,18,72,36,2,65,0,128,73,36,130,73,36,128,64,0,0,1,
32,2,0,4,128,1,32,18,65,36,144,65,32,0,73,0,16,73,4,18,65,4,0,64,36,144,9,32,18,0,36,0,0,4,16,65,4,146,73,4,144,64,36,128,64,0,18,73,36,144,0,32,128,65,0,18,73,0,130,0,36,0,1,4,146,8,36,0,72,4,144,1,0,
18,9,36,0,9,36,16,1,32,18,65,0,18,9,0,0,0,0,128,0,0,0,9,0,18,64,4,18,73,4,18,72,0,16,73,32,146,72,36,128,8,0,130,1,0,16,1,32,144,1,32,144,72,4,130,65,36,146,9,36,128,64,36,146,8,4,130,9,0,16,8,0,146,72,
32,0,0,4,0,73,36,128,72,0,0,64,4,144,1,32,146,9,32,0,0,4,144,8,0,144,8,0,16,73,0,128,9,0,144,64,0,0,9,4,0,65,36,130,72,0,0,72,0,2,0,32,0,8,0,146,72,0,144,64,36,144,0,0,146,9,0,18,64,0,0,72,32,130,73,36,
146,1,36,146,9,32,2,72,4,146,9,4,0,73,4,18,1,4,128,0,0,144,72,4,146,73,36,146,64,36,16,65,36,16,9,32,128,9,0,130,72,4,16,1,32,128,8,36,130,64,0,0,0,4,146,64,4,146,73,4,128,73,0,2,0,0,146,64,32,146,1,4,
144,8,0,2,72,0,0,0,32,16,9,36,146,65,36,0,65,36,146,72,0,0,73,36,0,73,0,128,9,4,130,0,0,128,9,0,16,9,32,130,65,36,18,64,0,16,1,36,0,8,0,146,8,32,0,0,32,0,73,32,18,9,0,146,73,0,0,1,32,18,1,32,146,65,36,
128,65,0,2,73,36,0,73,32,2,65,32,146,9,36,146,0,4,0,1,0,16,0,0,128,0,4,144,1,0,0,1,36,128,64,4,128,65,36,130,73,4,128,64,36,2,73,36,18,73,32,144,9,0,144,0,4,0,0,4,144,0,0,0,1,4,0,65,4,18,8,36,130,0,4,
2,9,36,18,65,0,128,9,36,0,8,0,130,8,36,130,64,0,2,64,0,18,64,36,18,72,32,146,64,36,128,72,4,146,65,32,2,0,0,2,1,32,2,65,36,146,8,36,146,0,32,0,9,4,144,65,4,0,9,32,18,72,0,128,8,4,130,0,4,18,0,32,18,73,
4,128,65,0,146,64,36,146,0,0,18,8,4,18,8,36,130,65,36,2,65,36,18,8,32,16,72,0,16,65,36,144,1,0,130,73,0,146,65,4,16,1,32,146,0,32,0,0,4,146,0,36,18,72,32,2,65,0,0,9,36,16,64,0,0,73,0,0,0,4,2,72,4,128,
0,0,2,0,4,18,73,36,16,8,32,146,9,32,0,0,36,2,1,0,18,1,36,128,0,4,18,64,36,2,73,4,2,0,4,16,9,4,18,9,32,0,0,32,18,64,32,146,64,36,130,73,36,0,64,32,0,72,36,128,64,4,2,64,4,2,0,4,146,64,0,0,73,36,0,0,36,
2,73,0,16,73,4,146,73,0,18,8,0,2,64,4,144,73,32,144,73,36,146,1,0,144,72,36,144,9,0,2,64,32,130,73,36,144,73,0,16,0,32,146,64,4,0,73,32,130,1,36,0,1,32,18,73,0,16,72,0,2,64,0,146,65,4,130,73,36,146,65,
32,0,73,36,144,72,0,0,0,0,2,73,36,128,73,32,128,1,4,0,0,36,146,72,4,128,9,36,0,73,4,2,64,4,144,8,32,144,8,4,0,73,4,144,9,36,146,73,36,18,0,0,18,64,36,146,9,0,146,64,4,18,64,4,128,65,32,0,65,32,16,72,0,
18,1,36,130,65,36,0,1,32,130,73,0,0,1,0,18,9,36,128,8,32,146,64,36,18,73,0,0,73,36,130,73,36,128,1,32,18,9,0,2,64,0,144,9,32,146,73,4,144,9,32,16,8,36,130,9,4,16,64,0,0,0,4,146,73,4,144,72,4,0,73,4,128,
9,36,146,64,0,0,65,36,18,1,0,2,1,4,130,65,32,146,1,36,128,64,32,18,72,36,144,1,36,146,73,0,18,72,0,2,72,0,0,1,4,18,9,0,2,64,4,144,72,36,130,9,36,146,73,36,146,0,0,130,64,4,146,0,0,128,72,36,146,9,0,18,
9,0,0,65,36,0,8,0,2,1,0,146,72,0,146,72,32,128,8,0,130,0,4,16,64,0,128,9,32,16,9,36,128,64,4,128,9,4,144,0,36,2,65,36,130,1,0,2,9,36,146,73,4,144,72,0,18,65,32,2,1,4,128,9,0,2,0,0,16,8,0,18,72,36,0,1,
32,128,8,0,128,73,36,146,64,0,2,1,4,18,73,0,144,0,36,16,73,36,0,64,32,0,73,4,16,65,32,130,9,32,18,73,32,16,73,4,128,73,4,0,9,4,128,1,0,146,64,36,128,9,32,146,73,36,146,73,36,146,9,0,144,9,0,2,1,36,146,
8,4,146,72,0,0,1,32,0,1,0,0,72,32,0,0,36,144,8,36,130,8,36,2,1,36,0,8,0,18,0,36,16,1,0,0,0,36,144,65,36,16,72,32,18,72,4,0,0,0,16,64,0,0,8,36,144,8,32,0,73,32,130,0,0,146,0,0,0,64,4,2,65,4,144,73,36,146,
9,36,16,65,32,146,0,0,128,1,0,0,0,0,0,8,4,144,0,0,18,65,32,0,73,4,18,0,0,18,73,32,18,0,0,2,73,36,146,65,32,2,73,32,146,64,36,144,64,4,144,0,0,0,64,36,2,73,4,146,73,36,146,72,32,130,9,0,18,64,0,0,1,36,
16,65,0,16,72,36,144,8,32,18,64,36,16,0,0,146,0,4,144,9,36,130,8,32,18,0,36,0,9,0,0,1,4,18,8,4,146,9,36,128,73,0,146,1,0,2,64,0,0,0,0,0,8,32,18,65,36,130,64,32,128,8,0,2,72,32,144,8,36,146,64,0,128,64,
36,144,0,0,0,1,36,128,72,32,146,0,0,18,0,32,16,72,0,144,73,32,146,73,32,128,8,0,0,72,36,130,8,0,130,72,4,144,73,36,146,73,36,146,1,0,2,73,4,144,72,4,16,64,0,128,65,4,128,9,36,146,0,36,144,9,36,130,0,32,
18,9,0,16,65,0,144,0,4,146,0,32,0,0,0,144,73,4,146,9,4,2,1,32,144,64,4,146,65,36,18,8,4,130,0,0,18,8,0,18,73,32,2,8,0,146,72,32,146,8,0,0,9,0,16,72,0,2,0,36,128,64,36,0,8,36,144,1,0,16,0,36,2,9,32,146,
8,0,128,65,32,2,1,32,18,0,4,130,0,0,0,64,32,2,72,0,0,73,32,144,0,0,0,0,4,130,0,4,144,9,0,16,0,4,146,73,0,2,72,0,2,64,0,18,73,36,146,0,0,144,0,0,0,64,0,16,64,36,130,1,36,146,73,36,146,8,36,144,73,32,146,
64,0,146,0,0,18,72,32,130,73,32,144,64,36,18,65,36,144,1,36,0,73,32,146,1,36,128,8,32,128,9,0,144,73,4,16,72,4,128,0,4,18,1,4,0,8,32,128,8,0,0,73,36,144,64,36,144,9,36,130,9,0,2,64,0,128,64,36,146,72,
36,146,73,36,146,65,4,128,64,36,130,9,32,146,73,0,144,72,4,144,0,0,0,64,4,144,0,36,18,9,32,144,73,0,0,72,4,146,9,0,146,73,36,146,0,0,18,73,36,0,73,36,18,8,36,144,1,36,146,72,4,0,1,4,146,64,0,18,8,36,146,
73,36,146,73,36,130,9,4,128,73,32,0,0,0,146,0,0,2,65,36,16,65,4,0,0,36,146,1,0,130,8,0,2,64,32,18,1,32,0,9,32,130,8,4,144,0,0,16,8,36,2,8,4,0,1,0,130,64,4,2,72,32,16,9,36,130,73,36,18,0,32,0,72,0,130,
64,4,16,9,4,128,73,32,2,73,36,2,65,4,146,64,0,130,72,4,2,9,0,128,8,0,130,0,4,146,72,36,128,1,32,130,72,4,0,0,0,146,64,32,144,73,36,146,9,36,144,9,4,144,8,0,2,1,0,2,73,0,18,1,36,0,65,4,18,65,36,18,65,32,
2,1,36,146,73,36,146,73,36,128,1,36,128,9,36,146,73,36,146,0,4,144,64,4,0,1,36,2,72,36,144,73,32,2,73,32,0,65,36,18,65,0,18,72,0,146,64,32,16,9,32,0,8,36,130,1,0,128,1,36,0,9,36,128,1,36,16,64,32,128,
72,36,146,73,36,130,1,36,128,65,4,128,73,4,146,64,0,144,73,4,144,1,0,2,64,0,144,72,0,128,72,4,16,0,0,0,0,0,0,9,4,16,8,0,0,9,0,128,0,0,0,0,32,128,1,36,146,0,36,144,8,4,144,9,36,144,1,4,0,73,36,128,72,0,
146,1,0,16,72,36,130,1,32,0,0,36,146,73,36,0,72,36,146,65,36,146,9,0,128,0,4,146,64,0,0,72,36,16,0,32,0,9,36,0,0,0,2,64,0,16,9,36,2,64,32,128,1,4,0,1,0,2,72,4,128,0,0,0,64,36,2,65,0,16,72,32,2,64,32,0,
72,4,128,72,4,146,73,32,0,1,32,18,0,0,2,72,32,0,0,0,2,0,0,128,73,0,2,0,0,16,8,0,146,65,4,144,0,0,0,72,36,146,72,32,2,64,4,16,64,0,0,0,32,0,9,36,146,0,0,18,73,4,146,0,0,16,65,4,130,9,36,144,8,36,130,65,
36,128,8,0,144,64,32,18,64,4,18,73,36,146,1,36,2,73,0,0,0,32,146,64,0,146,73,0,146,64,0,146,73,36,146,9,36,0,64,0,128,0,0,0,0,36,144,1,36,0,8,4,2,73,36,16,1,36,0,1,32,0,0,0,130,8,0,144,8,0,128,0,36,18,
73,32,146,72,36,2,8,4,2,73,36,18,72,36,0,0,32,16,9,36,0,0,0,0,73,36,16,0,4,128,0,32,128,73,0,0,1,36,18,72,0,0,1,36,128,8,0,18,0,0,2,9,0,0,0,32,18,73,36,128,1,32,18,9,32,146,73,32,0,73,36,146,0,36,0,9,
4,146,73,32,130,73,36,146,73,36,146,65,4,130,73,36,146,1,0,146,73,36,146,73,36,0,1,32,0,65,36,18,64,0,146,65,36,18,72,0,2,1,4,128,0,0,18,64,0,16,73,36,0,1,32,2,1,4,128,9,36,130,0,32,2,72,36,16,65,32,0,
65,0,146,64,4,146,1,32,146,64,0,0,64,0,144,72,0,144,64,36,18,72,36,0,8,0,146,65,36,128,73,0,16,0,36,0,72,0,16,9,0,146,64,36,146,65,36,146,8,0,128,1,36,146,65,4,18,9,0,128,0,4,0,73,36,0,0,36,146,73,36,
128,1,4,144,64,0,128,0,4,144,1,36,16,73,36,146,73,32,146,73,36,128,65,36,2,64,36,128,0,36,146,64,32,0,0,36,146,73,32,18,72,36,18,64,0,0,0,32,18,0,32,146,72,36,2,0,0,130,65,0,18,0,36,144,8,4,18,65,32,128,
1,36,144,0,0,18,65,36,146,64,0,130,9,32,18,64,36,16,73,32,2,64,0,2,9,36,128,9,0,0,0,4,0,0,0,144,73,36,146,1,32,128,65,0,0,0,4,128,73,36,16,64,36,0,9,0,16,9,36,2,73,0,128,73,36,2,73,36,146,9,32,18,72,0,
18,64,0,0,9,4,146,8,0,0,0,36,130,72,36,2,9,0,144,8,36,146,73,36,146,73,36,146,73,36,146,64,0,146,64,36,128,1,36,144,0,0,0,1,0,0,0,0,0,9,36,130,64,0,146,1,4,18,64,32,130,0,32,130,65,36,128,73,0,18,9,36,
0,1,4,144,0,36,2,65,0,2,9,0,16,9,36,0,8,0,18,1,4,146,73,0,18,0,36,130,0,0,144,8,0,130,65,32,2,73,32,128,9,32,128,9,0,2,64,32,0,72,0,146,1,36,0,73,0,144,0,4,2,64,32,2,0,32,146,72,0,18,72,36,144,64,4,128,
9,32,0,64,32,0,9,36,144,0,0,0,9,36,18,73,36,144,0,36,2,64,32,2,73,36,146,73,4,146,65,36,146,72,4,130,8,0,0,0,0,146,73,36,130,0,4,18,1,0,16,0,4,144,0,36,130,64,0,130,65,0,146,0,36,16,8,0,0,9,4,146,64,0,
0,0,4,144,0,32,144,65,0,2,1,36,130,8,32,128,9,36,128,0,0,144,1,4,146,64,36,130,73,32,128,9,36,146,8,36,16,8,32,16,73,32,128,72,4,130,8,0,0,8,36,2,72,0,144,0,36,128,1,32,2,0,36,18,64,4,146,64,0,144,72,
0,0,1,32,0,64,32,130,72,36,0,0,36,128,0,0,18,0,0,16,1,36,128,9,32,16,0,4,0,73,36,146,73,36,146,9,36,146,72,36,128,0,36,0,1,36,128,1,0,16,0,32,16,65,36,146,73,0,2,1,4,146,73,0,0,64,0,146,0,36,128,8,0,16,
9,32,0,8,36,128,0,4,144,9,4,146,64,36,144,0,32,16,0,0,0,1,4,0,9,0,146,1,36,128,9,0,144,73,32,128,1,32,128,1,32,144,1,4,146,73,0,18,65,36,18,0,32,16,0,32,18,65,32,0,9,36,0,65,32,130,0,36,0,65,36,2,0,32,
2,0,0,0,8,0,144,0,0,16,73,36,128,8,0,0,9,32,0,0,4,2,1,36,146,0,36,18,9,0,2,64,36,130,73,4,146,73,36,18,72,36,0,0,0,18,8,0,144,1,4,128,73,0,0,72,0,144,1,36,0,73,36,0,1,0,146,73,36,146,72,0,146,0,32,128,
8,0,146,8,0,128,64,0,2,73,0,18,73,36,146,0,4,2,64,4,18,0,4,2,73,32,16,72,0,0,65,36,130,72,0,146,72,32,0,72,0,146,73,36,16,8,4,144,73,4,128,9,0,0,1,4,16,9,32,18,0,32,146,73,36,0,64,36,146,64,36,2,65,4,
128,8,4,130,72,4,144,64,0,2,9,0,130,64,0,18,1,32,2,64,0,18,9,32,18,73,32,128,64,36,130,0,0,18,73,0,144,64,4,146,9,36,128,0,0,0,0,32,0,73,0,16,73,0,146,73,4,130,0,0,16,0,0,2,64,36,130,73,36,128,0,4,0,72,
0,0,8,36,16,0,32,144,0,4,128,72,32,128,1,4,130,1,0,0,65,4,144,1,4,144,0,0,128,1,4,146,73,4,128,8,0,144,64,4,16,64,4,128,72,32,144,1,0,128,0,4,130,9,36,146,72,32,146,65,4,130,64,36,128,9,36,144,64,0,16,
0,32,18,73,36,144,8,36,146,65,32,2,8,0,18,72,0,18,65,36,16,9,36,128,1,36,128,9,4,146,73,36,128,8,0,128,9,36,18,73,36,146,73,32,130,73,36,18,0,36,146,0,0,146,0,32,16,64,32,2,73,0,146,73,36,146,73,0,0,0,
4,18,9,0,130,8,0,16,9,0,2,72,0,128,0,4,144,1,0,0,1,4,0,0,36,146,65,4,2,73,32,2,1,32,16,1,36,146,9,32,0,8,0,0,8,0,128,0,32,16,1,0,18,65,4,0,9,36,18,73,32,0,73,36,146,0,36,16,64,0,146,9,4,0,0,4,18,9,36,
146,72,0,18,1,4,18,73,4,0,73,0,18,65,32,144,8,4,128,8,0,0,0,32,146,9,36,146,0,4,16,9,36,16,73,0,144,9,32,144,65,36,146,73,36,146,8,36,146,64,0,2,64,0,0,73,32,16,0,0,18,0,0,146,0,4,18,72,4,146,1,36,144,
0,0,0,0,36,146,0,4,128,72,36,146,72,0,146,8,4,130,64,32,2,9,36,2,8,0,0,0,0,2,1,32,130,1,36,146,1,0,130,9,4,146,1,32,18,9,4,0,64,4,130,0,4,146,73,0,2,9,36,16,8,36,128,73,0,18,1,32,18,9,36,2,65,36,2,73,
36,2,65,36,128,1,32,2,9,4,146,9,36,146,1,36,0,0,0,128,1,0,2,0,0,146,64,36,0,1,0,130,64,32,0,0,0,18,65,4,130,65,36,146,73,36,146,73,36,146,8,0,146,73,36,144,0,0,146,64,32,0,9,0,146,1,36,146,0,36,146,73,
36,146,0,0,0,0,0,18,72,0,2,9,36,128,73,36,128,72,0,128,9,36,130,0,36,16,0,4,0,0,0,0,0,32,18,72,4,130,65,32,0,73,32,144,73,4,16,8,4,146,65,0,130,64,32,0,72,4,128,9,32,18,73,0,144,73,32,0,73,36,128,8,4,
130,9,4,130,73,32,128,73,32,146,0,36,128,0,0,130,72,4,128,1,0,18,64,36,146,73,0,130,64,36,0,64,36,146,0,36,128,72,0,146,64,36,146,64,36,130,9,4,146,73,36,146,73,4,146,0,0,2,73,36,2,64,36,0,0,0,0,9,0,146,
9,4,18,64,36,128,9,32,0,9,0,0,1,32,0,1,36,146,72,0,18,73,36,0,0,0,0,72,0,146,64,36,128,1,36,18,9,36,0,0,32,0,64,4,16,73,32,18,9,0,0,1,32,0,65,36,128,73,32,2,72,4,128,9,36,130,72,0,146,64,0,0,9,0,16,1,
32,16,8,0,18,72,0,144,8,36,146,73,36,128,73,0,16,1,4,0,0,4,18,72,32,146,1,0,0,1,36,0,8,4,16,1,36,16,1,0,0,8,32,0,8,36,0,9,4,144,65,32,2,73,36,146,64,36,128,9,36,0,8,4,146,73,36,144,0,0,16,72,0,144,0,36,
146,64,36,0,0,32,18,9,4,0,73,32,0,0,32,0,8,36,146,73,32,0,0,36,144,8,0,146,9,0,0,0,4,128,0,0,146,8,36,18,0,4,146,72,0,144,72,4,128,9,4,144,0,32,144,8,4,18,9,0,16,9,32,18,9,32,130,0,4,0,64,32,146,0,32,
144,1,4,2,8,0,144,73,36,146,0,4,0,8,36,128,0,0,16,9,36,0,0,4,128,9,36,18,0,4,146,9,36,146,9,0,0,9,36,144,64,32,0,64,36,16,72,0,144,1,32,130,73,36,146,73,32,0,72,4,128,0,0,18,73,0,0,0,4,128,8,0,146,64,
0,0,64,0,16,72,36,146,64,0,0,1,32,2,8,32,0,73,32,18,73,36,146,73,32,16,0,36,18,1,36,144,73,4,2,73,36,146,64,0,18,72,4,144,0,0,130,8,32,144,64,0,144,8,4,0,72,36,0,0,0,144,73,32,0,73,0,144,9,36,128,72,4,
18,1,0,18,8,36,146,8,4,2,0,36,2,8,0,128,65,0,16,65,36,146,64,0,146,1,36,0,9,36,0,73,0,144,64,0,0,0,4,16,73,36,128,73,36,0,9,32,0,73,0,18,8,0,18,72,36,146,73,32,0,1,32,146,64,0,0,0,4,146,65,32,0,9,36,146,
73,36,0,8,4,144,73,36,146,65,36,130,64,32,0,64,32,146,73,32,18,73,36,18,65,36,146,0,0,18,1,32,130,65,4,0,73,0,0,72,0,2,9,36,0,72,36,146,64,32,18,72,0,128,8,36,18,9,0,144,65,4,144,72,0,144,1,36,0,73,32,
18,8,36,130,9,4,144,8,32,144,65,36,146,64,32,144,73,36,146,0,0,144,0,36,0,73,32,144,73,32,16,0,32,18,72,4,128,72,36,144,0,0,130,1,32,2,73,4,130,73,36,18,1,0,128,8,4,130,64,4,146,73,36,146,73,0,0,72,0,
144,8,0,2,64,0,0,0,36,128,0,36,18,64,36,146,73,32,130,65,36,146,64,4,146,9,0,128,1,4,18,73,36,146,73,32,146,72,36,128,8,0,0,9,32,128,1,32,0,64,36,0,9,36,2,9,0,0,8,0,18,1,36,18,72,36,0,73,36,130,72,36,
144,73,36,0,64,32,146,0,32,128,0,32,18,9,0,0,64,36,0,8,36,128,64,0,146,73,32,0,73,32,2,1,32,18,64,36,146,64,32,144,8,36,0,65,4,16,64,36,0,9,32,16,0,0,0,1,36,16,73,0,146,0,4,18,64,32,128,0,0,18,1,36,16,
73,4,144,1,36,146,0,0,2,8,36,128,0,36,144,0,0,18,73,4,146,73,36,128,1,36,144,9,36,18,73,4,146,64,0,146,64,4,0,0,32,18,73,36,146,64,36,146,8,4,130,64,0,0,9,36,146,64,4,18,73,36,144,64,36,128,0,4,146,72,
0,18,1,4,144,72,36,146,1,36,144,73,32,0,72,36,2,65,36,0,0,36,146,0,4,2,64,0,16,8,4,2,72,32,144,72,36,18,64,0,18,72,4,146,64,0,130,8,32,130,72,32,0,64,4,16,73,4,16,8,36,146,73,36,130,73,36,2,8,32,18,0,
0,128,8,0,16,64,0,146,72,4,128,0,0,18,0,36,146,72,4,146,73,36,128,0,0,146,1,0,16,73,32,146,73,36,130,73,36,146,73,32,146,64,32,18,64,0,0,64,36,128,0,36,146,73,36,146,73,32,2,0,0,128,0,0,18,73,36,16,73,
0,144,0,0,146,1,36,0,65,36,146,9,0,146,65,32,128,73,0,144,65,4,146,73,4,0,65,4,130,1,4,144,9,36,18,64,0,130,8,4,0,72,36,144,72,32,16,8,36,144,64,32,130,0,0,18,1,4,130,65,32,144,65,32,0,1,0,18,1,32,2,64,
36,146,65,32,146,65,32,130,1,36,144,1,36,128,8,36,18,8,0,16,64,36,146,64,32,18,1,4,128,9,36,146,0,32,0,0,36,144,73,32,0,0,4,18,73,36,0,0,36,128,0,0,128,73,36,146,64,4,0,72,4,146,0,32,2,65,32,0,72,0,0,
0,0,0,8,0,128,0,32,18,73,4,18,8,36,146,72,0,2,72,0,2,8,4,146,72,36,130,72,32,16,73,0,144,8,4,18,1,4,2,72,4,130,73,4,0,65,32,16,1,4,18,8,32,0,73,32,16,72,32,130,64,0,128,1,36,146,64,36,16,0,36,130,64,0,
18,9,0,18,64,0,144,1,0,144,72,36,128,9,4,146,72,0,146,0,36,128,1,4,146,65,32,18,73,0,18,73,36,146,0,0,0,73,36,146,0,0,0,0,36,144,0,0,0,64,32,18,64,4,146,64,4,146,1,4,2,9,32,146,72,4,18,73,36,146,72,0,
0,9,32,0,0,32,0,9,36,144,73,0,2,73,36,146,73,36,16,65,36,128,9,0,146,72,36,128,8,32,0,8,4,2,72,36,2,8,0,2,8,4,128,0,36,128,0,4,130,73,32,146,73,32,2,1,36,146,73,4,146,64,36,18,0,0,0,73,36,0,8,32,130,64,
32,16,8,0,16,9,0,146,64,0,146,73,4,2,65,0,18,0,32,2,0,0,18,73,32,16,73,0,16,73,4,2,64,0,0,64,4,144,64,0,2,65,4,146,64,0,16,64,4,144,9,0,0,73,0,144,72,4,146,65,4,146,64,0,0,9,4,2,73,4,130,0,0,0,1,32,146,
8,0,0,0,0,0,0,4,2,64,4,0,8,36,16,9,36,0,9,32,128,65,0,0,64,32,2,64,4,130,64,0,144,9,36,130,72,32,128,0,4,0,73,32,0,9,32,144,9,32,16,64,4,0,72,32,0,73,32,128,64,4,18,0,0,144,64,32,128,64,0,0,1,4,146,72,
36,18,73,0,146,73,36,128,73,4,144,1,0,146,8,0,0,9,32,18,65,36,0,73,36,128,73,0,0,0,0,2,0,4,0,8,0,128,1,32,0,72,4,18,64,4,18,73,36,128,0,0,18,1,4,146,73,36,130,0,4,16,0,0,0,1,0,18,8,36,146,72,0,16,8,4,
16,9,0,18,65,0,0,0,0,18,0,4,146,72,4,0,0,4,0,9,32,128,0,0,0,8,36,2,0,0,16,0,32,2,1,32,2,73,0,0,0,0,128,72,0,144,9,4,130,8,36,130,73,36,128,65,32,2,0,0,130,73,4,146,9,32,144,73,36,2,73,32,144,0,0,2,73,
36,0,72,36,130,65,32,2,0,36,146,0,0,18,73,0,146,64,32,18,9,32,146,0,36,144,0,0,0,0,4,146,64,36,144,0,0,16,1,32,130,0,0,128,0,36,0,0,32,0,0,4,128,72,36,128,9,0,18,65,4,18,72,4,2,73,32,0,1,0,144,64,0,146,
72,0,144,0,4,2,72,32,0,64,0,2,64,36,144,72,0,0,9,0,146,65,32,0,0,0,2,64,36,0,65,0,16,65,32,18,9,36,128,73,32,0,72,32,0,0,0,144,0,4,144,1,32,0,73,36,130,9,32,0,0,4,144,0,4,146,64,4,128,65,0,128,64,36,128,
65,32,0,73,36,18,8,0,2,9,0,16,1,0,0,0,0,16,65,36,0,64,0,144,0,32,0,65,32,0,0,0,0,9,36,146,73,0,146,8,0,2,0,4,0,1,4,2,64,4,0,0,0,2,65,36,146,73,36,146,8,0,18,9,36,146,73,36,146,73,36,146,73,32,0,9,4,0,
9,0,18,73,0,146,73,0,18,73,36,2,73,0,18,72,0,2,9,0,2,65,0,146,64,0,0,1,0,2,65,36,0,64,0,2,64,32,130,65,0,0,72,0,128,0,0,144,0,0,128,72,36,18,73,4,128,73,32,0,1,0,144,0,32,146,1,0,146,1,32,18,9,32,0,9,
36,128,73,36,128,9,32,18,1,32,130,64,32,2,9,36,2,1,36,146,65,36,16,1,32,0,72,0,128,64,0,2,64,0,0,0,0,0,0,36,128,8,36,128,1,4,2,8,0,0,1,36,128,8,4,130,8,4,144,9,4,128,64,36,18,73,36,18,73,36,146,73,36,
128,9,36,16,8,36,146,0,4,146,9,32,128,73,36,146,73,36,130,64,0,0,73,36,144,65,32,130,0,0,0,73,0,16,0,32,0,9,0,0,9,0,130,9,0,2,8,0,146,72,4,146,9,32,146,9,32,144,9,4,2,0,36,128,72,36,18,9,32,144,65,0,128,
65,0,2,73,0,146,64,0,144,72,32,146,0,36,128,0,0,16,0,4,144,9,4,18,0,32,18,1,36,0,8,0,0,0,0,0,0,0,16,9,36,0,8,0,16,8,4,144,0,0,146,73,32,128,73,36,146,73,36,144,65,36,18,0,4,144,0,0,0,73,0,0,0,4,2,73,0,
146,73,32,146,73,0,18,64,0,0,0,36,18,0,36,146,0,36,144,9,36,146,73,4,0,0,0,144,73,0,0,0,4,0,1,36,2,0,36,0,64,36,144,9,4,0,73,0,0,9,0,0,1,36,18,73,4,16,72,4,146,65,32,0,73,4,18,73,4,146,9,32,128,64,36,
128,72,0,2,9,0,128,0,0,144,0,4,2,64,36,18,73,36,144,72,36,146,65,4,146,8,36,0,0,4,144,0,0,2,73,32,128,64,36,128,73,32,128,0,36,146,73,36,146,73,4,146,64,36,146,73,36,16,9,0,146,73,36,0,72,32,128,64,4,
146,72,4,2,65,4,146,73,32,2,1,32,0,73,32,0,72,36,128,0,36,144,9,0,128,9,36,0,64,36,130,0,0,146,64,32,146,73,36,2,73,32,146,64,36,128,0,0,2,1,32,2,9,4,146,8,0,18,1,32,146,72,0,144,0,36,144,65,32,0,64,0,
18,64,36,16,8,4,144,64,0,18,1,32,0,9,36,130,8,4,146,0,0,16,0,36,146,72,0,0,0,0,130,65,36,146,72,0,0,1,36,144,0,0,0,1,36,144,1,32,0,72,0,2,64,0,18,72,0,0,8,36,130,8,36,146,64,36,146,1,36,130,73,36,146,
9,36,146,73,32,146,0,4,146,73,32,0,0,36,146,73,0,2,73,36,144,0,0,128,73,32,130,73,36,2,0,0,146,73,36,144,9,0,128,1,36,146,0,36,146,73,36,144,1,32,146,64,4,0,0,4,16,8,0,146,64,0,128,8,0,128,72,0,18,1,0,
0,0,4,146,73,4,146,65,4,2,0,0,146,9,36,146,0,0,16,0,0,18,64,4,0,8,0,0,0,36,144,8,0,2,72,4,146,1,36,18,9,32,2,64,32,146,73,32,0,1,4,0,0,32,0,8,0,0,73,0,2,64,32,146,0,0,146,0,36,146,0,32,16,73,0,2,9,36,
146,73,36,130,0,4,128,73,0,146,73,36,16,64,0,0,1,36,146,73,36,146,64,36,18,72,0,144,0,4,2,0,4,146,0,0,0,0,36,128,1,0,0,9,4,2,0,32,146,73,36,18,1,32,18,64,4,128,64,4,130,9,32,144,64,32,18,0,32,18,1,0,2,
64,0,16,1,36,144,64,0,0,64,32,128,8,4,130,73,0,2,0,32,0,64,0,144,72,36,146,72,4,144,1,4,0,0,0,146,0,36,130,73,32,16,73,0,0,64,0,146,65,36,146,64,4,130,73,36,18,72,0,130,73,36,18,64,0,144,1,36,0,73,36,
146,9,0,130,65,36,146,73,36,128,0,0,0,0,0,128,73,32,144,73,32,16,73,32,2,73,36,146,73,0,16,0,0,18,64,32,18,73,32,0,1,0,0,0,0,0,0,4,0,1,36,16,0,0,18,0,4,144,9,32,18,8,0,18,73,32,128,9,4,0,9,4,16,9,0,144,
1,0,144,0,0,0,72,0,18,65,36,0,0,4,144,72,0,146,64,0,144,64,0,16,65,36,144,9,36,144,64,4,18,1,32,0,64,0,144,73,0,0,72,4,0,1,0,0,0,0,16,73,36,146,72,0,146,8,0,146,73,36,128,1,4,130,8,36,2,0,0,130,72,0,2,
73,0,18,9,32,0,64,4,2,72,0,0,0,0,0,1,32,2,73,32,146,65,0,2,64,0,0,0,0,128,1,4,16,1,36,0,1,36,146,0,0,0,1,4,0,0,32,0,9,36,146,73,36,144,65,36,130,9,32,2,73,32,0,0,32,16,1,0,16,65,0,130,73,4,16,72,0,0,9,
0,2,64,36,0,0,0,0,1,0,130,8,36,16,72,36,2,8,36,0,9,36,146,64,4,144,64,0,2,65,4,128,9,36,0,8,32,146,65,0,146,0,32,2,73,32,146,73,36,0,1,0,0,73,32,130,73,36,146,72,36,2,1,0,128,1,36,144,73,36,146,9,36,2,
64,36,18,8,0,16,0,36,18,72,36,2,1,36,16,1,0,130,72,0,2,65,36,128,0,0,0,1,32,128,73,36,128,8,4,0,0,32,18,1,4,0,0,4,130,73,36,146,72,0,18,73,36,146,72,0,18,64,32,2,65,36,2,1,36,146,73,36,16,73,0,16,72,36,
2,65,4,16,0,0,2,65,36,128,0,4,0,9,32,130,72,0,18,72,4,130,1,4,146,72,32,0,1,32,18,64,0,146,65,36,18,0,0,2,0,0,0,8,36,146,73,32,0,0,0,18,73,32,146,64,4,130,73,0,144,0,4,146,64,36,16,65,36,144,1,36,146,
73,36,144,73,36,18,73,36,146,64,32,16,73,4,130,72,4,16,9,36,146,73,36,18,73,32,0,0,0,0,65,32,16,1,36,0,0,36,18,73,36,144,0,36,2,73,36,146,73,36,144,72,4,128,72,32,0,0,0,18,73,36,128,0,36,144,9,36,18,65,
32,0,73,0,0,0,0,18,0,0,18,72,32,128,9,0,18,0,4,18,65,36,16,8,0,16,65,4,146,65,36,16,72,32,16,0,0,144,73,32,16,1,32,0,0,36,2,64,0,0,9,36,130,65,32,0,0,0,144,0,4,18,72,36,0,9,36,16,9,4,128,72,32,2,72,32,
2,8,0,144,73,0,18,64,32,0,0,32,2,8,4,128,8,36,144,72,4,130,8,0,18,73,36,146,8,36,144,8,0,0,73,0,130,64,36,18,9,36,146,65,36,128,0,0,144,73,4,0,0,0,146,0,4,0,8,0,0,72,0,0,1,36,18,0,0,18,64,36,130,64,32,
144,0,0,0,8,0,0,1,32,16,0,0,18,64,0,16,9,36,128,1,32,144,0,36,16,9,0,2,65,36,144,64,36,144,9,4,130,65,32,146,73,4,130,9,32,146,73,0,130,73,36,0,64,36,144,64,0,16,0,0,128,0,32,0,64,0,128,65,36,144,1,32,
16,0,0,128,65,36,128,9,32,2,1,0,18,73,0,144,9,36,146,65,4,0,64,36,18,0,36,146,0,4,16,64,0,2,73,36,128,1,36,130,72,0,144,1,36,146,73,0,146,72,4,146,0,0,16,9,4,144,1,36,16,0,0,128,0,32,0,1,4,146,73,0,0,
0,0,130,0,4,146,73,32,144,0,0,0,8,0,0,0,32,16,65,0,130,1,32,16,9,0,130,64,0,18,0,36,144,0,0,130,73,4,2,65,32,18,65,36,16,72,0,16,72,4,128,73,36,0,9,0,146,65,0,144,0,36,18,64,32,0,1,36,2,9,36,144,0,0,144,
8,0,2,73,0,0,73,32,130,65,32,146,72,36,0,0,4,146,72,36,16,73,4,18,73,36,128,8,0,0,64,0,0,8,0,18,0,0,128,9,32,146,73,32,0,64,0,16,73,36,144,73,36,146,73,0,130,9,36,128,64,36,0,73,32,16,0,32,18,9,32,146,
64,0,144,72,0,2,1,36,2,64,4,146,0,0,146,72,0,0,0,0,2,64,4,16,73,4,0,0,0,130,8,4,16,0,4,130,0,0,128,0,4,128,73,36,130,65,4,144,9,32,18,65,36,146,9,32,144,73,32,128,1,0,0,9,36,146,65,36,2,1,36,0,1,0,18,
9,4,16,73,36,130,1,36,146,64,4,146,0,36,0,0,32,144,65,36,0,65,36,146,72,32,18,64,0,0,64,36,18,9,4,0,73,36,128,73,36,146,0,36,18,9,36,128,8,32,18,73,36,146,8,4,0,0,4,128,0,0,0,1,0,2,1,36,146,73,36,128,
9,36,2,9,4,146,72,36,128,1,36,144,1,4,144,8,4,2,72,4,144,73,4,130,64,32,0,0,32,2,72,36,0,0,0,146,8,32,2,1,32,146,64,4,130,73,36,18,72,32,128,9,4,0,72,36,130,8,4,146,73,36,144,73,36,128,0,32,2,0,0,18,0,
0,16,65,4,0,72,36,130,64,4,128,8,0,130,64,36,146,1,36,146,1,0,144,8,0,18,65,36,146,64,4,0,0,32,18,65,36,146,0,32,18,73,36,128,64,4,2,9,36,18,73,36,146,0,0,146,73,0,0,9,36,128,9,4,128,8,0,16,73,36,146,
73,36,0,1,36,128,9,0,0,0,0,0,0,0,18,73,0,18,73,32,2,73,0,144,72,4,0,1,0,2,72,32,0,1,32,144,0,0,0,73,36,16,72,0,18,8,0,130,72,36,128,0,0,146,1,36,2,8,0,2,64,32,2,73,0,18,72,4,146,1,36,2,8,0,2,8,36,146,
73,32,0,0,0,16,0,4,146,0,0,2,73,32,144,8,0,146,0,0,18,0,36,144,0,0,146,73,0,146,72,32,130,0,0,2,0,36,146,73,36,146,73,36,146,9,36,146,9,0,0,8,36,144,0,0,0,0,36,146,9,32,146,9,32,0,0,36,146,72,4,146,0,
0,0,0,32,0,0,4,144,1,36,16,8,4,146,73,36,146,0,0,146,73,36,144,1,36,2,65,36,144,64,36,128,1,36,18,1,36,146,73,32,130,1,36,146,72,32,16,1,36,146,1,32,2,72,0,146,72,0,0,0,4,128,0,32,18,65,36,128,72,32,128,
65,32,130,0,4,146,8,4,2,72,36,16,0,36,130,9,32,144,1,36,146,73,32,0,1,32,0,9,32,0,1,4,16,0,0,18,72,36,128,8,4,0,1,4,128,73,36,146,73,32,146,64,36,144,65,36,146,72,36,16,8,0,0,0,0,0,9,36,128,72,32,144,
72,32,16,0,0,146,73,32,0,0,0,0,64,0,0,8,0,146,72,0,146,72,0,128,65,36,144,9,0,146,64,36,128,0,4,16,72,36,0,73,36,128,9,32,146,73,36,128,72,32,144,0,4,144,0,0,130,72,32,146,73,36,18,64,36,146,64,0,2,64,
0,16,65,36,146,64,32,130,73,36,2,72,4,144,0,4,128,73,32,18,72,4,146,8,4,146,73,36,128,0,36,128,64,4,144,9,36,146,72,0,2,64,4,130,73,36,146,1,32,2,73,36,128,72,4,130,0,4,0,1,32,16,0,36,146,64,0,0,8,0,16,
0,0,2,73,32,18,73,32,18,73,36,0,1,4,128,9,0,2,73,0,0,1,36,146,73,32,0,73,36,144,72,0,2,0,36,2,73,32,18,73,0,128,0,4,16,1,4,130,1,0,146,73,4,128,73,36,146,72,0,0,0,4,18,0,36,128,64,0,146,1,36,144,9,36,
128,9,36,146,64,32,144,65,36,146,73,36,0,65,36,144,0,0,0,72,0,144,0,0,0,0,0,144,73,36,130,72,32,144,8,36,0,65,0,18,0,0,0,73,32,146,73,0,130,64,36,146,72,0,0,1,36,0,64,36,146,73,36,146,0,0,0,73,32,130,
65,0,0,0,0,16,9,32,2,72,4,130,72,36,128,64,4,2,73,36,146,64,0,130,73,32,146,73,32,146,73,36,146,73,36,2,0,0,18,8,4,18,73,0,2,73,0,130,72,0,0,9,36,18,9,0,2,65,4,0,8,4,16,9,4,128,0,0,0,72,0,128,64,0,0,0,
0,18,1,4,0,1,36,0,9,32,146,72,0,146,1,4,128,0,4,146,65,36,0,73,0,0,9,36,146,73,32,144,64,36,130,72,36,146,73,36,2,64,0,18,72,0,2,1,32,2,65,0,2,0,0,0,73,0,2,1,4,144,1,36,0,0,32,18,1,36,18,73,36,18,73,36,
146,0,32,18,73,0,0,65,36,144,0,36,2,72,36,18,72,0,18,64,4,0,0,4,146,65,36,144,64,0,146,0,36,2,73,36,0,9,4,128,73,32,2,0,0,0,1,0,130,73,36,0,9,36,2,73,32,146,8,36,128,8,4,18,0,0,0,1,4,128,0,0,128,8,0,18,
0,32,146,1,36,146,64,32,146,1,36,144,9,32,128,65,32,18,0,4,146,65,4,2,72,0,146,72,36,146,72,0,130,9,36,18,9,36,146,0,32,144,0,36,146,0,0,128,73,36,128,1,36,146,0,0,0,1,0,18,1,32,18,9,32,130,65,4,18,73,
32,128,0,0,146,64,0,146,73,0,2,64,4,2,73,36,130,73,36,146,9,36,144,0,32,0,8,32,18,72,36,144,73,36,144,0,36,146,64,0,2,0,0,146,64,0,144,0,0,0,1,0,0,64,0,2,73,4,128,1,32,128,9,32,130,65,32,130,64,36,130,
72,0,0,0,0,0,0,4,144,72,36,146,64,32,2,64,32,16,64,0,18,64,36,0,64,4,128,0,36,146,64,36,128,73,32,146,73,0,18,8,32,144,73,32,146,72,0,0,64,4,146,64,32,18,0,4,144,64,4,0,1,36,146,0,36,16,9,4,144,0,4,2,
73,36,146,65,32,128,64,36,18,64,4,146,9,36,146,9,36,146,72,32,18,9,32,18,72,36,146,9,36,144,0,0,128,0,0,144,64,4,144,64,32,146,73,36,144,73,0,0,8,0,0,0,4,128,0,32,2,73,32,2,9,0,0,0,0,0,0,4,146,73,4,146,
65,0,16,0,36,16,8,4,130,73,0,0,0,0,0,0,0,0,72,0,0,0,4,144,65,32,18,8,4,146,0,32,0,64,4,128,73,36,144,9,36,146,73,0,144,8,36,128,64,0,16,0,4,18,9,36,0,8,36,2,8,0,0,72,36,146,9,36,128,1,0,0,64,4,0,0,0,16,
73,0,146,72,0,128,0,0,0,72,0,18,64,36,146,73,4,146,65,36,144,73,36,144,72,4,2,72,4,0,0,0,146,72,0,0,73,36,18,64,36,146,1,4,16,9,4,146,64,0,0,1,36,0,73,36,16,0,0,146,73,36,130,0,0,2,73,32,16,8,4,128,1,
32,144,65,0,128,72,36,16,65,32,130,64,36,0,0,36,0,1,0,144,1,0,0,73,0,130,73,36,130,73,0,2,9,36,144,72,0,146,73,0,146,1,36,146,73,32,16,8,32,16,1,0,0,0,32,2,0,0,0,64,36,0,1,0,0,72,36,146,73,4,16,73,32,
0,73,32,0,73,0,2,0,4,146,64,0,0,0,0,16,9,32,16,1,36,18,0,36,16,9,0,18,73,36,0,0,0,146,65,4,0,0,0,128,1,36,146,73,36,130,73,4,144,8,0,18,73,36,146,73,36,146,73,32,0,0,4,128,64,0,2,64,4,128,64,36,2,65,36,
146,73,0,2,65,0,0,9,32,144,72,32,0,9,32,2,9,32,18,64,4,128,65,36,146,0,0,0,8,36,146,64,0,130,0,0,0,0,36,144,73,36,146,72,32,2,73,36,130,0,32,2,64,0,146,72,4,0,1,36,128,72,4,146,0,36,16,73,4,146,0,32,130,
9,32,18,72,0,130,73,32,0,9,36,0,0,4,146,65,0,0,1,0,146,0,0,16,73,4,146,73,32,0,1,36,146,72,32,0,72,36,128,0,0,146,8,36,146,73,36,130,64,36,0,73,36,0,64,32,0,9,32,146,73,32,2,1,0,0,0,32,2,73,36,128,9,36,
2,64,32,18,0,32,144,1,36,0,1,36,146,64,0,146,0,0,0,9,32,144,65,0,18,72,4,130,64,0,16,9,36,146,64,36,18,64,4,0,8,32,18,73,36,146,73,36,146,73,36,0,72,0,16,0,0,18,73,0,2,72,0,2,64,4,146,73,32,130,9,4,16,
72,32,18,0,4,18,72,0,0,0,4,146,73,36,2,1,0,0,0,36,146,73,36,146,64,4,16,9,36,128,64,0,0,73,32,0,73,0,18,0,36,144,8,36,130,64,4,146,72,4,130,73,36,130,64,36,144,0,36,146,64,4,146,72,36,0,9,36,0,72,0,144,
73,36,144,9,32,144,1,36,146,72,0,2,1,32,146,72,0,146,73,32,2,64,0,130,9,32,128,9,32,144,72,4,130,64,32,18,1,36,146,64,32,130,0,32,144,1,4,146,73,36,146,65,36,144,0,0,0,8,0,0,0,36,144,64,4,18,73,0,0,73,
32,144,64,0,144,1,32,144,0,4,144,0,36,146,65,32,0,0,36,146,64,4,144,9,0,0,0,0,16,9,0,0,0,0,2,72,36,0,65,36,146,1,36,128,0,36,130,0,4,128,0,4,146,73,36,146,9,36,144,73,32,146,73,36,0,0,36,146,72,36,146,
0,4,146,64,36,16,0,0,146,72,0,0,1,36,2,73,4,0,0,0,16,1,0,0,1,0,0,0,36,16,1,4,146,73,36,146,73,4,146,9,36,18,73,32,144,9,36,128,72,4,0,0,32,16,0,0,18,73,32,146,73,0,16,1,36,2,73,0,146,64,4,144,72,0,2,73,
0,128,9,36,146,73,32,0,64,32,2,64,0,16,0,4,2,9,36,16,72,0,0,73,0,0,73,4,18,9,0,2,73,36,0,0,0,2,73,36,146,0,32,0,0,0,130,73,36,128,0,0,0,72,0,18,72,36,16,72,4,18,65,36,16,73,32,2,72,0,0,1,36,130,73,36,
16,0,0,0,65,0,0,0,0,16,1,0,128,0,0,16,8,0,0,0,0,0,9,32,2,64,32,0,9,0,0,73,36,18,0,36,128,0,32,128,72,36,144,72,32,0,8,0,18,8,36,146,64,0,146,0,32,18,73,32,18,73,0,16,73,36,146,73,0,18,65,0,146,64,32,0,
73,36,0,0,36,146,64,36,16,0,0,144,1,32,128,0,4,128,73,4,0,0,0,2,0,4,130,9,36,144,73,32,130,73,4,144,1,32,18,64,0,130,73,32,18,0,36,16,1,0,0,0,36,146,73,36,146,65,36,18,72,0,2,73,32,0,0,0,0,9,36,146,73,
36,146,65,32,0,0,32,128,64,0,2,9,32,144,1,36,144,0,36,0,0,4,130,73,0,0,1,32,16,9,36,144,8,0,146,1,36,18,8,32,144,1,0,2,73,4,18,8,0,130,64,36,128,73,36,128,1,0,0,9,36,146,0,0,18,0,4,146,72,4,146,64,36,
0,72,4,0,8,4,128,72,4,16,64,0,16,1,0,2,64,0,16,1,36,130,73,36,128,8,32,0,1,32,146,73,0,128,64,36,130,65,36,146,64,0,2,65,36,2,0,0,0,73,32,146,72,0,0,9,36,146,9,36,146,73,36,146,73,0,0,8,32,18,73,32,144,
0,36,146,0,0,144,1,32,0,0,0,2,72,4,130,73,36,128,0,0,16,0,0,0,0,36,128,0,4,128,8,36,128,72,36,144,73,36,16,65,0,146,65,32,18,64,36,144,64,0,146,65,32,130,72,32,146,65,36,128,73,36,146,73,32,18,64,36,144,
73,36,18,65,4,146,0,0,18,73,32,18,72,0,146,65,32,2,1,32,16,9,36,128,9,32,0,64,4,2,0,0,130,0,4,0,0,36,146,73,36,146,73,36,146,0,0,128,0,32,0,1,32,146,64,0,144,1,32,0,1,32,146,65,36,144,0,0,0,9,0,18,73,
36,0,0,0,16,0,4,128,1,36,146,1,36,144,72,36,16,73,4,128,64,0,146,9,36,130,65,36,146,9,36,146,0,0,130,65,0,0,73,36,18,8,36,146,72,36,18,9,32,0,1,36,16,0,0,16,0,36,16,72,0,16,1,36,146,73,0,146,73,36,146,
73,36,146,0,0,2,72,4,0,73,32,128,73,0,144,65,4,130,9,4,144,73,0,146,64,0,18,0,32,146,0,32,0,65,0,0,73,36,146,72,4,18,73,36,128,9,36,146,0,0,144,0,4,128,73,36,144,0,0,0,64,32,18,8,0,146,73,0,16,9,0,16,
8,0,0,9,0,146,73,36,146,73,4,18,73,36,146,1,36,0,1,32,144,73,36,130,0,0,0,72,32,16,0,36,18,73,32,18,73,36,144,0,4,128,0,36,146,72,4,128,73,36,0,0,36,128,9,32,130,64,0,2,65,32,2,65,0,0,73,4,16,72,4,130,
0,36,146,65,36,0,0,4,146,65,36,146,0,0,0,0,0,0,1,32,2,72,36,128,73,36,0,1,36,146,73,0,144,73,32,144,1,32,146,0,32,146,64,4,128,64,4,130,73,32,18,9,36,18,9,4,130,9,4,146,73,32,18,0,4,2,72,0,128,73,4,146,
0,32,130,0,0,146,0,4,0,0,0,128,73,36,18,73,36,144,72,36,146,9,36,18,0,0,0,9,36,2,0,4,0,9,4,18,9,36,146,0,32,130,64,0,2,1,32,18,73,0,128,0,32,16,1,0,0,8,4,18,1,36,146,73,32,146,73,0,0,0,32,16,1,36,144,
9,36,128,1,0,130,9,0,146,0,4,144,0,4,146,73,36,144,8,32,146,73,32,0,1,36,130,73,0,146,65,36,2,72,36,130,64,36,18,73,0,146,73,36,144,9,36,146,0,4,146,64,0,146,72,4,128,73,36,146,73,36,146,8,36,0,0,36,130,
0,4,146,72,36,144,73,36,144,8,0,2,64,0,0,1,36,144,0,32,2,73,36,146,73,36,18,8,0,0,64,0,18,0,36,2,8,4,0,1,0,128,64,4,130,72,36,144,73,32,0,73,36,146,73,0,128,73,0,2,1,36,130,73,0,0,73,32,0,8,0,0,0,36,144,
1,4,0,73,36,130,0,36,130,73,4,16,9,4,146,65,36,146,0,4,146,65,36,146,0,4,0,72,0,18,73,36,144,9,4,0,0,4,18,64,0,0,8,36,146,64,32,130,73,36,144,73,32,146,0,32,2,65,4,144,9,0,0,0,0,0,0,0,144,73,4,0,1,4,146,
0,4,146,73,4,2,8,4,2,73,0,2,73,36,130,8,0,2,73,4,130,73,36,146,73,4,0,0,0,128,64,0,0,1,4,130,0,0,18,73,4,146,0,4,146,1,36,128,72,0,2,73,0,0,0,0,16,1,0,0,1,0,2,0,0,2,72,0,0,0,0,2,1,32,18,72,0,146,73,36,
146,64,0,128,1,32,144,9,32,16,8,32,16,73,4,146,73,36,0,0,0,146,0,0,128,0,36,2,72,0,2,64,0,146,0,36,144,0,0,18,1,36,144,0,4,0,64,32,146,64,36,130,73,32,146,64,4,2,72,0,0,73,0,128,72,4,146,64,4,2,0,32,0,
73,36,144,9,36,146,65,36,146,73,32,18,72,36,128,64,0,2,73,36,146,73,36,0,0,4,0,73,36,128,73,4,144,0,4,146,73,36,144,9,36,0,0,0,18,65,36,144,73,36,130,72,0,18,73,36,16,1,36,130,72,0,0,9,36,144,0,4,146,
0,0,16,9,32,18,64,4,144,1,36,146,73,32,2,8,0,18,73,0,144,9,4,146,64,36,146,72,36,130,8,0,18,73,36,146,64,0,128,9,32,2,64,0,144,9,0,146,1,32,0,0,4,146,73,4,146,72,32,146,64,4,2,0,0,18,73,4,128,0,4,146,
64,32,18,0,0,16,73,36,18,72,0,2,73,32,144,73,36,144,72,32,0,1,36,144,73,36,146,64,36,146,1,32,0,0,0,0,73,36,144,9,0,0,1,36,2,9,36,146,72,32,0,72,36,146,73,36,0,64,0,0,73,36,18,73,32,146,73,36,130,8,36,
146,72,32,0,0,32,0,1,36,146,72,36,128,0,36,128,9,36,144,8,32,18,1,0,0,8,4,144,1,36,128,9,36,146,73,36,18,73,36,16,73,32,146,73,36,130,72,4,146,72,0,2,72,4,144,0,4,144,1,36,146,64,0,146,1,36,18,64,36,146,
72,4,18,0,4,146,64,0,18,73,0,146,73,36,146,73,32,128,73,4,144,72,36,128,1,0,130,0,0,0,0,36,144,1,36,146,65,32,0,1,36,146,1,0,2,72,4,130,72,4,0,73,4,130,73,4,144,8,0,0,0,0,2,8,0,18,64,36,0,1,4,146,73,32,
0,1,36,144,0,36,146,65,36,146,1,36,128,1,36,146,73,32,0,0,4,146,64,36,18,73,32,128,9,36,146,0,36,144,64,4,0,0,0,146,73,36,144,9,36,146,72,32,146,72,0,144,0,36,0,0,32,18,73,36,0,0,0,146,72,36,16,8,0,18,
73,32,146,73,36,2,73,36,146,73,4,146,72,36,146,73,4,130,64,0,144,72,32,0,0,4,146,73,36,18,9,32,0,0,0,0,1,0,146,73,32,146,72,32,130,65,36,146,73,32,16,72,36,146,73,0,146,8,36,146,73,32,146,65,0,18,8,0,
0,73,32,0,73,36,146,64,32,0,64,0,0,9,32,18,73,36,146,64,0,18,8,0,0,9,32,0,0,36,128,9,36,146,73,4,0,9,4,128,0,0,0,0,0,0,1,36,144,0,4,2,64,0,0,73,32,0,0,36,128,64,0,146,0,36,128,0,36,128,64,0,2,73,32,2,
1,36,0,0,4,146,72,4,146,73,32,0,0,0,0,1,32,146,1,36,146,73,32,146,64,0,144,72,0,0,9,0,146,73,36,144,1,0,128,8,4,0,0,0,18,9,0,18,73,32,2,72,32,0,0,36,144,0,32,0,0,0,0,65,0,130,72,32,144,0,4,144,0,0,128,
1,36,146,1,32,0,0,0,0,0,0,2,9,32,16,1,36,0,0,4,128,0,0,146,64,4,16,0,36,18,72,0,0,0,0,0,9,0,0,1,0,144,0,0,146,1,32,0,0,4,0,73,0,146,64,36,130,0,0,18,64,36,128,0,0,2,0,4,144,65,36,128,0,32,2,73,36,0,0,
36,130,73,32,0,0,32,0,1,0,0,0,4,130,1,4,0,1,32,146,72,0,130,0,36,2,64,36,146,64,0,128,72,4,146,1,32,146,9,32,130,73,36,18,64,36,128,0,0,128,73,4,130,64,0,0,64,36,146,9,4,128,72,32,146,73,32,0,1,36,144,
65,36,0,64,0,0,0,4,0,0,0,0,72,4,16,64,0,18,0,32,0,0,0,2,9,36,146,0,32,0,1,36,0,0,0,0,0,0,0,1,32,0,65,0,0,73,32,0,8,0,18,8,4,128,73,0,0,1,36,16,0,0,144,9,4,18,0,0,146,9,0,128,9,36,144,0,0,2,72,4,130,73,
36,146,64,0,2,73,32,0,0,0,2,9,32,146,73,36,144,72,36,0,8,32,130,72,32,0,1,32,0,8,0,146,72,32,146,64,36,2,73,36,2,72,0,128,0,0,0,0,0,144,0,32,146,72,0,16,73,36,0,0,4,130,73,0,0,1,36,146,1,36,146,8,0,0,
9,4,0,64,32,146,73,32,0,9,4,144,72,32,128,64,32,0,0,0,144,64,36,0,73,36,144,73,4,0,0,4,128,0,0,0,1,36,18,8,36,146,64,32,2,73,36,128,64,0,2,0,4,146,1,0,2,1,36,18,9,32,0,0,0,2,0,32,0,0,0,144,73,32,0,1,4,
16,73,4,146,1,4,146,1,32,130,64,0,18,9,36,18,73,36,128,65,4,144,65,0,0,64,0,0,9,0,18,65,36,0,0,32,146,73,36,146,73,36,130,73,32,130,0,36,128,8,36,146,73,36,144,0,4,144,1,4,128,64,32,0,0,0,130,72,0,18,
0,36,130,73,32,0,65,32,18,9,36,2,73,32,0,8,36,0,0,32,146,64,0,0,73,4,144,1,36,146,73,36,16,73,0,0,0,0,2,9,32,0,73,36,146,72,36,146,64,36,16,0,32,146,0,0,16,0,4,0,0,0,146,73,32,144,73,36,0,72,0,2,72,0,
18,72,4,0,9,0,0,1,36,146,73,32,2,65,32,18,1,36,146,9,36,144,64,36,146,72,4,144,9,36,2,72,0,146,0,4,128,9,0,16,8,36,146,0,0,144,73,36,146,0,0,18,1,36,2,73,0,146,9,32,0,65,0,146,72,0,146,0,36,146,64,36,
18,73,36,146,0,32,0,0,36,2,65,0,146,73,32,16,8,4,146,64,4,146,64,4,128,8,32,0,1,32,146,0,0,2,73,36,18,73,36,146,73,36,146,73,4,0,1,36,130,72,32,146,73,36,18,73,0,128,0,0,0,73,36,18,72,0,0,1,4,146,64,0,
18,72,4,16,64,0,130,72,0,0,8,32,0,9,36,146,73,36,0,1,32,146,9,36,146,64,36,146,73,36,146,64,0,2,64,36,2,8,0,146,8,4,146,65,36,0,73,0,128,0,4,0,9,0,0,64,0,146,73,36,146,1,36,2,73,4,144,72,36,18,65,0,0,
64,0,0,0,32,0,1,36,144,65,0,2,73,4,146,73,36,18,73,0,0,0,0,18,73,36,130,72,0,144,0,0,2,73,36,146,64,0,0,0,0,146,73,36,128,1,36,128,73,36,146,64,0,18,73,32,130,64,0,0,8,0,0,0,4,0,0,0,2,73,0,0,1,36,0,1,
36,146,73,4,146,73,36,0,73,32,144,8,0,0,0,0,18,73,0,18,0,0,0,1,36,128,72,4,144,1,32,18,72,0,0,0,0,146,64,36,144,72,0,146,73,36,146,9,32,0,0,0,18,0,36,18,9,0,0,1,36,128,8,32,2,73,0,128,73,0,144,65,36,130,
64,0,18,1,0,2,73,36,146,64,4,128,72,0,18,64,0,0,9,0,18,1,4,144,73,32,130,1,0,0,72,32,144,0,0,2,0,0,0,1,36,146,73,36,144,1,36,130,64,0,0,0,0,18,65,32,18,72,36,146,9,0,0,0,0,2,73,32,0,0,0,0,0,0,0,9,0,0,
0,4,0,0,0,0,0,4,18,73,36,146,73,36,146,64,36,128,1,0,144,9,0,18,64,0,2,73,36,0,0,0,130,8,0,0,8,4,146,73,36,128,64,36,128,73,4,146,72,0,0,0,0,146,73,4,144,73,32,18,0,32,18,73,36,146,1,36,146,73,32,18,1,
0,0,1,36,146,73,0,16,72,4,146,73,36,146,0,0,2,73,0,128,8,36,146,73,32,16,1,0,16,73,0,2,0,4,16,8,36,18,73,36,146,73,36,144,1,36,146,72,32,0,9,0,0,8,0,128,0,4,128,1,0,0,72,0,128,0,0,2,0,36,0,0,0,0,8,0,144,
8,4,146,73,36,144,1,32,130,64,4,18,0,36,146,73,0,0,1,36,128,73,0,146,65,36,130,73,36,128,65,36,0,0,4,130,65,0,146,64,36,146,73,32,2,72,36,146,73,32,0,0,32,0,9,32,146,64,36,0,1,0,144,9,36,146,9,36,144,
0,0,146,65,0,128,73,36,0,73,36,144,9,4,2,65,36,18,8,4,2,1,36,146,72,32,16,8,0,130,73,36,146,73,32,18,65,36,146,0,0,0,72,32,144,65,0,0,1,36,144,1,0,0,0,0,16,0,36,144,73,0,16,1,36,18,0,0,2,64,4,0,0,0,0,
73,32,146,72,0,146,65,36,146,73,4,146,0,0,130,65,0,18,73,0,0,0,0,16,9,36,146,72,4,144,0,0,0,1,36,146,0,0,2,0,0,146,73,36,144,0,0,18,0,0,128,0,0,0,73,36,2,73,0,0,0,0,0,8,4,144,65,0,146,9,36,146,73,36,144,
0,36,144,9,0,16,73,32,146,0,36,146,9,36,18,73,36,146,73,36,130,1,0,0,73,4,0,0,4,2,73,32,2,0,0,146,0,32,18,72,4,18,64,4,128,72,36,2,65,0,0,0,0,0,1,36,128,9,36,144,0,0,0,0,0,0,0,32,0,1,0,0,0,0,128,0,0,146,
73,36,146,73,32,18,1,32,128,0,32,146,73,36,130,9,4,146,9,36,2,72,0,18,72,0,0,0,4,128,72,4,130,64,0,128,0,0,0,0,4,144,0,0,0,9,32,128,0,0,128,0,32,0,9,0,2,72,0,2,9,36,146,64,36,128,65,4,128,9,36,146,73,
32,0,1,32,0,0,0,146,73,36,146,1,36,128,73,36,18,8,0,0,9,36,130,65,0,0,73,32,146,0,36,144,0,0,0,73,0,2,8,36,2,73,36,2,64,32,0,73,36,146,73,36,18,73,4,128,0,0,0,0,4,146,73,0,2,1,36,128,0,4,128,0,0,146,73,
4,146,73,32,0,64,0,0,72,4,0,0,0,130,0,4,128,9,32,2,1,36,144,1,36,0,1,36,128,9,36,146,73,36,16,8,0,144,0,0,0,73,36,144,73,36,0,0,0,0,0,4,144,9,0,0,8,0,2,73,0,2,65,0,128,73,36,128,73,32,146,72,4,144,72,
36,128,0,36,146,65,0,0,73,36,144,9,36,146,0,36,146,0,36,128,0,36,0,65,0,2,65,32,128,1,32,146,72,0,18,73,32,2,73,0,0,73,32,146,9,32,18,65,32,144,0,0,2,72,0,0,73,32,0,0,0,2,1,36,146,64,0,2,73,36,128,73,
36,128,73,36,146,73,32,2,73,36,18,65,4,0,8,4,144,1,4,0,1,0,18,72,0,146,72,0,128,0,0,0,9,4,146,64,0,0,0,0,2,64,0,0,1,36,146,73,32,0,72,32,0,0,4,146,1,0,144,0,32,0,64,32,18,8,32,128,73,36,2,9,0,146,65,36,
0,64,32,146,65,4,144,72,36,146,64,0,0,64,0,128,72,36,0,9,0,130,73,32,2,9,36,146,64,0,2,73,4,146,73,36,144,73,36,0,73,36,128,0,0,128,8,32,128,8,0,0,0,0,0,8,0,144,73,0,16,1,32,0,1,36,0,0,0,146,72,0,16,73,
36,128,1,32,18,73,36,128,1,36,144,73,36,0,65,32,146,72,4,128,9,32,0,73,32,128,1,36,146,73,4,0,65,36,18,73,0,0,0,0,0,9,0,146,73,4,2,72,36,0,0,4,128,73,32,2,1,36,18,73,36,146,72,4,2,9,32,128,9,0,0,64,0,
146,0,4,0,8,0,146,73,4,16,73,32,146,65,36,2,0,0,0,1,32,18,65,36,146,8,4,130,0,0,18,73,32,128,64,0,146,73,36,128,72,36,0,72,0,0,0,0,130,73,4,144,0,0,18,0,4,18,8,0,18,73,36,146,0,0,2,64,36,146,64,0,146,
64,4,130,64,32,2,72,32,2,0,32,144,64,0,0,72,32,146,72,36,128,9,36,128,72,32,18,9,32,18,73,4,144,0,0,18,73,36,130,73,36,128,65,0,18,73,4,128,8,0,146,0,4,0,0,36,0,0,0,0,73,4,18,72,0,2,65,36,146,73,4,130,
72,0,2,73,0,128,73,32,18,1,32,128,0,4,144,73,36,146,9,36,146,72,4,0,73,32,16,0,0,2,0,0,144,0,0,146,65,0,0,0,36,144,9,36,0,9,32,0,9,32,128,9,0,144,1,32,130,0,0,18,1,36,146,1,4,18,64,36,128,0,0,2,9,36,128,
0,36,0,73,36,0,8,4,128,9,36,144,72,4,16,8,4,130,73,36,146,64,4,144,0,4,18,1,0,2,73,36,144,9,0,2,0,4,0,8,0,0,0,0,130,9,0,146,72,4,146,64,36,146,73,32,0,0,36,144,0,4,16,9,32,144,1,4,130,73,32,18,72,32,16,
1,4,130,73,4,0,64,32,130,0,0,146,72,36,128,9,36,146,8,32,0,9,36,128,1,36,0,73,36,0,73,36,128,65,36,16,9,36,146,65,32,146,0,0,128,0,32,0,1,4,146,72,0,0,72,0,2,73,0,146,64,32,130,64,0,146,1,0,18,64,0,0,
0,4,144,64,4,128,9,36,146,72,36,146,65,0,2,73,36,146,64,4,146,8,36,128,73,36,128,0,36,0,0,32,18,64,32,146,9,32,18,0,0,0,73,36,146,65,36,16,0,36,18,8,4,0,9,0,18,73,36,0,0,4,146,9,0,2,73,36,2,72,36,144,
0,36,0,0,36,18,72,4,146,0,4,128,0,0,0,65,36,146,73,36,18,73,36,16,64,0,128,1,4,146,73,4,146,0,0,0,1,0,18,73,4,146,9,32,144,65,36,18,73,36,146,65,32,130,73,32,2,64,32,144,9,0,2,64,4,144,1,36,144,73,0,0,
0,0,2,73,32,146,72,4,146,0,32,0,73,0,146,73,32,146,0,4,128,1,4,144,64,36,146,72,36,144,1,36,0,8,0,128,0,0,18,73,32,0,72,32,144,72,4,144,0,4,16,73,36,144,9,0,128,73,36,130,9,4,0,9,32,0,0,0,2,65,36,146,
64,0,2,73,36,2,0,0,128,0,36,2,65,0,0,9,0,146,73,32,146,0,36,0,9,36,146,72,0,0,0,4,128,9,36,146,73,36,146,0,4,128,0,36,146,73,36,18,9,36,18,73,32,0,64,4,18,64,36,18,9,32,128,73,36,146,0,0,0,65,36,16,0,
0,128,0,36,146,72,4,146,72,4,128,9,4,146,1,32,130,64,32,0,73,32,0,0,0,130,73,4,130,0,4,128,0,32,0,0,0,128,8,0,130,73,36,146,1,36,146,64,0,0,64,36,128,1,0,144,72,0,128,1,36,130,9,4,146,64,32,2,73,4,0,73,
0,0,8,36,0,8,0,18,72,32,146,64,4,2,9,0,0,73,36,146,73,4,18,8,36,146,72,36,16,64,0,144,0,0,2,9,0,146,1,32,0,0,0,16,8,0,0,8,36,144,9,36,146,73,36,128,0,32,0,1,0,18,9,36,130,64,36,2,8,4,144,9,0,128,9,36,
146,73,0,146,65,32,0,0,0,146,0,4,144,73,0,18,8,4,128,64,0,0,0,4,16,73,36,0,0,0,0,0,0,0,0,0,0,73,32,0,64,36,130,8,0,2,65,0,18,73,36,146,64,0,0,72,32,128,72,4,146,64,4,146,73,36,0,0,32,16,8,0,0,9,36,0,0,
36,146,73,36,146,73,36,146,72,4,144,73,36,0,73,36,144,9,36,18,1,32,16,0,36,146,73,0,18,0,36,146,8,0,0,9,36,16,0,0,0,9,0,2,1,32,130,73,36,130,73,36,0,0,0,128,1,32,0,0,0,144,73,4,0,1,32,0,0,4,128,64,32,
128,8,36,2,0,0,2,72,0,16,73,4,146,65,0,144,9,36,144,72,36,128,0,0,0,65,4,16,9,36,146,64,4,146,65,32,146,73,4,146,73,32,2,0,0,0,72,36,144,0,32,0,72,0,146,72,0,146,64,36,146,73,36,146,64,0,0,1,32,18,73,
36,146,73,36,146,73,36,128,0,0,16,0,36,146,64,0,0,0,0,0,0,32,144,64,36,146,72,0,0,73,32,0,65,4,18,1,32,0,0,0,0,8,0,16,9,0,0,0,36,146,9,0,0,0,0,0,8,0,0,0,0,0,9,36,146,1,0,0,1,4,0,1,36,2,73,36,128,73,32,
146,8,0,0,8,0,0,73,36,146,73,32,128,73,0,0,1,36,146,72,0,130,64,0,0,1,32,146,1,32,0,73,36,130,1,36,144,65,0,0,9,32,144,0,4,144,9,36,144,1,4,128,64,0,2,1,0,0,0,0,2,1,36,146,73,36,146,73,36,128,0,0,18,73,
36,146,73,36,144,73,36,146,73,36,146,64,0,0,0,36,146,65,36,130,72,36,146,73,36,128,1,36,2,73,36,128,1,36,146,73,0,2,72,0,18,0,4,146,72,36,146,72,36,16,8,0,0,0,4,0,0,0,2,8,36,128,9,32,0,64,0,16,0,36,144,
0,0,146,9,36,146,72,0,0,9,36,146,72,0,130,72,32,16,64,4,144,65,32,146,72,36,146,73,36,144,0,32,0,1,36,128,0,0,144,73,0,144,64,0,128,72,0,144,73,36,146,9,36,146,0,0,2,1,0,2,73,0,16,0,0,146,73,36,146,73,
36,146,65,36,18,65,36,0,72,4,128,0,0,0,9,4,144,72,0,2,0,4,146,65,36,146,73,32,18,65,36,16,1,0,0,0,0,16,73,36,144,0,36,0,0,0,2,72,36,130,8,4,18,64,4,16,8,36,2,8,4,128,0,36,146,73,0,0,8,0,130,0,0,18,9,36,
144,64,4,128,1,32,144,9,36,146,73,32,18,8,32,144,1,32,2,1,4,0,9,36,128,9,0,130,72,0,0,73,36,130,1,32,18,73,4,144,72,36,130,9,32,0,9,36,146,64,32,0,73,0,146,64,4,146,73,36,146,73,36,146,65,4,130,73,36,
146,73,36,146,0,0,146,0,0,128,0,0,0,8,0,144,0,4,144,64,0,2,73,36,0,0,36,144,73,36,0,8,36,2,73,36,0,0,0,16,1,4,2,72,0,0,8,0,0,0,0,144,0,0,146,73,36,146,0,0,0,1,4,144,0,32,0,0,0,0,9,4,130,73,36,146,0,4,
146,72,4,18,73,36,146,72,0,16,9,0,16,1,32,128,73,0,18,0,32,146,1,32,130,72,36,16,72,32,144,73,36,146,72,36,146,8,0,18,1,36,146,73,36,128,73,32,0,64,4,0,0,32,16,73,4,146,64,4,0,73,32,0,9,0,130,1,36,128,
8,32,144,73,36,146,0,0,18,73,36,128,64,0,146,73,36,130,1,0,0,64,36,144,1,4,146,65,32,0,8,36,0,64,0,0,73,36,146,0,0,0,72,0,16,0,4,146,73,36,146,73,36,146,73,32,144,64,0,0,8,0,0,64,0,0,73,4,2,65,0,146,0,
4,146,0,0,0,64,4,0,9,4,128,64,32,16,72,0,0,0,0,0,1,4,146,73,36,146,73,32,18,8,0,144,72,4,144,9,36,146,73,36,18,0,32,16,1,0,18,64,0,2,8,0,0,0,0,2,73,36,128,0,0,18,64,0,128,1,4,16,9,32,128,1,0,0,8,0,2,1,
36,130,64,4,18,72,0,128,0,4,16,0,32,18,73,36,144,73,0,18,65,4,2,64,32,18,73,36,146,65,4,146,73,36,144,9,36,144,0,36,146,9,32,2,73,32,18,73,36,146,73,32,0,9,36,146,73,0,0,0,4,0,9,32,18,65,0,16,72,32,0,
9,32,18,0,32,18,0,0,16,72,0,0,0,32,0,1,36,128,9,0,18,64,0,144,9,0,16,0,32,146,73,36,144,73,0,128,1,36,146,73,36,146,72,0,0,9,36,18,73,32,16,73,0,0,0,4,128,0,0,0,1,0,0,0,32,0,0,0,0,72,32,146,72,0,2,1,0,
2,73,36,146,9,36,18,73,32,18,0,0,18,73,36,146,9,36,144,1,0,2,73,36,146,0,4,146,9,36,146,73,36,146,64,36,144,64,4,0,64,0,128,72,4,144,8,0,16,1,0,0,65,32,18,73,0,128,65,36,0,8,32,144,0,32,2,65,0,146,0,0,
2,8,0,18,64,36,144,72,36,146,65,0,0,9,32,18,73,36,0,1,0,0,9,0,128,64,0,0,0,4,0,65,0,18,72,32,144,9,36,144,9,36,128,1,36,18,73,36,144,64,0,16,64,36,2,73,0,2,72,0,0,0,32,2,73,36,130,0,36,130,9,32,146,72,
0,146,73,4,130,72,36,130,72,0,144,0,0,146,0,4,146,73,32,146,1,0,16,64,0,2,73,36,144,73,36,146,65,36,146,73,32,18,73,0,0,1,36,146,0,0,0,0,0,146,73,0,18,0,0,146,72,36,144,0,32,144,65,0,0,72,32,0,64,4,2,
0,0,0,0,0,0,72,0,18,9,36,146,64,36,144,65,0,0,73,36,18,72,36,128,72,36,128,0,4,146,0,36,130,72,0,16,0,0,2,8,36,130,72,36,144,1,32,0,9,32,146,65,32,16,0,4,16,64,0,0,0,36,128,0,36,0,9,36,146,72,4,18,73,
36,128,0,0,128,1,0,0,1,32,128,0,0,16,8,36,146,73,36,146,9,36,18,73,36,2,73,0,0,0,0,16,0,0,2,0,0,0,0,0,0,9,36,144,9,32,0,64,0,128,0,36,0,0,4,128,72,0,2,72,36,146,73,0,0,1,0,2,0,0,128,64,0,16,0,0,0,73,36,
146,73,36,146,73,4,128,0,0,146,73,36,18,1,32,146,0,0,18,73,36,130,0,36,144,8,36,2,0,0,2,0,36,128,1,0,0,9,36,146,73,36,0,65,36,0,1,0,16,0,4,18,0,0,16,64,4,2,64,36,144,0,0,2,65,36,146,73,0,16,0,0,16,9,0,
16,64,0,146,1,0,0,9,36,128,73,36,144,0,0,0,73,4,128,0,0,0,0,32,0,8,0,0,73,4,144,0,0,2,73,0,18,73,32,18,64,0,2,72,4,128,8,0,2,8,36,146,72,0,146,65,32,0,0,0,144,0,0,0,64,0,2,73,36,146,73,0,18,73,36,146,
9,36,146,9,0,0,0,0,0,1,36,128,72,4,0,73,0,2,9,32,0,73,36,144,1,0,0,1,36,144,73,36,2,64,0,146,73,0,0,1,32,2,0,4,18,8,36,128,1,36,146,8,32,128,73,0,0,9,36,146,0,0,18,9,0,144,64,0,16,0,0,128,0,0,18,9,0,0,
64,0,0,64,0,0,72,4,0,8,0,128,72,0,0,73,0,2,73,36,2,0,0,2,73,36,16,0,4,144,1,4,144,65,32,0,0,36,146,73,4,130,73,0,146,65,36,146,72,36,2,73,36,0,0,0,0,0,0,0,9,36,144,1,32,18,73,36,146,72,0,128,73,32,18,
0,4,0,0,4,2,65,32,144,0,4,2,72,0,128,1,0,0,1,32,146,0,0,0,0,0,16,65,4,146,72,36,144,73,36,0,0,0,16,72,0,128,64,36,146,0,32,2,0,0,0,8,36,18,72,36,146,73,36,144,1,32,0,65,36,146,73,32,2,73,36,128,73,32,
128,1,4,130,72,36,0,73,36,144,9,36,146,1,32,146,72,0,146,73,36,146,0,4,0,0,0,128,73,4,128,73,32,18,64,4,0,1,36,146,0,4,18,73,36,144,9,36,18,73,36,146,73,36,146,73,36,16,8,4,146,73,36,128,0,0,128,64,0,
18,73,36,0,0,36,144,0,4,146,9,0,0,9,36,144,73,32,18,0,0,16,65,0,16,73,32,146,73,36,146,73,0,0,0,4,2,0,0,128,0,0,128,0,0,18,0,32,18,0,0,0,0,36,146,72,0,0,73,36,18,72,0,144,0,0,18,73,36,18,72,0,144,1,36,
16,64,36,146,73,36,146,73,36,146,9,36,130,65,4,146,9,0,0,73,36,146,1,0,18,0,36,146,64,0,18,72,4,146,64,36,0,0,32,0,9,0,0,0,0,146,0,4,0,73,4,146,73,36,144,73,0,146,73,36,146,73,36,2,73,36,146,9,0,0,73,
36,130,72,36,146,64,0,0,0,0,0,0,0,146,65,36,146,9,36,18,73,32,2,73,32,18,72,32,16,65,36,146,73,4,146,64,0,0,1,36,128,8,32,18,73,36,146,73,36,0,9,0,128,1,36,18,8,36,146,73,36,144,72,0,16,0,32,146,65,0,
0,73,0,2,9,36,146,73,36,16,9,36,144,1,36,146,9,0,0,65,0,130,0,0,146,9,32,0,0,4,144,64,32,16,8,0,0,0,36,146,73,36,146,64,0,0,1,36,2,73,0,146,64,0,0,1,32,0,73,36,146,73,32,146,73,36,146,73,36,146,73,4,146,
73,32,2,64,36,2,73,36,2,0,0,18,64,4,144,1,36,146,9,32,0,8,0,18,9,32,18,65,0,18,0,0,0,8,4,0,0,0,128,9,36,146,1,4,146,64,0,16,72,36,146,9,36,0,1,36,144,73,4,130,9,4,0,72,36,2,1,0,16,9,36,130,64,0,2,1,0,
130,65,36,146,1,32,18,73,0,0,9,36,146,72,36,18,73,36,18,1,36,18,64,36,2,9,36,128,65,4,128,8,36,0,64,36,2,72,0,2,73,0,2,9,36,146,0,36,146,9,0,128,0,0,0,9,36,146,72,36,18,9,36,146,73,36,146,73,36,146,73,
36,130,0,4,0,72,0,2,73,36,0,9,0,144,0,0,128,0,0,0,64,36,146,1,4,146,65,0,0,73,36,128,64,0,144,73,32,146,73,36,130,0,4,146,72,36,0,0,36,16,64,4,128,8,4,144,65,36,0,72,36,0,1,36,146,73,36,146,72,36,0,64,
0,0,0,0,16,9,36,130,8,0,16,0,4,146,64,0,0,65,36,18,9,32,128,64,4,128,8,4,18,64,32,146,0,32,2,1,32,144,9,32,16,0,4,18,9,0,0,64,36,128,72,4,144,0,4,128,0,0,18,64,36,0,64,0,128,73,32,18,9,4,146,72,4,146,
9,0,0,0,0,128,72,0,0,73,4,128,73,0,2,73,4,18,72,0,128,0,0,0,0,4,128,8,0,2,65,32,0,0,0,2,0,4,146,9,36,146,64,0,146,0,4,128,8,0,2,72,0,18,72,0,128,0,4,128,0,4,0,73,4,18,73,0,146,1,32,2,72,32,0,9,0,0,72,
36,2,73,36,146,73,0,0,1,36,146,8,0,0,72,36,144,1,0,18,1,36,128,9,32,0,0,4,18,8,32,130,0,36,144,73,32,16,0,0,128,9,32,146,0,4,18,65,32,128,64,0,16,1,32,18,0,0,0,1,32,18,1,32,0,0,32,0,1,0,0,0,4,128,73,0,
0,64,4,2,8,0,16,1,36,128,0,36,146,65,36,0,73,36,146,65,36,146,73,36,16,73,32,146,73,36,146,73,36,128,0,32,0,0,0,128,0,36,128,0,4,144,72,4,144,0,4,18,72,36,144,73,0,18,73,36,18,65,4,146,72,4,0,72,36,0,
1,36,146,73,36,146,1,36,2,72,36,146,73,36,128,73,36,144,0,0,0,8,0,0,73,36,146,65,32,0,73,0,146,65,36,128,9,32,0,72,32,18,1,36,0,0,4,128,72,0,18,73,32,2,65,32,18,73,36,2,73,36,0,65,32,128,8,36,0,73,36,
146,64,32,146,73,32,0,0,0,18,1,36,0,0,0,0,0,36,16,72,32,146,73,36,144,9,36,128,0,4,146,73,36,146,73,32,18,72,0,0,0,0,2,0,0,0,64,4,18,0,0,16,0,0,18,64,4,2,8,0,0,9,0,2,73,32,16,1,36,146,72,0,0,0,0,0,1,32,
18,73,36,18,9,36,144,9,32,2,0,0,146,73,32,0,64,36,144,9,32,128,64,0,146,1,36,2,73,36,146,72,0,128,73,4,144,72,0,0,0,0,128,0,0,16,65,36,146,65,32,18,73,36,2,64,36,2,8,32,16,64,0,16,65,0,146,73,36,144,64,
0,128,65,4,128,9,36,146,73,36,128,73,32,130,1,32,0,73,36,146,73,36,128,73,36,146,73,36,146,1,32,146,73,36,0,72,0,0,1,4,0,9,0,0,0,0,2,1,4,128,8,0,0,0,36,146,0,36,146,73,36,146,64,36,146,73,36,18,73,36,
146,1,32,0,1,4,144,0,0,0,9,36,146,73,4,146,73,36,146,0,0,16,73,0,130,8,4,146,72,36,146,64,0,18,1,0,144,1,32,0,9,0,0,0,36,146,72,36,130,72,0,128,65,36,128,73,36,2,65,36,128,73,32,146,8,36,2,72,32,0,1,0,
144,0,0,2,9,0,144,65,36,0,0,0,146,72,4,146,73,36,146,73,36,128,1,36,146,73,36,146,73,32,146,72,0,0,65,36,146,73,36,144,1,32,0,64,0,0,0,36,0,64,0,0,0,0,2,9,32,16,0,0,0,0,4,146,73,36,146,73,36,2,72,36,0,
72,4,130,64,0,0,8,36,146,73,32,0,65,32,0,8,36,146,73,4,130,73,36,144,8,36,146,65,0,146,73,32,2,9,32,144,73,0,128,1,0,2,9,0,16,0,4,146,65,36,130,73,36,146,73,32,146,72,0,0,0,0,146,73,36,2,73,32,144,73,
32,128,9,36,0,1,36,16,1,4,144,0,0,0,1,36,0,73,0,144,0,0,2,0,0,0,9,0,128,73,36,146,65,36,146,65,4,144,73,32,18,65,36,146,73,36,146,73,0,0,1,0,2,72,32,0,9,0,16,9,32,2,73,32,18,73,36,146,72,4,146,1,36,18,
72,4,144,0,0,16,65,0,144,64,32,146,72,4,146,73,36,128,0,0,146,72,0,0,73,36,144,73,36,130,73,32,144,9,36,146,72,0,146,65,32,144,73,32,2,64,36,130,73,36,146,73,4,146,73,36,130,0,0,2,72,36,0,72,0,0,64,0,
0,0,4,18,73,0,144,73,36,128,9,0,16,0,0,2,72,36,2,1,0,128,1,0,0,0,4,0,8,0,0,0,0,0,65,0,2,73,36,146,9,36,146,1,32,146,73,4,146,72,36,146,73,36,130,65,4,146,64,32,144,64,0,144,64,0,0,64,36,18,64,36,0,0,36,
130,72,36,128,0,4,144,73,36,0,1,0,0,8,0,18,64,4,128,0,0,146,73,36,146,73,36,146,73,36,18,73,36,146,72,4,146,72,36,146,64,36,146,0,0,0,73,36,144,8,4,2,73,0,144,73,32,18,65,32,144,0,4,146,65,36,146,72,0,
0,1,32,144,1,4,130,73,36,128,0,36,18,65,0,18,0,0,0,0,0,0,0,36,144,0,36,128,8,32,146,73,36,2,73,32,128,9,32,2,73,36,146,73,36,128,0,0,0,0,0,0,8,0,144,1,36,146,65,0,0,1,32,2,8,4,146,1,32,0,0,36,18,73,36,
146,1,36,0,0,0,144,64,0,2,64,0,18,0,0,0,0,0,18,0,0,144,1,4,146,73,36,146,73,36,146,73,36,144,1,36,144,0,32,0,8,32,0,0,0,16,73,36,128,8,4,146,64,0,0,0,0,146,64,32,0,73,32,146,73,36,0,0,0,18,1,36,128,0,
4,146,0,4,144,65,0,128,9,32,2,65,36,0,73,36,0,0,0,0,1,0,0,72,0,2,64,0,146,72,4,18,9,32,18,0,36,18,73,4,146,72,36,146,9,32,0,0,0,0,1,4,146,73,32,0,0,4,144,0,0,2,72,0,18,1,0,2,8,0,146,9,36,146,73,36,0,64,
0,144,65,36,146,0,4,128,0,36,0,64,0,0,0,36,146,73,36,18,9,36,146,65,4,146,73,36,144,0,0,130,1,4,130,0,36,128,72,0,0,0,36,0,1,36,146,72,36,128,65,36,146,72,0,18,73,4,18,64,4,144,8,32,18,64,0,146,73,36,
146,72,32,0,0,0,2,72,4,128,72,32,128,73,4,128,73,36,18,8,0,144,9,36,128,9,36,128,0,0,144,0,4,18,0,0,2,73,4,130,0,0,2,0,36,2,8,0,0,8,0,146,0,0,18,1,36,0,1,4,0,8,0,144,0,0,18,73,36,146,73,32,0,73,36,146,
73,36,146,73,36,146,65,36,146,73,36,0,0,4,0,9,36,146,73,36,146,64,0,0,72,36,146,73,32,18,64,0,128,9,0,16,9,4,128,8,0,2,73,32,144,73,36,146,72,36,144,0,0,18,64,36,128,9,0,16,1,36,18,73,4,128,8,32,0,64,
0,146,73,36,18,73,0,2,65,0,2,0,4,146,73,36,128,72,32,0,0,4,18,1,0,0,1,36,16,9,0,16,65,0,146,72,36,130,72,4,146,64,32,2,73,36,144,0,0,2,73,36,146,73,0,0,73,36,146,73,4,144,1,36,146,65,4,146,9,32,0,0,0,
146,73,36,146,73,36,0,9,36,146,73,36,146,73,36,18,64,0,2,1,0,0,0,0,0,9,36,146,73,0,16,73,36,18,73,0,0,9,4,0,1,36,18,73,36,130,73,32,146,73,36,146,65,36,146,73,36,144,1,32,146,1,36,144,9,0,2,8,36,16,73,
4,128,8,0,144,64,32,16,8,4,18,9,4,146,0,36,18,73,36,146,8,4,146,73,4,128,0,0,0,0,36,146,1,0,146,64,0,18,64,32,16,8,0,18,72,0,0,8,4,128,9,36,130,72,36,146,9,36,0,73,0,0,1,36,146,0,0,0,9,36,146,9,0,18,1,
36,146,73,36,146,73,36,146,73,36,146,73,36,146,73,4,0,73,36,144,9,36,2,64,4,18,0,4,146,73,32,144,0,4,0,0,36,0,1,32,0,0,0,146,72,0,146,73,36,146,73,36,130,8,4,146,0,36,144,72,36,146,64,36,2,73,0,18,0,36,
128,72,4,0,9,32,0,1,32,146,8,4,18,9,4,128,9,36,128,9,32,0,9,36,146,65,4,144,72,0,128,1,36,144,72,0,0,72,32,2,0,4,128,73,36,144,0,36,130,64,0,130,0,36,18,72,0,0,72,0,128,73,36,128,0,0,0,0,0,0,0,0,0,73,
36,146,73,36,128,0,4,146,73,36,146,73,36,146,65,36,146,73,36,146,72,0,0,1,32,0,0,0,144,9,36,0,1,36,128,72,0,2,0,0,0,0,0,146,73,0,16,9,36,146,9,36,146,1,36,146,73,36,146,9,36,128,0,36,146,8,0,2,0,0,0,0,
36,146,0,36,2,65,0,144,9,0,146,9,32,128,73,32,128,8,0,146,0,36,0,9,4,18,73,36,146,73,32,18,9,4,0,1,36,144,64,36,146,1,36,18,73,36,0,9,0,0,0,32,2,1,36,16,0,0,144,0,0,0,72,0,2,73,32,0,0,0,16,65,0,0,0,32,
2,72,0,128,73,36,146,73,0,16,0,4,146,73,36,146,65,36,146,73,36,146,73,36,144,0,32,0,1,36,18,73,32,0,64,32,0,0,0,0,0,36,146,73,36,146,73,32,18,73,36,146,64,0,130,73,0,0,73,36,128,1,0,2,73,0,2,64,4,144,
65,4,144,8,0,146,1,4,146,9,36,2,73,36,18,65,0,18,8,36,146,73,32,146,73,36,18,65,36,146,1,32,18,64,32,0,73,32,144,1,32,0,1,32,2,73,36,144,1,32,128,72,0,2,9,0,0,0,0,0,0,0,0,1,32,0,0,0,18,64,36,128,1,0,130,
64,0,146,73,32,2,73,4,146,72,36,144,8,36,18,72,0,146,0,36,0,0,0,2,72,4,18,64,36,0,73,36,2,73,0,0,0,0,0,0,0,0,9,36,18,73,0,0,0,0,0,0,4,0,0,36,144,1,32,146,72,36,146,72,0,0,0,0,2,64,32,128,1,36,128,1,36,
128,0,4,146,73,36,18,65,36,146,72,4,128,9,32,0,1,32,0,73,32,146,73,32,2,73,36,146,72,32,0,73,36,0,0,32,146,73,4,146,73,32,0,73,36,0,72,32,128,0,36,130,9,0,146,1,0,0,73,36,128,0,0,130,0,36,18,0,36,146,
0,36,146,0,0,0,1,36,146,0,36,130,8,0,0,0,32,18,64,32,130,65,32,146,9,36,146,64,0,0,73,0,0,0,32,0,0,0,2,73,0,0,0,32,146,73,32,144,73,36,144,0,36,18,64,32,0,64,32,144,0,0,18,73,36,130,73,0,2,65,0,2,64,32,
128,1,36,16,73,32,2,65,32,128,1,0,0,72,36,146,9,4,18,73,36,130,73,36,130,65,36,146,73,36,146,73,32,146,73,36,146,73,36,144,73,0,16,72,32,0,9,4,146,73,36,146,65,36,0,72,0,2,73,36,146,64,36,144,65,0,0,64,
4,18,72,4,16,8,36,128,0,0,2,0,36,18,73,36,18,9,4,0,8,0,18,8,0,146,64,32,130,1,0,128,65,36,2,1,4,130,73,36,0,64,32,0,1,4,0,73,0,16,0,36,146,73,0,146,73,36,146,73,36,0,0,0,2,9,36,144,8,0,146,73,36,0,0,0,
2,0,0,130,64,36,0,1,0,0,72,32,18,65,0,18,9,36,146,73,36,18,72,0,144,64,0,128,65,32,130,65,36,146,73,36,146,73,36,146,73,36,146,73,36,146,73,36,128,8,0,0,1,0,128,73,4,144,73,36,144,0,0,146,64,36,2,73,32,
16,0,4,18,72,0,128,72,36,128,73,0,0,64,4,128,73,4,130,9,36,144,65,32,2,0,0,130,73,32,18,73,36,144,65,32,18,1,4,146,64,4,144,0,0,146,73,32,0,72,0,130,73,36,146,64,0,146,73,36,144,1,36,146,73,36,146,73,
36,0,64,0,0,0,4,0,0,0,0,0,0,144,73,0,146,73,36,0,0,32,0,0,0,0,1,0,128,72,4,146,73,0,2,64,32,2,1,0,130,73,32,146,73,32,144,73,36,146,73,36,146,73,36,146,73,36,2,73,32,18,73,0,146,64,0,2,9,4,144,0,0,146,
73,36,146,72,0,16,8,32,128,72,0,144,72,0,146,72,0,18,73,0,130,64,32,128,65,4,146,73,36,146,73,36,128,0,4,18,72,0,0,0,0,0,0,36,128,0,0,146,0,36,146,64,32,16,0,4,144,73,36,146,73,0,128,0,36,146,73,36,146,
73,36,146,73,36,146,73,36,146,73,36,0,9,0,18,73,0,18,73,36,146,0,36,146,72,0,146,72,4,146,73,0,146,73,36,128,8,36,2,72,0,128,8,0,16,64,4,16,1,4,18,73,36,18,72,36,146,73,36,146,73,36,146,73,36,146,72,36,
130,73,32,146,72,0,144,9,36,146,65,0,18,73,36,146,65,0,146,72,0,16,0,4,2,1,32,128,65,36,18,0,36,128,0,0,130,9,0,146,72,4,146,73,32,144,73,36,130,0,0,0,8,0,0,0,0,2,9,36,146,73,32,18,73,36,146,73,36,130,
0,36,144,0,36,146,73,36,146,73,36,146,73,4,2,0,0,146,64,0,18,73,36,128,9,36,128,0,0,0,0,0,2,64,32,0,8,4,18,65,36,146,72,32,0,0,0,0,64,0,0,64,0,18,72,4,146,0,0,18,72,32,18,0,36,18,73,36,146,73,36,146,73,
36,146,73,36,146,73,36,146,73,36,146,65,36,146,64,0,146,73,32,0,73,32,144,9,32,16,9,36,18,0,0,2,73,4,144,73,4,130,73,32,0,1,36,128,0,36,2,73,36,2,73,36,146,8,4,0,8,0,2,73,36,18,73,4,2,73,36,146,73,0,128,
1,0,0,1,36,0,0,0,18,73,36,18,73,36,146,73,36,144,1,36,146,64,0,0,8,0,0,9,0,16,0,32,146,8,4,144,9,32,146,73,36,146,73,36,144,73,36,128,0,0,0,0,0,0,0,0,2,73,36,2,73,36,130,1,36,130,73,36,2,64,32,146,9,36,
146,73,36,146,73,36,146,73,36,146,73,36,146,73,36,146,73,36,146,73,32,144,72,0,2,0,0,0,1,36,146,0,0,0,9,36,146,8,0,2,72,32,146,72,0,0,1,36,144,9,0,0,72,0,0,0,0,2,73,4,128,73,32,18,0,4,144,0,0,18,72,4,
18,0,32,18,73,0,144,72,0,0,73,36,130,64,36,130,0,4,2,73,36,18,72,0,18,73,32,2,9,32,144,0,0,16,64,36,18,73,32,128,65,36,0,73,32,0,0,4,146,73,36,144,0,0,0,1,4,146,73,36,146,9,36,0,72,0,2,73,36,18,73,36,
146,65,36,146,73,36,2,65,36,144,9,4,146,73,36,146,73,36,146,73,36,146,73,36,146,73,36,146,73,36,146,65,36,18,65,32,146,64,0,146,1,36,146,64,0,2,64,0,18,0,0,130,73,36,128,73,0,2,73,4,146,73,0,128,64,0,
16,0,4,0,65,36,146,0,0,2,0,0,0,72,32,146,72,0,18,0,0,16,72,36,146,64,36,144,0,0,146,73,32,16,0,36,128,0,4,0,65,4,144,65,32,128,0,36,18,73,32,0,0,0,16,9,36,146,73,36,146,73,36,144,0,32,16,9,32,2,72,36,
146,64,4,0,0,0,0,1,0,146,73,36,144,73,36,146,65,36,146,73,32,146,65,36,144,65,36,146,73,36,146,73,36,146,73,36,146,73,36,146,73,36,146,73,36,146,73,32,0,0,4,146,1,32,146,73,36,128,1,36,146,73,36,0,9,36,
0,1,4,2,0,4,146,64,4,0,73,36,2,0,32,16,0,32,2,0,0,0,73,4,16,72,32,16,73,4,130,1,4,18,72,4,128,73,4,146,73,4,146,1,32,128,0,36,2,73,36,0,65,32,146,73,32,144,73,36,16,0,4,18,0,0,128,0,0,18,64,0,128,9,0,
0,73,32,2,0,32,0,1,36,146,73,36,146,73,32,130,65,36,146,64,0,146,65,36,146,72,0,0,64,36,18,72,36,0,1,36,0,64,36,146,73,36,146,73,36,146,73,36,146,73,36,146,73,36,146,73,36,146,73,32,0,73,32,16,73,32,128,
65,0,130,9,36,128,8,4,130,65,32,146,1,0,18,1,36,18,73,0,128,1,36,0,1,36,146,64,36,146,0,0,2,0,0,16,9,0,128,0,0,0,0,0,0,0,0,2,0,0,0,1,0,146,0,4,18,72,4,2,73,36,128,0,36,18,72,4,146,73,36,146,73,36,146,
73,32,0,73,32,0,73,36,146,72,36,18,73,36,146,65,32,0,73,36,146,73,0,146,0,0,2,0,0,0,0,0,128,0,4,146,73,4,16,0,32,144,8,4,128,9,36,146,73,32,146,65,36,146,73,36,146,73,36,146,73,36,130,64,4,128,64,32,16,
0,0,18,1,36,146,65,4,144,73,0,2,9,32,128,8,4,0,64,4,144,65,36,0,0,0,18,0,0,0,72,0,146,64,0,0,73,4,146,1,4,2,9,36,130,1,32,18,9,36,146,64,4,128,9,0,0,8,0,2,0,0,0,0,0,0,9,4,146,1,36,146,73,0,0,0,36,144,
9,36,146,9,36,128,73,0,128,73,36,146,73,36,128,0,0,0,9,4,0,0,0,0,0,0,0,0,0,0,0,0,2,0,32,146,0,0,0,73,4,146,64,0,2,8,36,146,73,32,0,9,36,146,73,36,146,73,36,146,73,36,2,72,4,0,9,36,130,72,4,146,73,32,18,
8,4,146,73,0,18,64,0,18,73,0,146,73,36,146,73,4,18,73,36,146,72,36,146,73,36,146,73,32,2,9,36,18,72,36,18,72,4,146,73,32,0,0,36,18,73,36,128,8,4,146,0,0,0,73,36,0,72,0,0,0,0,0,0,0,0,1,32,146,73,36,128,
0,0,0,0,0,146,73,36,128,72,36,128,65,0,18,8,36,146,73,36,2,64,0,16,64,4,0,9,32,2,1,36,146,73,36,146,73,36,146,65,32,2,9,32,0,1,36,146,73,36,146,72,32,18,9,32,0,64,0,0,0,0,0,1,4,128,0,32,146,0,0,144,65,
0,2,72,4,2,73,32,146,9,36,0,0,0,146,9,36,18,72,36,146,0,0,2,64,36,18,1,0,128,0,4,146,73,32,16,9,36,146,73,36,18,73,36,146,1,32,18,1,36,16,0,0,0,72,0,2,9,32,146,1,36,0,64,32,18,73,36,144,65,36,18,73,0,
146,64,4,2,0,0,2,72,0,0,0,36,146,73,4,2,72,0,128,65,0,18,9,36,146,73,36,144,0,0,146,73,0,0,1,32,18,72,0,0,8,0,2,64,0,0,64,36,2,72,0,128,9,36,18,65,36,144,73,36,146,73,32,0,73,36,146,1,36,146,72,36,146,
73,36,144,0,0,0,8,0,144,0,4,146,72,4,144,0,0,18,73,36,144,0,0,0,65,36,128,1,36,130,73,32,0,1,36,128,0,0,2,73,36,146,65,36,128,1,32,0,0,4,144,65,36,2,64,0,2,8,4,146,9,0,128,8,36,0,72,32,0,1,36,2,65,36,
128,73,0,2,8,4,144,72,4,2,64,0,2,73,4,0,0,0,0,0,4,128,9,36,144,9,32,146,73,36,146,73,36,146,0,4,146,73,0,128,72,36,130,65,36,146,1,36,146,73,4,146,64,0,16,73,36,144,73,4,146,64,0,130,73,32,146,73,4,144,
73,36,144,73,36,146,72,36,146,8,0,2,0,4,0,64,0,2,0,0,0,8,32,144,72,4,16,9,36,146,73,36,146,64,36,128,0,4,2,73,0,128,8,36,130,72,0,2,9,36,16,65,0,146,73,36,130,65,32,128,72,4,146,9,36,128,1,0,128,73,32,
2,65,36,0,1,32,18,73,4,144,0,4,18,0,4,0,64,36,0,65,4,128,64,32,18,73,36,128,73,36,18,8,0,0,9,36,146,73,36,146,73,36,146,73,36,144,73,36,0,73,32,16,8,0,2,0,0,16,0,32,128,73,4,2,64,0,18,64,0,0,0,0,2,72,
0,146,9,0,128,1,36,146,65,4,18,73,36,18,65,32,128,9,36,128,0,36,0,9,0,0,8,0,0,0,32,144,72,4,0,73,36,146,73,0,146,1,0,0,0,4,16,64,0,16,0,0,144,8,0,16,64,0,18,8,0,0,9,0,18,1,32,18,64,32,16,0,0,2,65,0,0,
0,4,0,73,36,16,9,32,130,73,36,144,0,0,128,64,32,16,1,36,144,72,36,144,1,4,146,1,0,144,1,0,0,0,4,128,8,4,146,73,36,146,73,36,146,73,36,2,73,36,146,9,0,2,65,36,0,73,36,144,65,36,2,73,36,146,73,36,144,64,
32,128,1,0,18,73,36,128,1,0,146,73,36,144,9,36,128,0,32,18,64,4,146,0,0,2,73,36,146,73,36,146,73,0,2,65,36,18,72,32,144,9,0,2,72,36,146,64,4,128,0,32,146,9,36,128,73,36,130,65,36,16,0,0,0,0,0,0,0,0,0,
9,0,0,1,0,0,9,36,0,0,0,146,72,32,18,1,4,128,0,0,0,0,0,18,0,4,146,64,0,18,73,4,128,73,36,18,73,36,130,1,4,144,72,4,0,0,4,18,9,4,146,73,36,146,73,36,146,73,36,128,0,0,0,0,36,18,9,32,18,65,36,146,73,36,128,
8,0,0,1,36,130,9,36,2,8,0,146,73,36,146,73,36,2,9,36,128,9,36,130,73,36,0,0,0,2,1,32,2,73,32,18,73,36,146,8,0,128,1,36,146,73,36,146,64,4,18,1,36,146,64,0,0,1,36,2,65,36,0,73,36,146,73,0,146,73,32,16,
0,36,146,73,36,18,73,32,144,9,32,144,9,4,0,9,36,16,0,32,0,73,0,0,73,32,0,64,0,128,0,32,0,0,0,0,0,0,16,72,0,18,73,36,0,0,0,0,0,32,128,73,4,130,9,36,146,73,36,146,73,36,146,73,36,2,0,0,18,73,32,2,73,36,
146,64,4,16,0,36,144,72,0,0,0,36,2,1,4,146,72,0,0,73,32,18,73,36,146,73,0,146,73,36,146,73,32,2,73,36,18,73,36,18,73,0,146,73,36,2,73,36,144,72,36,146,73,36,146,64,0,128,73,32,2,73,4,146,72,4,0,73,0,2,
1,36,146,73,32,0,65,4,146,9,32,144,9,36,16,8,0,18,73,32,0,9,0,0,0,36,146,73,36,146,73,36,146,73,36,128,9,36,0,9,36,18,64,32,0,1,36,146,73,32,130,73,36,144,73,36,146,0,4,144,73,36,146,73,36,146,73,36,146,
73,36,146,73,36,0,72,0,146,1,36,128,73,36,146,64,4,2,9,36,146,73,32,0,0,36,146,73,36,146,73,32,0,73,36,0,9,4,0,72,0,0,72,0,18,73,32,2,8,32,0,0,0,2,72,0,128,1,32,18,73,36,146,73,4,146,0,36,146,1,36,146,
72,4,128,73,32,146,73,0,18,73,36,146,73,32,2,0,4,128,0,0,0,9,36,128,1,0,128,1,32,0,1,36,0,73,4,146,64,0,146,72,36,146,64,0,18,73,32,0,0,0,0,73,4,0,8,4,146,72,0,0,8,4,144,64,0,16,73,36,146,1,32,0,1,36,
0,72,0,16,72,0,0,73,36,144,65,4,146,72,36,128,8,0,146,9,36,146,9,0,16,0,0,0,72,36,2,73,36,144,1,36,144,0,0,0,0,0,0,0,0,18,73,0,128,0,0,146,0,36,128,0,0,16,0,4,146,64,32,0,8,36,146,72,36,144,9,0,144,73,
36,0,8,0,0,9,36,146,73,36,130,72,36,146,72,36,128,0,0,18,73,4,128,73,36,0,73,0,146,73,0,2,1,36,128,64,0,0,1,36,146,9,36,146,73,4,146,65,36,128,72,0,146,73,0,146,0,32,146,9,32,144,8,36,130,0,4,18,1,0,146,
9,0,2,64,0,146,73,0,16,0,0,146,64,0,146,0,4,144,64,36,0,0,4,128,0,0,130,65,36,146,0,4,130,73,36,18,73,36,146,72,0,0,0,0,18,73,36,144,64,4,146,72,36,146,73,36,146,0,4,2,73,32,146,73,4,144,9,36,128,0,0,
146,0,0,2,72,32,0,1,0,144,1,0,2,73,36,146,73,32,128,0,4,146,73,0,0,0,0,0,1,4,128,9,4,2,1,32,146,0,32,0,73,0,146,1,4,146,72,4,146,73,4,146,73,36,146,1,0,18,64,36,146,73,36,146,64,32,144,9,36,146,73,36,
144,9,36,146,72,4,2,0,36,144,8,4,146,73,36,144,9,36,130,64,4,0,64,0,146,0,36,18,8,4,0,1,36,146,72,36,128,64,36,144,0,0,0,0,36,146,72,36,0,1,36,146,73,0,0,73,36,146,73,36,128,0,0,0,0,0,18,73,0,0,1,4,130,
73,36,146,72,0,144,1,36,146,73,36,130,73,36,146,65,36,0,64,0,0,0,0,2,73,36,128,64,4,130,73,4,16,0,0,0,9,0,144,0,36,146,64,36,146,64,32,16,0,36,146,1,36,146,73,32,146,73,32,16,73,36,146,73,32,144,9,0,0,
0,0,18,73,36,2,1,36,146,73,36,146,72,36,144,0,0,16,0,36,128,1,36,2,73,4,16,65,32,18,72,32,130,73,36,144,9,4,16,0,0,146,8,4,0,9,32,2,73,32,0,0,0,0,0,0,144,1,0,146,65,0,2,9,36,146,73,36,146,73,36,146,64,
4,128,73,36,146,73,36,146,73,36,146,73,4,146,64,36,146,64,0,144,8,0,0,0,36,130,8,0,2,0,0,128,0,0,0,0,0,144,73,4,0,8,0,18,9,4,146,73,36,130,73,36,146,0,36,128,1,36,144,73,36,146,64,0,0,64,4,18,0,0,0,72,
36,0,1,32,146,64,4,18,64,4,146,72,0,0,0,0,0,0,4,2,73,36,130,1,32,0,8,4,146,73,36,146,72,0,146,73,36,146,73,36,146,73,36,146,72,36,144,0,36,0,0,0,128,0,0,0,0,0,144,65,36,128,72,36,128,0,4,146,73,36,144,
0,4,0,0,0,0,0,0,2,8,0,0,64,32,128,1,36,146,73,36,144,0,0,16,9,32,0,72,0,16,0,0,128,0,0,2,64,0,144,1,32,0,8,0,0,65,32,0,8,32,144,8,32,0,8,0,2,9,36,146,73,0,0,0,4,0,8,0,18,64,0,0,1,0,144,72,4,128,9,0,16,
64,0,130,72,0,0,0,0,146,72,36,18,73,36,146,64,0,144,0,36,128,0,0,146,0,36,0,0,0,18,1,4,130,64,0,0,73,36,146,73,36,146,73,36,146,65,36,146,73,36,146,73,36,2,73,0,0,73,36,146,72,0,146,73,36,146,73,36,146,
73,36,146,0,0,146,73,36,144,73,0,146,0,32,0,65,4,2,64,0,144,0,0,128,64,0,2,72,32,128,65,36,2,65,36,146,72,36,2,73,36,18,72,0,146,73,36,146,72,4,0,0,32,0,1,36,146,73,36,144,1,4,146,0,0,130,73,32,0,0,32,
0,1,0,16,1,36,0,72,0,0,8,36,2,64,0,0,65,36,146,64,36,16,9,32,146,8,36,146,8,36,130,64,36,146,9,36,128,73,0,0,0,4,128,0,4,128,73,0,146,65,36,146,72,36,146,73,36,144,9,36,146,73,36,146,73,36,128,0,0,2,73,
36,16,9,36,144,0,36,128,9,32,146,0,4,146,73,36,16,0,4,0,0,0,0,73,32,0,1,36,0,73,4,146,73,36,146,73,36,146,73,32,2,73,36,146,73,4,128,0,32,0,64,4,130,64,4,146,65,0,144,0,0,18,73,4,0,9,4,146,72,4,128,1,
32,130,72,0,144,65,32,2,1,32,2,8,4,128,72,0,2,73,32,18,1,32,16,1,4,0,8,36,16,72,0,130,0,0,144,8,0,144,0,0,146,0,0,2,73,36,146,1,36,146,72,4,0,64,0,2,0,0,18,64,0,16,0,4,18,73,36,146,73,36,144,0,36,130,
0,36,130,0,32,16,1,36,146,1,36,146,73,36,146,72,0,18,0,0,146,73,0,0,73,36,146,73,32,18,65,36,146,73,36,144,0,4,146,73,36,146,73,4,146,73,36,144,73,4,2,73,36,0,9,0,128,0,4,146,73,0,2,73,36,146,73,36,146,
0,36,2,64,0,18,0,0,2,73,0,128,72,0,0,9,32,146,73,36,144,9,4,144,9,32,2,72,32,16,72,36,146,65,32,2,64,0,128,0,0,2,72,32,18,72,0,16,0,36,146,0,0,0,0,4,16,8,32,18,73,0,0,0,4,128,0,36,2,72,0,144,0,0,18,64,
4,146,73,36,130,64,0,146,1,32,128,0,0,146,65,36,18,73,36,146,73,36,144,0,4,146,9,36,146,72,4,144,73,32,16,0,4,18,73,36,146,73,36,0,0,0,18,73,36,146,73,36,144,1,0,0,73,32,130,72,0,2,8,0,16,0,36,0,0,0,0,
64,0,0,0,0,146,73,32,16,72,0,16,9,0,130,73,4,0,0,4,18,72,36,0,73,0,18,9,0,2,73,36,144,9,32,2,64,36,146,64,0,146,73,0,0,1,36,16,9,36,128,8,0,0,65,36,130,73,0,0,9,36,16,9,36,0,0,0,0,0,0,18,8,4,2,0,4,146,
73,4,146,72,0,144,9,36,128,1,0,0,1,36,144,9,36,128,8,0,0,72,0,146,73,32,2,0,0,0,73,32,18,1,36,146,72,0,0,0,0,0,1,36,144,73,36,146,73,36,146,73,36,128,0,0,0,0,0,128,9,0,0,0,0,0,1,0,0,0,0,0,72,0,16,1,32,
0,73,0,130,1,36,18,64,0,2,9,0,128,65,36,144,0,0,146,73,36,146,0,36,144,0,4,2,9,0,0,0,36,0,0,0,0,0,4,2,73,36,2,65,36,146,0,32,128,1,0,146,73,36,146,73,36,146,64,0,0,65,36,146,72,32,0,1,36,18,72,36,146,
0,0,146,73,0,18,73,32,2,9,36,144,0,0,0,1,36,146,73,0,0,1,36,146,72,0,0,9,0,2,64,0,18,72,0,0,64,0,0,8,4,128,73,36,2,1,36,146,72,32,144,0,0,0,1,36,16,73,36,144,72,4,0,73,32,0,0,0,18,72,0,2,73,36,2,9,36,
2,64,0,0,0,0,2,8,0,146,65,4,144,73,0,2,1,36,146,73,0,0,0,4,128,73,36,146,73,36,130,0,4,146,64,0,0,9,4,2,72,0,2,73,0,0,9,0,16,0,0,0,0,0,2,73,36,128,0,36,144,1,36,144,1,4,0,0,32,0,0,0,16,65,4,18,72,0,2,
73,36,146,73,36,146,73,36,146,73,36,128,72,0,2,73,0,0,0,0,0,73,36,128,1,4,146,64,0,18,73,36,18,72,0,0,9,36,144,0,0,0,1,32,130,72,0,0,0,0,0,0,0,0,72,0,18,73,36,146,72,4,146,72,0,0,73,0,18,73,36,146,72,
0,0,73,36,146,73,36,146,73,36,0,0,0,128,72,0,2,64,0,0,64,0,0,0,4,146,0,36,146,64,0,146,8,0,18,73,0,18,65,0,0,0,36,0,0,36,0,9,36,146,72,0,144,64,4,146,1,32,146,0,32,144,64,0,146,73,0,146,9,36,146,1,0,146,
73,36,146,73,36,146,73,36,146,73,36,146,73,36,144,72,36,0,8,0,18,73,0,2,73,36,146,64,0,18,72,4,128,0,4,144,0,0,0,1,36,0,72,36,18,0,0,0,1,32,130,73,0,146,8,0,0,0,0,18,8,0,2,0,0,144,65,32,18,73,36,144,65,
36,146,73,32,2,73,36,146,73,0,130,64,32,0,0,0,0,0,0,16,64,0,146,0,36,144,9,4,0,0,0,130,0,36,128,0,0,2,73,32,0,0,0,146,73,32,18,64,36,0,0,0,130,0,4,128,73,36,146,73,32,0,0,32,0,8,0,0,1,36,2,72,36,146,9,
36,128,73,36,146,73,36,146,73,36,146,73,36,146,73,36,146,73,36,16,73,32,146,72,0,18,73,36,146,73,36,0,73,4,146,73,32,0,0,36,2,73,36,0,0,4,146,73,32,18,64,36,146,72,0,18,72,4,146,73,36,130,73,36,2,64,36,
0,0,4,0,0,4,146,72,4,146,64,0,146,1,36,18,73,36,146,73,0,146,73,32,0,0,0,0,9,36,0,0,4,0,0,36,2,64,4,146,65,32,146,65,32,0,65,36,18,65,36,128,0,0,146,9,0,2,73,32,0,0,4,130,0,0,144,0,32,144,8,0,146,8,36,
144,1,32,2,1,4,128,0,0,18,0,0,0,73,36,130,73,36,146,73,36,146,73,36,146,73,36,146,73,36,16,0,32,128,0,0,18,9,0,0,0,0,2,0,36,144,0,0,146,72,36,144,0,0,0,0,0,0,73,36,146,65,32,16,65,32,2,73,36,130,73,36,
146,65,36,144,8,0,18,73,36,130,72,36,146,73,36,144,0,0,2,73,36,146,73,36,2,73,36,146,1,36,128,0,0,18,64,4,144,8,32,18,73,36,0,8,32,2,0,32,0,0,32,0,0,4,130,73,36,2,8,4,144,73,36,0,73,36,2,8,0,144,1,0,144,
1,36,146,9,32,0,0,4,18,64,32,0,0,0,16,0,0,2,72,4,16,9,36,18,73,36,146,73,32,146,72,36,146,73,36,146,73,36,146,73,0,18,1,36,128,0,0,0,0,0,146,73,36,146,64,0,18,64,0,146,73,36,144,73,32,0,9,36,146,72,0,
0,72,0,0,1,36,18,73,36,130,73,36,146,0,0,0,73,32,0,72,36,144,1,0,0,73,36,146,72,0,0,8,0,0,0,0,130,1,0,146,64,0,18,64,0,144,1,32,2,65,36,128,0,0,0,1,0,18,72,36,2,72,0,144,8,4,128,0,0,144,73,36,2,1,36,144,
0,0,146,73,36,0,1,32,146,64,0,128,73,36,2,72,36,2,73,4,18,72,32,0,73,36,0,73,36,146,65,36,2,0,0,146,64,4,146,73,36,128,1,36,146,72,36,0,0,0,0,0,0,0,9,36,2,65,36,18,64,36,146,73,36,146,73,36,128,73,32,
0,9,32,0,0,0,18,72,0,0,1,0,18,73,36,130,73,36,146,73,36,18,64,0,0,73,36,0,0,0,0,0,0,0,73,0,0,0,0,0,0,0,2,64,0,144,72,0,144,1,4,0,8,4,146,73,32,128,1,36,130,0,0,0,8,0,0,0,0,2,64,0,18,72,0,0,0,0,128,64,
0,0,1,36,18,0,0,0,0,0,144,73,36,146,73,36,18,9,36,130,72,0,0,9,32,0,0,36,128,73,36,144,1,4,146,64,0,130,64,0,2,1,36,144,0,4,16,1,32,0,0,0,0,0,0,144,1,36,0,9,32,0,1,36,2,73,36,146,73,36,146,0,36,146,0,
4,146,72,36,0,64,36,146,73,36,146,73,36,146,73,4,146,73,36,146,73,36,128,73,36,144,73,36,146,0,0,0,9,36,146,73,36,146,73,36,146,64,36,0,1,36,146,72,0,0,9,32,0,8,32,0,0,0,18,73,36,18,73,36,146,64,36,0,
9,36,144,73,36,144,9,36,146,73,36,146,73,36,146,73,36,18,0,0,146,0,0,0,0,0,0,1,0,0,1,36,146,0,0,18,73,36,146,73,0,146,9,4,0,73,32,128,64,0,0,9,4,144,64,0,146,73,0,146,72,4,144,73,0,0,0,4,146,73,0,0,0,
0,18,0,0,0,0,0,0,0,0,2,73,36,16,0,0,0,1,32,0,72,4,146,73,36,146,73,36,146,73,36,18,72,0,146,65,36,18,73,36,146,73,36,146,1,36,144,0,0,18,1,36,128,0,36,146,0,0,0,1,32,146,72,36,146,72,0,0,0,36,130,73,32,
2,0,36,146,73,32,0,0,36,146,72,4,0,1,36,0,0,4,0,0,32,0,0,0,0,8,4,0,0,0,0,0,0,0,0,36,128,0,4,18,8,4,0,1,0,0,0,0,2,73,36,146,73,36,146,73,36,144,9,0,0,73,36,128,0,0,146,72,36,144,73,0,146,0,36,146,65,36,
0,0,0,16,0,0,0,0,0,18,73,36,146,73,4,146,0,32,144,0,0,0,72,4,0,65,36,18,73,36,146,8,4,144,0,36,146,73,36,144,9,36,144,73,32,18,73,36,146,1,32,146,73,36,2,65,32,18,1,4,128,9,36,144,0,4,16,0,0,146,9,36,
2,9,36,0,1,0,0,64,0,2,0,0,16,0,0,2,73,36,144,0,32,0,0,0,2,9,0,0,65,36,0,64,4,2,0,0,144,9,32,0,0,0,0,0,36,128,0,4,0,0,32,144,1,0,2,72,36,0,0,0,18,9,36,146,73,36,128,73,0,0,0,0,146,73,32,146,64,0,0,0,0,
0,0,0,146,65,0,18,0,0,130,73,36,2,73,4,146,65,4,0,0,0,0,1,0,0,8,32,0,1,36,130,0,4,0,9,36,146,73,32,146,73,4,2,0,0,18,1,4,128,1,32,128,1,36,146,0,4,18,9,36,144,72,36,146,8,36,146,65,36,16,64,0,146,65,36,
0,0,32,146,0,0,0,0,0,144,0,0,2,73,32,18,64,4,146,73,36,146,73,4,128,1,32,2,73,32,146,73,0,18,72,0,18,73,36,0,0,36,128,1,0,0,0,0,0,0,36,18,73,36,146,73,32,146,9,36,144,73,0,146,73,36,16,0,0,0,0,0,0,64,
4,18,65,0,2,73,32,0,0,0,16,1,0,146,73,0,146,0,0,2,9,36,128,1,36,130,72,36,146,73,32,146,73,36,146,65,36,144,0,0,0,9,0,146,73,4,0,64,32,146,9,36,18,0,0,128,9,0,18,0,0,16,1,32,18,64,0,18,64,32,0,0,36,146,
65,0,18,1,36,18,9,36,0,0,36,128,64,4,130,72,36,2,9,0,0,0,0,0,0,0,0,0,4,144,8,32,18,73,36,146,72,4,146,73,36,146,73,36,130,9,36,146,73,4,16,8,36,0,0,4,0,73,0,130,73,36,146,72,0,144,73,36,146,73,36,2,73,
36,144,0,32,144,64,0,144,1,0,18,73,36,146,73,36,146,73,36,146,73,36,146,72,0,128,1,36,130,73,4,146,64,0,18,9,36,146,72,36,2,73,36,0,0,0,0,0,36,146,65,32,18,72,36,146,64,0,0,0,36,144,8,4,128,8,0,128,9,
4,146,73,36,128,9,0,146,72,0,144,64,0,18,8,36,144,0,4,18,64,0,0,73,0,0,1,36,2,0,36,16,9,36,146,0,0,0,0,0,2,0,4,128,9,36,128,1,36,128,0,0,0,9,32,0,73,0,0,8,0,0,73,36,0,8,4,146,73,32,144,73,36,144,9,4,146,
72,4,146,73,36,0,64,0,2,72,0,0,0,0,0,0,0,0,0,0,2,1,36,0,0,0,0,1,36,144,9,0,2,72,0,2,72,36,130,72,0,0,0,0,0,73,36,146,73,36,2,0,32,2,1,36,18,73,0,16,72,36,146,8,36,146,0,0,18,1,36,146,72,0,18,72,4,128,
1,0,146,72,0,144,72,0,128,73,32,0,8,0,0,9,32,18,65,0,0,0,0,146,73,36,130,73,36,144,8,0,0,0,0,2,64,4,0,0,0,146,65,36,146,9,32,0,1,36,146,64,0,128,73,4,146,73,32,2,1,36,0,65,0,0,1,36,2,0,4,0,73,36,146,73,
4,130,64,32,144,0,36,146,73,32,2,65,0,2,73,0,2,65,4,128,65,36,2,1,0,0,0,36,128,0,0,0,0,36,0,0,0,2,73,36,146,73,36,144,0,36,128,0,36,146,65,32,144,72,36,146,0,0,0,0,0,0,72,0,18,0,4,146,72,0,0,0,36,130,
65,32,144,65,36,2,65,36,146,73,36,0,1,36,144,0,0,146,73,36,146,9,4,146,73,36,146,73,32,146,73,36,144,73,36,130,73,36,146,65,36,146,73,32,0,0,0,2,0,0,0,72,0,16,0,32,0,72,36,130,72,0,144,64,0,146,65,32,
0,73,32,128,65,32,16,73,32,0,0,36,2,64,0,144,8,36,128,0,0,130,65,36,128,9,0,2,0,0,0,0,0,18,0,0,0,0,0,0,0,0,16,73,36,0,1,0,18,64,36,128,0,0,0,0,0,16,65,4,146,65,36,146,73,36,0,0,36,0,72,4,144,73,0,18,72,
32,0,64,0,0,73,32,18,0,36,128,64,0,2,65,36,2,1,36,0,73,36,0,0,0,146,73,36,146,73,36,146,64,36,130,1,0,146,73,36,146,1,0,146,0,36,146,73,36,130,73,36,146,72,36,18,73,36,146,73,36,146,72,4,146,73,0,0,0,
32,16,73,36,2,64,36,146,64,36,146,73,0,144,0,32,16,9,4,130,64,0,0,72,0,18,73,4,146,64,0,0,0,0,0,0,0,0,1,0,0,0,0,146,73,36,146,73,32,2,73,36,146,73,36,146,73,36,0,0,0,0,1,36,146,73,36,128,73,36,146,73,
0,146,73,36,146,73,36,144,64,0,2,72,36,146,9,4,146,73,32,18,9,36,0,0,0,0,9,36,144,64,0,0,73,36,146,73,36,146,73,36,146,73,36,144,1,0,128,72,4,0,0,36,0,64,0,0,64,0,18,64,0,0,0,0,0,0,0,2,0,36,144,73,36,
146,72,36,146,73,36,146,73,36,146,73,36,146,73,36,146,73,36,146,72,0,2,65,4,146,64,32,2,65,36,0,0,0,146,73,36,146,64,0,0,73,36,128,9,32,0,1,0,0,0,4,130,73,36,128,73,32,146,73,36,130,65,0,144,8,36,146,
73,36,146,72,0,146,73,36,146,73,36,146,73,32,146,73,36,146,73,0,144,1,32,2,65,36,146,73,36,146,73,36,146,0,0,0,0,0,18,73,32,0,0,32,0,8,0,16,0,4,128,0,4,144,73,36,0,73,36,128,72,0,128,73,36,144,72,0,18,
64,36,146,0,0,2,0,0,2,64,0,16,0,0,0,0,0,146,1,36,146,64,32,2,73,36,146,73,36,146,73,36,146,73,32,146,73,36,146,73,36,144,73,36,2,72,0,0,0,0,0,72,4,146,64,0,0,0,36,128,1,36,144,0,32,146,64,4,146,73,36,
128,65,36,146,0,4,146,73,4,146,73,4,144,1,36,146,0,0,16,1,36,146,73,0,146,73,36,146,9,32,18,73,36,146,73,36,146,9,36,146,73,36,130,9,36,144,73,36,128,0,36,16,65,32,130,73,0,2,65,32,18,72,32,0,72,32,146,
9,32,2,8,4,0,73,0,2,9,32,18,0,0,0,0,0,0,73,4,16,0,0,0,9,0,16,0,0,130,73,0,0,0,0,128,9,36,2,9,36,146,73,36,146,73,36,146,73,36,144,73,36,146,73,36,146,73,36,146,73,36,16,1,0,0,0,0,0,9,36,146,0,0,0,0,0,
0,1,36,146,0,0,0,0,0,0,65,36,128,0,4,128,73,36,128,73,0,2,72,4,18,64,0,2,64,36,146,64,0,2,73,36,146,73,0,146,1,32,0,73,36,146,65,36,130,1,4,146,0,36,144,73,32,146,0,0,16,72,36,146,9,4,144,9,32,2,64,4,
144,8,4,128,0,36,128,72,36,130,73,36,144,73,36,146,64,0,2,0,0,0,64,0,0,0,0,0,9,0,128,8,4,0,0,36,146,72,4,146,65,0,144,64,4,146,73,36,146,73,0,146,73,36,144,73,36,146,73,36,146,73,32,18,73,36,128,0,4,16,
0,4,144,0,36,144,0,0,18,64,0,2,73,32,16,0,0,146,72,36,146,73,36,0,73,36,146,73,36,144,9,4,128,0,32,18,73,32,16,73,4,146,73,36,146,73,32,16,0,0,0,72,32,0,0,0,0,73,32,2,9,4,146,65,0,0,9,4,2,0,0,2,9,36,146,
1,0,0,8,36,0,0,4,146,0,0,2,1,36,144,9,0,18,73,36,146,73,36,18,72,32,2,0,4,0,0,0,0,0,36,146,73,36,146,73,4,146,72,36,144,1,32,16,73,32,144,9,4,144,9,32,0,73,0,0,0,0,18,65,36,146,73,36,130,73,36,0,0,32,
128,73,36,146,9,36,144,0,0,0,0,0,18,73,36,146,0,36,146,73,4,146,8,36,146,73,0,0,73,36,144,9,36,128,0,4,130,64,0,18,64,32,0,73,36,144,73,36,146,73,36,16,64,36,128,9,0,0,73,32,0,73,36,144,0,36,144,64,32,
2,72,0,144,73,36,146,9,36,0,1,0,128,72,0,0,8,0,0,0,36,146,64,4,0,9,32,146,73,36,146,73,36,128,73,36,144,0,0,0,0,0,0,64,32,0,0,0,0,0,4,144,0,4,146,72,36,146,72,0,2,9,36,128,73,36,144,1,36,128,8,4,18,73,
32,144,0,32,146,73,36,146,0,32,130,73,36,0,9,36,146,73,36,146,73,36,146,73,32,0,0,4,130,64,4,2,1,0,0,0,0,2,9,36,146,73,36,146,9,36,18,72,32,2,9,0,2,73,36,146,72,4,146,65,4,144,9,36,130,73,0,144,64,36,
0,1,4,0,8,32,2,64,32,2,65,36,128,0,0,2,72,4,146,9,4,0,9,36,0,8,36,128,9,36,146,73,36,146,73,36,146,73,36,146,73,36,0,64,36,146,65,0,0,0,36,0,64,36,144,1,0,0,9,36,0,8,36,130,64,32,2,0,32,0,73,0,16,65,0,
146,72,36,146,9,36,0,73,0,18,9,32,144,73,32,0,73,36,144,1,36,144,73,36,18,9,36,146,73,36,146,73,36,146,73,32,2,73,36,146,72,0,0,0,0,2,73,32,0,72,36,146,9,36,130,65,32,144,0,0,2,0,36,130,65,0,18,73,36,
146,72,32,18,9,36,146,9,0,0,1,36,0,64,36,144,0,0,128,0,0,0,1,36,2,72,32,0,73,32,18,0,4,18,9,36,0,9,32,146,73,36,144,8,36,146,73,36,146,73,36,144,64,0,2,73,36,146,73,36,146,73,32,18,8,36,144,72,0,18,0,
0,0,0,0,18,0,4,0,73,32,130,72,4,0,0,36,146,73,32,0,9,4,16,0,0,146,0,4,2,1,32,128,0,0,128,73,32,2,73,36,146,73,36,146,73,36,146,73,32,18,73,36,146,65,4,146,9,36,146,72,0,18,73,36,146,73,36,146,72,0,130,
0,36,144,73,36,146,73,36,130,73,32,16,64,0,16,8,32,18,0,0,146,73,32,2,73,0,0,0,4,0,0,0,0,0,0,2,0,4,130,0,0,128,8,36,18,0,0,0,8,32,130,0,36,2,0,36,146,73,36,146,73,36,144,9,4,146,64,36,146,73,36,2,73,36,
146,73,36,146,73,36,146,73,36,146,72,0,146,64,0,130,9,32,0,73,4,128,1,4,2,73,36,146,73,4,144,0,0,18,9,36,0,72,0,0,0,0,0,0,4,128,1,36,146,73,32,144,65,32,2,73,32,18,73,32,2,0,36,146,65,36,16,72,4,144,0,
0,144,1,36,146,73,36,146,73,36,146,73,36,130,0,0,18,65,32,18,72,36,146,73,32,2,64,4,146,9,36,130,73,0,0,0,36,144,0,0,128,0,36,146,0,0,128,73,36,146,72,36,146,73,36,146,73,36,146,73,36,146,73,36,146,73,
36,146,73,36,146,73,4,0,1,32,0,72,0,146,73,36,146,72,4,144,72,4,144,9,32,16,1,36,128,65,0,16,1,32,0,73,36,146,0,36,130,73,36,146,73,32,144,9,0,0,9,32,130,0,32,2,0,36,146,64,0,0,9,36,144,1,0,0,0,36,18,
64,4,144,72,36,146,64,32,0,9,0,128,1,32,144,1,4,2,73,0,146,73,36,18,72,0,0,64,32,0,0,0,130,73,32,0,0,0,0,73,36,146,72,0,130,73,36,144,0,0,0,0,0,0,0,4,146,9,0,18,8,0,146,8,0,146,73,36,146,64,32,0,9,0,0,
0,36,128,1,36,146,73,36,146,73,36,146,73,32,146,73,36,146,1,4,146,73,36,128,0,0,18,64,0,146,72,32,0,73,0,0,64,32,0,0,4,146,9,36,146,73,36,146,73,36,146,73,36,128,9,36,146,73,0,2,73,0,146,0,36,146,72,0,
0,9,32,0,0,32,0,0,0,18,72,0,128,0,0,0,9,36,146,73,32,146,64,0,0,0,0,18,9,36,18,73,0,2,9,36,146,64,4,130,9,32,130,1,36,18,64,0,0,9,4,146,0,36,16,65,0,16,73,0,146,65,36,146,72,0,0,1,36,0,1,0,0,0,0,18,73,
32,0,0,0,0,9,32,16,0,36,146,72,4,146,73,36,146,73,36,146,0,0,0,8,32,0,0,4,146,73,36,0,64,0,0,9,32,0,0,36,146,1,36,130,1,36,146,73,36,16,8,36,146,73,36,146,73,36,146,73,36,146,73,0,18,73,0,18,65,36,146,
73,36,130,72,0,18,73,4,144,1,0,0,0,0,0,1,36,18,0,0,2,9,36,0,0,36,146,73,36,18,9,32,146,0,0,128,0,36,146,72,0,144,73,4,144,73,4,146,72,0,144,64,4,144,0,0,128,1,4,18,72,0,18,0,4,144,9,32,146,64,36,146,9,
36,146,73,0,18,73,36,18,64,0,0,0,36,146,72,0,2,64,36,146,73,36,18,73,36,146,73,36,128,73,36,16,64,0,128,8,0,0,1,36,128,0,0,0,0,36,18,73,36,128,0,0,16,1,36,146,73,36,146,64,36,146,73,36,146,73,36,146,73,
36,146,73,36,146,73,36,146,9,0,0,0,0,128,72,0,18,72,36,146,0,0,0,0,0,18,65,4,2,64,0,18,73,32,2,0,0,130,73,36,128,73,36,18,73,36,146,0,0,144,0,4,144,9,32,18,73,36,146,72,36,146,73,0,16,73,4,16,9,0,18,73,
4,144,0,0,18,1,36,0,9,32,144,64,4,2,1,0,146,0,0,0,9,4,16,0,0,130,1,36,146,73,0,144,0,0,144,9,36,146,73,32,18,73,32,130,0,0,146,72,4,128,64,0,2,0,36,146,72,4,146,1,4,128,0,0,2,73,0,146,0,32,128,0,0,0,0,
32,0,1,0,130,64,0,146,73,36,146,73,32,18,73,0,18,0,0,146,64,0,0,0,4,146,0,0,0,73,36,146,72,32,146,65,36,130,73,36,2,0,36,146,9,36,146,73,36,128,73,4,146,73,4,144,1,36,130,65,36,146,73,36,146,72,4,0,8,
36,144,65,0,144,0,4,18,73,0,18,73,36,146,64,0,2,64,0,128,64,36,18,0,4,130,0,32,144,0,4,144,73,4,18,8,0,0,8,36,0,0,4,144,1,32,146,73,0,16,65,36,0,0,4,2,72,4,146,73,36,2,0,32,144,9,4,144,9,36,146,73,36,
146,0,0,146,73,4,146,0,4,146,9,0,18,8,0,0,8,0,18,73,32,130,73,36,146,73,36,2,0,0,146,73,0,0,0,0,0,0,0,0,72,36,146,73,36,146,9,36,18,9,36,18,1,36,146,73,0,0,9,4,144,73,36,146,73,36,144,9,4,146,73,32,146,
64,36,18,73,36,144,0,0,18,73,32,0,9,0,0,8,4,144,8,0,0,0,0,146,65,4,130,0,0,0,9,0,130,64,36,144,65,36,146,0,0,2,9,32,16,0,32,0,64,0,146,73,0,16,1,36,146,1,0,18,64,4,144,0,0,144,73,36,18,73,32,18,73,0,146,
72,0,18,9,0,146,1,36,18,72,32,18,65,4,0,0,4,128,0,0,0,8,36,0,0,0,0,0,0,0,64,0,144,73,36,146,73,36,0,0,0,0,0,0,0,9,36,0,1,36,146,73,0,16,0,32,0,0,36,144,0,0,0,72,32,0,9,0,0,8,0,0,9,36,146,73,32,16,73,36,
146,73,4,146,0,32,2,64,36,146,1,36,128,0,32,0,8,32,0,0,0,18,73,0,18,1,4,0,9,0,146,72,36,0,0,4,16,0,36,128,9,36,2,73,36,146,73,36,146,73,36,146,73,36,2,72,4,146,73,32,146,1,36,144,72,36,146,0,0,16,9,4,
16,72,0,0,0,0,18,72,36,146,9,4,0,8,36,0,9,36,146,64,0,0,9,4,146,0,0,18,0,36,128,1,32,18,73,36,128,72,4,0,72,0,0,0,0,0,0,4,0,0,0,18,73,32,146,73,36,0,1,36,18,64,0,16,0,0,0,9,0,0,0,36,18,73,36,0,0,0,2,0,
0,2,9,0,0,73,0,130,0,4,144,0,0,146,73,32,18,73,4,146,65,36,128,0,36,146,9,0,128,8,32,146,64,36,128,1,32,146,73,36,130,73,32,128,64,36,16,1,0,146,72,36,144,0,4,2,64,0,2,73,32,18,65,36,144,73,0,18,0,36,
16,1,32,144,0,36,128,73,32,128,0,32,18,1,0,128,64,4,0,1,36,18,73,0,18,1,36,146,73,36,130,73,32,16,0,0,0,0,0,2,73,4,146,73,36,18,8,0,0,0,0,0,0,0,146,73,0,18,65,36,146,72,32,128,1,32,0,0,32,2,0,0,0,72,32,
2,73,32,16,0,0,130,8,4,18,72,0,146,64,4,144,64,0,2,73,36,144,9,36,128,0,32,0,9,36,16,0,4,144,0,0,0,0,0,0,0,32,18,73,36,0,9,36,144,9,0,0,9,0,144,9,36,18,0,0,16,73,36,146,1,32,0,1,36,128,1,0,0,64,36,144,
0,0,0,64,4,146,8,0,146,64,0,16,1,36,128,72,36,0,1,4,128,1,36,130,73,36,146,64,36,2,73,0,0,9,4,18,8,0,0,8,0,0,0,0,0,9,36,146,72,4,0,0,0,146,73,32,0,73,36,144,9,36,146,73,36,18,9,32,16,73,32,128,72,0,2,
9,36,0,73,32,128,65,0,2,73,0,128,73,0,18,72,4,146,8,4,128,0,4,144,65,4,128,0,0,16,72,0,128,1,0,18,72,36,144,1,32,0,72,0,0,0,0,0,9,4,128,0,0,18,64,32,16,64,36,144,72,0,0,0,36,146,9,32,146,1,36,146,0,0,
0,0,0,16,64,36,0,73,0,0,8,36,130,72,4,146,73,32,146,65,36,146,73,36,18,73,32,146,1,36,18,0,32,0,8,32,2,73,0,146,72,32,146,72,36,146,64,0,0,0,0,0,0,0,146,73,0,146,73,36,144,0,36,128,1,36,144,1,36,130,73,
4,146,73,36,0,1,36,2,1,36,144,1,36,144,72,32,2,8,4,128,1,36,146,65,0,18,72,0,2,0,4,128,73,36,0,9,0,144,65,0,130,8,36,16,72,0,0,1,32,146,0,4,2,0,32,18,72,36,146,72,32,128,0,4,128,0,4,128,9,32,128,65,32,
16,0,4,130,65,36,144,0,0,0,72,0,0,73,4,144,1,36,146,73,36,130,0,36,2,1,0,0,8,0,2,0,32,2,64,36,2,73,0,128,1,32,2,65,4,0,73,32,18,8,36,16,73,4,146,73,36,128,0,0,0,73,32,146,64,0,0,1,36,146,64,0,18,0,36,
144,0,0,128,8,4,144,0,4,128,0,32,146,72,0,128,64,0,128,72,0,144,8,0,0,0,4,0,9,36,144,73,4,146,9,36,144,9,0,2,64,0,2,9,32,18,73,0,144,73,0,130,73,36,18,64,0,16,64,36,146,8,0,0,0,36,146,73,32,128,73,4,144,
8,32,18,9,36,146,73,36,146,72,0,130,65,0,18,9,0,2,9,36,146,0,4,2,73,4,146,73,32,0,1,4,0,1,0,16,1,0,128,0,36,130,64,0,2,65,36,144,73,32,18,9,4,130,9,0,146,64,0,0,72,4,2,73,36,2,64,36,146,72,36,146,0,4,
0,0,0,146,0,0,18,72,36,146,9,0,18,0,4,16,73,0,0,64,0,18,73,36,128,73,36,0,72,36,144,73,0,0,1,4,146,1,36,146,73,36,128,64,0,0,8,4,130,64,4,128,8,0,18,72,32,144,1,4,2,65,36,146,1,0,2,1,32,16,72,36,144,1,
4,144,1,4,130,8,0,146,1,4,2,64,0,0,64,0,2,8,32,0,0,0,18,8,0,0,9,36,128,73,36,146,1,4,146,64,0,0,8,0,18,65,36,146,64,36,146,64,36,18,65,32,0,1,36,0,9,4,146,73,4,0,8,36,0,0,36,130,0,4,146,9,36,146,73,0,
2,73,32,0,1,36,128,0,0,0,9,36,128,72,36,18,9,32,146,73,32,128,65,0,16,0,0,130,1,36,2,73,32,146,73,4,16,9,36,16,72,32,18,1,4,146,73,36,2,64,36,146,64,0,0,0,36,146,65,36,0,73,36,144,72,4,146,0,4,130,72,
32,130,65,36,16,73,0,16,0,0,144,0,0,0,0,36,144,64,36,18,1,32,128,0,4,146,72,0,146,0,36,130,9,4,2,9,0,146,73,32,128,0,4,0,0,0,0,64,0,0,9,0,18,72,32,18,73,36,146,9,36,0,64,36,18,64,0,130,0,36,0,9,36,128,
1,32,0,0,0,2,65,36,0,72,4,16,0,36,128,72,0,0,0,0,0,73,32,2,0,4,146,8,32,130,64,36,144,65,32,18,64,0,128,65,4,146,65,32,16,73,36,18,1,36,146,73,36,130,64,0,0,0,0,2,72,36,144,1,36,2,72,4,146,72,36,146,9,
32,2,72,4,130,72,0,16,9,36,2,9,0,2,64,4,146,72,0,2,65,32,16,73,32,130,64,36,0,64,32,128,0,4,0,73,36,146,8,32,128,72,0,18,0,32,16,0,0,146,9,0,130,9,0,18,72,32,2,65,4,128,8,4,144,0,0,18,73,36,146,73,36,
146,64,36,146,72,0,2,73,36,2,0,4,144,0,0,0,72,0,144,0,4,128,65,36,0,9,36,130,1,32,0,0,36,130,73,36,146,73,36,144,72,36,18,73,36,146,73,36,146,73,36,146,73,36,146,73,32,0,73,36,146,73,36,146,73,36,146,
73,36,130,73,36,146,73,36,146,73,36,146,73,36,146,73,36,146,73,36,146,9,36,146,73,36,146,73,36,146,73,36,146,73,36,146,73,36,146,73,36,130,73,32,18,0,0,16,73,36,144,73,0,16,9,36,2,64,32,144,64,36,128,
72,0,130,8,0,144,0,36,146,73,36,146,73,36,146,73,36,146,73,36,146,73,36,146,73,36,146,73,36,146,73,36,146,73,36,146,73,36,146,73,36,144,73,36,146,73,36,144,73,36,146,73,36,146,73,36,146,73,36,146,73,36,
146,73,36,18,73,36,146,73,36,146,73,36,146,73,36,146,73,0,144,72,4,146,73,36,146,73,36,146,73,36,146,73,36,146,73,36,146,73,36,146,73,36,146,73,36,130,73,36,146,73,36,146,73,36,146,73,36,146,73,36,146,
73,36,146,65,36,146,73,32,2,73,36,146,73,36,146,73,36,146,73,36,146,73,36,146,73,36,128,9,36,146,0,0,18,73,36,146,73,36,146,73,36,146,73,36,146,73,36,146,73,36,146,73,36,146,73,36,146,73,36,146,73,36,
146,73,36,146,73,36,146,73,36,146,73,36,146,73,36,146,73,36,146,73,36,146,73,36,146,73,36,146,73,36,146,73,36,146,73,36,146,73,36,146,73,36,146,73,36,146,73,36,146,0,36,146,73,36,146,73,36,130,73,36,146,
73,36,146,73,36,146,73,36,146,73,36,146,73,36,146,73,36,146,73,36,146,73,36,146,73,36,146,73,36,146,73,36,146,73,36,146,73,36,146,73,36,146,73,36,146,73,36,146,73,36,146,73,36,146,73,36,146,73,36,146,
73,36,146,73,36,146,73,36,146,73,36,146,73,36,146,73,36,146,73,36,146,73,36,146,73,36,146,73,36,146,73,36,146,73,36,146,73,36,146,73,36,146,73,36,146,73,36,146,73,36,146,73,36,146,73,36,146,73,36,146,
73,36,146,73,36,2,73,4,146,73,36,146,73,36,146,73,36,146,72,0,18,73,36,146,73,32,146,73,36,0,73,36,146,9,36,146,73,36,146,73,36,146,73,36,146,73,36,146,73,4,146,73,36,146,73,36,146,73,36,146,73,36,146,
73,36,146,73,36,146,73,36,146,73,36,146,73,36,146,73,36,146,73,36,146,73,36,146,73,36,146,73,36,146,73,36,146,73,36,146,73,36,146,73,36,146,73,36,146,73,36,146,73,36,146,73,36,146,73,36,146,73,36,146,
73,36,146,73,36,146,73,36,146,73,36,146,73,36,146,73,36,146,73,36,146,73,0,146,72,36,146,73,32,18,73,36,2,64,0,0,1,0,2,0,0,0,73,4,144,0,0,0,64,36,18,72,0,0,0,4,2,65,36,2,73,36,128,9,36,146,73,0,18,73,
36,146,73,36,144,0,4,146,73,36,146,73,36,146,72,0,144,0,36,146,73,36,146,73,36,146,73,36,146,73,36,146,73,36,146,73,36,146,73,36,146,73,36,146,73,36,146,73,36,146,73,36,146,73,4,146,73,36,146,73,36,146,
73,36,146,73,36,146,73,36,146,73,36,146,73,36,0,0,36,146,73,36,146,73,36,146,73,36,146,73,36,16,0,36,144,73,36,146,73,32,18,73,36,146,72,32,144,0,32,130,8,36,18,64,32,146,73,0,146,65,4,144,64,4,128,0,
36,146,65,32,0,8,36,144,8,4,146,73,36,128,1,0,0,73,36,146,72,0,18,64,36,128,9,32,2,64,0,2,73,4,130,73,36,146,73,36,144,73,36,146,1,36,146,73,32,146,73,0,2,73,36,146,73,36,146,0,4,146,73,36,146,73,36,146,
73,36,146,73,36,146,73,36,146,73,36,146,64,36,146,73,36,146,73,36,146,73,36,146,73,36,146,65,36,146,73,36,146,73,36,2,65,36,18,73,36,146,73,36,0,0,4,146,73,32,2,64,0,0,72,0,146,72,4,18,73,32,2,64,32,0,
72,32,144,65,4,128,0,4,146,73,0,130,8,4,146,1,0,128,64,0,0,64,36,2,72,36,16,1,0,2,73,32,146,64,0,16,0,0,144,0,36,18,1,0,2,64,0,146,72,4,130,1,32,18,73,32,146,72,0,18,73,36,2,72,0,146,0,36,0,0,0,0,0,32,
0,64,32,0,9,4,146,73,36,146,73,36,146,73,36,146,73,36,146,73,36,146,73,36,146,73,32,0,73,36,146,73,36,146,73,36,146,73,36,146,73,36,146,73,4,144,65,36,128,0,0,128,0,0,0,0,0,2,73,0,18,9,36,128,0,32,128,
65,36,146,64,36,146,73,32,0,73,0,144,0,0,16,0,32,0,73,36,0,65,36,146,1,0,0,9,32,0,1,36,130,73,36,130,72,36,2,73,36,0,9,36,16,0,4,146,73,32,2,72,36,146,64,0,0,64,0,130,1,36,0,0,0,144,8,32,146,0,0,0,0,0,
16,73,36,146,65,0,0,9,4,146,64,0,128,73,32,0,0,36,146,73,36,146,73,36,2,73,36,18,64,0,144,0,36,146,64,0,130,73,32,0,0,0,0,0,32,0,0,36,16,73,36,146,73,4,0,0,36,128,1,36,0,1,36,146,73,32,18,9,36,2,9,0,2,
73,36,128,0,32,0,1,36,144,1,4,146,73,32,144,65,0,2,64,32,128,0,32,18,9,32,146,73,36,144,0,0,146,8,0,130,65,36,146,73,36,130,73,36,146,64,4,0,0,4,144,0,0,18,73,36,2,73,32,146,64,36,128,72,32,130,8,0,0,
73,32,146,1,32,146,73,36,0,8,0,0,64,0,16,8,36,146,73,36,130,72,0,130,64,36,128,0,32,0,0,0,16,1,4,144,0,0,16,8,0,0,0,0,0,9,36,0,9,36,18,73,36,144,72,0,0,0,0,0,0,0,16,65,4,146,65,36,0,0,36,2,64,0,128,1,
0,130,65,32,146,65,36,18,64,0,130,9,4,2,9,36,144,9,32,2,8,4,130,73,0,146,64,36,128,73,32,18,64,32,144,73,32,18,64,0,2,73,4,146,65,36,128,64,36,128,1,32,0,1,32,0,72,4,0,0,0,0,0,0,16,73,32,146,0,32,16,8,
0,146,1,32,0,73,32,18,72,0,0,8,36,128,0,0,18,0,0,16,64,4,0,1,36,144,0,32,18,72,36,146,73,32,146,0,0,0,0,0,128,0,4,146,72,0,0,64,0,16,64,0,2,0,0,146,73,36,144,0,36,18,64,0,0,0,0,0,0,36,146,65,36,0,0,0,
146,73,4,16,1,4,18,8,32,144,0,4,144,64,36,146,0,32,2,8,0,18,0,0,146,65,0,16,72,36,146,65,36,0,72,4,146,73,36,16,9,32,128,64,4,18,65,36,18,0,0,146,73,36,146,1,36,0,73,4,146,73,36,0,73,32,144,72,0,0,0,0,
0,8,4,18,72,36,16,65,32,146,65,32,2,73,36,146,1,36,146,73,36,18,73,0,0,0,4,144,0,0,0,9,36,130,73,32,0,64,0,0,73,36,146,73,36,2,73,32,18,73,36,130,73,36,2,9,36,146,73,32,144,64,4,146,8,4,128,1,36,146,73,
32,16,72,36,146,73,36,0,1,36,128,9,32,2,8,36,128,9,36,0,72,4,146,72,32,18,64,32,0,65,36,2,72,4,144,1,36,128,64,0,0,9,36,130,65,4,146,1,0,0,73,0,2,1,4,0,1,36,128,9,32,0,72,4,146,72,0,144,73,0,128,73,36,
146,9,36,128,9,32,18,9,0,18,72,4,146,72,0,18,8,32,0,64,32,2,73,36,146,0,0,0,1,0,128,9,36,128,73,36,144,0,0,0,0,0,16,1,0,0,0,0,146,73,36,146,64,36,130,8,0,18,73,36,16,9,36,144,64,0,0,64,0,2,1,32,130,1,
4,130,0,36,0,0,36,2,73,36,130,73,36,144,8,4,2,9,36,18,0,32,16,0,36,146,73,36,144,8,36,0,65,36,2,65,32,146,9,36,128,1,0,18,9,36,144,0,36,130,8,36,130,73,36,18,65,36,128,0,4,146,73,32,146,73,36,16,1,0,16,
0,32,144,72,0,2,1,36,128,1,0,16,8,36,128,73,0,146,73,4,18,1,0,2,64,36,18,65,0,144,1,36,0,64,36,0,0,4,0,64,4,18,73,36,0,1,0,16,9,36,130,72,32,0,73,32,18,73,32,18,65,32,146,64,36,146,73,0,2,65,36,16,73,
36,146,73,36,146,9,36,146,65,36,146,72,0,18,9,36,144,9,4,0,0,4,130,65,4,146,64,0,18,8,0,146,1,36,18,73,36,146,65,32,146,64,32,18,9,32,18,73,36,18,73,32,2,8,0,0,9,32,2,65,0,18,8,36,0,9,0,18,73,36,128,73,
36,144,73,4,0,0,32,2,64,32,130,65,4,18,1,0,18,1,32,16,65,0,144,65,4,2,1,0,18,0,0,18,65,36,2,73,32,0,0,0,16,0,0,2,72,0,2,64,0,2,0,4,146,73,36,130,64,32,130,9,4,144,0,36,128,0,0,146,65,32,146,1,4,146,72,
36,16,64,32,16,8,0,16,0,32,16,9,0,2,73,32,130,65,32,146,73,36,144,72,36,146,9,32,2,73,4,0,0,4,130,65,32,18,73,32,146,72,36,128,72,32,146,73,36,18,64,4,130,0,4,128,65,32,0,64,0,18,9,0,16,0,32,146,9,36,
144,9,0,0,0,36,146,1,0,144,1,4,0,9,0,128,1,32,130,64,32,16,9,4,16,65,32,18,73,36,0,8,4,144,0,4,0,0,0,146,72,36,0,1,36,128,9,36,128,73,0,0,0,4,0,1,36,0,73,0,18,8,0,146,9,32,130,73,32,128,0,4,0,72,36,0,
1,0,128,9,4,128,0,0,0,72,36,130,64,36,0,73,36,146,73,4,2,64,0,146,65,32,16,73,36,128,1,36,146,0,32,144,1,4,0,64,36,130,8,4,146,72,0,18,64,4,0,65,36,130,0,0,16,9,4,128,8,32,146,8,0,130,73,4,128,1,4,2,73,
4,2,0,32,0,9,32,18,72,4,128,8,0,130,72,4,128,72,0,146,1,0,128,73,0,18,8,36,130,73,0,2,65,0,130,8,0,2,0,4,2,0,36,144,9,32,130,8,4,2,73,36,146,0,4,0,0,4,16,72,0,146,9,0,0,64,0,0,1,4,0,73,4,128,9,4,128,73,
36,0,1,32,144,64,36,128,65,36,2,1,0,130,9,36,130,9,32,130,65,36,0,72,4,130,72,4,18,64,0,144,9,32,16,8,0,2,0,36,0,0,4,128,72,36,0,65,0,144,72,32,128,72,32,144,1,36,16,0,4,0,8,4,18,1,0,0,72,36,144,72,36,
144,0,36,128,65,36,0,0,0,18,72,36,144,72,32,16,65,36,128,8,36,128,8,36,18,72,32,18,73,0,130,72,32,2,0,0,2,1,32,2,0,36,0,8,4,128,73,36,130,8,4,128,73,32,0,65,0,16,1,36,0,8,32,18,72,0,18,73,0,130,64,32,
128,72,32,130,73,0,0,9,0,0,73,32,2,64,0,144,8,32,144,0,32,144,64,4,2,0,4,16,0,4,2,1,32,16,1,36,144,0,36,144,9,32,2,8,0,146,8,32,18,0,0,0,72,36,144,8,32,146,9,36,146,65,32,18,1,32,146,73,0,144,0,4,0,73,
0,2,0,0,16,73,32,0,64,4,2,64,4,146,72,0,16,1,0,16,65,4,0,72,36,0,65,36,0,9,0,18,1,32,128,73,4,130,9,32,146,0,36,18,0,36,144,9,4,144,65,0,128,64,0,128,0,32,18,65,36,144,73,0,18,1,0,18,72,0,146,65,36,128,
72,4,18,8,4,130,73,4,144,0,32,0,72,36,128,73,36,144,8,0,0,8,32,146,65,32,0,73,4,128,72,36,2,72,0,146,72,36,0,73,32,128,1,36,146,73,36,0,8,0,2,9,36,16,0,32,130,72,4,128,1,32,18,9,36,0,9,32,18,0,36,144,
73,36,144,64,36,144,0,0,128,9,4,0,73,4,16,65,0,0,0,4,18,64,32,16,0,4,144,73,0,130,73,0,2,0,0,0,65,32,146,73,0,16,9,36,144,9,32,146,72,0,146,73,0,146,9,36,130,8,36,144,0,4,128,0,4,130,1,32,144,0,0,130,
65,0,144,0,0,130,64,0,2,8,0,2,72,0,18,1,32,146,64,0,0,0,36,144,1,32,2,73,0,0,8,36,146,0,0,144,1,36,18,72,4,0,0,36,2,73,36,0,0,0,0,1,36,130,73,0,0,1,4,146,72,0,146,8,32,16,8,32,2,64,32,146,73,4,130,8,0,
2,64,36,2,73,32,144,9,0,0,1,4,146,9,36,130,0,0,146,9,32,146,73,36,144,73,4,0,8,0,144,65,0,146,8,32,144,0,32,16,65,0,16,1,36,0,1,36,130,72,36,146,73,0,130,72,36,146,73,32,2,0,32,2,8,4,0,8,36,128,8,32,128,
0,36,16,0,4,16,64,4,128,65,4,146,0,32,0,0,0,2,65,0,146,65,4,128,9,36,146,73,36,146,64,4,18,0,32,16,73,36,144,1,4,0,8,32,146,0,0,18,73,36,18,0,0,18,0,0,0,0,0,0,0,32,146,72,4,146,72,0,128,73,36,0,8,4,144,
73,36,18,1,36,144,9,32,0,65,36,2,0,4,16,64,4,2,72,0,2,1,0,18,8,4,144,0,36,144,65,4,146,64,36,144,0,0,16,1,4,0,8,32,18,9,32,18,65,4,130,65,4,16,65,32,146,1,36,144,64,32,2,65,36,146,65,32,0,0,4,2,0,32,18,
9,4,128,9,4,16,65,0,18,73,36,16,1,36,144,8,4,0,72,32,0,1,36,18,65,36,128,1,4,128,0,0,0,64,36,146,0,4,146,1,36,130,0,4,130,9,36,130,73,36,0,73,36,128,0,4,146,72,36,146,72,0,144,8,4,146,65,36,146,73,4,146,
65,36,146,9,0,18,0,0,146,65,4,16,72,0,144,65,36,146,1,32,2,9,36,128,64,0,2,8,4,2,8,0,146,65,32,16,1,36,18,9,32,128,0,0,144,64,4,146,73,0,0,72,32,0,72,32,16,73,4,144,9,36,2,65,0,16,8,4,18,9,36,144,64,0,
18,64,32,16,8,32,0,0,32,144,1,32,144,72,4,146,1,32,144,64,32,144,8,4,128,0,4,18,65,0,144,0,36,18,0,0,146,0,32,2,65,32,0,73,36,144,9,36,146,73,4,16,73,36,16,8,36,0,0,0,128,9,36,146,65,32,16,9,0,128,8,32,
146,64,0,0,73,36,130,72,36,146,73,0,146,72,36,130,72,36,144,64,32,128,1,0,128,8,0,2,64,0,0,73,36,0,64,4,146,64,32,130,0,36,146,8,0,18,73,0,0,64,32,18,1,32,2,9,32,18,73,32,2,9,32,144,72,32,2,73,36,146,
0,4,0,72,32,2,64,0,146,72,36,0,1,36,2,1,36,0,64,0,18,73,4,0,8,36,146,73,32,0,9,36,146,9,4,0,73,0,146,64,0,128,1,32,2,64,32,0,0,36,146,64,0,0,8,0,146,8,0,146,72,32,18,65,36,128,65,32,18,0,0,130,73,36,130,
0,0,18,73,0,146,65,36,0,9,36,0,1,32,0,72,32,0,1,0,144,1,4,18,65,0,0,1,32,128,73,36,18,0,36,18,73,36,18,73,36,18,64,0,130,9,4,0,65,0,18,8,36,144,73,32,0,1,4,146,0,36,130,1,0,16,9,4,18,72,36,128,0,4,18,
72,0,16,73,4,2,8,36,130,73,32,128,65,4,0,65,0,18,0,36,18,64,32,2,8,4,146,65,36,128,72,4,128,1,36,146,73,4,0,72,4,16,72,32,18,0,32,16,73,0,2,65,0,2,0,0,2,65,36,144,64,4,18,0,36,144,73,32,18,65,36,2,9,36,
2,73,36,18,8,0,18,1,32,0,73,32,144,9,4,16,1,0,18,65,32,128,73,32,2,0,0,2,72,32,18,73,4,146,8,36,144,64,32,130,8,4,144,65,4,144,0,0,128,72,0,2,65,36,2,1,32,18,0,32,144,73,32,2,73,0,16,64,36,2,64,32,144,
0,36,130,73,4,144,0,4,2,9,36,2,72,0,0,9,4,144,8,36,130,8,4,0,65,36,2,8,4,146,1,4,130,64,36,144,72,0,144,8,36,146,73,0,146,0,4,130,0,0,16,73,4,128,73,32,146,73,36,16,0,0,144,65,0,0,0,0,144,65,0,2,8,4,146,
72,4,144,65,32,2,73,36,146,1,36,18,72,4,128,1,36,2,73,4,18,9,32,2,64,36,130,73,4,128,0,4,18,1,4,128,64,36,130,8,32,146,0,0,0,64,0,146,72,32,0,8,36,18,1,0,0,64,32,0,0,4,0,64,36,146,0,0,146,8,36,2,9,36,
146,65,36,2,72,0,18,65,32,2,73,32,130,72,0,0,1,32,128,73,0,128,64,4,16,8,4,2,9,36,18,0,4,2,0,36,0,0,32,18,64,32,18,72,4,144,65,36,2,65,32,130,73,0,0,0,36,2,73,32,146,73,0,144,1,4,0,8,0,128,64,0,0,0,0,
128,73,36,16,72,4,2,72,0,128,9,32,146,1,36,0,0,0,146,72,0,2,0,32,144,72,32,0,73,0,16,72,0,18,9,32,16,72,32,146,73,36,144,0,0,0,64,4,128,1,36,128,8,36,128,9,32,144,72,36,2,0,0,144,1,32,144,0,32,0,72,4,
0,8,4,2,72,0,18,1,36,128,0,4,18,0,0,144,9,0,16,1,32,16,72,36,0,65,4,144,65,0,144,1,4,2,65,32,16,0,4,146,9,0,2,73,32,144,73,0,128,0,32,0,0,32,128,65,32,18,73,4,2,73,32,146,1,0,2,72,0,130,72,32,128,65,36,
144,1,36,18,64,4,0,0,4,128,72,4,128,72,0,18,1,36,146,65,32,0,73,4,130,64,36,128,0,4,144,1,36,18,64,36,130,0,36,2,72,4,0,0,4,2,64,0,130,0,0,0,0,0,144,9,36,146,1,36,130,72,4,146,64,32,2,9,4,130,64,36,0,
1,0,144,9,4,146,64,32,128,65,36,0,65,36,130,72,32,146,0,0,130,9,36,128,0,36,2,8,0,130,64,0,0,0,0,0,73,32,146,1,36,2,0,32,16,65,36,128,72,0,146,73,36,130,65,32,128,1,36,18,0,36,2,0,4,146,72,4,144,0,4,16,
9,36,0,8,4,128,65,0,0,72,32,146,1,36,146,72,0,144,9,0,128,1,32,146,65,0,144,8,4,2,73,32,146,73,32,0,64,0,144,65,4,144,8,0,18,64,32,18,65,0,130,0,4,2,64,0,0,0,4,16,73,36,144,8,36,2,0,32,146,72,36,18,9,
36,0,8,32,130,73,0,18,9,4,2,0,32,128,1,4,0,64,36,144,1,4,146,0,4,146,64,0,144,8,36,128,9,36,128,73,36,2,64,32,0,0,0,0,72,4,18,65,32,144,1,32,146,9,32,16,73,32,2,65,36,18,9,36,0,0,0,0,1,36,144,73,0,146,
64,0,144,0,4,2,9,4,128,72,36,16,0,0,2,64,0,2,9,36,146,8,36,130,9,0,146,64,4,146,73,36,0,8,36,130,73,36,128,0,0,128,0,0,144,9,4,144,1,32,0,0,4,146,72,4,18,9,36,128,1,0,2,8,4,2,73,36,146,64,4,16,64,4,130,
73,36,144,1,4,146,64,36,2,64,0,0,0,36,2,72,0,0,73,32,18,72,4,128,0,36,128,65,32,146,0,0,2,64,36,128,73,36,146,73,32,146,72,32,0,0,0,0,0,0,16,73,32,144,73,32,18,65,32,18,9,4,18,1,4,130,0,0,2,73,36,16,0,
4,146,72,4,146,1,0,146,0,0,0,8,4,146,1,36,130,72,0,0,0,0,146,73,32,144,72,0,0,73,32,128,1,32,144,72,32,146,0,36,128,72,36,144,72,4,146,9,0,146,8,36,0,8,0,146,72,32,146,73,36,16,8,32,0,65,4,146,73,32,146,
1,36,0,8,0,2,1,36,0,0,32,2,73,32,146,1,32,0,0,32,146,65,4,0,9,32,18,0,4,2,0,4,144,73,36,144,73,0,0,0,36,144,65,36,130,64,36,18,1,32,18,65,32,0,0,0,0,0,0,18,72,32,18,73,32,0,65,36,2,1,4,128,9,0,2,9,36,
128,73,0,2,9,36,144,1,32,146,64,0,128,0,0,0,9,36,0,73,0,0,9,0,144,0,4,146,65,32,0,73,4,128,72,32,146,64,0,2,0,0,128,65,0,0,64,36,2,72,4,146,8,4,16,64,32,0,8,36,18,0,4,146,72,32,18,64,0,16,1,0,146,8,0,
144,9,32,0,9,0,2,9,32,144,9,36,130,8,36,130,64,32,0,72,0,18,9,4,0,9,0,146,72,36,2,0,32,144,72,4,146,73,0,0,9,32,0,8,4,2,0,4,128,1,32,2,8,36,0,0,0,0,0,0,0,9,32,2,72,36,130,9,0,146,9,0,144,0,4,128,73,4,
128,8,36,16,1,36,18,0,0,128,9,36,18,9,32,18,65,32,128,72,36,128,8,4,0,0,0,130,9,36,128,65,36,16,72,32,128,64,0,16,9,36,16,1,32,144,9,36,0,9,36,2,0,4,130,9,36,0,9,4,130,0,0,0,8,36,2,72,4,0,72,36,146,0,
36,18,9,36,16,8,0,128,65,0,128,1,32,146,72,0,146,65,36,0,65,32,128,0,0,2,65,36,18,72,0,0,8,4,130,64,0,18,9,36,128,0,0,0,8,32,128,0,4,144,1,4,144,0,4,16,64,0,0,0,0,2,64,4,18,1,4,130,72,32,16,9,0,146,1,
4,0,8,36,0,65,0,146,65,32,16,65,4,18,72,36,144,73,36,130,0,0,2,64,0,130,1,0,0,73,36,128,0,0,0,73,0,16,1,0,144,8,36,130,0,0,16,72,32,0,72,0,0,73,36,18,0,4,0,73,32,18,64,36,128,72,0,16,73,4,146,1,0,128,
0,0,144,72,32,16,1,0,130,65,4,130,65,36,16,0,4,0,1,36,2,1,36,130,72,32,146,0,36,144,9,4,2,0,32,0,0,0,16,9,0,146,1,36,130,72,0,2,64,36,2,65,36,18,9,36,146,9,32,16,72,32,130,9,32,18,64,32,18,72,4,144,9,
32,130,72,0,130,8,0,0,65,36,16,1,36,16,1,0,0,8,4,146,8,0,16,72,4,16,65,36,16,9,36,146,0,32,16,64,32,0,73,32,130,0,36,146,0,0,146,9,36,130,72,0,146,72,32,2,64,0,2,65,0,16,1,0,0,0,0,144,0,36,0,73,36,130,
8,32,0,72,36,16,73,36,146,1,32,0,8,36,0,1,32,0,1,0,144,73,0,0,0,36,0,1,0,144,0,0,144,0,4,144,65,32,128,8,36,2,0,0,2,8,0,0,1,36,144,0,4,146,72,4,128,72,4,0,8,0,18,72,32,128,0,0,16,0,4,18,1,4,0,72,0,144,
72,32,2,65,4,146,65,0,2,8,0,18,64,32,0,9,36,2,8,4,0,64,0,144,9,36,144,65,32,16,64,0,18,72,4,128,9,36,128,0,36,2,72,0,144,72,0,146,65,36,130,64,32,144,73,36,146,64,0,0,1,0,128,72,4,146,73,36,146,9,36,2,
73,4,144,65,4,18,65,36,130,8,4,130,73,0,18,9,36,146,8,0,16,8,4,2,73,4,146,9,36,128,9,0,130,0,0,0,0,4,128,72,36,16,64,4,144,72,0,128,73,36,144,1,32,0,9,4,130,9,36,16,72,32,18,1,36,128,73,4,16,72,36,18,
0,4,16,9,32,130,72,4,128,73,0,18,72,36,130,1,0,128,64,0,130,8,0,128,65,4,18,73,36,16,1,36,146,73,0,0,1,36,0,0,36,16,64,36,146,64,36,2,64,0,146,1,32,128,0,0,18,64,36,146,1,32,128,65,0,0,0,4,144,0,4,18,
0,4,146,1,36,146,0,0,2,1,36,0,0,0,0,65,36,128,72,0,2,65,0,18,65,0,0,8,0,0,0,36,18,1,32,0,73,32,18,73,0,2,0,0,0,0,0,2,9,32,0,64,4,146,64,0,2,1,36,144,72,4,144,72,4,130,9,36,0,73,32,144,0,32,2,72,0,146,
0,4,0,1,32,2,73,36,16,1,4,130,73,0,128,72,36,18,65,4,18,1,0,2,64,0,146,65,36,146,73,0,18,0,0,0,0,36,130,73,36,0,8,4,0,65,32,2,0,32,0,9,32,0,73,36,18,72,36,144,9,36,0,0,0,0,72,4,130,1,36,146,0,36,0,73,
36,146,1,36,146,1,0,16,1,36,2,1,0,0,1,0,0,0,0,16,1,4,144,8,0,0,0,0,0,9,0,0,0,0,2,72,0,144,73,36,144,9,32,16,9,32,18,8,0,2,73,4,146,65,36,128,1,32,130,72,32,0,9,4,128,9,36,146,9,32,2,1,36,130,64,0,0,8,
4,0,0,32,130,9,4,128,73,36,16,65,0,18,8,0,16,9,32,128,1,0,144,9,32,130,8,0,144,64,36,146,64,0,146,73,32,18,9,36,146,0,32,0,72,36,16,0,4,146,73,4,144,0,0,16,65,4,128,65,4,144,8,0,0,1,36,0,73,4,128,64,0,
16,72,0,130,64,36,0,0,0,146,9,0,144,1,4,18,73,32,0,0,0,0,9,4,0,0,0,0,0,0,128,65,32,18,8,4,2,65,32,130,73,36,144,1,4,146,64,0,146,72,36,144,65,0,2,0,4,146,8,36,0,9,36,16,1,4,16,0,32,0,64,32,128,64,0,18,
65,36,16,9,36,128,1,0,16,0,0,18,65,4,144,8,32,18,1,32,128,64,0,16,9,0,0,9,0,0,9,36,0,73,32,146,9,4,146,73,0,144,72,0,144,0,0,130,8,4,130,64,0,2,73,36,130,64,0,0,1,32,18,72,32,146,72,0,2,0,0,128,0,4,128,
1,32,2,72,32,130,1,36,18,1,32,146,65,4,2,0,4,146,0,32,0,1,36,18,0,32,2,9,0,0,0,0,144,8,32,16,1,36,2,8,4,146,8,32,128,9,36,130,72,4,2,73,4,144,72,36,0,73,36,130,65,36,0,65,32,146,9,0,16,1,32,16,8,4,2,0,
0,146,73,4,144,65,36,128,1,36,130,65,32,0,64,4,2,64,0,130,73,36,144,73,0,16,9,32,130,0,36,2,9,4,2,1,0,2,72,32,2,73,0,0,0,0,2,0,0,146,9,36,130,8,0,130,72,36,0,65,36,128,0,4,0,9,0,146,73,32,0,1,0,144,72,
0,2,73,36,18,73,4,16,64,0,2,1,4,146,72,32,130,73,32,2,8,0,0,0,0,16,64,0,0,0,0,2,65,36,144,64,36,130,64,4,144,0,32,18,0,32,146,72,0,18,72,4,18,64,0,130,8,0,18,72,36,0,72,4,146,9,0,130,72,4,144,64,0,128,
1,0,146,1,0,128,0,0,2,65,32,18,72,32,130,72,0,18,0,0,18,64,0,144,65,4,18,64,32,0,8,0,16,1,36,0,65,4,0,9,0,144,73,4,18,64,0,18,0,4,130,73,4,128,9,32,146,9,32,16,64,0,144,65,4,0,9,4,0,1,0,0,64,4,146,73,
0,146,73,4,128,8,4,146,64,0,128,64,0,0,1,4,130,73,32,16,8,32,144,65,36,130,9,32,128,0,4,144,64,0,144,1,4,130,72,4,2,9,4,0,65,0,130,65,36,2,8,32,130,8,36,18,65,32,130,0,4,144,0,0,128,65,36,146,9,4,16,0,
0,0,8,0,144,1,4,130,0,4,146,64,4,144,0,36,146,64,4,0,0,0,144,0,4,146,9,36,0,0,0,144,73,32,16,72,0,16,0,32,146,65,0,16,0,0,144,9,0,128,72,32,128,9,36,146,9,0,0,64,4,0,72,4,144,0,0,130,72,36,16,1,32,146,
64,32,16,65,4,130,73,36,2,65,36,144,64,0,0,8,36,146,1,0,18,73,36,18,1,4,18,0,4,16,73,0,128,73,32,130,64,36,2,72,32,128,73,4,130,72,0,130,0,0,130,65,36,2,72,4,130,65,36,146,8,32,0,72,0,16,64,36,144,65,
36,0,65,0,144,64,32,0,65,36,130,0,32,144,8,32,16,0,32,146,0,0,18,0,32,18,0,4,128,9,36,16,0,32,16,73,4,0,9,0,0,9,36,144,9,0,0,0,0,144,64,36,130,9,32,2,64,32,2,72,32,0,0,4,144,1,36,18,9,36,2,72,0,18,9,0,
144,1,0,0,1,0,146,73,32,144,1,32,146,73,4,128,8,32,146,73,36,0,1,32,146,1,36,16,8,36,146,1,32,16,1,32,144,9,36,146,73,32,130,64,4,2,64,0,130,73,32,0,65,0,130,9,0,2,1,36,146,0,4,18,1,32,16,8,0,16,9,4,0,
8,36,0,8,32,18,0,0,146,64,36,128,0,36,0,1,36,2,65,0,146,0,36,146,64,36,146,8,32,2,0,32,18,73,36,18,65,32,128,0,0,16,0,32,144,1,0,18,65,4,130,64,0,16,73,36,130,65,32,18,64,36,130,72,0,2,64,36,128,9,36,
128,64,0,128,73,0,0,73,36,128,0,32,2,73,36,146,73,36,128,9,32,130,72,4,128,0,0,130,72,0,130,8,32,18,72,0,2,73,32,16,8,0,16,64,36,0,8,4,146,8,36,2,65,36,128,65,36,146,8,0,146,1,4,130,9,4,144,1,4,146,0,
36,0,8,4,128,8,4,146,64,36,144,1,0,128,9,32,2,8,4,0,1,36,0,0,32,0,73,0,0,73,0,130,0,36,128,0,0,2,65,32,130,64,32,128,0,32,2,72,0,0,9,4,0,8,32,18,0,32,146,73,36,130,65,36,2,64,0,16,64,36,128,9,36,144,9,
4,146,73,36,2,73,4,144,8,4,128,9,0,16,1,0,130,72,32,2,64,4,128,0,4,0,65,0,0,72,32,128,8,36,128,64,32,18,9,32,146,64,0,16,0,0,130,73,36,18,0,0,0,64,0,0,64,0,18,65,0,18,73,36,0,64,32,144,64,0,18,65,4,0,
0,0,0,73,4,18,64,32,146,64,36,18,73,0,130,9,0,128,64,32,144,8,32,144,1,0,0,1,4,146,0,4,130,64,36,130,9,4,130,1,0,18,65,32,18,72,32,2,0,32,130,0,0,128,72,32,0,9,36,144,64,0,146,64,32,130,64,0,130,9,4,16,
1,0,146,65,32,144,64,0,146,73,32,0,65,4,0,8,0,130,65,4,0,1,36,0,8,0,2,72,36,0,8,36,128,9,0,18,9,32,130,72,0,144,65,4,128,72,36,144,73,36,0,9,0,2,73,0,0,65,36,2,64,36,144,64,4,0,9,32,18,0,0,130,9,0,130,
73,4,146,64,32,0,0,0,18,73,36,18,64,36,0,0,0,146,9,32,0,73,36,2,8,36,144,73,36,18,73,0,0,0,0,130,9,4,16,72,4,130,8,4,130,9,0,18,73,36,130,8,4,2,8,36,16,1,36,146,72,36,16,0,0,2,72,4,0,0,36,18,65,4,0,1,
4,146,65,0,128,64,0,16,73,0,0,8,0,0,9,32,130,0,0,0,9,0,18,64,36,0,0,0,130,72,4,18,0,4,130,0,4,130,0,36,16,0,0,18,65,4,0,0,36,144,1,36,146,64,36,0,0,0,130,1,36,18,1,32,18,65,4,146,9,32,0,8,36,16,8,36,130,
73,36,146,73,0,144,0,4,2,73,4,16,64,4,0,1,32,2,0,0,128,9,36,146,73,4,144,73,36,18,72,32,0,0,4,146,64,32,2,0,36,128,64,0,144,0,0,2,73,36,2,72,0,2,73,36,146,9,32,0,65,36,144,9,0,0,73,36,144,0,4,146,64,0,
18,9,4,144,1,0,144,1,4,128,1,32,0,73,0,0,0,0,146,8,0,128,0,0,128,64,0,130,64,0,0,73,0,146,1,36,16,0,4,0,64,4,144,65,32,2,64,36,18,1,36,18,8,36,2,64,36,18,64,32,146,0,36,16,1,32,146,73,0,144,0,0,2,73,32,
16,1,36,130,0,32,18,73,4,146,64,32,2,72,0,2,0,36,130,0,0,2,9,4,16,72,0,0,9,4,128,64,36,128,0,36,0,0,0,2,72,32,16,0,0,2,72,0,0,0,0,18,9,36,146,73,4,0,65,0,0,1,4,146,73,32,18,73,0,2,73,36,16,1,0,144,72,
4,130,73,4,146,64,0,0,9,36,0,1,36,0,72,32,0,9,32,130,73,4,146,64,4,146,64,4,130,72,4,0,9,36,18,9,32,0,73,0,0,64,36,146,72,32,16,72,0,144,65,36,2,8,36,146,9,4,146,8,0,130,64,4,146,73,36,146,1,0,0,0,0,2,
65,32,130,72,0,16,1,36,16,1,32,146,72,0,128,72,4,130,1,4,144,73,36,144,9,32,2,1,0,130,72,4,144,9,0,16,72,4,128,0,0,146,64,0,144,0,0,2,73,32,0,8,0,130,64,4,144,0,32,0,9,36,2,8,32,144,0,0,18,73,32,2,64,
0,144,9,0,146,73,36,146,1,0,146,1,36,146,73,0,0,1,36,18,72,0,0,64,0,130,8,32,0,64,0,18,72,0,2,65,0,2,9,32,128,64,4,144,73,36,130,9,0,146,72,36,2,73,0,2,1,36,144,73,0,146,64,4,18,64,32,0,72,32,16,0,0,130,
0,36,128,0,0,0,64,32,0,72,4,128,8,0,16,0,0,0,73,4,146,9,0,144,9,32,128,64,36,128,73,0,18,0,4,2,9,36,146,9,4,128,0,36,130,9,36,144,1,0,128,8,0,18,9,36,16,9,36,130,73,0,2,73,0,18,73,32,2,65,4,2,72,4,146,
72,0,128,73,36,0,0,4,0,72,4,128,0,32,16,72,4,130,9,36,146,72,32,16,8,0,0,9,36,128,64,36,0,0,0,2,9,4,144,9,36,128,64,0,128,65,36,0,64,0,2,8,0,2,73,4,144,9,32,146,65,36,146,8,36,128,9,0,144,1,0,0,64,4,128,
73,4,18,9,32,18,72,4,146,64,32,2,64,0,144,1,4,0,0,0,0,1,36,0,0,32,18,8,4,130,64,0,0,73,0,0,8,0,0,8,36,18,64,36,0,1,0,18,65,0,2,65,0,128,1,36,144,8,36,16,73,4,128,8,4,128,0,36,146,72,32,2,73,32,128,9,36,
2,9,0,146,1,0,128,72,32,128,73,0,0,8,4,130,0,4,128,65,36,18,72,0,0,8,36,16,1,0,146,65,36,0,0,0,2,0,0,2,72,4,146,64,0,18,64,36,144,64,0,18,65,4,18,73,32,18,73,32,18,65,4,146,8,4,0,0,0,146,65,36,18,65,4,
130,1,4,130,65,36,144,0,36,146,1,36,146,72,4,146,0,0,144,0,0,146,72,32,144,72,4,146,0,4,16,0,4,144,72,4,144,9,36,128,9,32,2,9,4,146,9,4,146,0,0,2,1,32,0,0,32,0,64,32,130,0,0,16,1,36,128,72,0,18,73,36,
0,73,4,146,73,32,2,9,4,2,73,36,146,8,4,144,9,4,146,72,4,130,8,0,2,9,0,2,65,36,2,65,0,144,1,32,144,1,36,0,9,0,144,8,4,0,0,0,0,1,32,2,73,32,144,65,36,128,1,36,18,0,0,16,72,0,18,0,36,146,64,36,16,72,36,18,
73,4,18,65,36,130,8,4,146,72,0,0,0,32,128,73,36,128,73,4,146,73,36,0,0,0,2,65,32,0,72,36,18,9,4,0,9,36,2,73,0,146,72,36,128,1,36,0,1,0,130,73,4,18,73,32,130,9,0,144,65,32,2,65,36,130,73,32,16,0,0,2,64,
36,16,8,36,0,73,0,16,1,36,144,72,36,146,64,32,2,1,36,144,72,36,146,1,36,144,9,0,16,64,32,18,0,4,144,0,36,128,0,36,128,65,32,0,0,0,16,1,0,18,72,0,2,73,4,2,73,36,0,73,36,2,8,4,16,1,32,144,9,32,18,0,36,146,
64,0,146,0,0,128,1,32,18,64,36,130,0,4,128,0,32,16,73,36,2,64,0,2,8,36,146,72,36,0,1,4,130,65,0,0,9,36,128,64,0,128,8,32,16,72,0,128,9,36,130,64,36,128,8,4,16,1,0,144,1,4,130,72,4,146,0,0,130,65,4,146,
65,0,130,0,0,128,0,4,0,1,0,0,72,4,130,65,36,146,72,4,16,64,4,146,64,32,130,1,32,130,1,32,128,73,32,2,9,0,18,0,36,146,8,4,0,9,36,128,1,0,2,72,4,2,9,36,146,73,0,2,73,0,128,73,36,2,0,0,16,65,36,146,72,32,
146,9,32,2,1,36,144,73,36,2,0,0,130,73,0,0,8,32,128,0,32,0,1,4,128,9,0,144,64,0,2,9,36,146,72,0,130,8,0,146,72,36,18,8,36,2,73,36,130,73,36,144,65,4,130,73,4,2,65,0,0,64,4,0,72,32,144,8,0,128,8,0,144,
0,0,2,9,32,16,64,36,128,73,36,146,0,4,128,8,32,2,64,0,2,72,0,16,0,0,18,73,4,2,8,4,2,65,36,16,64,4,0,64,4,0,65,36,144,0,36,146,65,36,0,8,4,128,0,0,0,73,32,0,1,0,0,9,36,146,73,32,146,0,0,130,64,36,144,72,
36,130,0,0,2,0,32,2,9,4,0,0,0,2,65,0,130,72,36,0,73,0,0,72,4,0,8,0,0,1,36,128,73,4,2,72,4,2,0,32,0,65,36,2,0,0,130,72,4,18,0,4,146,9,32,0,73,32,130,9,4,18,64,0,18,9,0,144,72,0,16,0,0,146,73,4,16,9,4,128,
65,0,2,0,32,18,9,4,146,1,32,16,0,4,128,0,0,0,72,32,2,65,4,144,73,4,18,73,36,146,64,0,0,64,4,2,73,32,128,65,32,144,64,32,130,64,36,16,1,32,128,73,36,18,9,0,146,73,32,0,1,36,144,72,4,130,64,0,0,73,36,146,
65,36,16,1,4,18,64,4,144,72,0,0,65,0,18,0,0,146,1,36,144,0,36,130,64,0,130,64,36,144,0,36,16,9,32,128,0,0,2,73,4,128,73,0,146,8,0,0,65,32,16,9,36,0,0,32,144,64,36,16,9,0,0,9,0,0,72,4,128,64,36,128,65,
0,0,73,32,130,65,4,128,9,36,0,1,0,18,72,0,2,72,0,2,73,36,0,0,0,0,64,32,146,64,32,146,65,36,146,0,4,130,73,36,2,9,36,146,9,36,128,73,32,0,1,4,146,9,4,146,64,36,2,73,4,146,65,36,16,9,36,146,8,0,16,65,4,
0,1,36,0,64,32,0,1,0,2,9,4,146,0,36,2,9,4,0,1,0,18,9,32,2,72,0,146,73,36,144,9,4,130,65,36,130,73,4,144,73,4,146,72,4,2,0,4,128,1,32,0,73,36,144,1,36,18,72,0,18,73,4,130,73,0,144,0,36,0,8,0,144,73,32,
2,1,32,130,64,36,18,0,0,128,8,4,144,72,32,146,8,0,18,73,32,0,72,0,2,64,0,16,0,4,146,72,36,18,1,36,130,64,0,144,72,36,0,9,4,144,0,4,2,0,36,18,65,32,0,8,4,146,1,0,0,0,36,146,73,0,2,9,32,0,9,36,146,72,36,
130,65,36,130,9,0,144,8,0,18,73,0,18,73,4,146,65,36,0,73,32,18,64,36,144,72,36,144,73,36,18,1,4,16,64,0,146,8,4,0,64,0,0,9,36,146,0,0,144,9,4,2,9,0,128,72,36,128,65,0,144,65,4,2,9,32,144,64,32,2,73,4,
18,73,36,146,72,4,2,73,4,128,0,4,18,73,32,146,8,36,130,65,4,130,73,36,146,64,36,0,8,0,16,0,0,0,0,0,2,64,32,144,73,36,128,72,0,128,0,32,146,64,32,2,0,0,0,65,36,144,9,4,146,9,32,0,9,32,0,0,32,144,8,36,144,
0,32,146,8,0,146,73,36,146,1,36,144,64,32,0,1,36,130,64,36,146,9,4,18,1,4,128,9,36,0,8,0,146,73,0,18,72,4,18,1,36,18,0,0,2,1,0,2,65,4,146,8,32,2,0,0,0,72,4,130,64,32,146,72,36,146,64,36,128,0,4,144,1,
32,128,72,4,144,1,36,0,73,0,128,72,0,144,8,36,0,65,36,146,73,4,2,64,36,130,1,0,0,65,0,146,65,32,128,64,0,0,0,4,2,72,32,0,73,4,146,0,32,18,0,0,18,0,32,0,9,36,144,73,0,130,64,36,130,9,36,0,9,4,128,0,4,18,
0,32,0,0,0,0,64,36,144,64,36,146,73,36,128,0,36,146,73,32,2,64,4,144,64,0,130,72,4,130,72,36,2,1,32,128,1,4,128,9,0,128,0,32,130,65,4,146,72,0,128,9,36,144,1,36,2,1,36,130,1,36,128,9,32,2,9,0,144,0,32,
146,73,32,144,73,36,2,1,36,16,73,4,18,64,0,0,65,36,16,73,0,130,65,0,2,65,0,144,0,32,130,8,4,146,65,0,2,72,36,146,0,4,128,64,4,128,64,36,130,8,4,144,9,36,130,1,32,16,0,0,144,8,4,128,65,0,2,73,36,16,73,
36,146,72,0,0,9,36,144,64,0,18,73,0,0,72,0,146,9,0,146,72,36,128,72,0,144,9,0,18,73,36,146,1,4,130,9,0,144,8,0,16,72,36,130,1,0,16,1,4,128,8,0,0,0,36,18,9,4,18,65,32,130,8,0,2,9,0,146,73,0,144,0,0,2,1,
36,128,8,0,144,65,0,144,65,36,130,73,32,144,9,4,144,64,0,128,0,4,144,9,36,146,73,0,144,72,4,144,64,4,144,1,4,128,73,36,146,65,32,18,72,4,2,8,0,144,8,36,16,1,36,18,64,0,0,0,36,144,73,36,0,8,36,0,9,36,146,
0,0,144,0,32,130,9,32,128,9,36,0,64,0,128,9,4,18,64,36,128,9,36,130,0,36,146,65,36,0,9,4,128,8,0,0,1,36,146,1,36,128,73,32,146,64,36,0,65,36,2,0,4,2,9,36,146,73,0,16,64,36,144,73,36,144,73,0,146,64,0,
146,72,32,144,73,36,128,73,36,0,8,32,0,0,4,2,73,32,146,65,32,2,73,32,2,64,36,128,9,4,0,8,4,146,64,4,0,1,0,16,1,4,0,72,36,144,1,0,146,64,4,146,64,36,2,0,36,128,0,32,144,0,0,18,72,32,144,73,4,146,73,0,2,
64,4,128,0,4,146,73,32,2,1,0,0,1,36,16,65,32,144,8,4,0,65,4,18,64,4,16,73,36,128,65,32,16,73,36,144,8,0,146,73,4,130,73,0,18,0,4,128,0,0,144,64,0,144,9,32,144,65,32,0,1,4,0,64,4,146,73,36,2,72,32,0,73,
4,128,8,0,16,65,0,146,0,0,18,72,32,146,9,32,128,73,0,128,0,4,2,64,0,144,0,36,146,1,4,130,9,4,2,64,4,144,0,0,144,64,32,18,1,4,0,1,36,18,64,0,18,1,4,130,64,0,2,1,36,144,64,36,146,64,0,146,73,0,128,73,0,
16,9,4,18,65,0,0,0,0,18,64,0,0,9,4,18,64,0,0,1,36,16,9,32,144,73,36,146,72,32,16,1,36,18,1,36,146,64,32,18,73,36,18,73,0,130,65,4,144,9,0,2,64,0,18,73,4,144,73,0,18,64,0,128,9,0,18,65,36,146,1,32,2,64,
4,144,65,36,18,8,36,128,73,4,0,0,4,146,0,32,146,73,4,144,65,36,128,0,4,0,1,0,128,8,36,0,0,4,146,1,32,128,8,36,144,73,36,144,0,0,0,64,32,18,64,0,16,1,36,144,1,36,16,8,0,16,64,36,144,64,4,18,9,36,146,9,
36,16,64,4,18,73,0,2,73,36,146,1,4,18,64,0,0,73,32,16,9,36,18,0,0,0,0,36,18,64,36,130,73,36,128,73,4,128,0,4,144,73,4,16,0,4,146,72,36,146,1,36,2,9,36,0,0,0,16,0,4,146,73,32,144,65,0,0,65,36,128,8,32,
2,9,0,146,1,36,146,72,0,2,64,0,18,72,36,2,9,4,146,72,36,144,9,32,130,72,0,128,8,36,18,0,4,0,1,32,130,65,36,128,0,4,146,1,36,0,8,0,18,73,36,0,0,0,0,8,0,2,64,0,128,0,0,18,73,36,128,8,4,0,8,0,16,8,36,146,
72,4,146,0,4,2,1,0,0,0,0,144,1,32,18,65,0,2,73,0,2,1,36,0,65,32,130,64,0,2,9,32,2,64,0,128,9,32,144,0,36,16,64,0,18,73,32,0,0,0,16,1,36,146,64,36,2,9,0,0,1,36,144,73,36,128,0,36,144,8,32,2,65,0,16,1,0,
0,1,0,128,8,0,2,65,36,130,64,0,0,72,4,146,8,36,130,73,4,16,64,32,18,9,32,18,0,32,18,0,36,2,65,32,16,64,0,144,1,32,0,64,0,146,72,36,146,9,36,2,64,0,0,64,0,0,8,0,144,8,32,0,64,36,146,1,0,2,65,36,146,72,
0,146,0,36,130,1,4,130,9,36,18,73,4,130,9,36,130,65,36,16,72,4,128,64,36,16,65,36,128,1,4,144,64,4,146,0,4,146,0,32,128,64,36,128,73,36,146,73,32,16,9,32,146,0,0,130,73,0,128,9,36,146,9,4,146,73,0,18,
72,4,18,0,32,146,9,0,144,9,32,0,8,0,2,0,4,128,65,0,0,9,32,2,9,32,128,64,36,16,8,36,144,1,4,146,9,32,144,1,4,130,73,36,18,73,0,0,8,0,18,0,4,0,9,32,18,73,32,16,0,0,130,64,0,18,65,36,0,0,4,128,65,4,144,72,
0,2,72,32,2,72,36,128,1,0,18,0,0,128,8,36,144,8,4,18,64,4,146,73,32,0,9,32,2,1,4,18,0,36,146,0,0,130,8,4,128,72,36,130,8,32,0,73,32,130,64,0,146,9,32,18,73,36,0,0,4,18,9,0,0,0,0,144,65,0,0,65,36,0,8,32,
2,65,36,18,0,32,144,1,32,144,9,36,146,8,4,144,8,4,16,0,36,0,1,36,144,65,36,16,64,0,128,8,0,146,72,0,0,73,36,144,72,36,16,0,36,16,0,0,0,0,0,0,0,4,2,73,36,146,65,4,16,0,4,146,72,0,18,65,32,144,1,4,0,0,36,
128,72,36,128,1,32,16,8,32,0,72,0,0,64,36,146,73,36,128,0,4,146,65,0,0,0,0,18,8,0,16,8,4,2,9,36,18,73,32,146,1,0,2,0,4,2,73,36,2,64,4,146,9,0,2,64,36,16,9,0,0,0,32,146,65,36,146,72,4,128,72,4,0,64,0,128,
64,36,16,0,4,0,73,0,144,72,36,146,73,32,0,72,36,146,72,0,128,64,36,146,64,36,128,73,4,2,73,36,130,1,36,2,64,4,128,9,36,146,9,36,130,9,4,0,64,0,146,0,0,0,0,32,18,73,0,2,65,4,18,0,36,144,73,36,0,9,0,144,
1,32,0,0,4,146,8,32,130,72,32,128,1,36,2,64,36,130,73,36,146,73,36,2,1,36,128,64,32,16,0,0,128,1,32,128,0,0,2,1,36,18,1,36,0,8,0,16,73,4,18,0,4,130,73,0,146,64,4,16,9,36,146,9,36,2,73,36,128,0,0,146,73,
36,0,1,32,2,65,4,0,64,0,128,73,4,0,8,0,2,65,36,146,0,0,0,65,4,146,73,32,144,1,36,146,72,0,0,64,4,144,9,4,16,9,36,146,72,0,18,64,0,128,0,4,146,64,32,16,72,0,16,0,36,146,72,4,2,0,36,18,64,32,144,64,36,146,
73,4,0,9,36,130,65,36,0,9,36,0,72,0,2,73,36,144,65,0,130,0,0,146,72,0,0,0,4,2,73,0,144,65,36,130,64,36,128,73,0,18,8,4,130,0,36,18,0,36,128,73,0,18,64,0,128,8,32,0,8,0,0,1,36,16,9,4,130,72,4,0,0,0,144,
8,32,18,73,36,0,8,4,130,9,36,2,9,32,146,1,36,18,64,0,128,0,32,18,9,32,146,64,36,146,73,36,146,8,4,18,73,32,0,65,0,144,0,4,146,9,36,16,0,32,16,9,4,130,8,32,128,1,36,0,0,0,18,73,36,128,9,4,144,72,4,18,73,
0,2,1,0,144,9,32,130,0,4,0,72,36,128,0,4,146,65,0,0,0,4,2,8,0,146,64,0,128,0,32,16,65,0,128,9,32,18,8,36,128,9,4,2,0,36,128,1,32,0,1,32,130,64,4,128,65,32,18,9,32,144,64,32,144,9,0,16,0,4,18,73,0,130,
1,32,18,73,36,128,1,32,128,1,36,146,0,0,128,64,32,144,9,0,0,0,32,16,73,36,0,9,0,130,73,4,146,73,4,146,73,36,144,64,36,128,64,0,144,64,0,128,73,36,16,64,36,18,72,32,18,73,36,0,0,0,130,0,32,146,8,36,0,73,
32,2,65,4,144,64,36,146,64,32,0,9,36,130,73,32,144,0,0,0,8,0,128,72,0,128,65,36,18,1,4,144,64,32,0,8,4,128,8,32,16,65,36,16,0,36,2,0,36,18,9,4,0,64,4,146,72,4,128,72,4,130,1,36,16,73,0,0,1,36,130,0,36,
146,73,36,130,64,4,146,64,4,144,65,0,144,0,0,18,72,4,146,64,0,18,65,4,144,8,36,128,9,4,0,9,36,0,1,32,0,0,4,146,73,0,18,73,32,146,73,36,146,72,0,18,0,36,128,0,4,130,73,0,18,0,36,146,9,32,18,1,36,146,65,
36,0,65,32,144,73,36,16,73,4,128,64,4,144,1,32,0,64,0,128,72,0,0,0,32,18,73,4,144,64,4,18,64,0,146,0,32,0,0,32,2,65,36,18,72,32,130,1,32,2,73,4,144,65,4,144,0,0,0,73,0,144,73,32,2,73,32,128,65,36,128,
9,0,146,0,36,146,64,32,16,64,32,0,73,36,18,72,0,18,73,36,128,0,32,128,72,0,18,72,32,0,65,36,130,64,36,144,9,32,146,72,0,146,72,0,2,65,36,0,65,36,18,73,32,0,0,0,146,73,36,146,73,32,0,0,0,18,72,0,2,0,0,
2,64,32,2,72,0,0,65,0,18,8,32,2,64,0,18,0,32,128,64,36,16,64,36,2,64,0,144,73,36,146,65,32,2,72,36,2,73,32,146,0,0,18,1,0,128,65,36,128,73,36,146,73,32,16,73,36,0,0,4,130,65,0,16,0,32,146,64,4,128,9,36,
146,64,0,2,73,32,146,73,0,2,72,32,0,8,4,146,72,4,128,8,4,18,73,4,146,8,32,0,0,0,0,0,0,2,65,0,146,1,4,2,65,4,16,9,32,128,0,4,146,9,4,0,73,36,146,72,36,130,9,32,130,1,36,146,0,36,2,73,36,146,73,36,128,65,
0,128,0,0,128,64,0,2,65,32,128,9,32,18,72,4,18,1,36,128,73,4,146,73,0,146,65,36,146,1,36,128,9,0,130,0,0,18,1,4,128,1,36,2,73,36,144,72,4,146,8,32,146,0,36,146,64,36,146,73,0,2,65,0,146,9,36,144,0,0,18,
0,36,146,1,36,0,0,4,128,1,36,2,73,0,146,64,0,16,0,32,144,9,0,144,8,36,2,65,32,2,8,36,2,9,0,18,65,36,18,9,32,128,64,32,130,9,0,2,0,36,144,73,0,144,9,32,16,9,32,128,64,36,2,72,0,16,9,32,130,73,0,0,0,0,0,
0,0,146,73,36,2,9,0,128,0,32,130,0,0,128,72,36,0,9,0,128,72,0,0,9,32,130,64,4,146,0,0,18,65,36,18,72,32,144,8,4,0,72,4,128,1,36,18,64,0,0,0,4,18,1,0,146,73,4,146,72,4,0,1,0,130,73,0,16,64,36,2,1,36,128,
0,4,16,65,36,128,72,36,128,0,4,0,8,32,2,65,32,2,8,0,16,73,0,144,72,32,146,73,32,18,9,32,0,65,4,0,8,4,128,72,32,130,72,36,0,64,4,2,0,4,2,0,0,146,9,36,146,1,4,130,9,36,0,64,0,0,0,0,130,9,32,146,0,0,0,65,
36,0,0,0,18,73,36,0,65,4,16,65,36,146,0,0,16,1,32,146,0,36,16,73,32,2,1,36,146,9,36,146,9,36,0,9,4,146,8,32,18,0,4,2,73,36,18,1,36,146,72,36,18,64,0,18,8,4,0,0,32,0,0,36,0,1,4,130,73,36,18,72,32,16,9,
32,0,1,0,16,0,0,16,9,32,0,0,0,0,64,0,18,73,4,128,0,0,144,65,4,18,64,0,18,1,0,2,0,4,18,64,32,0,0,36,146,72,0,128,0,36,2,8,0,2,1,0,0,72,32,16,73,32,18,64,36,130,72,36,0,1,32,18,73,32,130,73,0,18,8,0,128,
65,36,0,0,0,0,0,4,144,9,32,16,0,32,2,64,36,16,0,0,146,1,4,0,9,0,18,73,32,0,8,0,2,1,36,0,1,0,0,64,0,0,0,0,16,73,36,0,8,4,0,73,0,128,0,0,18,8,0,0,0,0,0,0,0,2,0,32,0,1,36,144,72,0,0,1,32,0,9,0,18,0,0,146,
0,4,144,0,0,2,9,32,2,72,4,0,0,36,130,8,4,2,8,0,128,0,32,128,0,36,130,64,36,128,73,36,16,0,4,146,72,4,146,65,36,18,65,4,130,72,36,0,9,4,146,73,36,130,9,4,146,72,0,16,65,32,0,8,32,0,64,36,2,72,0,255,196,
0,20,17,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,224,255,218,0,8,1,3,1,1,63,16,127,254,255,0,253,255,0,250,123,63,255,196,0,20,17,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,224,255,218,0,8,1,2,1,1,63,16,127,254,255,0,253,
255,0,250,123,63,255,196,0,38,16,0,3,1,1,0,3,1,1,1,1,1,1,1,1,0,3,0,1,17,16,32,33,48,49,65,64,81,97,80,113,129,96,112,128,145,255,218,0,8,1,1,0,1,63,16,39,111,183,179,97,59,190,202,95,66,217,224,253,30,
125,36,31,145,34,114,139,232,107,175,36,23,52,163,124,81,178,228,108,132,23,193,121,230,106,8,121,50,248,62,147,30,44,132,253,62,141,15,35,63,79,204,189,205,132,200,76,121,249,197,47,104,132,33,33,9,205,
41,244,132,233,56,241,140,67,196,33,8,38,145,79,34,31,55,33,9,147,19,240,81,179,201,54,151,32,145,8,120,69,41,74,81,178,149,173,151,169,148,165,226,143,138,121,101,16,162,127,240,172,242,244,139,47,45,
148,175,208,182,50,127,5,230,148,165,41,228,242,82,241,74,94,104,242,240,178,109,41,79,35,33,8,77,191,200,222,47,251,173,143,97,8,44,155,121,165,27,254,235,180,187,73,125,119,152,65,100,22,172,165,27,
46,39,6,162,244,93,98,218,94,39,84,162,199,204,32,132,133,72,108,162,106,31,252,37,250,65,224,240,81,63,59,70,196,93,79,201,248,65,161,136,76,104,63,240,42,79,250,69,151,19,230,113,8,46,145,9,176,125,
33,137,81,168,47,43,167,177,144,130,68,198,136,120,41,74,134,95,163,67,94,79,135,214,35,192,223,248,121,30,82,248,199,176,153,224,185,74,81,178,229,47,130,242,138,138,95,84,215,234,186,185,165,43,60,144,
104,155,118,16,240,82,179,203,230,98,45,242,121,24,153,74,125,230,12,165,62,245,120,165,35,121,15,7,130,148,120,169,56,156,44,132,226,162,137,158,114,116,189,87,95,182,117,251,180,185,74,82,148,108,79,
89,49,226,254,43,196,217,234,69,199,205,238,19,102,60,125,66,19,133,179,97,5,234,165,41,120,131,71,193,12,154,216,133,143,27,196,139,48,197,43,212,60,135,141,104,72,67,27,42,46,76,132,39,12,79,253,225,
245,120,72,130,238,116,188,159,6,232,152,135,195,196,202,60,185,6,207,36,120,141,98,103,212,60,135,146,9,15,38,173,159,244,240,54,161,244,120,165,19,27,219,218,60,234,94,197,183,110,249,33,10,139,137,
13,16,130,93,82,237,43,226,17,144,132,71,130,148,180,130,68,33,63,195,192,136,65,61,184,186,189,87,168,66,16,152,137,234,168,191,225,228,140,132,33,54,229,245,126,113,53,250,174,124,31,83,60,144,153,8,
66,100,197,168,106,141,13,11,212,137,232,158,139,232,158,184,33,249,196,245,36,76,188,222,169,247,37,38,174,102,204,132,38,53,159,73,144,97,44,185,25,255,0,68,198,242,16,132,23,146,114,198,203,224,167,
156,132,39,109,148,67,68,98,120,151,145,174,25,8,76,126,151,232,250,66,9,240,242,8,91,225,15,252,21,177,34,31,15,211,233,17,60,139,193,224,124,66,113,8,120,67,41,224,172,190,10,134,255,0,193,249,38,82,
159,156,194,9,16,138,158,63,50,109,226,250,225,15,7,131,238,194,101,46,34,148,190,136,66,16,104,77,16,177,79,44,155,8,120,35,14,137,141,30,118,78,33,4,136,124,244,190,152,185,100,33,224,168,98,226,191,
195,201,25,8,178,237,41,74,82,255,0,52,244,34,147,166,45,132,225,172,124,209,60,55,235,76,250,52,78,102,190,16,251,89,226,116,179,233,4,145,224,111,32,145,6,181,188,132,226,236,226,98,98,41,114,99,47,
80,131,46,194,16,241,181,178,101,202,54,247,193,113,113,144,133,19,37,184,156,45,202,138,38,51,198,34,151,41,113,20,66,85,147,16,248,33,159,165,23,157,165,203,176,158,184,78,83,154,197,144,152,200,65,
15,238,166,83,201,56,95,113,62,124,147,30,10,150,41,5,88,105,127,153,58,98,244,210,159,157,207,77,230,151,33,49,147,41,74,92,132,201,190,8,63,225,13,183,176,157,81,180,49,27,35,228,73,33,42,20,56,36,145,
228,196,169,240,148,132,33,15,133,47,169,241,56,133,72,131,254,74,43,103,145,232,130,219,176,248,60,132,32,208,134,178,11,133,175,171,147,136,66,108,230,226,25,251,140,94,134,62,166,60,100,245,33,253,
230,151,105,121,107,103,182,148,184,177,99,99,103,146,13,101,30,177,19,97,54,112,150,92,130,68,68,25,74,93,152,138,92,99,120,139,176,68,31,129,234,88,160,230,54,54,38,60,81,51,203,18,34,62,98,9,99,120,
190,108,18,32,183,240,108,165,135,230,84,162,246,183,221,40,222,94,17,8,126,148,163,31,9,249,62,144,73,19,24,222,254,159,190,4,224,227,60,149,209,121,202,64,248,33,253,40,156,31,172,150,39,15,189,206,
158,206,239,175,199,16,132,202,127,193,91,35,255,0,72,78,82,33,8,67,193,227,25,255,0,210,137,226,4,136,116,233,94,95,28,54,82,241,249,50,117,56,255,0,233,7,255,0,37,63,195,201,8,65,34,113,75,141,137,151,
139,147,105,70,95,101,69,196,137,204,230,240,241,15,213,74,82,148,243,141,118,198,241,120,13,79,117,245,38,125,39,166,119,4,136,76,165,121,251,205,62,243,113,33,177,246,137,73,137,195,225,34,109,41,74,
54,94,211,40,216,184,165,197,101,230,99,115,227,164,76,94,70,133,141,164,49,88,147,38,212,19,199,210,235,193,74,121,60,144,139,30,189,165,43,219,143,148,198,92,172,120,186,95,71,138,144,156,222,25,247,
30,66,16,147,206,45,107,60,144,111,38,94,231,178,122,106,69,41,231,252,43,252,60,255,0,167,253,50,35,194,252,45,201,144,241,197,40,197,41,68,246,17,226,17,100,38,205,158,9,196,216,66,16,241,254,141,172,
86,121,60,159,150,36,143,30,207,36,100,33,49,179,232,138,121,98,88,200,66,33,228,202,95,84,198,93,153,125,75,105,70,202,42,66,16,157,210,151,88,244,100,215,169,50,251,80,152,251,186,131,91,8,50,148,242,
242,107,41,56,89,121,185,8,65,226,39,12,111,104,149,120,120,153,75,183,30,194,113,251,151,137,141,151,97,8,160,146,60,33,186,66,77,121,4,32,242,49,45,95,10,54,86,200,66,8,252,216,124,99,15,134,121,202,
82,239,131,193,75,147,30,222,127,68,178,8,147,33,9,171,201,7,232,188,37,113,177,147,102,204,98,88,144,137,150,13,222,38,166,92,165,47,165,227,103,156,189,121,200,120,41,244,242,121,33,54,148,187,4,182,
119,49,8,137,72,32,150,92,91,213,198,33,177,190,106,68,19,15,254,74,200,223,214,75,244,132,230,9,122,102,211,224,221,46,120,69,242,92,165,123,53,151,33,54,108,218,82,191,109,41,74,202,242,16,241,179,181,
195,233,227,254,88,79,99,68,18,203,144,248,81,188,132,16,209,241,20,185,15,204,158,198,202,60,88,242,148,242,61,97,177,253,245,78,161,15,167,238,120,46,204,81,224,246,228,22,81,50,237,43,201,141,173,133,
238,15,225,53,137,195,200,99,66,66,27,69,218,145,228,75,254,147,132,253,127,188,219,218,112,188,182,81,62,46,38,54,84,84,55,158,73,195,18,199,183,186,92,167,210,116,182,141,117,8,76,240,85,143,254,10,
196,255,0,164,18,101,225,100,32,184,185,97,114,241,79,59,224,191,225,116,165,237,242,149,28,88,183,240,242,66,8,132,150,46,231,16,157,82,143,10,87,145,147,60,100,225,235,121,32,159,115,214,200,120,41,
74,200,200,66,19,33,57,186,189,15,89,74,37,195,94,165,212,39,111,148,62,89,5,151,21,147,154,81,144,153,118,151,23,16,153,70,245,226,32,217,113,36,81,162,12,89,6,136,65,112,184,165,41,113,137,194,243,250,
33,178,139,169,15,193,89,231,35,16,248,81,112,199,158,69,74,241,159,152,177,148,162,38,66,11,192,208,97,182,242,151,39,161,23,152,65,229,237,246,159,224,241,141,228,32,150,92,165,60,145,137,113,79,221,
104,94,151,141,114,159,11,86,49,51,233,9,181,31,240,136,217,31,250,66,9,19,221,228,72,135,140,82,158,72,63,76,164,67,60,237,242,34,149,237,223,132,13,137,179,254,137,252,19,209,8,52,66,19,41,228,242,121,
38,92,82,138,41,79,44,147,233,82,24,66,126,201,149,20,172,242,70,78,169,118,241,53,12,91,120,188,66,16,144,207,175,92,234,151,169,202,233,33,148,119,148,152,209,114,236,225,236,229,99,101,212,181,226,
46,75,151,46,50,11,95,209,99,238,31,7,148,140,153,9,136,152,202,44,92,71,143,3,99,121,96,147,124,194,107,215,150,13,151,90,15,10,199,72,53,136,36,78,47,11,164,76,187,50,151,132,62,38,124,23,156,124,210,
148,186,159,141,156,82,227,39,162,243,4,177,180,53,200,67,199,16,152,218,27,109,248,32,95,237,144,143,5,244,190,38,70,200,76,99,101,111,154,121,18,34,34,237,252,27,203,10,92,68,223,5,69,27,111,244,241,
50,98,27,245,207,109,225,8,65,44,152,223,12,185,50,255,0,132,111,8,65,15,233,8,66,19,106,199,147,207,250,66,19,47,185,151,24,208,181,63,93,17,240,126,3,250,63,93,244,77,165,229,20,132,218,125,18,30,164,
66,141,136,130,91,7,176,156,60,71,194,228,31,76,90,158,184,76,89,68,56,81,108,32,246,148,120,144,161,71,151,152,66,16,132,33,50,141,161,49,147,193,250,125,98,249,195,22,95,67,123,8,67,225,118,151,110,
254,113,69,203,66,229,190,81,50,108,229,15,26,226,116,132,252,206,161,10,95,76,38,39,195,249,139,198,215,70,121,16,218,255,0,79,249,71,151,252,39,250,200,132,82,148,184,246,237,197,46,182,33,113,244,156,
78,23,165,147,12,124,212,241,69,108,143,253,32,209,56,162,101,41,81,114,16,132,33,224,190,138,82,245,70,249,111,96,146,33,17,22,65,9,144,163,255,0,5,103,145,181,49,138,217,8,120,238,127,12,26,38,174,238,
83,233,15,133,151,201,125,111,86,204,165,245,252,31,52,184,143,133,238,164,54,50,16,91,71,183,151,204,229,8,123,8,60,76,251,137,147,30,254,107,66,223,28,175,68,15,6,60,177,47,244,108,124,34,101,31,11,
42,27,41,228,74,137,33,206,94,177,122,210,39,11,168,94,111,146,231,145,114,245,13,16,130,16,132,196,38,67,224,157,26,32,148,216,53,233,156,76,152,211,132,98,98,198,209,95,136,242,41,253,98,68,127,208,
201,149,250,38,204,77,33,224,136,240,84,93,47,48,157,46,124,13,162,71,101,8,196,32,209,49,240,165,47,30,74,39,40,165,203,183,139,196,39,170,99,242,66,99,19,46,172,165,68,21,190,74,120,151,86,52,52,124,
198,189,215,103,162,151,34,26,22,38,58,214,184,184,197,141,23,248,47,51,136,66,111,209,15,25,56,162,242,78,105,120,165,203,233,155,53,12,163,17,249,136,253,39,130,143,149,173,137,234,66,36,132,81,190,
82,38,210,237,41,79,164,33,8,77,132,67,38,35,231,85,103,150,47,244,69,191,158,133,140,88,251,88,189,72,188,50,16,248,248,79,95,40,107,199,9,240,248,248,47,40,94,49,190,232,241,45,153,11,8,71,159,225,95,
224,219,19,253,98,60,227,224,75,41,75,199,230,125,234,162,148,165,41,74,207,39,146,190,97,57,185,56,168,164,14,254,21,226,63,247,9,34,19,151,144,155,255,0,193,8,84,177,31,209,139,39,162,123,110,206,110,
182,61,143,41,88,233,25,76,66,9,139,231,47,162,231,210,16,132,226,151,208,182,107,99,101,40,184,101,38,190,127,58,98,246,60,92,162,109,40,241,33,44,124,74,65,35,232,74,112,253,87,153,180,94,72,67,224,
245,137,100,196,137,159,163,89,9,158,33,74,121,38,45,191,238,125,38,76,165,47,20,187,63,131,232,226,41,89,228,132,218,94,41,114,148,165,47,9,143,201,9,176,132,39,15,39,162,148,188,34,23,164,137,139,33,
8,34,31,133,69,33,4,177,61,101,226,100,33,8,127,250,64,255,0,201,95,225,66,95,232,184,72,75,41,60,99,231,201,56,185,9,72,68,68,66,16,132,68,198,79,68,38,65,52,53,89,191,240,161,25,66,18,101,245,94,96,
162,199,156,76,141,139,238,188,94,169,236,190,70,202,93,152,130,23,18,33,88,137,16,52,40,173,137,49,70,82,15,252,12,255,0,70,81,49,100,31,47,33,8,66,19,167,176,104,130,215,137,122,224,209,225,143,222,
178,19,91,133,165,98,22,60,91,8,36,65,243,75,233,132,196,92,194,16,240,121,75,172,98,203,227,41,113,34,30,10,139,179,154,44,131,92,58,66,16,153,8,56,17,56,185,227,82,241,151,24,136,67,240,68,199,232,187,
251,176,122,178,19,19,27,240,62,150,94,47,183,201,231,136,74,53,4,133,202,42,255,0,70,159,133,21,191,210,98,92,204,132,38,65,77,133,255,0,79,143,8,161,231,253,60,190,145,232,156,66,194,148,242,196,137,
235,153,74,82,148,165,46,194,16,132,60,21,33,168,221,252,69,30,69,8,65,58,132,39,170,158,72,66,17,139,253,30,11,140,78,31,180,251,140,162,104,165,245,44,156,212,82,236,38,210,140,164,44,211,192,102,182,
47,164,18,18,198,63,184,216,216,188,137,122,91,22,210,151,138,95,103,210,113,50,19,148,132,134,145,224,199,252,9,235,101,33,8,77,123,79,194,141,137,229,219,232,140,132,33,224,99,240,46,174,150,221,124,
165,72,66,149,243,70,241,8,120,153,114,108,27,212,76,101,30,92,162,199,197,23,253,39,23,88,153,122,123,9,236,120,190,159,70,167,190,229,201,220,234,92,132,65,95,225,89,231,18,33,60,115,227,46,124,63,4,
215,209,180,95,248,92,140,143,17,47,204,124,220,156,254,148,165,254,3,108,242,196,143,29,204,168,131,254,81,66,137,254,143,250,100,17,127,156,66,37,255,0,221,165,126,170,82,149,228,33,8,68,84,125,22,228,
35,26,121,5,141,30,66,16,158,165,181,98,149,144,130,60,109,198,203,143,60,143,102,18,18,98,66,88,199,71,244,186,151,174,19,209,9,232,188,66,16,158,164,16,207,175,85,47,51,63,50,19,33,8,82,151,20,163,103,
225,41,39,15,22,82,241,74,93,189,44,153,41,38,50,108,26,225,51,240,186,203,145,225,33,193,100,38,38,93,120,222,210,148,98,213,240,101,19,237,119,8,124,103,137,202,147,104,159,111,127,75,202,99,238,15,
154,81,241,125,117,99,201,41,18,20,30,175,27,118,228,25,244,76,165,23,253,200,76,165,46,34,30,54,100,226,158,89,8,44,98,63,56,132,233,240,246,60,120,69,255,0,133,108,242,255,0,113,4,30,49,234,229,239,
142,110,183,176,132,39,9,226,235,40,158,84,120,30,124,27,40,159,83,19,105,79,39,146,16,139,124,172,242,121,41,69,195,33,8,76,94,5,255,0,204,72,132,33,60,227,104,134,77,90,242,108,39,186,148,188,66,111,
238,191,74,44,17,126,39,147,31,174,243,6,82,148,76,165,46,92,132,212,132,163,27,38,177,114,184,165,197,179,38,189,98,225,151,32,151,16,144,242,66,31,6,202,82,241,70,196,33,151,39,43,83,19,27,19,242,62,
16,249,158,41,54,148,79,26,16,151,142,231,16,153,74,47,74,243,141,121,197,171,208,189,14,34,141,148,163,203,33,17,23,12,108,76,185,228,243,136,111,41,231,26,201,169,140,100,198,126,107,238,13,119,113,
50,159,115,192,242,247,228,131,71,132,66,41,158,68,255,0,94,210,241,75,143,208,245,19,132,120,226,241,74,92,165,41,89,74,33,3,194,140,148,104,66,16,136,248,84,48,216,242,251,185,58,100,194,91,8,77,130,
197,15,28,63,249,27,21,241,6,33,63,108,199,151,60,158,73,212,230,151,167,196,38,50,148,111,215,8,66,107,196,53,204,39,15,19,133,62,147,86,150,172,111,33,8,67,225,224,185,49,190,27,196,94,83,22,77,185,
224,108,165,46,66,111,210,101,169,135,204,245,33,243,56,67,83,170,33,140,53,232,165,41,228,132,230,31,165,201,171,201,54,246,185,165,27,43,244,188,252,226,17,19,22,194,108,88,215,11,19,243,175,211,54,
109,72,165,40,222,65,44,130,235,225,117,162,165,244,131,254,80,219,16,139,210,151,15,136,77,140,135,131,199,48,132,201,143,167,244,72,154,66,12,92,210,136,75,16,226,67,100,33,18,46,124,46,92,94,218,82,
148,185,231,60,231,255,0,165,46,193,162,16,248,93,94,50,151,39,85,98,158,79,44,132,225,140,88,187,71,204,240,84,84,84,125,27,133,43,43,60,236,31,170,9,115,74,94,102,165,141,227,200,76,88,247,238,77,89,
8,77,185,9,151,139,205,63,54,16,162,101,41,74,81,121,98,65,34,116,208,158,95,3,116,92,76,67,16,144,209,249,183,193,244,94,15,171,31,162,210,19,41,8,152,144,252,51,234,31,211,243,42,40,233,25,50,250,41,
68,198,241,226,112,78,242,190,50,120,187,58,92,74,201,196,202,94,22,82,235,240,38,95,241,10,191,6,155,253,38,83,200,255,0,250,84,84,87,144,144,79,193,114,151,33,61,211,208,202,93,131,69,240,255,0,148,
121,11,253,17,47,117,203,144,132,60,21,20,243,220,26,22,62,124,236,33,9,203,87,72,36,202,34,68,32,158,124,198,45,47,130,136,153,125,11,137,218,199,149,20,242,121,35,33,8,78,110,193,139,233,54,21,21,20,
172,172,243,144,132,226,240,197,228,132,18,214,209,74,138,86,121,35,35,35,33,11,50,116,199,236,165,47,16,131,18,32,202,82,234,68,17,74,92,67,17,49,174,19,203,140,132,18,230,151,87,75,41,70,250,133,16,
101,241,194,68,198,60,132,26,22,44,130,99,16,160,163,216,36,49,61,154,158,209,50,227,93,80,67,203,35,255,0,72,37,221,234,55,136,143,210,151,133,171,41,247,103,23,82,108,141,115,73,140,156,67,225,68,147,
25,63,250,121,124,71,147,203,235,18,72,176,183,37,25,231,252,35,210,16,248,66,252,45,66,18,120,137,125,45,249,181,23,110,82,151,127,9,144,156,92,242,65,180,143,248,69,100,127,172,255,0,166,36,191,195,
193,74,94,239,83,124,34,148,165,213,159,11,151,105,74,82,240,138,53,235,153,227,41,70,207,56,241,113,74,55,139,137,197,47,109,162,10,207,36,255,0,164,18,66,240,82,151,211,74,121,35,120,74,13,136,152,248,
154,184,132,39,12,248,92,86,207,36,100,34,33,50,123,82,199,238,92,81,188,165,217,144,132,32,242,49,33,134,160,137,143,32,209,15,140,180,75,41,74,92,55,179,32,209,251,144,72,104,155,9,176,132,16,144,208,
223,129,228,196,203,147,152,124,40,139,11,136,121,7,235,155,228,153,244,62,105,30,18,157,221,165,223,34,243,148,175,102,33,8,89,148,201,7,158,73,227,169,169,204,126,54,100,38,54,144,208,143,194,132,207,
36,255,0,68,163,196,36,92,126,144,139,89,50,8,112,241,190,104,219,43,127,79,1,178,158,114,158,121,153,15,59,240,167,131,199,227,41,228,255,0,225,88,167,245,158,4,230,147,220,242,148,132,39,182,16,152,
178,234,101,163,234,148,163,101,207,152,173,237,216,124,17,79,210,148,72,132,244,44,101,71,252,35,201,31,250,66,19,95,23,138,82,148,250,66,98,31,10,52,27,49,34,255,0,130,201,136,132,26,32,144,209,224,
240,84,64,212,98,182,121,32,146,199,183,47,241,81,250,167,169,33,161,16,132,225,178,140,75,17,112,159,108,130,46,204,121,61,43,94,46,252,137,30,70,222,44,69,62,141,35,198,188,79,88,132,65,16,249,139,198,
66,196,95,5,200,36,52,45,250,78,33,10,84,83,201,4,144,216,222,47,25,8,62,238,92,188,44,94,70,188,107,27,38,193,16,153,56,132,44,31,159,37,68,159,242,81,79,195,255,0,162,55,245,144,68,78,174,66,33,228,
38,94,28,41,88,144,144,208,182,9,105,33,9,176,139,60,21,23,31,252,21,146,144,132,72,241,149,23,47,48,248,85,234,154,182,30,61,179,105,118,237,244,62,124,145,144,136,176,98,136,132,32,197,225,120,75,131,
226,148,110,22,252,60,145,144,139,187,205,41,69,176,154,166,81,177,188,131,68,162,67,80,162,37,16,121,80,215,240,191,240,109,178,54,78,4,202,81,113,8,52,65,101,254,25,236,157,36,124,197,232,82,137,193,
60,186,152,217,118,12,75,25,114,228,18,39,141,67,121,7,171,30,174,13,98,22,180,36,35,224,220,196,98,94,70,249,66,209,243,72,217,243,104,200,65,33,65,248,62,139,97,8,120,27,69,217,250,124,196,181,44,98,
126,70,207,15,195,244,157,81,189,143,99,100,25,74,225,228,132,71,255,0,11,87,43,30,50,8,109,19,248,83,35,18,127,233,255,0,88,240,38,174,102,81,177,178,137,148,135,132,65,70,197,240,165,62,237,41,114,99,
98,126,54,151,105,80,197,108,140,152,158,152,76,103,206,166,44,132,33,8,120,199,232,74,177,253,254,58,61,130,238,17,107,101,41,255,0,66,16,190,231,145,15,254,30,10,178,137,40,52,191,4,187,242,121,32,145,
61,237,159,72,36,65,56,55,74,93,158,7,224,242,70,66,98,13,65,6,224,219,100,242,124,47,252,40,172,119,253,33,49,35,225,114,148,101,46,82,148,165,226,123,24,223,111,87,161,188,140,72,152,216,222,164,78,
89,9,137,141,226,18,39,18,46,54,87,179,154,94,83,240,54,92,88,199,194,22,151,20,111,22,147,19,218,55,176,92,44,240,126,30,90,245,50,228,62,12,78,144,165,103,150,66,100,32,190,18,8,163,120,153,74,63,162,
244,66,17,107,164,17,73,169,236,32,188,60,123,6,84,63,240,138,200,197,127,162,82,22,45,165,27,41,114,16,240,82,141,158,70,35,255,0,153,74,74,66,16,67,67,226,98,41,70,47,250,33,72,120,234,9,113,125,247,
39,240,79,226,165,197,175,209,70,239,113,144,132,71,194,148,36,242,138,158,16,109,176,232,208,121,74,121,33,61,207,25,117,237,229,148,162,228,33,224,153,24,110,158,74,194,91,224,240,55,228,71,140,189,
66,98,113,125,13,139,211,9,211,213,232,72,156,50,16,130,68,31,23,201,114,16,132,202,38,32,252,141,19,87,48,92,60,185,7,179,17,71,72,246,205,250,60,76,165,108,140,74,231,194,69,69,203,144,132,213,144,104,
79,111,129,249,66,230,8,240,81,51,199,193,125,198,46,223,156,165,46,44,136,179,136,66,19,103,250,65,120,200,77,131,196,245,188,79,240,252,18,42,67,81,182,255,0,4,156,38,93,121,74,92,132,33,8,82,228,46,
194,16,132,229,137,159,121,132,201,140,132,98,247,193,250,90,212,78,96,189,11,215,243,222,215,63,130,227,243,143,5,88,108,165,62,225,34,18,137,63,70,255,0,194,242,197,231,111,170,148,243,218,33,61,16,
154,153,26,86,121,35,38,82,226,148,163,101,37,18,214,33,144,140,132,214,54,82,226,225,236,38,77,130,68,207,5,69,69,40,217,71,233,132,226,19,110,82,141,229,200,76,67,137,13,151,147,30,177,20,108,185,124,
11,99,35,18,34,215,168,111,16,185,89,74,62,40,217,89,228,130,65,161,42,124,46,36,69,143,238,202,53,24,138,92,156,198,200,67,225,89,248,61,158,137,144,132,218,82,8,156,126,101,203,138,147,134,203,194,243,
145,112,172,47,244,200,66,198,78,88,132,39,13,149,241,70,200,65,16,157,210,226,126,59,132,212,49,177,58,134,61,68,244,189,156,82,228,223,193,136,153,127,149,144,132,223,164,218,87,158,79,56,145,8,66,122,
97,8,65,4,166,65,161,184,58,70,36,66,15,198,27,23,130,231,130,151,143,57,9,143,153,204,60,13,173,88,158,185,50,82,98,17,11,159,36,33,8,52,121,8,36,120,24,139,22,34,149,23,21,158,72,241,4,184,132,244,210,
141,177,187,196,34,132,32,215,108,75,27,40,137,233,132,26,196,81,107,236,252,116,199,168,136,68,25,49,139,195,25,115,50,148,156,206,46,204,75,17,74,63,36,202,92,98,207,131,242,46,46,44,69,24,181,227,234,
16,132,25,74,51,201,8,65,248,47,52,108,167,146,54,78,39,16,155,97,81,79,44,143,244,79,36,226,151,135,147,171,180,167,220,135,231,23,86,86,92,253,202,44,156,190,38,62,99,213,205,201,213,46,181,176,140,
135,205,103,209,255,0,28,217,158,15,3,101,103,146,19,153,197,41,123,187,8,120,41,225,13,50,255,0,201,91,16,178,148,105,191,163,72,75,244,111,207,252,19,186,145,9,159,188,94,38,178,172,82,178,55,245,144,
154,137,149,149,234,216,200,36,124,230,99,241,138,23,38,154,46,121,35,33,8,78,110,194,19,138,82,247,8,65,34,100,19,209,75,136,69,27,197,21,33,6,178,148,111,132,61,157,27,148,38,37,139,44,16,130,151,86,
174,105,113,226,203,147,110,120,27,83,21,237,41,113,46,146,26,24,145,6,49,101,19,24,153,81,70,196,202,33,106,69,69,40,200,65,162,101,41,89,51,193,124,158,72,200,78,209,8,120,42,27,45,18,108,175,214,68,
132,132,95,136,105,226,76,83,233,8,60,47,153,57,163,101,202,94,32,188,31,164,230,243,8,66,99,17,243,231,94,11,148,165,24,134,181,101,47,13,228,234,148,165,41,75,75,139,103,23,169,221,218,82,191,240,242,
200,66,19,135,139,39,83,87,51,30,65,35,225,48,212,216,70,36,76,111,41,41,240,93,76,184,196,70,43,43,60,147,253,34,244,66,103,131,193,244,93,23,31,55,60,17,54,52,16,137,175,15,152,196,82,241,8,124,17,116,
108,242,70,70,78,4,38,204,101,27,27,60,137,251,20,184,184,150,38,81,178,247,240,123,114,151,79,47,60,106,126,79,164,243,212,244,94,30,47,68,229,15,32,108,185,50,236,164,18,24,138,48,159,158,90,22,249,
35,33,6,166,44,79,124,245,81,127,225,228,132,33,56,165,163,223,59,74,136,197,103,146,8,36,38,60,76,129,211,45,19,19,41,121,165,41,121,153,59,185,122,94,23,161,62,31,115,83,201,233,191,231,13,109,203,137,
98,91,251,148,188,174,38,82,162,158,79,36,38,94,230,223,115,226,136,188,82,148,165,29,98,94,60,144,133,16,163,116,158,113,241,4,54,39,151,89,79,254,231,156,132,244,77,167,129,180,92,92,66,69,152,165,41,
70,202,82,237,88,111,254,228,29,26,9,148,108,164,35,70,241,247,115,201,4,65,34,30,50,237,40,223,23,40,252,144,138,13,248,233,107,101,41,120,93,164,65,174,16,202,50,247,124,99,68,38,44,127,120,67,250,65,
193,227,226,113,75,176,107,23,19,134,121,18,120,240,120,25,4,136,67,225,114,98,62,135,224,187,79,188,124,46,81,188,73,178,9,11,139,15,248,21,100,39,20,185,56,248,34,162,148,172,242,201,136,18,153,11,10,
87,254,30,72,199,16,207,63,233,88,149,98,44,132,196,200,41,70,242,19,137,175,138,93,132,36,234,149,142,158,73,239,163,196,242,245,118,116,252,57,144,152,139,197,19,243,196,219,148,165,111,33,23,165,101,
68,98,148,162,126,223,60,54,92,165,216,76,124,92,140,132,214,226,45,99,248,81,54,34,164,82,179,203,33,50,148,165,244,81,188,165,216,65,98,99,243,144,132,33,8,68,68,120,41,247,31,73,148,110,188,68,100,
26,33,243,38,32,152,203,148,185,74,92,82,172,122,251,127,114,143,87,165,46,160,145,96,216,249,125,194,11,135,204,212,82,148,125,38,60,164,26,215,220,229,226,212,182,98,44,108,162,126,51,247,46,220,88,
136,66,100,198,209,228,76,64,230,216,38,93,112,45,101,196,178,158,72,200,120,60,178,101,80,81,227,33,9,159,126,141,151,41,231,90,33,49,16,139,24,188,184,56,32,214,66,117,75,196,33,9,139,138,138,138,83,
201,56,121,229,106,223,25,75,137,221,250,121,196,198,34,139,203,217,179,89,49,227,218,81,182,81,121,99,80,75,200,188,144,171,253,41,114,100,62,23,215,240,167,146,103,131,199,170,241,115,206,204,156,121,
23,16,143,19,111,12,55,68,63,70,147,18,17,23,30,79,36,200,66,117,74,70,70,76,240,84,93,111,86,210,237,46,65,162,16,136,136,132,38,18,35,200,130,74,158,17,114,228,164,219,148,188,94,33,9,221,225,49,178,
159,70,167,11,139,168,157,172,187,74,93,107,182,44,165,201,140,79,184,120,25,8,76,95,50,98,13,106,38,190,33,56,187,70,202,125,26,218,81,187,151,106,46,44,101,41,91,18,18,112,218,24,242,241,9,175,134,242,
54,70,36,65,158,89,15,8,127,224,166,120,32,188,60,165,43,25,249,147,73,172,250,71,140,94,11,199,194,209,177,121,33,4,135,196,234,19,63,250,85,139,72,201,255,0,113,9,199,145,101,216,76,163,39,16,72,136,
49,19,92,152,252,45,79,76,217,72,76,132,34,60,38,127,240,54,197,255,0,9,137,173,139,239,23,152,53,220,33,224,240,82,158,73,233,83,182,179,206,254,108,26,218,134,203,148,184,242,241,15,130,242,201,140,
185,56,185,75,236,121,50,158,94,121,196,153,9,147,154,92,165,216,202,202,43,32,177,229,212,248,165,202,92,185,74,121,60,148,165,218,82,137,141,151,201,113,191,5,16,208,253,107,211,8,49,174,16,223,115,
97,53,234,226,151,105,73,141,121,18,238,9,13,136,107,41,68,61,89,74,92,98,62,180,189,63,184,177,179,235,18,153,74,49,69,108,130,19,27,47,145,60,124,168,176,232,77,139,201,225,13,63,6,227,172,132,36,201,
232,186,249,67,198,34,16,121,113,121,26,223,171,133,205,43,35,100,33,243,41,122,92,194,49,44,121,97,127,225,95,248,54,233,68,252,121,238,30,53,49,120,41,75,220,207,5,67,127,224,175,248,70,201,227,26,38,
66,16,165,25,8,137,148,67,135,140,110,13,139,30,67,193,227,60,158,73,136,165,46,210,251,47,116,184,108,165,46,44,132,18,91,244,75,60,177,255,0,193,120,66,242,125,63,69,252,23,135,158,72,77,66,201,151,
46,66,14,34,162,226,202,82,19,60,22,97,50,141,139,139,204,216,65,147,22,191,5,41,74,84,82,237,46,167,227,31,173,9,81,243,69,247,95,181,229,230,19,47,158,168,223,31,163,121,113,236,62,9,225,50,243,74,54,
38,55,238,132,241,221,62,137,12,111,32,146,197,53,177,134,241,23,104,235,33,50,49,91,16,145,8,40,65,226,226,19,139,138,198,121,39,11,31,4,200,78,18,131,63,53,172,153,127,230,66,47,74,245,82,148,175,252,
43,35,16,130,204,132,27,140,167,130,10,252,66,175,193,39,250,69,254,158,7,137,79,91,99,33,15,28,177,139,232,178,226,111,168,39,224,251,148,104,240,138,38,82,239,130,246,145,56,190,143,5,71,151,13,137,
237,60,139,152,65,113,81,70,41,91,199,228,248,46,47,30,61,23,183,137,148,74,253,60,38,40,92,92,72,153,74,207,56,146,100,88,217,120,135,156,132,17,117,99,16,182,16,104,132,32,145,8,66,99,243,143,219,125,
15,132,95,5,238,148,111,213,56,155,70,41,120,91,8,66,19,150,197,203,99,243,176,132,227,243,212,184,154,132,135,183,99,32,146,131,45,18,218,82,142,48,252,236,30,81,114,252,141,16,66,114,200,65,120,42,133,
43,41,115,206,81,60,156,195,225,69,19,23,205,121,124,13,161,62,25,125,147,150,62,17,8,65,65,180,83,205,41,74,167,209,63,240,242,121,26,196,170,26,75,240,165,46,47,36,207,190,138,54,136,43,127,132,100,
127,238,34,38,126,140,153,57,92,76,111,89,79,205,191,199,74,82,139,136,47,5,27,27,213,173,21,63,185,123,165,67,203,72,36,136,76,251,190,15,210,16,132,201,141,151,215,63,210,16,153,117,109,47,12,35,230,
37,147,29,63,117,162,17,147,18,108,90,32,146,60,17,105,178,148,163,101,197,20,172,165,27,254,119,224,98,254,40,66,115,74,86,121,200,37,210,242,77,165,41,74,121,201,179,124,76,120,177,240,182,19,150,47,
162,250,68,68,56,37,70,166,177,112,182,148,153,3,126,10,121,199,137,54,47,244,53,50,121,24,135,230,66,16,156,79,3,198,81,54,121,38,47,187,249,151,165,234,33,146,225,99,213,244,157,53,176,158,169,203,88,
161,22,84,91,181,158,72,223,215,136,145,74,78,15,194,19,84,22,144,179,136,63,248,43,252,60,132,223,172,130,46,111,178,137,144,95,248,121,103,255,0,68,126,145,107,217,252,12,111,110,126,241,75,228,187,
25,56,88,211,250,241,30,17,74,121,62,228,38,52,120,207,44,132,33,248,33,164,144,165,26,17,72,31,248,41,70,238,47,69,25,75,195,22,92,165,27,43,214,137,158,113,16,75,32,214,210,141,228,62,8,82,151,213,248,
120,42,210,139,252,137,82,106,30,37,151,41,75,213,201,159,130,251,212,38,33,8,65,44,51,6,154,18,196,152,188,143,192,207,162,68,38,188,88,253,175,135,196,36,18,44,250,63,39,194,145,178,99,88,248,76,165,
23,129,143,44,156,194,10,211,203,32,242,248,27,242,33,162,159,113,173,165,43,255,0,8,217,8,143,133,16,254,226,242,53,220,26,18,60,15,153,183,200,232,150,65,137,222,102,60,189,55,11,75,227,225,228,152,
184,171,253,42,60,136,217,255,0,76,130,16,81,113,71,228,73,175,195,201,74,93,253,198,242,11,24,159,130,183,241,17,191,172,73,34,151,139,205,41,68,203,183,42,45,60,159,244,37,25,111,55,95,52,94,230,70,
36,65,34,115,57,75,133,141,182,85,10,138,83,201,27,33,4,143,131,34,60,20,167,146,148,180,127,74,160,202,38,198,54,82,178,54,65,122,33,49,120,31,20,167,146,114,182,17,9,164,54,38,82,179,206,188,140,132,
195,80,80,100,196,202,82,148,188,10,202,203,168,53,61,169,16,107,38,60,69,30,166,125,39,147,240,185,8,76,189,188,184,217,9,204,33,8,178,150,151,206,124,41,114,159,117,60,121,75,136,163,124,77,93,46,214,
60,125,30,67,71,198,85,137,141,168,92,91,8,33,23,238,194,16,153,5,247,135,172,78,33,58,137,71,224,167,146,19,23,107,171,233,245,122,172,45,219,143,224,234,22,39,207,198,81,58,82,148,252,201,148,167,156,
159,244,116,37,56,109,20,175,241,30,95,232,217,254,158,7,193,249,62,23,90,98,227,193,70,202,38,120,46,44,163,126,75,88,155,40,217,124,27,115,206,39,249,211,212,62,41,86,45,252,60,147,254,147,201,17,224,
165,27,24,159,136,86,181,249,38,126,117,61,87,27,41,224,241,233,89,49,34,50,50,30,63,210,175,194,191,197,136,255,0,211,205,82,48,132,219,134,207,34,76,132,196,37,192,250,63,239,9,65,215,240,136,241,248,
82,188,95,71,205,41,120,184,177,161,137,237,212,88,82,146,136,52,136,120,40,217,229,144,155,86,27,27,185,71,200,41,125,9,11,15,136,78,166,124,12,98,229,114,133,140,98,123,71,204,200,76,76,93,82,151,70,
241,137,151,13,243,75,136,165,46,76,243,146,144,95,79,3,225,121,226,103,221,158,49,101,202,55,68,202,82,149,244,241,162,31,16,222,164,32,215,131,224,158,40,196,148,207,210,116,156,30,16,158,124,143,60,
20,185,7,183,132,201,171,60,254,241,9,147,104,252,158,117,112,252,139,202,130,240,33,224,169,99,203,26,38,194,101,69,255,0,133,103,159,244,155,53,179,207,30,63,6,82,140,38,207,39,151,136,37,171,17,74,
190,179,235,196,22,69,159,128,223,163,199,250,66,41,252,71,150,127,244,68,68,62,166,34,17,31,163,22,180,85,70,197,210,124,92,242,78,26,164,33,54,107,66,68,34,95,89,83,247,47,254,75,252,67,10,255,0,210,
17,113,54,137,151,31,54,105,248,36,120,8,60,22,138,175,145,164,246,33,178,255,0,204,159,247,46,124,69,200,66,17,103,130,231,150,124,22,36,70,70,71,132,62,20,97,178,229,40,240,190,244,124,40,220,222,23,
31,6,242,15,216,199,137,136,132,200,66,109,215,180,165,46,66,100,32,203,197,41,114,151,210,181,11,15,46,60,79,150,38,53,148,121,5,60,189,32,145,16,212,88,248,154,183,224,79,23,130,151,11,179,46,193,73,
143,135,180,111,101,18,84,99,63,232,255,0,131,234,230,179,203,200,72,82,241,250,120,131,127,224,138,82,227,203,10,48,152,139,158,120,242,127,251,205,41,74,87,171,46,67,199,251,137,16,132,33,38,82,148,
162,99,85,30,97,244,98,68,218,74,66,121,23,157,240,139,158,72,134,145,23,19,215,74,55,197,43,98,13,52,33,121,215,194,247,212,54,88,127,240,121,60,255,0,167,253,16,156,173,165,41,79,187,24,255,0,232,137,
107,103,150,65,33,248,19,46,199,250,66,40,69,141,145,178,119,81,74,87,204,33,48,191,238,92,185,8,65,97,8,68,68,34,148,108,184,98,141,148,98,255,0,18,84,126,5,243,31,131,238,164,50,127,2,237,62,23,178,
228,19,68,218,84,54,82,239,238,60,91,56,156,50,115,75,196,32,214,45,131,16,159,130,228,100,127,164,34,44,202,49,50,141,227,198,37,123,148,144,88,190,13,140,66,40,222,27,60,144,156,39,196,18,38,60,166,
73,199,154,53,141,210,227,216,65,227,104,168,250,71,248,143,48,140,155,117,30,10,178,8,130,88,185,124,193,34,19,151,141,148,167,210,11,42,42,199,252,35,203,196,200,121,13,64,170,15,255,0,249,46,249,212,
135,150,12,233,244,132,71,129,241,243,132,55,204,226,228,33,60,8,153,6,226,46,46,23,221,126,24,153,125,20,242,121,35,255,0,73,254,177,195,198,44,165,41,79,59,56,123,50,139,225,70,203,169,98,68,33,171,
35,23,134,39,132,202,94,33,124,149,20,172,255,0,244,132,33,48,132,211,193,224,191,240,109,146,144,101,41,79,39,145,223,244,77,240,40,223,146,148,98,177,70,202,203,252,51,143,131,242,82,190,105,75,252,
11,181,194,223,220,165,202,94,82,18,200,53,10,59,158,72,79,114,229,16,101,62,141,9,81,176,178,226,246,90,65,16,248,49,226,203,166,160,161,224,149,19,16,133,41,113,60,95,72,22,81,6,144,202,121,98,162,36,
65,236,37,26,17,248,33,136,79,24,177,66,60,178,121,38,59,194,17,50,162,15,248,71,150,68,120,255,0,49,20,163,99,125,164,67,255,0,135,156,77,53,203,33,8,77,155,51,192,225,117,105,4,120,40,199,147,233,8,
68,33,12,132,223,36,121,18,98,98,19,42,27,63,250,40,54,135,229,136,252,46,191,227,86,227,104,108,165,100,116,73,255,0,162,75,154,66,107,17,75,233,175,89,240,165,60,245,30,66,16,152,144,136,240,60,132,
62,115,15,193,13,121,18,196,252,114,138,54,82,148,104,83,102,66,19,17,115,240,187,244,176,163,104,163,106,148,240,44,165,101,103,146,178,248,60,143,105,75,151,209,57,72,132,219,238,157,194,16,132,245,
207,67,41,117,241,74,81,50,225,174,194,116,201,194,100,38,174,169,70,132,51,224,235,38,65,178,148,164,184,132,241,151,152,33,174,20,165,43,226,60,140,140,75,17,118,231,255,0,15,44,135,238,88,63,188,82,
148,107,192,208,158,60,69,67,106,228,17,251,140,163,215,137,249,27,127,136,141,253,33,115,82,27,46,66,16,131,89,227,30,33,243,24,188,8,110,104,158,181,205,46,194,19,34,226,100,39,75,136,52,58,20,17,20,
108,84,152,154,66,16,242,137,239,185,10,36,199,150,36,52,159,210,33,227,62,184,132,18,69,242,120,41,119,198,207,225,132,244,217,134,196,202,47,59,255,0,238,194,127,184,252,226,36,33,187,151,41,74,121,
201,137,100,17,227,60,109,41,74,50,101,213,154,46,42,42,42,42,63,250,40,168,168,168,163,120,164,196,174,26,241,238,69,124,66,16,132,39,170,113,9,196,201,234,99,16,186,240,50,151,162,19,248,94,166,61,79,
152,77,75,42,41,113,89,231,18,98,97,109,200,47,3,225,137,159,118,113,53,8,95,16,185,49,189,92,33,63,193,139,231,62,50,248,226,99,135,198,82,149,178,49,47,36,92,121,226,163,200,147,100,255,0,167,140,67,
198,248,132,200,88,82,141,179,201,8,66,33,139,230,54,55,69,137,72,34,38,210,236,33,54,250,91,197,141,148,162,45,226,235,98,243,199,130,162,162,149,20,108,67,19,252,254,22,69,168,109,228,38,220,165,17,
15,206,23,220,184,184,158,184,78,169,74,55,233,243,147,137,255,0,120,83,152,36,53,138,36,33,224,165,23,145,237,41,74,82,145,189,23,248,146,203,170,55,235,132,200,79,84,230,100,33,8,77,186,143,9,15,239,
177,226,226,158,72,200,66,108,18,31,129,86,76,190,169,218,198,181,121,214,82,158,88,211,43,89,50,9,17,30,6,242,11,224,196,241,227,238,98,68,38,61,140,248,92,92,242,121,217,169,136,165,199,224,67,241,203,
100,98,177,167,137,17,33,253,230,148,185,51,193,113,67,192,232,242,201,215,146,98,68,33,8,72,120,27,47,252,35,121,227,253,28,23,129,185,240,77,209,49,180,82,179,200,185,167,146,226,127,243,138,139,120,
92,204,68,120,40,249,152,74,61,130,17,13,17,229,145,136,132,218,94,161,8,201,6,214,27,127,135,147,206,194,151,134,35,244,253,226,148,109,204,89,8,78,33,240,250,60,191,226,207,3,101,212,136,120,226,151,
168,66,108,203,232,152,66,17,16,135,141,242,121,255,0,73,221,152,105,138,82,250,255,0,61,87,111,23,136,36,66,19,215,50,19,102,194,122,41,114,19,30,61,126,169,148,94,89,61,23,40,216,217,102,144,126,136,
37,233,67,38,39,175,22,55,72,76,153,74,125,39,15,248,196,50,151,63,125,41,127,164,73,142,65,121,198,199,89,8,76,76,135,198,38,120,104,104,253,40,190,15,26,62,20,165,60,146,225,47,36,75,23,193,253,198,
51,206,37,203,31,252,60,178,11,21,198,84,92,132,238,151,97,50,111,239,195,207,226,41,253,99,68,79,35,39,244,83,153,204,18,27,230,111,142,168,153,74,55,25,251,194,27,67,121,68,245,191,240,242,196,18,207,
5,41,99,43,47,143,130,200,57,254,144,127,242,127,202,195,203,244,255,0,182,66,33,9,145,30,50,116,249,132,203,203,198,198,83,206,205,132,39,119,254,30,79,36,39,16,75,143,28,210,172,83,243,206,210,158,74,
242,103,130,148,165,41,75,17,112,197,47,246,194,16,132,201,147,41,127,142,151,223,125,107,138,94,23,223,92,24,209,4,196,224,252,241,61,244,186,190,118,165,225,159,68,163,198,60,137,113,6,185,108,165,47,
41,81,161,31,135,221,162,126,113,74,83,235,62,107,17,116,242,70,36,53,53,101,219,136,156,81,124,229,39,48,208,161,75,74,92,94,118,229,46,71,136,79,79,194,8,27,35,99,80,92,88,82,179,200,184,75,91,240,38,
54,86,81,61,24,167,210,16,156,44,163,98,172,72,240,134,203,227,23,134,35,198,65,194,150,150,13,159,135,151,214,37,254,249,25,224,152,222,207,226,68,33,17,224,111,9,182,52,219,33,50,123,97,5,233,165,203,
180,185,114,16,106,30,69,221,63,11,163,209,70,239,182,19,221,8,66,16,241,173,229,41,114,151,97,8,66,19,153,168,127,122,165,47,51,180,94,223,162,100,38,82,246,184,122,134,82,250,238,82,237,200,66,11,97,
9,171,91,197,137,23,63,6,136,208,134,241,144,132,26,38,46,41,250,60,184,180,108,242,200,200,200,66,34,13,16,140,131,68,18,71,131,244,108,79,200,252,158,2,101,41,120,135,232,153,75,196,33,225,20,172,165,
164,38,120,47,248,143,255,0,15,39,150,66,98,34,47,76,38,54,47,44,240,240,37,68,145,225,97,249,22,94,167,20,163,103,157,131,68,200,36,68,120,46,177,124,41,70,130,101,41,89,228,143,13,36,36,124,197,45,18,
33,82,62,145,126,158,76,111,207,129,125,238,115,121,131,226,148,188,171,248,81,39,233,244,75,87,175,206,78,38,206,161,224,165,230,98,100,38,66,16,153,74,134,241,113,69,41,125,237,112,109,122,167,84,165,
202,94,102,76,94,182,95,224,95,121,100,39,48,132,200,53,139,136,76,190,148,52,36,53,61,16,132,196,202,95,68,33,8,53,159,156,82,151,136,76,132,33,240,186,158,81,189,67,202,81,177,61,188,253,26,27,18,18,
34,98,80,121,15,205,72,127,113,177,137,239,224,201,135,244,136,126,56,163,116,186,137,147,97,16,145,60,30,10,138,121,60,132,135,131,192,216,187,187,74,82,7,133,27,41,244,147,17,224,108,165,47,145,101,
40,158,45,46,76,131,66,69,119,62,139,18,233,188,242,37,136,33,17,227,252,225,189,151,120,176,143,3,242,240,87,250,40,120,24,253,242,63,36,244,50,158,10,138,82,148,165,108,140,130,68,31,255,0,70,47,248,
134,194,79,253,26,255,0,162,152,134,250,94,10,93,132,234,148,185,118,148,165,103,146,16,132,33,8,66,9,101,69,41,74,60,30,23,83,13,150,80,72,32,212,126,166,167,63,155,8,78,161,8,78,97,50,19,133,213,254,
41,196,245,220,124,210,235,234,246,135,194,40,188,15,133,244,114,15,105,117,113,8,79,74,124,194,101,41,70,202,82,151,33,75,233,116,140,140,74,53,15,193,177,49,106,41,105,5,148,188,125,225,178,144,107,
23,204,167,138,54,161,99,24,153,247,38,121,216,52,39,221,60,145,191,164,82,9,196,25,57,110,12,39,113,178,149,148,242,121,33,9,183,24,190,113,5,247,135,144,130,93,205,75,152,77,94,120,135,142,37,39,253,
27,72,165,18,33,150,197,43,35,35,33,8,76,103,207,27,57,163,123,8,36,65,104,242,51,255,0,162,35,192,134,54,121,35,35,255,0,113,31,164,89,79,47,88,169,9,183,20,242,66,109,41,127,230,206,33,9,181,23,71,170,
148,172,172,188,169,124,138,76,99,72,75,198,183,235,156,55,151,136,66,9,19,185,151,217,59,190,216,62,169,120,165,46,82,237,41,74,82,151,222,249,67,31,47,211,5,232,163,120,149,210,72,123,113,161,34,12,
155,75,218,19,134,198,202,88,86,198,65,45,188,210,148,91,74,81,132,217,41,53,137,151,22,49,12,76,180,68,198,177,103,232,158,203,136,34,244,189,75,46,29,100,62,20,140,153,50,241,6,132,181,30,24,208,241,
30,11,176,190,239,222,41,79,162,156,94,124,144,132,68,66,72,132,196,18,18,33,6,210,27,255,0,165,27,60,222,106,35,240,242,200,255,0,210,36,66,16,240,120,47,252,43,43,37,33,6,61,242,66,30,79,36,234,144,
39,114,190,39,53,255,0,135,150,66,16,132,38,144,132,226,161,166,149,251,220,88,217,70,131,174,39,16,132,33,8,76,132,33,9,213,214,189,19,211,75,207,211,231,170,148,185,118,148,188,82,244,253,51,213,75,
194,99,225,60,189,205,155,54,148,165,27,46,82,240,163,71,193,226,13,121,31,80,65,172,132,38,76,167,147,207,51,13,77,165,46,92,131,89,249,194,35,18,98,88,147,41,70,66,19,17,120,132,216,50,16,242,65,114,
153,49,177,139,68,191,221,165,40,248,153,8,176,145,16,196,201,197,212,60,76,99,225,46,23,241,182,121,98,80,185,227,133,211,130,197,158,10,150,24,109,177,175,250,65,162,53,228,75,103,250,66,113,75,205,
42,35,20,189,182,93,163,207,36,100,18,22,92,109,20,242,70,36,196,136,71,164,68,66,162,250,3,208,220,172,183,221,51,198,168,67,255,0,5,45,39,80,132,225,243,61,19,103,116,111,168,36,66,19,38,66,98,29,212,
203,205,226,151,152,66,16,158,137,194,249,232,93,63,90,39,20,164,254,25,202,99,124,38,94,232,144,110,250,41,71,178,137,107,116,121,5,228,52,68,52,37,137,134,133,224,184,184,165,184,207,37,18,198,47,71,
232,207,34,38,205,168,162,198,43,32,189,151,47,107,147,111,145,107,71,194,147,250,40,216,222,39,254,100,235,231,21,139,47,252,41,89,89,228,140,135,193,86,76,169,14,252,36,38,223,209,86,127,206,31,16,155,
30,145,99,87,19,124,30,10,121,35,62,252,19,189,226,88,109,95,3,40,238,66,16,130,240,197,54,143,7,193,175,47,44,111,240,165,47,170,15,215,9,196,39,177,250,47,87,209,61,116,185,75,237,189,66,16,75,63,61,
175,212,245,118,249,79,155,49,122,132,38,223,106,231,196,198,44,253,245,78,90,39,43,41,110,49,101,214,33,178,233,162,109,41,68,137,144,132,242,44,112,120,196,137,232,163,20,250,65,163,193,74,82,143,46,
82,242,249,101,200,45,163,121,113,140,91,70,252,204,92,175,251,140,87,159,223,77,27,41,74,103,255,0,100,162,79,68,201,144,132,218,34,50,50,13,165,138,216,168,177,136,190,6,174,177,253,200,66,16,103,210,
17,30,61,14,145,225,107,73,254,145,65,40,121,24,222,42,204,17,16,153,8,138,138,180,121,48,217,127,130,99,226,16,132,216,200,66,46,225,61,112,154,242,117,61,45,113,71,168,123,123,191,192,189,51,221,54,
123,231,80,158,133,245,164,63,24,187,162,225,250,33,49,139,139,144,156,164,52,78,214,50,16,132,16,198,174,38,254,105,75,224,188,37,213,47,16,248,121,43,47,47,62,137,112,248,108,95,192,248,132,26,116,83,
22,76,154,189,175,132,39,138,217,60,113,74,36,249,168,168,186,153,82,26,12,86,65,47,39,130,15,159,5,111,39,146,45,81,13,255,0,204,54,219,38,210,226,37,194,194,33,36,69,148,165,40,178,168,49,178,139,11,
148,124,146,148,190,153,141,40,126,106,104,125,66,16,132,244,121,234,122,33,8,78,211,62,255,0,5,237,34,50,16,156,66,127,224,66,122,175,15,221,74,76,155,74,93,72,104,126,133,203,234,242,184,188,65,168,
66,136,124,81,178,220,95,49,174,19,198,62,23,73,15,94,70,74,49,50,113,70,197,233,162,101,219,89,248,92,104,248,94,23,210,249,23,145,46,167,250,120,75,186,81,62,38,193,149,241,9,139,23,43,31,240,35,255,
0,193,20,110,21,178,49,98,148,111,97,9,203,98,33,8,222,144,68,120,252,33,9,168,132,26,200,207,35,255,0,162,33,33,46,41,116,99,200,108,165,41,88,163,227,192,108,87,238,68,108,240,93,50,122,39,23,33,4,166,
94,33,57,158,139,143,151,213,29,46,60,165,41,74,92,188,193,33,56,55,74,83,207,41,164,125,226,127,67,197,151,24,158,61,132,38,55,252,211,164,198,199,196,230,250,214,177,112,249,250,124,225,240,197,148,
243,147,22,90,33,141,11,23,167,243,104,152,222,189,122,153,74,92,122,217,25,24,138,126,116,209,38,126,234,69,133,40,197,41,70,202,45,243,5,171,135,136,101,234,151,40,153,75,204,219,139,39,161,8,169,13,
210,228,17,61,55,39,11,193,15,7,131,255,0,135,151,250,66,13,44,69,213,205,252,10,188,72,136,101,42,67,202,137,148,172,75,255,0,192,209,43,183,154,81,191,66,84,84,64,151,146,42,63,184,245,120,62,61,116,
185,8,66,19,152,79,84,217,151,138,92,188,220,185,96,232,88,248,158,200,36,66,7,224,188,223,84,254,57,218,60,106,104,165,27,246,66,116,189,44,163,122,136,95,69,203,235,92,33,143,17,248,37,195,216,36,126,
113,117,121,63,69,141,114,139,171,134,92,92,126,235,31,8,111,252,40,145,49,17,60,100,24,159,129,139,211,6,65,120,248,36,223,2,16,131,22,54,39,180,76,95,56,67,207,27,240,108,167,131,243,23,174,9,16,101,
26,197,197,20,133,19,197,231,41,123,104,69,41,74,55,72,222,62,9,112,202,93,73,8,76,89,6,255,0,8,121,253,207,5,67,101,27,67,101,219,169,185,6,204,165,41,68,97,26,47,162,111,193,53,240,255,0,2,187,74,242,
16,223,170,16,130,68,33,61,116,190,138,82,148,163,101,41,75,196,244,66,79,123,90,156,25,102,16,98,186,132,33,9,252,208,132,38,220,123,8,77,165,229,98,31,223,68,38,165,195,244,66,19,39,170,19,91,217,151,
63,125,41,231,232,241,243,228,132,33,53,19,206,54,94,144,242,240,248,98,40,217,74,82,240,209,4,18,203,172,251,162,30,194,109,40,156,27,16,151,84,163,243,142,145,178,76,123,74,198,203,141,228,165,69,69,
186,178,98,233,178,186,94,93,32,137,172,186,145,8,45,72,188,210,236,111,9,9,31,6,207,221,79,137,149,23,103,146,99,127,130,22,13,48,222,43,93,82,148,165,46,93,159,148,36,100,212,207,188,66,16,132,33,59,
158,139,232,98,238,16,131,88,217,74,94,166,66,17,109,217,183,47,80,152,158,159,206,102,79,69,244,95,93,19,41,114,151,41,118,151,105,75,233,94,7,143,206,204,155,113,148,165,244,222,165,38,62,155,16,186,
145,55,244,154,135,171,133,203,244,33,228,202,81,53,173,122,105,75,195,31,132,54,66,49,34,16,156,209,107,35,194,83,27,47,146,220,89,240,111,33,25,4,150,209,190,26,18,217,224,252,38,194,33,172,132,153,
7,227,31,211,200,145,49,33,61,44,191,224,152,135,243,94,183,147,9,33,193,44,120,197,243,30,47,44,117,22,178,127,209,44,162,124,76,66,14,30,17,74,197,176,133,72,130,6,136,127,227,40,223,20,165,46,41,75,
202,39,210,120,184,155,195,173,159,72,77,132,33,59,132,39,162,13,122,155,200,124,62,241,74,81,178,151,215,8,61,157,66,19,211,125,48,152,159,87,41,122,189,78,175,166,140,76,191,193,61,55,110,193,248,27,
164,98,76,65,169,151,168,78,80,216,223,160,144,184,111,217,249,197,47,162,148,165,27,22,61,72,252,198,49,63,85,80,253,199,144,72,132,33,240,108,71,230,37,79,12,133,242,40,120,28,20,97,187,137,148,165,
46,41,91,46,52,200,63,11,110,31,248,19,98,92,248,202,83,201,228,135,147,238,81,187,139,238,54,39,195,103,238,124,219,194,77,81,186,33,175,35,94,8,200,78,83,30,182,95,5,45,39,253,16,252,149,68,182,19,193,
6,84,82,179,201,7,225,9,243,68,199,228,104,115,42,41,114,121,202,82,235,83,40,248,196,147,88,210,252,200,66,16,153,114,228,39,130,127,5,46,210,245,243,186,49,120,47,190,237,60,251,167,170,117,4,252,122,
103,87,47,23,41,114,227,245,207,100,201,207,231,162,12,68,16,217,74,125,18,196,240,223,105,16,72,82,226,40,253,31,68,186,132,203,172,67,196,94,150,193,241,6,136,66,101,196,34,141,146,141,11,187,176,72,
125,61,155,228,76,185,254,35,242,60,17,101,165,226,97,169,137,127,163,159,130,60,103,194,226,221,104,159,232,146,229,248,40,178,121,34,69,19,225,162,13,109,16,184,132,33,53,113,41,35,17,250,63,47,87,146,
109,40,153,74,81,89,88,147,33,8,65,34,34,249,198,120,42,90,121,23,20,251,148,186,82,194,143,130,148,165,41,74,125,26,156,127,216,212,240,70,63,49,186,201,213,41,118,18,101,41,125,55,88,191,138,243,74,
83,201,25,61,80,135,130,148,165,230,15,210,222,183,194,234,111,145,147,122,255,0,138,251,159,186,151,248,83,27,41,74,93,158,196,196,198,241,205,40,223,240,76,93,207,66,198,184,156,81,242,178,136,65,62,
27,62,137,31,53,139,134,197,68,178,19,97,49,248,15,232,145,53,9,49,45,250,201,136,153,114,19,60,147,63,114,100,33,6,138,82,158,68,155,38,45,99,68,136,75,16,132,207,130,101,233,101,19,131,116,76,186,135,
244,163,99,99,33,49,4,178,16,72,130,67,115,12,38,38,60,80,240,82,148,168,184,165,41,74,83,240,188,37,147,197,201,226,226,81,159,6,238,66,8,106,46,169,70,255,0,197,144,132,217,176,132,39,30,123,158,203,
213,203,176,132,33,6,239,174,228,39,41,47,244,124,66,40,79,84,38,47,77,200,66,114,242,117,50,16,132,254,217,151,150,137,236,158,229,142,178,123,174,174,46,49,125,225,122,238,92,104,92,49,16,132,31,20,
65,186,44,165,229,148,189,38,82,151,155,144,104,140,75,198,67,196,60,107,200,66,11,193,244,131,66,16,125,210,148,165,108,132,242,52,120,202,94,217,225,20,167,147,201,95,23,159,41,148,130,241,74,82,151,
127,114,111,130,151,41,89,88,247,243,41,74,54,82,148,165,244,95,16,78,62,62,120,16,197,244,190,33,59,188,205,153,58,186,159,130,148,190,134,82,136,165,41,75,197,202,38,54,82,241,59,165,234,19,137,151,
94,177,123,30,93,165,238,113,8,35,199,241,79,230,132,237,61,132,33,6,33,243,125,179,180,44,100,90,157,45,94,184,47,92,230,137,151,134,198,252,9,249,46,190,24,181,242,177,142,137,19,211,74,81,178,158,68,
134,44,99,240,121,18,22,55,169,12,69,45,24,158,249,218,81,119,192,218,32,167,232,215,143,84,33,4,182,161,181,143,192,188,237,27,41,251,147,207,55,209,74,92,75,41,120,168,168,165,41,74,94,33,48,208,153,
141,147,42,112,164,18,238,229,226,19,47,63,8,66,115,8,66,20,121,113,113,118,136,123,74,82,148,165,219,232,156,206,111,174,19,16,253,75,47,178,127,21,254,248,79,119,130,162,140,88,255,0,129,15,87,9,148,
101,196,155,198,66,19,80,250,188,206,215,223,99,88,158,194,19,23,43,214,134,65,46,152,181,148,242,70,68,84,124,20,162,250,54,144,203,161,60,167,209,33,170,242,159,70,137,231,127,118,231,146,30,15,254,
30,113,9,44,162,185,6,184,165,229,35,206,120,26,16,63,213,197,105,75,113,127,5,41,118,225,240,148,82,151,133,224,53,8,65,107,112,164,17,79,36,39,178,119,54,16,132,244,182,92,187,244,132,30,209,188,188,
77,153,8,60,153,53,16,153,56,175,33,9,54,122,105,113,177,114,197,179,250,17,8,66,100,234,113,9,252,48,157,93,188,210,136,157,204,127,210,188,14,77,67,84,107,198,209,251,147,254,55,137,98,27,47,79,86,81,
117,61,40,126,87,30,5,16,208,163,98,76,131,112,172,172,173,226,18,98,44,43,40,241,49,189,129,177,16,124,252,46,67,225,79,221,186,144,209,9,172,69,46,178,241,247,138,81,226,27,238,148,165,212,203,210,18,
31,222,18,23,209,32,213,234,33,224,115,41,75,197,244,83,239,83,138,47,77,238,226,101,207,36,39,166,19,169,144,132,39,83,35,197,183,138,82,136,165,41,118,159,72,66,127,51,125,76,98,41,120,155,40,212,216,
66,117,57,92,252,99,251,183,139,218,25,50,101,226,226,24,217,249,252,80,153,58,72,135,194,143,200,132,26,26,245,33,172,98,244,39,236,155,122,94,212,183,243,137,195,101,60,158,79,37,98,16,73,141,19,82,
18,225,33,175,37,223,36,223,164,91,123,188,124,62,138,200,44,242,121,188,92,156,79,109,202,125,245,210,148,165,196,170,162,84,98,16,254,236,23,140,132,244,93,132,39,146,19,207,146,34,16,132,33,5,232,158,
164,54,94,167,11,63,249,204,247,94,169,75,148,78,49,13,69,204,226,19,111,107,152,66,16,153,9,235,153,8,79,85,218,84,82,148,189,177,50,247,75,204,225,115,61,84,165,47,23,217,75,144,132,233,15,138,45,152,
134,198,232,215,83,150,46,222,166,46,41,125,115,133,148,165,196,197,139,209,57,165,37,18,226,239,225,75,176,248,38,82,148,85,147,91,46,62,151,94,50,231,146,9,100,32,186,132,139,154,85,148,185,70,252,148,
186,207,36,203,165,46,40,217,162,151,133,176,174,65,81,166,33,31,164,18,226,148,188,77,130,202,83,203,116,72,159,187,60,94,17,249,195,248,94,33,5,148,165,41,74,95,123,126,216,65,172,153,240,187,9,233,
240,55,145,250,33,57,248,82,162,82,148,65,178,148,165,203,180,165,230,136,108,185,59,188,76,157,253,39,52,189,206,102,60,165,46,182,82,148,165,230,15,215,75,151,149,239,76,165,41,247,30,65,161,173,67,
101,22,193,34,16,132,26,229,136,126,153,233,252,22,52,66,12,248,34,139,152,76,108,165,200,69,203,240,92,165,63,8,120,46,50,108,219,233,190,118,150,231,146,17,19,33,9,234,162,42,31,80,110,190,124,20,186,
165,27,229,112,33,144,132,33,56,121,74,82,151,97,4,136,76,132,33,8,61,74,137,87,15,158,11,228,165,19,243,159,153,250,55,94,87,15,30,87,205,217,141,148,165,101,103,220,156,205,88,246,236,244,204,241,143,
154,94,217,74,82,159,123,130,67,126,61,183,30,87,145,144,132,33,8,66,122,166,76,165,40,253,115,214,201,202,226,241,74,55,196,196,63,232,158,228,62,214,66,8,162,68,30,194,112,181,101,41,74,82,151,154,78,
238,60,165,47,73,141,249,23,204,107,154,39,180,108,185,4,177,179,207,47,201,243,105,75,205,197,204,197,224,108,165,41,75,228,175,33,38,207,69,41,114,148,165,226,148,165,41,74,82,148,165,46,175,190,69,
229,31,60,143,28,67,198,38,224,201,180,189,210,143,97,4,136,77,132,33,9,228,157,55,146,249,171,240,162,73,180,124,111,38,126,16,131,234,148,132,241,179,136,79,225,242,200,65,243,54,101,238,148,188,66,
100,32,215,76,67,68,231,233,31,248,52,66,16,72,132,33,10,95,5,47,240,126,235,244,66,122,30,210,240,250,94,135,233,165,234,114,255,0,240,124,98,214,178,148,250,79,69,47,84,111,80,245,11,221,74,82,177,63,
37,203,224,187,50,19,46,81,188,75,103,12,79,41,118,17,111,231,16,75,143,36,98,67,68,33,8,66,19,46,162,229,41,120,20,165,43,41,75,138,82,148,165,41,117,15,169,151,241,16,68,233,122,95,19,33,20,19,226,227,
157,254,114,255,0,9,28,23,220,252,60,231,194,139,203,31,135,63,206,105,75,210,164,33,59,176,165,226,113,50,229,41,74,82,148,165,46,210,148,165,47,19,148,151,151,238,126,119,58,132,33,15,60,121,40,217,
89,120,155,59,132,38,77,100,234,243,74,82,148,108,165,47,178,127,10,31,87,153,196,245,44,125,207,74,26,203,137,137,148,165,201,196,237,11,46,61,67,226,98,98,99,226,113,8,66,19,16,72,152,217,68,66,31,54,
237,60,144,156,82,151,24,189,95,72,76,253,28,34,241,120,165,41,74,82,148,165,41,74,95,37,225,100,218,47,34,162,7,156,130,84,126,55,243,86,50,19,110,210,251,102,194,19,93,200,37,147,86,93,67,171,195,226,
148,108,67,173,214,126,231,230,120,143,253,30,47,15,185,171,187,213,46,206,41,68,242,12,109,63,139,41,117,175,76,244,76,88,146,153,71,213,238,19,110,33,188,125,194,19,30,66,19,105,74,84,54,82,244,132,
132,219,252,107,209,8,66,100,244,50,19,33,9,144,107,33,9,173,16,158,213,203,229,49,250,32,178,151,23,97,54,151,38,206,81,8,79,224,188,60,75,104,197,175,18,234,122,33,8,77,130,162,6,91,108,140,94,29,27,
49,20,187,113,74,82,151,138,82,151,171,138,82,190,23,135,131,223,8,92,66,122,225,8,66,19,33,8,76,132,244,82,151,17,125,171,192,252,240,241,53,70,252,248,248,95,3,75,196,196,184,241,207,193,144,132,244,
44,156,194,122,82,137,120,198,133,91,34,208,132,111,225,69,20,88,178,73,14,21,114,247,201,54,151,211,49,103,130,137,229,30,161,245,4,185,121,74,82,148,189,175,156,53,137,177,255,0,161,189,68,38,79,252,
187,137,81,168,82,151,40,245,241,50,117,74,93,157,33,240,132,242,103,145,228,32,184,111,155,183,171,151,23,186,151,200,136,53,234,107,102,166,54,62,214,55,138,217,25,49,4,68,226,148,111,19,41,118,226,
226,249,199,225,114,160,207,193,121,30,60,158,232,66,16,152,132,238,19,16,132,195,240,55,227,138,222,79,7,193,186,44,135,130,16,152,202,81,122,30,81,210,126,6,246,15,201,6,167,146,215,147,137,213,201,
144,124,83,206,47,83,200,38,84,49,50,148,77,21,13,162,204,127,240,38,111,194,27,99,113,82,135,131,250,82,16,158,181,199,158,47,52,190,137,180,185,74,82,151,165,196,247,164,66,16,158,136,78,39,182,100,
215,237,185,69,8,111,110,82,151,209,125,84,189,44,124,166,82,149,15,22,210,141,143,184,78,39,169,19,212,144,188,20,250,76,66,19,151,197,197,196,33,8,76,103,220,69,46,66,118,253,52,165,30,47,12,117,8,131,
15,184,66,16,152,139,209,114,239,156,132,196,71,143,243,138,48,197,101,103,146,16,132,32,156,31,162,148,188,38,54,82,148,165,19,242,126,159,92,26,242,66,91,241,202,94,76,162,111,252,29,164,182,17,30,49,
117,32,230,85,183,248,46,38,144,218,98,19,27,252,88,216,242,137,151,193,75,176,121,127,69,197,47,181,113,57,188,189,80,168,171,23,33,243,111,16,156,126,117,8,76,132,247,191,114,227,243,133,171,47,178,
148,187,122,122,149,17,37,233,190,250,63,83,246,66,117,117,19,87,11,136,53,179,41,113,16,162,31,156,91,70,45,75,68,26,196,81,188,48,197,22,126,9,114,223,9,171,228,100,217,118,250,161,50,100,33,8,50,113,
60,14,8,163,198,93,252,60,106,26,16,196,82,158,72,36,66,16,132,39,119,88,184,165,47,20,190,181,225,211,225,175,247,33,8,36,66,10,126,34,121,33,47,206,23,116,120,151,174,250,94,167,231,23,201,74,83,235,
26,107,239,170,236,200,66,103,140,93,174,31,162,162,148,165,47,181,165,232,123,75,151,136,78,215,51,39,240,94,168,217,124,101,215,232,126,133,232,181,98,254,212,66,99,225,15,169,136,135,204,124,174,127,
53,109,27,225,239,233,6,214,124,89,56,98,230,148,97,186,81,140,132,18,38,92,176,184,184,98,148,165,234,19,33,8,200,66,16,152,246,227,234,240,223,131,200,243,225,6,136,66,19,33,50,122,89,68,252,151,139,
172,165,245,66,9,16,132,244,94,25,248,37,113,226,17,9,205,119,168,77,242,60,81,181,248,95,248,120,108,131,68,246,94,215,129,155,63,54,251,174,164,124,24,178,19,47,84,190,245,196,238,113,50,19,97,8,66,
99,40,185,184,242,148,165,40,222,46,105,75,151,105,75,252,9,141,228,201,195,16,249,132,23,128,255,0,145,112,158,172,108,76,190,10,63,74,114,133,240,120,248,189,38,62,27,233,159,131,60,137,9,228,108,190,
70,202,46,24,242,240,135,196,38,210,49,124,148,108,165,41,113,42,124,100,124,9,136,65,4,33,57,132,223,5,69,226,16,132,225,242,135,221,219,141,237,60,241,240,184,136,62,95,112,72,157,66,16,153,5,144,191,
7,156,252,63,79,247,31,255,0,68,82,151,148,60,189,162,248,107,253,34,33,243,27,199,232,135,193,190,175,243,204,163,124,54,82,241,123,75,209,9,196,33,8,63,71,205,165,226,229,203,143,22,49,62,111,20,187,
74,82,255,0,34,25,8,66,12,132,18,31,194,100,23,193,253,230,16,97,38,41,48,253,239,138,95,90,99,234,113,70,207,164,23,19,30,188,89,59,132,18,32,254,8,98,144,242,248,120,20,132,98,60,72,240,47,75,105,74,
62,132,162,139,213,244,49,36,137,176,72,132,38,162,141,148,162,99,20,185,8,76,104,156,204,93,210,229,41,79,37,47,20,163,101,41,118,137,240,250,132,217,210,40,178,229,27,41,74,93,95,232,245,33,253,47,146,
122,111,163,207,8,168,108,189,254,226,112,186,250,91,8,67,193,119,199,162,148,185,74,94,161,54,16,132,25,9,180,190,135,204,200,66,19,171,213,41,74,94,39,16,156,207,234,158,137,232,156,60,130,33,15,1,81,
194,47,245,174,22,78,82,234,16,248,55,69,183,163,18,23,182,242,241,20,126,72,77,163,101,43,60,229,195,202,242,148,73,169,224,139,151,209,3,250,45,108,79,32,144,225,4,13,113,69,20,86,207,36,100,33,9,179,
22,209,178,242,177,245,74,82,151,193,114,148,190,186,38,92,165,201,195,196,76,158,153,171,134,47,62,16,197,244,162,101,227,244,125,36,65,240,167,233,251,175,209,9,232,126,132,94,80,167,233,118,151,47,
170,247,74,82,148,125,175,123,101,197,238,255,0,60,38,78,33,56,158,153,236,189,46,25,114,226,16,144,215,129,185,136,126,219,252,84,188,34,245,74,50,101,41,123,153,54,255,0,21,41,79,57,8,138,138,94,139,
202,109,14,188,250,41,71,176,95,113,251,137,31,132,33,240,165,103,157,132,33,9,197,41,120,184,139,213,218,82,148,165,41,75,197,216,66,19,102,172,94,9,179,39,161,137,159,121,165,225,242,138,54,81,71,194,
151,167,204,196,13,16,105,66,98,93,66,11,137,118,13,19,143,210,13,123,127,249,179,209,9,204,230,240,137,140,75,39,51,104,159,20,165,41,74,87,147,111,186,19,219,121,153,50,122,174,182,82,148,165,202,94,
91,46,92,66,41,70,248,88,132,198,207,175,238,76,126,153,218,219,137,9,13,109,244,46,238,210,245,74,82,162,50,205,21,151,153,137,19,38,126,11,207,40,132,216,78,255,0,59,75,39,48,132,202,82,148,162,209,
74,92,243,158,120,135,130,19,207,83,63,56,133,30,36,69,177,158,57,253,199,197,230,245,74,82,151,134,126,8,124,126,107,234,248,197,202,66,122,161,54,30,10,78,41,75,176,89,56,155,15,159,201,8,66,115,70,
238,46,124,20,165,46,38,95,77,41,228,132,39,11,103,44,165,217,176,158,155,205,203,232,125,50,117,8,76,124,36,77,132,196,32,147,35,41,70,198,245,120,31,245,38,94,230,66,19,137,210,47,43,209,74,82,148,165,
41,113,52,55,231,150,165,245,206,33,25,8,66,122,238,220,241,148,190,75,234,132,30,95,82,99,207,133,207,194,116,136,132,144,231,225,53,65,229,218,239,48,130,19,235,247,169,204,200,79,92,225,44,252,240,
67,147,38,46,238,161,240,197,158,50,101,229,60,94,39,83,136,120,30,193,34,45,130,159,168,98,225,115,50,226,219,179,138,82,148,165,46,164,217,126,148,33,9,143,187,159,156,66,16,152,189,143,187,232,249,
143,221,120,131,197,194,84,88,49,4,135,240,248,245,173,74,141,79,232,131,94,155,149,255,0,23,156,185,75,180,184,165,46,93,43,63,56,132,33,60,115,8,66,19,33,54,151,23,162,237,219,232,72,248,37,228,106,
23,41,114,178,189,158,184,36,66,13,108,197,247,27,226,159,156,120,153,55,201,228,67,203,24,221,219,235,123,8,125,92,66,100,198,184,75,32,196,143,56,208,101,19,200,65,34,127,210,47,247,60,9,165,248,63,
7,147,201,228,243,141,243,79,44,243,232,132,197,50,17,122,23,142,33,5,240,123,61,148,184,138,82,229,41,74,94,225,8,65,50,141,229,244,78,80,136,66,16,156,82,148,186,138,82,148,67,202,82,151,249,23,51,210,
189,69,141,19,24,246,12,143,211,224,221,226,123,31,162,151,219,9,139,97,9,232,164,65,178,141,240,41,117,42,120,93,63,50,19,152,76,156,223,77,41,124,255,0,21,196,143,250,63,156,172,93,76,132,200,66,103,
196,34,245,8,36,77,74,161,107,93,178,148,165,41,75,139,132,135,147,33,6,186,131,158,137,211,62,34,151,30,44,165,216,66,34,36,82,229,27,233,100,255,0,167,231,162,151,134,78,104,241,50,242,223,231,83,171,
239,153,8,66,109,22,94,103,87,41,75,148,165,23,52,101,245,188,126,87,254,20,238,8,124,176,184,99,30,46,21,251,215,185,108,33,8,66,16,132,39,84,165,41,71,133,101,20,189,66,104,177,158,7,54,19,137,252,23,
152,78,159,170,19,191,156,63,68,245,220,163,218,93,108,190,38,254,143,105,113,236,33,9,140,130,92,45,158,170,82,241,120,132,33,6,177,187,10,53,17,49,249,119,240,253,25,228,158,53,101,41,114,229,254,56,
78,38,78,38,71,148,191,248,208,72,136,241,213,41,125,215,23,240,79,84,39,162,151,154,82,148,165,214,184,184,139,173,249,236,185,99,67,248,39,139,73,224,162,246,79,235,187,75,135,169,138,93,252,196,77,
132,33,54,148,190,187,219,246,66,19,133,199,239,162,151,97,49,139,23,83,91,41,118,151,199,165,18,75,207,146,242,252,11,224,143,209,159,69,168,158,127,130,148,165,41,75,151,143,206,161,9,144,88,133,196,
33,226,49,31,134,196,45,45,165,47,51,218,253,151,211,68,203,148,165,41,114,148,165,41,122,157,66,19,154,95,85,218,82,151,249,47,161,112,187,189,82,245,8,37,136,66,99,197,147,152,66,19,132,50,99,196,38,
38,46,26,26,26,24,72,76,184,134,33,251,161,8,79,68,196,242,148,165,197,197,224,86,94,225,8,66,19,41,119,206,206,158,33,23,139,252,84,162,242,167,16,153,6,182,117,120,165,245,210,229,41,113,38,216,252,
99,248,126,103,194,222,60,189,130,226,148,165,60,242,178,100,25,231,97,8,66,100,33,15,27,127,135,235,132,168,66,86,255,0,194,154,243,248,55,7,73,23,148,78,215,193,191,76,230,236,215,221,47,112,115,243,
97,8,66,16,153,8,78,110,82,237,200,66,123,33,8,66,115,8,79,75,33,50,100,33,8,66,9,33,194,13,119,8,76,153,8,76,165,212,76,165,47,162,159,75,10,82,151,30,81,50,148,188,33,49,132,203,173,16,157,204,153,61,
16,132,33,56,163,101,47,52,109,13,193,248,226,178,178,148,94,72,70,36,53,196,33,8,65,33,248,254,9,204,246,78,38,66,16,156,76,156,65,106,199,148,165,238,140,189,50,151,171,179,191,29,254,31,254,111,142,
161,54,122,62,100,233,151,181,196,226,250,106,45,223,207,24,221,126,72,66,99,66,237,242,209,58,187,51,198,213,10,92,188,71,58,156,175,31,253,47,250,120,196,134,137,212,241,213,218,92,153,120,165,47,48,
132,218,82,241,118,229,41,75,139,201,74,82,151,154,81,177,50,148,169,137,117,75,227,182,34,19,138,94,22,94,124,19,68,95,74,82,141,151,103,94,50,136,69,241,170,41,221,249,234,113,7,144,132,39,20,165,195,
101,46,94,153,95,9,120,30,36,124,19,185,74,52,65,46,147,155,74,95,232,155,59,158,148,63,87,220,98,249,195,36,27,41,249,197,219,176,80,109,82,148,124,92,165,203,139,86,45,124,66,100,226,122,103,161,240,
197,144,158,138,82,148,165,199,243,86,63,147,90,38,66,16,132,38,194,100,38,120,24,249,165,230,143,47,112,72,111,193,8,76,157,190,95,15,153,204,39,23,168,76,155,74,207,37,230,115,8,76,66,34,19,152,66,16,
159,205,75,238,185,121,248,55,125,176,130,32,138,82,137,148,131,21,33,178,196,229,124,27,27,41,120,165,41,116,165,41,74,82,151,208,135,151,104,252,136,163,226,241,63,130,16,132,33,50,19,96,182,245,57,
154,138,83,247,98,249,31,209,109,40,223,146,148,165,46,167,181,191,226,132,33,9,136,78,97,58,158,153,173,106,214,45,170,58,95,87,233,74,82,151,41,125,79,168,65,249,33,56,165,202,82,231,142,105,74,82,162,
250,33,8,66,113,227,180,135,231,33,9,233,156,210,148,165,41,120,132,33,59,165,47,30,72,66,101,22,49,34,119,61,55,30,65,44,132,32,178,151,211,120,165,226,16,153,61,48,123,56,130,68,75,97,53,227,200,67,
247,132,200,166,35,95,68,241,81,48,112,124,23,138,82,148,165,41,74,82,234,63,54,19,97,8,77,132,33,9,196,254,53,73,210,254,24,79,90,41,74,82,148,189,66,19,38,66,115,9,176,157,207,67,226,19,135,54,250,40,
154,202,84,91,207,210,101,41,121,187,125,80,132,216,66,16,132,33,59,165,41,74,94,233,75,151,103,83,137,197,244,94,16,250,94,50,226,101,41,74,95,35,251,227,153,139,164,198,233,121,132,33,8,66,118,214,46,
231,80,132,202,92,101,27,218,82,237,254,90,92,94,164,50,31,58,165,238,9,99,41,74,82,139,200,217,33,210,30,178,159,126,13,69,76,249,62,54,148,165,197,41,74,38,76,132,38,207,76,33,61,52,190,137,212,32,151,
87,213,122,121,74,82,226,26,229,228,23,145,166,66,19,39,20,184,178,113,125,19,219,227,249,33,54,151,46,210,148,165,207,60,220,188,66,114,144,153,8,76,132,226,113,75,158,79,58,150,66,109,41,118,63,108,
30,66,19,248,105,74,94,30,50,236,38,195,193,54,19,33,5,63,79,240,216,242,19,221,75,176,132,38,210,148,165,41,74,82,178,243,61,13,241,127,142,19,97,8,66,108,30,33,241,75,197,240,92,79,108,46,66,16,152,
188,51,192,45,66,6,73,120,27,49,138,48,217,113,13,19,18,33,60,250,238,206,233,74,92,188,164,66,16,156,66,19,170,95,84,217,205,46,82,237,46,41,74,81,60,87,62,144,248,62,97,4,178,116,178,15,143,59,56,242,
66,122,38,79,90,250,82,148,165,47,112,132,33,9,196,244,45,132,202,93,131,240,92,190,11,139,97,8,66,8,103,193,188,78,50,151,208,151,145,164,147,18,212,68,63,101,244,210,245,8,66,103,131,193,61,16,130,72,
156,194,117,75,235,132,26,225,241,74,82,247,8,79,68,234,30,56,189,194,16,72,132,195,68,33,8,66,108,25,75,205,27,27,244,206,146,200,198,132,136,65,125,198,65,34,121,17,175,0,255,0,200,252,231,225,247,136,
37,252,148,189,66,19,208,159,241,82,148,188,210,143,46,82,251,161,9,227,84,165,27,42,92,153,9,180,187,122,121,74,81,178,234,218,95,95,140,165,244,37,204,200,76,156,205,156,66,117,8,76,157,181,147,167,
224,165,203,138,121,216,66,16,153,50,114,136,67,198,177,123,225,8,66,19,38,206,223,210,127,135,230,206,127,68,172,126,28,89,95,52,242,121,46,66,34,106,39,116,116,87,148,185,25,8,77,155,224,240,61,165,
244,92,132,197,244,117,249,144,132,238,227,196,135,158,118,148,188,38,81,241,127,145,49,16,79,2,68,38,190,31,194,118,189,116,191,201,122,153,56,184,253,119,213,68,241,178,151,102,193,241,114,148,165,46,
55,210,250,49,251,234,132,39,142,102,36,78,150,212,94,41,125,112,121,61,23,41,231,134,132,181,229,241,234,163,98,243,247,96,209,4,82,149,99,101,41,74,82,151,181,212,33,61,20,165,234,151,18,245,220,190,
246,82,231,147,201,228,242,82,243,120,120,217,74,95,37,92,120,28,95,10,95,249,158,125,9,19,41,75,196,100,39,19,33,8,66,16,132,226,148,155,7,183,47,162,229,133,27,230,151,215,224,115,137,144,132,200,53,
144,152,132,48,252,137,9,13,49,49,228,82,17,7,147,255,0,224,103,241,181,72,33,77,155,8,66,119,64,245,75,213,47,11,27,219,144,124,53,195,93,36,53,31,20,162,101,40,217,75,171,143,36,202,92,153,247,105,113,
34,113,121,80,240,54,87,148,165,40,152,223,43,209,9,202,230,151,103,240,94,149,60,147,86,77,165,201,232,188,222,166,94,110,62,124,20,165,41,87,248,82,148,109,178,9,119,51,193,8,66,18,108,39,117,20,165,
40,217,74,82,148,111,17,123,191,195,63,130,148,92,178,106,227,192,137,148,165,19,240,74,53,6,198,198,79,252,154,82,250,47,16,130,68,33,61,171,206,33,16,139,133,140,165,244,222,39,54,11,239,186,137,194,
190,174,254,8,94,126,143,39,130,117,231,152,66,16,132,33,61,19,132,92,189,82,229,41,114,190,126,247,249,204,33,8,68,76,98,226,240,189,55,152,126,115,228,242,66,16,241,179,110,84,57,158,68,62,81,57,163,
33,8,49,30,15,5,172,248,82,161,191,87,130,151,97,8,66,113,228,242,253,199,140,158,169,149,20,77,13,162,148,165,47,244,66,16,158,168,66,16,154,136,66,123,175,51,134,196,194,9,141,141,15,194,19,17,71,46,
51,243,223,50,127,27,213,213,219,143,210,177,243,8,78,126,98,178,190,86,55,250,53,9,203,200,66,18,12,165,244,162,228,254,143,232,177,33,124,216,52,77,132,33,9,233,132,203,233,243,147,39,51,41,117,169,
203,199,215,129,174,160,177,108,39,166,137,151,201,120,132,33,8,76,67,41,75,239,153,228,242,66,98,45,92,194,19,33,56,130,225,178,148,184,76,185,231,31,13,36,151,251,159,72,37,196,38,194,19,143,36,207,
28,249,201,221,46,24,191,197,61,19,137,144,157,66,16,132,39,52,188,210,247,58,165,196,249,165,225,49,121,198,60,184,110,137,161,178,148,75,253,33,63,150,151,170,82,243,54,16,153,8,66,16,132,33,8,66,117,
61,72,107,133,144,132,63,68,209,100,250,152,138,138,138,62,97,4,151,233,22,252,197,147,213,9,148,165,201,144,130,60,13,151,134,74,78,39,23,180,84,94,40,223,51,137,147,219,74,82,143,103,162,151,41,74,82,
151,23,111,185,144,136,136,136,240,92,165,225,245,75,205,212,202,41,89,228,132,207,25,50,151,136,124,24,178,164,254,31,70,218,248,121,40,246,16,91,53,178,151,154,81,188,185,125,176,158,136,66,19,216,178,
19,102,209,188,165,40,223,158,41,74,94,86,95,92,26,218,81,50,151,110,176,134,27,41,123,165,244,188,191,197,8,66,16,72,153,9,221,46,61,165,229,250,111,171,231,12,187,120,252,215,179,87,5,172,132,237,123,
105,74,82,247,4,93,165,46,54,38,121,60,158,120,187,41,8,65,236,34,31,119,60,147,34,39,178,151,105,74,82,151,23,50,147,97,243,97,57,136,241,205,41,75,175,19,225,243,114,239,158,124,158,113,210,50,19,16,
132,103,195,198,174,233,105,9,158,55,198,47,35,126,75,158,6,209,74,82,151,184,201,237,165,244,249,60,243,8,66,19,159,39,147,204,60,231,147,206,194,16,132,38,66,16,132,33,9,183,171,221,47,177,177,60,182,
101,47,242,172,165,244,66,106,225,151,22,82,148,189,206,33,4,184,131,89,9,183,215,114,148,165,27,246,166,92,162,110,23,20,189,54,70,77,184,143,167,233,39,162,148,165,202,82,151,199,87,81,75,204,38,82,
236,33,4,178,113,8,120,245,220,165,247,78,33,8,47,4,30,254,144,131,32,132,67,156,249,196,138,33,16,210,69,41,74,82,148,188,193,47,244,240,120,218,61,140,132,33,50,151,129,74,93,132,207,25,70,252,20,165,
42,40,138,92,249,180,111,33,61,52,165,230,16,132,33,8,66,16,132,33,9,50,16,157,82,247,7,221,47,43,187,151,95,241,189,158,185,212,18,31,20,190,185,235,90,203,143,209,74,81,99,230,148,165,218,50,20,111,
215,120,132,39,72,107,159,153,54,122,105,125,15,225,74,144,188,39,11,4,252,141,151,139,224,241,61,16,156,76,132,33,49,236,32,145,61,84,165,202,38,82,251,126,141,101,197,204,33,8,66,100,38,76,162,196,184,
165,41,68,198,202,121,32,146,40,202,133,139,240,45,200,66,100,216,78,124,144,132,60,118,241,228,201,197,41,68,217,89,94,55,255,0,118,15,165,139,199,157,125,79,68,197,196,39,170,19,210,251,78,23,46,34,
148,162,41,127,138,241,8,78,23,83,33,61,15,213,56,91,8,76,132,33,8,66,16,132,201,203,203,219,217,234,156,38,54,81,190,211,198,198,238,82,237,234,106,238,100,255,0,192,132,219,238,156,46,23,112,153,227,
186,93,163,123,4,136,51,240,95,79,211,231,40,132,60,99,17,8,67,224,156,69,147,197,33,54,31,58,243,212,32,132,71,130,8,252,210,178,178,241,15,28,179,233,49,21,137,99,249,213,41,75,180,163,101,41,95,169,
246,242,16,132,66,68,31,31,188,79,230,72,241,211,202,93,185,68,253,215,209,103,162,19,248,33,50,30,58,94,164,136,66,98,67,89,117,99,244,82,148,165,254,181,183,38,62,169,74,49,123,91,57,156,180,76,90,253,
183,175,29,49,220,132,33,50,17,19,33,50,115,57,132,226,162,162,235,93,220,165,214,159,73,100,216,77,100,33,9,235,95,74,138,92,165,60,158,79,35,103,145,100,237,162,109,32,88,90,55,176,156,36,200,201,144,
152,249,165,43,41,75,180,188,82,151,62,250,188,78,161,95,158,129,115,63,130,234,92,124,46,82,237,47,171,207,186,237,237,245,57,165,234,108,33,8,68,57,180,190,137,233,105,9,194,148,69,19,243,143,162,91,
54,122,151,84,185,54,123,30,55,183,111,23,132,78,161,63,134,148,165,41,74,34,243,75,139,63,15,222,161,56,252,38,66,117,59,121,74,83,247,23,31,164,131,234,248,238,19,138,94,233,74,82,148,189,66,16,132,
26,33,4,137,144,132,33,9,183,97,15,5,69,41,75,49,68,202,81,183,139,33,224,240,84,93,130,68,22,220,189,79,35,80,165,230,19,33,52,132,201,227,38,66,113,4,136,66,100,33,228,116,243,196,39,43,222,247,198,
210,151,201,125,247,248,166,66,16,132,200,76,135,239,162,16,132,38,79,67,125,81,251,239,52,95,70,45,167,192,197,245,34,127,37,41,75,236,125,95,68,39,44,68,244,66,16,136,126,223,57,8,66,9,16,135,129,237,
31,162,255,0,10,250,126,159,11,70,66,19,213,74,125,41,70,215,225,114,148,162,103,209,228,33,59,189,220,165,41,74,82,148,172,172,167,146,229,101,108,131,219,169,19,124,30,11,176,130,68,230,237,40,217,75,
202,240,63,59,4,153,58,165,41,119,231,80,155,74,92,153,75,204,234,16,157,81,115,114,241,231,33,15,2,127,224,82,151,139,144,165,246,249,202,46,174,190,239,72,165,41,75,234,67,249,148,243,68,73,9,58,132,
245,222,41,74,95,236,107,46,194,109,27,218,92,176,187,74,92,252,41,74,82,226,216,76,132,39,165,97,244,188,246,132,151,210,113,41,47,116,190,138,120,31,83,18,76,104,60,36,196,16,156,161,60,94,136,88,90,
44,165,27,230,118,183,201,54,108,231,230,66,58,66,117,97,113,74,92,79,200,223,231,83,139,151,17,79,59,9,144,131,219,183,41,228,242,121,18,39,241,34,148,165,41,74,54,82,148,188,163,198,84,120,202,82,250,
97,9,196,216,77,132,62,23,136,77,132,219,180,165,41,118,19,165,139,105,74,94,111,182,19,136,78,215,23,83,131,116,126,121,76,165,46,93,165,41,74,82,151,137,234,132,244,78,30,33,46,39,23,249,32,139,179,
133,203,104,165,238,239,230,169,6,35,240,75,149,204,33,240,250,66,16,132,33,60,16,132,18,26,33,8,66,9,13,16,132,39,6,202,82,245,9,139,22,94,27,234,245,60,9,98,85,141,121,33,8,68,84,84,82,162,151,152,200,
66,19,23,22,13,148,165,41,117,106,60,66,19,154,66,65,249,33,8,66,16,92,205,153,30,66,16,130,92,248,42,41,75,180,187,123,185,114,250,167,9,148,165,41,74,82,158,125,30,79,60,121,35,38,121,229,236,39,241,
210,237,41,75,151,193,115,243,153,220,33,8,79,93,255,0,193,132,201,233,132,245,206,158,210,148,165,47,162,151,168,66,100,39,165,120,81,140,163,101,234,8,126,9,87,84,185,117,50,164,41,113,16,132,39,20,
91,249,233,108,165,230,251,41,74,81,112,177,178,151,23,8,126,154,82,150,51,201,228,242,121,253,33,8,66,16,72,135,142,41,118,148,165,47,112,132,33,53,100,36,60,144,153,8,36,76,107,137,143,18,226,31,11,
136,131,80,165,197,197,46,95,30,214,253,80,132,216,200,200,66,19,175,30,137,143,39,116,165,47,75,209,114,136,125,95,103,231,244,78,167,178,103,140,159,199,63,154,229,46,95,98,33,9,224,132,38,78,165,68,
244,60,167,196,31,178,220,159,199,120,253,216,207,37,235,207,51,31,210,241,74,92,165,254,5,180,187,228,132,39,130,16,153,9,147,33,9,207,130,148,80,165,243,141,148,165,41,120,242,77,148,72,136,135,143,
243,33,250,76,72,66,16,101,216,77,79,182,202,92,162,123,74,243,201,229,20,165,217,232,132,69,255,0,132,164,39,23,33,8,78,39,51,60,149,151,136,66,16,132,38,78,23,174,143,213,75,221,254,25,176,123,53,116,
189,176,153,8,78,225,4,136,76,132,207,131,243,196,230,127,29,41,74,82,148,165,41,117,236,38,66,117,8,65,23,138,93,165,218,40,80,253,31,20,189,66,19,97,61,144,132,247,46,169,114,229,67,98,41,113,70,202,
94,23,185,45,38,33,4,68,33,9,149,113,74,82,229,41,74,85,148,165,41,68,197,158,50,148,79,16,248,132,34,153,54,13,108,207,7,130,249,198,82,149,104,223,157,165,47,69,46,220,240,82,148,165,125,194,16,153,
15,4,71,130,119,71,148,69,203,252,111,214,242,151,41,74,82,229,255,0,192,158,136,79,84,33,8,66,16,155,9,213,219,233,165,218,82,223,228,165,41,74,95,235,156,78,46,95,68,33,8,66,100,33,8,76,76,126,125,55,
97,9,252,180,165,45,101,41,122,190,249,176,132,33,8,36,66,115,74,125,207,5,69,84,98,228,214,82,149,151,171,236,157,193,35,196,28,245,60,171,252,28,216,200,200,66,19,25,114,229,244,194,16,132,33,48,240,
158,127,138,151,211,54,113,74,82,234,23,20,165,42,27,41,74,82,151,248,239,241,66,19,184,66,19,169,144,132,39,170,16,156,53,168,158,132,52,66,16,156,249,18,39,182,151,211,56,153,50,16,132,33,8,76,132,39,
75,41,74,94,90,33,8,66,16,132,219,213,41,75,236,68,203,159,155,61,23,42,41,75,148,188,207,82,249,177,144,132,33,8,77,132,225,61,165,218,92,82,148,165,41,118,100,226,117,8,121,16,158,136,66,16,131,32,242,
108,217,199,232,248,165,203,139,198,82,148,165,47,170,16,132,39,51,17,74,83,239,162,16,126,50,237,47,19,83,207,204,176,165,20,41,74,82,151,139,11,151,215,8,76,92,77,156,254,19,26,39,190,15,221,125,51,
153,235,111,83,217,210,225,51,234,32,168,240,55,144,132,218,63,228,132,217,179,217,74,94,105,125,148,189,66,122,41,9,143,169,211,101,218,82,159,188,78,151,166,113,9,176,153,15,130,135,129,206,105,114,
151,23,208,154,133,162,131,156,126,108,38,34,19,38,189,132,215,204,34,33,9,219,17,120,165,202,82,255,0,195,201,9,194,39,20,167,220,132,33,49,147,97,4,120,60,20,188,82,148,189,54,39,199,205,163,120,242,
141,148,165,41,122,249,141,19,209,5,31,11,113,250,167,119,171,252,119,249,233,74,82,240,184,68,26,225,226,234,12,184,190,125,87,42,77,185,74,82,178,148,188,66,115,8,66,108,254,42,82,148,190,152,76,155,
54,113,61,237,101,226,143,151,212,201,139,209,74,66,19,97,59,72,153,75,183,60,15,41,231,46,94,239,142,41,125,211,153,235,154,139,148,108,167,146,127,188,120,201,140,152,92,254,9,141,226,141,255,0,60,33,
9,221,47,20,125,53,242,47,99,80,74,143,155,204,230,250,161,8,76,243,232,165,47,72,124,76,187,125,83,97,58,165,216,66,19,39,8,165,41,74,44,176,165,41,74,93,140,141,20,165,41,120,76,165,230,251,167,20,187,
61,87,215,75,196,126,185,235,152,253,84,111,46,92,163,124,220,165,30,47,143,76,33,8,66,16,132,33,9,61,87,193,74,82,178,227,244,162,251,126,144,132,33,8,66,16,132,244,210,149,101,201,180,187,69,179,39,
161,148,187,124,66,148,165,47,19,136,33,172,132,207,155,95,162,116,196,50,229,41,74,82,151,39,107,221,248,66,16,132,33,54,108,38,66,16,157,66,113,58,130,68,33,5,144,132,38,194,16,107,33,8,78,151,55,41,
113,148,76,111,38,82,137,150,148,165,200,66,136,241,52,135,131,193,80,208,165,40,153,75,139,232,158,169,233,91,9,144,140,243,139,170,95,224,156,38,50,16,132,33,8,78,41,120,165,226,148,187,74,95,84,33,
57,67,217,144,132,38,193,34,122,233,114,159,120,186,135,137,141,151,133,191,50,19,149,244,107,201,9,147,33,8,66,19,46,82,148,165,30,78,87,15,185,205,219,148,250,82,237,41,120,153,8,65,108,238,119,227,
102,188,72,124,210,231,204,126,239,60,121,33,50,16,140,153,8,189,76,93,120,219,176,132,201,147,97,58,77,76,152,248,242,78,41,74,121,125,53,169,162,243,74,82,148,165,233,240,179,193,84,42,41,26,86,82,149,
151,211,118,148,188,65,99,234,16,158,165,139,33,50,148,185,121,132,238,119,9,144,132,39,178,16,154,248,187,56,132,244,174,225,8,61,75,164,53,137,249,27,46,79,27,226,127,209,242,151,220,162,99,109,158,
102,210,240,143,207,92,38,79,109,217,232,165,41,75,197,69,41,74,81,5,24,230,221,165,229,57,235,241,171,33,8,121,33,8,181,19,47,80,121,75,183,23,208,185,132,33,8,67,198,46,33,50,8,155,249,204,234,16,72,
107,184,76,136,154,151,31,10,34,244,208,208,156,19,41,79,164,218,209,75,204,121,9,148,163,124,76,190,187,147,218,136,45,19,16,132,33,9,176,132,33,9,233,158,249,139,201,4,183,247,139,239,132,33,61,83,105,
74,92,163,19,218,82,244,189,48,132,39,174,148,165,202,82,148,165,46,61,121,124,9,148,190,202,60,73,182,66,13,34,16,132,200,66,16,153,9,235,132,229,229,254,132,203,204,244,164,66,16,156,248,216,76,156,
194,101,40,248,165,41,71,212,33,61,19,143,5,41,123,185,120,108,69,40,217,121,66,30,63,193,239,156,185,9,144,130,254,42,94,30,92,76,162,148,108,165,41,74,38,94,66,250,90,217,144,156,78,63,56,132,33,8,77,
253,18,33,52,199,49,36,198,145,8,68,68,79,56,65,164,191,130,16,132,199,236,93,66,115,8,76,156,79,125,41,74,94,60,158,121,132,33,9,171,186,82,151,60,65,136,107,248,46,82,148,165,230,115,57,132,26,199,228,
130,254,6,94,175,20,165,47,242,78,35,200,76,132,33,9,196,244,161,244,198,252,33,125,226,151,151,197,46,82,236,197,196,234,19,199,45,50,16,132,210,16,132,33,8,77,176,165,244,92,165,124,207,4,30,44,123,
117,229,41,74,82,162,148,163,101,46,47,35,105,126,148,165,41,74,82,236,200,76,157,194,16,157,66,16,135,132,120,121,49,173,157,223,114,165,240,81,96,205,20,65,249,97,229,111,13,114,149,26,107,138,34,227,
123,8,65,33,161,234,16,198,210,30,23,23,116,190,138,82,148,165,41,74,55,220,33,8,78,151,20,184,165,219,151,217,61,79,38,193,16,132,230,113,9,236,93,210,229,230,148,162,46,82,250,175,170,120,200,66,16,
132,39,12,188,161,15,231,18,122,89,122,72,185,56,165,246,205,75,63,5,235,190,53,66,175,204,75,38,46,158,44,253,30,174,41,71,205,41,74,83,238,92,108,185,74,86,223,186,115,8,78,161,11,10,241,243,114,148,
165,62,137,127,163,251,224,72,132,33,8,66,8,156,78,224,151,19,167,143,19,133,23,150,61,67,233,20,165,198,93,76,111,154,82,148,165,197,46,148,165,41,89,122,165,41,74,82,148,165,41,74,82,151,139,196,33,
9,144,132,38,173,165,47,244,207,76,244,93,187,9,169,197,232,88,189,116,165,238,122,252,78,161,58,125,172,188,95,90,88,163,19,99,9,248,41,74,82,149,20,94,223,207,68,126,94,137,139,211,50,16,131,46,66,22,
8,65,84,27,19,202,55,139,148,186,246,161,190,233,117,234,244,194,16,132,196,33,6,33,8,65,31,69,50,19,33,61,158,50,15,208,109,16,176,188,34,19,254,19,31,44,165,203,205,47,16,132,60,15,211,123,165,47,254,
13,203,234,155,8,66,16,132,226,241,74,82,151,41,121,190,181,235,93,194,19,47,162,16,158,196,62,104,222,210,227,46,63,124,234,119,7,234,165,40,157,38,209,15,139,136,75,20,32,108,132,200,67,207,181,119,
54,113,15,206,161,8,66,19,39,75,30,92,130,131,88,241,172,165,41,117,121,227,193,113,79,39,158,33,50,16,132,39,113,16,136,135,130,44,132,231,200,139,204,33,6,33,147,35,33,8,37,212,33,61,19,22,194,101,19,
76,139,253,33,97,217,118,148,185,9,147,105,74,82,148,165,8,86,95,253,249,147,209,74,82,235,244,79,124,39,243,173,132,230,148,190,139,232,185,114,241,8,36,53,227,101,39,48,98,242,77,163,234,100,39,161,
227,230,109,41,75,190,79,36,103,156,76,172,184,151,242,66,19,137,147,184,66,16,130,73,99,47,20,165,33,16,214,81,151,105,70,202,82,148,188,210,148,165,225,30,63,194,228,103,204,156,36,77,123,50,113,63,
134,239,220,185,74,82,148,165,218,138,34,148,185,75,212,39,22,31,88,229,238,148,185,74,82,151,251,38,66,100,234,19,38,66,16,159,221,75,252,80,132,33,56,107,33,63,240,46,174,41,125,179,19,136,50,228,38,
194,113,49,113,60,122,33,56,144,143,154,93,165,47,240,46,94,79,100,33,9,196,238,9,19,31,129,19,39,20,162,99,30,194,149,103,225,53,148,167,129,180,82,229,245,70,70,66,16,68,18,25,8,66,114,184,162,239,206,
49,101,154,165,202,82,229,41,70,202,126,140,156,83,199,232,218,133,196,249,165,46,210,229,41,74,82,227,46,46,210,151,251,33,8,66,16,153,8,66,100,38,194,100,26,33,9,239,132,200,66,122,174,82,148,165,41,
125,112,132,33,8,79,107,202,95,101,47,177,63,77,47,241,81,63,3,120,178,241,75,183,209,248,37,207,231,30,4,252,151,46,191,192,217,75,212,200,62,103,80,95,79,3,159,154,188,23,152,77,132,238,19,198,204,115,
47,52,97,58,89,136,187,74,55,148,165,19,47,146,151,81,74,82,148,165,216,201,176,132,33,50,109,202,82,241,74,139,179,170,94,40,223,64,165,69,30,210,148,165,69,69,41,114,172,94,50,239,222,150,82,151,185,
218,67,126,217,238,132,33,9,144,132,213,220,245,172,132,38,61,190,203,195,230,226,126,184,66,16,158,171,175,186,94,47,166,148,165,47,162,250,161,8,66,16,132,33,8,77,132,33,9,204,246,194,16,132,33,54,248,
47,141,162,99,101,218,253,23,33,61,208,132,33,8,78,97,8,79,100,240,66,16,158,148,198,63,156,194,205,165,41,113,251,46,188,152,151,48,132,33,4,136,79,117,202,82,151,41,120,165,62,237,226,251,33,49,19,253,
32,254,111,159,226,69,196,62,47,80,107,97,9,141,144,131,68,18,200,76,132,33,9,203,215,172,69,226,148,165,41,74,92,165,46,139,128,165,27,247,210,151,249,39,244,92,165,47,19,39,182,108,234,148,190,171,203,
124,188,69,203,220,36,24,178,227,98,101,43,41,120,131,19,26,157,93,155,54,122,124,113,74,82,151,92,218,92,190,155,148,69,47,186,235,41,121,132,39,174,16,132,33,8,66,35,198,67,198,210,148,163,101,19,200,
65,236,230,148,108,165,41,74,248,165,245,66,19,143,57,9,220,241,196,240,66,107,216,65,34,16,132,98,108,132,38,194,16,132,33,8,66,16,158,137,212,244,210,237,41,114,148,165,47,240,194,30,63,186,16,157,66,
16,155,125,52,165,47,178,16,132,216,76,132,26,198,136,79,124,226,119,114,227,246,165,83,255,0,153,74,95,35,202,81,110,82,229,215,147,154,91,139,33,8,66,13,122,47,84,165,62,243,227,248,40,211,74,254,116,
177,109,203,148,165,203,143,41,118,190,110,194,60,153,8,66,19,26,223,57,74,139,140,185,74,94,40,242,101,41,74,81,57,139,183,81,59,132,33,8,66,113,240,253,18,207,205,253,245,220,240,120,27,241,138,93,68,
39,84,185,75,197,46,82,237,214,62,233,122,165,41,74,95,101,255,0,192,132,234,19,184,78,169,122,165,41,74,82,148,165,47,112,158,247,202,30,55,204,226,16,132,202,95,85,46,222,160,215,9,23,170,126,113,50,
16,154,136,78,230,67,196,216,126,109,47,23,217,53,52,152,213,248,254,91,98,225,237,19,234,241,118,16,132,200,66,19,33,4,137,143,193,244,253,60,23,33,9,205,215,139,37,201,232,165,41,75,233,240,68,78,38,
66,13,98,33,8,78,18,30,83,233,49,229,212,126,141,114,108,33,4,137,147,208,242,151,184,79,101,41,74,93,121,118,151,47,52,190,233,176,132,216,66,108,33,8,66,16,132,38,204,90,150,18,136,18,67,132,199,180,
76,189,66,113,125,51,33,8,77,132,33,8,66,122,103,161,177,60,95,85,41,74,94,175,23,102,66,117,75,180,79,47,161,98,33,8,66,16,155,8,66,106,244,62,41,125,23,138,45,189,194,19,155,199,158,60,250,103,77,121,
200,77,165,230,159,68,136,76,240,95,35,121,8,36,76,101,45,41,228,165,69,84,240,220,71,131,156,210,148,108,165,41,120,132,216,77,153,8,78,94,66,109,226,148,163,68,203,227,40,242,227,135,143,67,17,19,232,
218,252,69,41,113,178,148,165,41,74,82,245,74,82,148,165,202,82,148,165,41,75,205,245,194,116,139,143,164,181,36,76,132,33,9,212,39,63,72,78,233,112,167,8,58,41,113,113,34,19,170,95,116,245,79,69,47,55,
105,74,82,250,110,82,247,61,51,221,57,158,234,139,148,165,69,227,196,41,74,44,101,41,124,237,199,247,41,74,94,110,49,22,20,165,41,114,229,27,41,124,246,146,60,108,33,56,132,219,210,124,66,16,131,93,88,
82,177,63,36,34,196,78,138,82,137,148,165,41,103,194,241,118,113,9,144,132,33,8,65,162,19,152,66,117,124,237,69,40,176,108,165,46,66,148,190,38,121,223,36,33,8,45,153,41,8,77,155,8,201,203,226,19,248,
233,247,184,66,19,33,30,205,154,136,78,97,50,148,165,43,43,226,241,228,132,212,197,179,169,235,165,47,20,189,206,167,178,148,165,41,121,66,225,251,161,8,66,16,153,9,195,246,194,19,153,151,95,8,157,190,
105,68,50,148,163,101,41,124,109,46,206,166,194,127,4,33,57,187,9,175,102,190,102,77,188,55,137,204,82,148,187,75,148,165,46,148,165,240,121,60,158,121,132,32,145,54,16,132,33,8,66,19,96,224,185,242,121,
217,175,41,89,121,140,147,46,174,33,15,139,26,239,207,249,181,20,94,72,66,113,125,49,144,132,32,178,16,132,38,66,16,132,200,66,16,132,39,173,34,9,100,198,38,63,74,101,47,83,38,78,111,84,111,110,82,233,
122,127,217,74,82,229,41,118,151,218,139,218,201,238,188,194,16,132,38,38,66,16,154,151,19,152,78,32,241,98,163,212,55,221,244,78,232,222,223,68,39,8,132,33,8,66,19,217,120,122,153,74,92,165,40,217,75,
224,124,44,157,205,185,9,178,244,201,212,33,50,16,153,50,112,243,206,78,158,82,148,165,27,238,247,54,16,132,16,255,0,163,194,248,50,30,67,159,211,255,0,161,87,233,31,233,16,161,47,233,17,22,34,60,127,
157,78,33,9,197,246,66,113,9,211,201,232,188,210,237,246,66,113,74,82,148,189,34,229,46,95,101,41,127,137,247,74,82,255,0,12,33,54,16,130,67,68,216,66,19,213,75,196,33,8,52,67,230,139,69,21,237,47,174,
246,241,172,66,101,202,94,190,144,132,225,122,38,66,17,145,144,132,24,144,200,66,16,107,105,120,188,78,102,52,32,209,54,148,187,125,116,188,34,144,132,33,9,219,196,207,133,101,240,82,134,219,60,137,16,
98,66,153,224,168,126,170,92,165,41,114,237,60,250,234,199,130,16,157,198,121,19,41,6,132,177,178,139,35,60,255,0,187,228,89,79,164,67,131,68,32,145,7,136,132,200,184,79,137,235,185,8,69,180,165,47,77,
127,154,189,52,185,120,165,46,95,228,127,210,189,179,136,66,19,33,8,66,119,9,252,16,158,137,144,130,234,148,185,114,122,238,41,74,82,148,185,74,82,231,231,165,250,97,8,66,79,68,207,27,75,195,156,82,136,
101,245,120,47,20,108,165,41,111,72,185,8,74,66,16,252,200,76,132,216,126,226,101,41,74,86,121,33,7,143,254,116,151,251,147,38,121,32,212,241,137,13,120,219,205,41,74,82,245,8,76,153,56,155,31,84,76,165,
25,9,134,178,19,104,201,105,229,241,9,63,209,33,42,79,36,34,200,88,125,202,50,226,202,54,44,165,27,41,75,136,165,41,74,82,151,20,184,184,165,41,74,92,82,226,101,41,74,82,140,143,41,118,255,0,39,231,254,
36,234,16,132,254,213,252,20,165,246,210,244,246,229,41,118,151,248,223,112,132,33,8,77,188,193,174,39,31,165,60,158,120,190,114,148,76,184,185,121,74,82,151,211,8,79,82,62,20,108,78,13,150,31,189,79,
85,40,154,133,27,230,149,12,165,42,41,74,180,98,148,165,41,74,87,170,16,104,157,65,34,19,33,30,32,132,32,144,255,0,248,50,57,158,69,255,0,72,95,132,99,199,232,214,44,165,202,82,227,120,77,30,24,226,42,
255,0,4,225,244,95,7,130,45,32,124,106,89,138,81,179,201,231,31,30,79,39,156,248,84,84,95,68,216,66,19,218,230,120,219,253,243,217,8,78,38,66,16,132,216,66,98,30,65,234,101,41,125,23,255,0,2,148,165,41,
74,82,148,185,125,119,223,8,66,19,191,222,38,254,144,107,103,170,226,98,121,74,47,33,188,89,121,162,112,168,165,215,180,165,41,75,204,33,9,179,33,17,22,222,31,43,41,74,94,83,230,151,187,177,247,8,66,50,
116,136,121,60,193,16,132,33,60,158,39,23,97,9,168,130,248,120,37,196,32,151,140,132,68,68,67,130,77,140,66,82,34,16,213,141,10,138,202,202,202,43,255,0,70,235,207,207,165,111,244,73,17,30,15,209,149,
21,20,188,194,20,255,0,228,180,165,41,74,82,250,94,65,47,69,203,196,200,66,100,33,9,212,200,78,33,63,240,97,61,43,38,190,151,15,150,34,151,217,74,82,237,46,82,148,189,94,239,247,76,158,137,235,165,225,
122,111,11,208,202,38,82,229,41,70,249,252,244,76,125,66,115,74,62,110,82,250,175,18,141,19,47,52,189,77,132,33,7,236,132,33,8,76,124,94,25,249,233,165,209,10,93,35,129,228,94,15,146,43,31,30,11,252,23,
39,19,132,198,242,83,224,251,153,8,66,98,132,67,44,46,94,161,6,82,148,165,41,247,47,41,16,132,244,66,115,50,100,31,48,158,200,66,16,132,246,82,148,165,212,92,79,41,74,82,151,248,46,95,229,132,33,8,66,
16,159,203,8,79,83,202,82,229,202,55,195,197,147,47,166,148,165,214,94,103,190,19,155,144,131,93,206,105,122,132,39,87,180,93,153,50,111,220,132,33,8,76,94,113,226,108,36,66,34,16,131,92,79,171,151,168,
50,35,198,191,71,156,152,225,224,101,133,103,147,206,82,137,255,0,194,149,250,40,223,112,156,194,115,56,132,35,71,150,65,108,33,8,78,41,75,151,19,37,30,194,119,8,66,13,16,158,132,55,212,38,67,193,224,
136,240,54,138,92,189,78,33,61,119,41,118,151,135,255,0,133,56,132,33,8,66,118,150,78,38,175,230,190,234,82,237,46,95,93,254,216,66,122,40,132,226,100,38,222,105,74,94,95,83,39,83,152,37,235,131,66,91,
53,151,95,195,240,69,41,74,82,148,165,18,245,220,185,74,54,82,241,127,11,194,200,66,31,61,16,132,39,240,71,137,37,244,240,79,82,115,23,22,45,157,37,224,153,9,170,11,115,225,125,23,20,165,69,89,8,76,165,
46,206,41,75,197,41,75,183,111,162,148,185,114,148,165,234,255,0,45,41,118,127,12,39,254,42,246,94,110,82,151,248,32,215,16,132,33,61,80,132,217,147,137,197,41,117,114,156,80,165,41,74,95,35,126,74,82,
148,165,60,245,9,235,253,199,195,207,222,108,245,33,15,233,114,148,186,135,149,20,99,123,74,82,141,148,76,188,82,151,105,74,82,250,47,119,213,54,151,38,248,216,76,157,92,130,67,26,229,174,82,25,124,136,
165,203,151,41,74,49,68,126,13,12,185,97,74,82,148,165,41,74,203,180,165,41,74,82,228,202,82,148,165,41,75,151,248,105,127,138,100,255,0,203,124,210,148,165,41,74,95,85,218,82,148,165,41,114,148,165,41,
74,82,151,168,78,225,9,235,67,39,119,215,74,95,5,41,120,190,218,81,178,250,230,33,255,0,45,218,55,114,9,16,158,171,159,152,177,191,193,60,76,185,74,81,180,125,226,16,158,232,66,8,153,8,68,120,25,53,42,
243,193,9,4,120,199,183,33,50,16,130,90,214,124,41,125,136,163,202,81,62,169,127,137,161,139,155,233,132,217,196,38,61,187,9,252,243,209,9,175,187,253,240,158,59,67,202,95,228,131,234,19,215,8,76,153,
50,106,27,226,127,224,222,38,206,33,58,132,33,8,126,115,50,16,132,201,239,165,31,55,215,224,241,151,171,204,197,220,39,72,189,82,141,137,162,148,165,216,77,155,79,27,8,70,70,36,53,139,201,4,49,12,79,104,
242,148,156,65,174,30,38,144,216,166,68,120,60,30,10,120,19,41,231,170,82,250,230,120,245,67,224,113,201,120,132,33,8,200,200,243,207,167,207,162,16,155,8,66,30,88,131,38,66,16,132,33,49,144,132,33,8,
77,132,38,65,227,245,66,112,137,213,4,73,228,237,101,219,233,132,33,8,77,130,31,182,19,97,8,79,82,254,42,95,69,41,74,82,237,246,206,225,56,100,244,220,132,196,54,174,207,226,92,47,101,226,16,132,26,68,
33,16,146,200,77,156,76,67,203,151,105,74,82,151,13,137,143,231,51,62,103,145,30,73,140,140,132,38,124,41,74,125,34,198,136,76,53,56,52,215,220,162,133,255,0,17,74,38,198,202,86,82,247,89,119,207,169,
16,132,157,222,97,50,16,152,163,254,136,143,8,168,163,116,92,81,228,196,225,74,82,148,165,230,16,132,33,9,171,33,8,78,111,16,156,84,94,166,210,251,231,52,187,114,246,161,23,201,75,211,254,132,62,175,182,
151,217,74,82,229,41,74,82,148,165,245,190,166,207,84,38,66,107,254,68,50,112,178,148,165,202,95,92,244,70,66,19,33,8,81,4,63,162,227,104,103,157,132,32,215,248,50,121,36,41,244,73,17,30,58,252,200,76,
132,200,184,136,132,33,61,19,41,74,55,137,148,165,41,70,247,192,154,32,242,40,162,138,27,82,202,101,47,131,103,197,101,101,101,101,41,75,136,185,56,83,34,60,122,35,36,218,92,132,200,76,66,19,34,60,30,
40,166,120,207,2,149,15,82,216,248,125,194,16,241,179,87,44,72,132,230,151,134,186,93,79,117,254,155,253,20,165,41,74,82,148,165,46,92,165,41,74,82,151,41,74,82,148,108,165,41,74,94,47,170,106,200,78,
33,54,122,174,37,202,31,130,234,101,41,74,93,185,75,148,165,41,75,183,41,74,92,165,41,231,134,253,48,132,38,194,9,16,148,240,120,20,100,131,33,9,227,143,4,13,8,136,83,12,104,148,240,33,25,56,92,37,143,
239,47,254,117,226,123,83,215,11,148,165,207,39,147,206,207,25,17,227,41,120,69,39,105,113,41,15,254,143,254,136,181,122,105,228,141,254,105,8,124,43,71,255,0,5,47,167,199,194,63,194,163,193,19,253,194,
71,143,214,85,248,84,80,223,248,121,20,71,254,142,149,150,148,79,193,113,74,202,86,81,76,168,165,41,69,40,159,108,165,46,210,250,219,41,74,93,165,41,74,82,148,165,41,74,82,148,186,216,255,0,134,255,0,
230,66,100,33,9,220,216,66,19,33,61,16,158,216,66,16,132,39,165,177,118,165,40,222,82,179,206,193,15,87,19,39,178,16,152,150,34,119,114,19,168,66,98,103,194,237,207,3,104,162,76,104,72,249,240,110,142,
20,172,108,86,121,60,149,148,60,60,10,43,42,20,242,125,13,166,252,124,197,250,88,207,2,150,227,248,35,232,252,101,22,254,98,77,250,97,56,132,39,62,51,193,70,248,132,33,224,153,8,66,103,140,242,77,132,
33,8,65,30,120,140,255,0,247,32,144,252,9,255,0,193,149,127,133,95,168,161,96,154,252,29,31,254,141,164,121,100,111,5,31,63,73,56,114,100,40,162,50,50,18,20,165,219,227,105,125,23,208,138,54,82,148,165,
60,30,58,165,41,74,82,229,27,234,148,165,234,250,174,95,252,152,66,19,211,8,66,98,19,136,66,117,56,156,66,108,200,77,132,33,8,66,13,16,132,254,74,87,180,165,246,82,148,188,66,16,132,39,19,210,187,132,
33,6,137,144,155,8,66,16,132,16,209,54,148,79,24,150,34,249,41,88,202,65,15,22,178,19,198,68,49,30,11,224,130,81,98,87,31,13,147,41,121,136,132,68,135,140,249,179,155,151,175,57,8,66,16,132,33,60,113,
121,93,34,13,19,199,158,22,182,132,82,137,149,21,14,14,125,19,242,83,195,200,47,24,210,124,36,231,255,0,98,69,255,0,75,255,0,6,202,81,245,74,82,148,185,59,132,32,151,159,35,148,132,39,8,240,56,120,31,
186,250,239,240,194,127,77,254,40,76,157,221,155,9,147,23,51,217,63,162,243,120,187,74,92,163,244,66,19,137,212,226,100,39,11,184,66,108,234,119,123,132,200,200,71,204,38,81,151,90,202,38,37,227,108,198,
200,27,202,203,180,165,41,74,82,148,165,19,19,41,75,7,133,41,113,108,39,130,111,158,107,246,44,165,230,9,31,186,251,157,34,148,117,228,18,103,204,189,210,148,126,10,38,94,2,8,209,6,163,46,202,82,149,141,
229,112,165,197,229,143,239,190,16,140,152,209,63,162,19,249,102,183,252,111,249,233,125,119,209,120,108,175,130,151,138,82,151,41,74,82,148,165,41,74,82,148,165,41,75,151,105,74,82,151,213,58,115,209,
57,132,230,16,132,33,8,65,162,16,132,33,49,100,33,8,52,65,34,77,131,200,53,147,47,62,72,37,140,101,68,17,116,109,189,132,33,248,66,16,79,243,27,197,126,169,233,156,125,121,25,63,157,92,252,41,75,75,183,
180,82,237,41,74,82,148,76,165,207,25,81,74,47,251,220,100,100,101,78,46,196,85,34,67,41,118,115,7,144,132,33,31,19,38,67,201,50,105,63,233,8,66,16,140,143,35,33,8,70,70,70,86,38,32,146,197,95,136,242,
255,0,5,141,34,35,194,127,233,127,230,74,78,102,194,16,131,33,8,66,16,132,38,40,63,36,38,194,19,102,194,16,155,8,66,180,132,26,226,127,60,246,47,84,33,118,242,134,249,123,74,95,236,191,201,58,132,38,194,
16,132,33,8,66,19,106,42,42,42,27,41,81,124,148,165,41,74,94,81,6,202,121,18,175,5,143,20,165,19,185,243,28,39,9,141,249,41,231,22,178,250,41,116,165,101,126,230,63,98,200,79,35,196,134,39,228,240,120,
39,249,151,97,6,183,199,19,181,179,41,74,121,60,145,147,129,17,225,126,149,34,166,70,148,251,179,191,5,80,171,41,79,2,104,240,200,135,23,193,66,162,162,151,105,74,92,186,188,184,68,120,60,23,33,24,155,
253,33,8,121,126,158,39,21,22,98,148,168,168,171,252,63,248,197,112,82,159,72,37,164,33,8,76,36,66,16,130,136,109,62,38,222,97,51,193,113,86,25,88,125,98,13,16,135,194,148,111,23,33,9,176,107,168,66,16,
132,199,212,254,42,82,151,38,82,245,75,151,213,8,79,252,104,78,24,250,132,33,9,194,131,19,68,105,120,31,24,165,41,74,82,148,165,27,195,101,41,74,82,148,76,165,40,157,33,182,207,34,248,76,94,8,51,193,49,
189,253,31,147,243,152,65,248,21,21,16,52,46,151,248,211,254,10,93,76,187,243,63,50,137,149,203,138,82,148,79,111,141,132,60,228,203,194,240,82,151,254,23,254,20,191,233,103,194,177,210,16,249,169,56,
223,225,125,19,170,127,220,121,68,242,16,132,40,66,60,74,144,52,33,5,67,95,244,126,15,204,152,132,218,103,158,26,23,79,36,82,178,178,178,178,148,76,250,33,61,43,105,122,165,41,75,190,17,74,81,178,243,
56,100,20,33,248,95,56,168,168,108,183,30,94,62,144,132,244,66,122,105,113,108,216,66,19,39,48,126,180,210,67,117,255,0,84,33,50,19,137,147,97,50,19,136,66,16,132,217,143,23,52,186,217,75,136,228,27,101,
101,40,162,148,162,198,95,236,74,69,23,134,60,6,217,89,240,54,87,150,235,202,144,172,165,98,9,227,11,232,197,12,191,133,243,255,0,53,14,145,226,17,144,132,33,8,70,52,66,120,33,57,165,16,211,226,46,87,
202,87,247,41,75,212,32,241,253,196,210,11,203,131,92,44,73,149,143,2,153,39,248,51,207,248,70,66,10,54,66,253,199,143,194,149,148,86,28,41,72,43,47,252,63,60,17,145,149,178,30,69,254,153,4,201,175,212,
149,99,83,164,46,207,244,241,205,19,41,74,82,158,69,147,154,121,234,242,156,196,202,125,30,81,7,162,185,6,242,151,38,249,215,148,190,184,66,19,171,165,41,74,82,250,233,75,255,0,183,75,237,153,59,153,8,
66,16,132,33,8,65,34,21,164,210,105,8,66,16,132,33,8,66,16,84,47,12,162,121,254,134,239,182,137,158,32,152,200,110,255,0,51,83,180,134,167,73,81,237,216,65,46,216,159,11,30,86,92,156,175,68,38,66,16,132,
18,196,38,63,3,19,207,133,255,0,5,225,127,220,104,73,35,198,33,67,167,147,201,74,92,187,17,17,17,4,148,207,27,70,252,237,46,81,139,25,243,223,30,30,138,82,148,190,138,82,148,172,172,172,172,172,172,165,
41,74,82,241,74,82,151,126,115,75,221,46,252,19,45,214,92,251,221,219,180,76,165,41,74,82,255,0,229,82,148,165,41,74,82,148,165,41,74,82,148,165,41,114,148,165,41,74,82,148,165,47,182,16,132,230,16,132,
33,8,66,19,97,49,46,233,118,227,126,185,171,47,145,15,129,234,244,62,127,63,240,38,160,221,233,56,62,151,185,50,226,88,139,10,134,163,157,60,132,33,9,147,80,252,20,188,183,180,165,133,164,223,25,248,81,
159,131,202,82,148,165,198,202,138,82,229,27,218,92,165,46,38,82,148,67,120,253,51,103,174,16,156,79,68,38,194,123,233,111,240,220,165,41,120,165,41,75,180,191,201,4,134,137,235,132,35,33,68,196,38,144,
105,145,147,167,239,132,254,72,79,68,39,243,222,60,107,244,94,94,193,237,37,194,80,250,196,198,47,254,163,244,47,190,70,44,83,247,209,227,102,82,148,162,137,7,254,7,232,76,165,202,82,151,154,92,92,124,
47,135,145,240,165,41,75,138,82,236,247,161,249,254,121,147,137,212,39,166,16,132,226,241,57,104,72,159,243,33,8,73,176,132,33,8,78,33,56,132,39,245,194,19,209,224,165,41,231,33,10,14,8,66,16,130,18,32,
138,84,82,172,241,148,109,16,81,143,16,248,132,38,194,19,33,50,122,209,8,66,16,132,254,123,233,190,170,38,95,93,41,241,148,243,159,117,13,149,143,255,0,109,10,13,114,156,30,47,164,240,77,153,54,16,132,
39,112,135,205,252,219,138,82,148,163,108,242,86,121,234,16,155,97,54,108,254,57,254,243,224,132,60,13,33,45,240,120,34,201,72,78,33,50,16,241,196,201,236,66,11,89,8,66,122,110,46,248,200,66,10,30,15,
24,217,79,164,33,9,232,152,200,66,100,39,112,158,203,235,171,40,138,82,158,30,82,172,186,125,230,148,175,166,191,241,105,74,82,229,202,82,148,165,41,74,92,165,46,82,151,41,75,252,247,211,69,228,156,53,
255,0,187,75,139,132,168,148,43,47,15,20,25,110,210,148,121,74,82,148,165,47,51,97,4,169,8,69,59,186,165,31,107,199,224,233,6,139,212,196,38,253,194,99,227,27,225,120,252,60,79,250,120,226,139,138,82,
148,165,202,92,163,125,195,193,54,151,211,248,82,228,238,147,152,35,195,39,169,56,153,8,242,19,70,136,66,112,196,66,19,97,54,19,39,48,130,26,32,209,8,65,34,16,131,90,249,184,250,173,30,73,203,17,124,136,
241,144,158,186,82,148,165,41,118,151,46,126,101,41,75,203,245,207,93,226,122,105,75,239,132,33,9,210,133,27,165,41,127,254,13,21,36,63,12,76,172,76,134,205,241,74,249,68,32,145,8,120,33,68,234,19,138,
94,97,15,156,66,33,36,65,33,51,28,16,240,120,163,125,194,11,38,39,23,97,49,246,183,233,8,78,97,54,148,165,19,240,82,148,184,180,187,75,183,104,153,74,82,159,10,93,165,19,41,63,122,165,47,166,148,165,41,
75,148,165,41,117,122,103,166,245,74,82,148,76,108,165,41,75,71,213,47,116,165,202,82,240,177,20,165,41,127,149,16,130,25,50,117,125,144,132,200,77,132,33,9,196,33,9,220,201,204,200,77,139,175,60,37,73,
143,83,31,255,0,193,66,19,104,153,74,33,227,200,66,16,68,100,242,52,143,3,68,60,141,223,162,157,175,37,16,132,33,15,2,148,105,31,63,75,254,13,17,17,17,31,163,41,72,76,191,195,203,250,121,69,229,237,41,
74,92,39,134,202,82,148,162,101,41,74,82,229,226,148,165,200,78,126,144,132,234,122,105,117,15,139,220,33,49,183,232,185,120,165,234,148,184,139,194,47,130,148,185,74,92,165,30,195,225,75,151,111,254,
4,33,50,19,105,75,220,33,8,66,16,132,33,8,66,16,132,32,151,48,132,201,234,132,217,232,191,193,8,66,16,132,38,78,46,190,91,233,49,249,230,123,47,254,12,33,50,16,155,9,204,200,66,19,62,122,87,115,111,63,
154,133,191,185,224,78,13,153,75,136,104,130,73,34,138,126,149,15,233,231,27,201,177,101,41,74,82,250,166,66,16,73,150,53,8,201,147,129,9,204,245,194,113,122,88,249,155,49,162,16,132,200,66,122,33,9,252,
83,248,238,222,166,66,106,158,153,232,157,66,21,164,193,162,66,68,26,33,49,255,0,224,210,151,255,0,9,113,9,168,123,75,197,230,141,227,72,188,92,165,202,82,255,0,224,66,100,33,8,66,16,132,210,12,252,201,
112,171,16,152,132,218,44,132,32,145,6,134,38,165,211,46,36,67,225,75,203,203,147,138,62,111,94,120,165,47,80,132,34,255,0,72,136,185,121,8,66,117,75,138,82,229,43,202,62,145,224,240,50,113,9,136,168,
111,41,114,148,76,185,74,92,38,133,67,159,133,19,255,0,74,38,54,138,82,148,165,41,74,82,151,143,59,8,66,19,97,8,200,66,19,249,167,182,151,248,226,25,8,65,34,9,113,113,44,109,20,108,108,127,199,224,121,
75,213,47,84,185,74,92,185,118,151,138,82,229,41,74,82,148,185,74,82,151,41,74,82,148,165,41,74,82,148,189,161,255,0,44,33,8,66,19,33,9,194,200,66,19,136,77,132,33,5,172,142,94,101,36,226,229,230,136,
241,169,137,148,190,155,202,47,23,217,8,53,164,33,50,9,16,74,141,19,97,4,134,137,212,33,9,252,191,10,84,84,84,82,151,41,115,206,249,60,158,72,242,50,16,155,8,65,33,164,120,38,33,50,34,16,154,145,24,200,
66,9,16,72,141,38,33,50,137,173,162,39,85,100,33,63,224,214,33,8,66,19,103,19,103,19,97,50,19,184,79,224,156,210,137,139,186,82,138,87,237,163,101,41,127,161,250,39,241,191,124,33,54,19,39,80,132,100,
33,8,66,19,33,8,66,108,39,79,137,235,75,138,94,40,153,74,92,47,35,230,20,66,144,168,76,108,165,46,95,93,203,10,82,178,231,146,236,254,63,5,68,44,252,98,152,133,46,214,86,54,38,67,109,178,248,130,207,27,
89,231,215,4,54,94,103,174,16,132,33,50,16,132,39,19,135,233,79,132,66,100,237,50,237,20,100,89,224,123,79,60,221,89,122,153,74,92,84,38,93,250,66,138,40,132,33,8,76,241,158,54,148,187,120,191,200,253,
9,226,240,41,127,134,16,132,217,234,132,224,76,132,68,38,66,16,132,33,9,204,33,8,79,68,33,8,77,132,39,241,66,16,153,56,132,33,56,156,50,251,111,174,248,234,151,41,125,62,125,233,107,33,9,205,201,204,244,
163,199,40,140,140,132,33,9,233,135,204,132,33,58,132,229,143,33,8,77,107,41,231,238,206,103,76,92,210,226,47,23,219,74,92,82,237,27,17,4,136,77,158,184,65,174,161,8,52,76,125,82,148,183,17,95,8,136,136,
52,66,13,113,81,227,33,30,144,157,66,122,41,117,15,153,255,0,159,75,194,148,88,253,207,208,248,190,185,147,213,120,165,41,74,41,7,148,165,197,41,74,82,244,189,48,155,5,237,158,152,65,34,19,97,9,224,156,
194,98,27,41,75,252,243,213,56,132,18,34,32,215,145,113,7,197,60,101,33,8,66,16,132,163,202,138,139,197,46,221,183,170,92,165,46,82,229,41,74,82,151,139,180,165,41,74,82,143,248,169,71,219,41,118,240,
132,202,82,151,164,65,162,98,112,165,41,224,168,240,82,137,140,184,216,242,113,68,19,68,98,241,8,79,92,212,66,16,132,26,33,63,170,151,248,111,51,41,75,180,165,219,197,41,122,165,41,75,197,244,223,5,41,
118,151,209,74,82,229,46,46,33,50,112,144,248,165,244,210,237,47,173,23,16,241,162,19,149,5,225,109,226,250,159,83,181,176,156,120,244,254,108,18,37,38,60,72,98,68,33,244,131,194,60,101,41,81,81,74,135,
200,27,241,244,165,230,241,9,252,19,97,8,121,217,252,255,0,157,93,188,82,237,46,210,241,74,82,229,41,75,194,230,117,75,148,165,47,83,33,9,197,41,74,82,148,165,47,178,237,41,79,194,255,0,224,93,165,41,
75,183,137,180,189,78,151,83,249,175,243,222,239,47,80,210,23,47,250,81,74,94,167,55,39,175,230,95,225,124,194,19,103,117,66,248,197,163,105,20,165,124,47,156,39,226,183,165,41,114,148,172,242,86,92,132,
200,66,100,33,8,66,19,35,26,100,164,33,9,169,16,156,67,243,184,77,132,34,34,60,122,146,234,115,8,66,16,132,33,8,65,34,16,132,32,131,98,50,100,33,8,66,16,157,214,82,148,165,41,75,180,165,41,74,82,234,99,
101,41,74,82,151,41,74,94,102,66,103,131,193,224,240,84,84,55,205,255,0,195,152,242,106,229,50,148,111,209,68,199,151,211,124,127,2,33,53,147,82,38,194,19,209,59,132,216,66,122,167,112,132,223,30,137,
252,19,167,252,175,155,179,217,74,94,27,234,247,61,48,153,9,196,230,229,46,42,41,68,202,83,193,227,136,45,132,26,200,200,248,132,231,206,221,132,33,50,16,132,230,16,152,152,248,188,82,148,165,41,74,81,
31,60,90,124,40,217,74,138,143,25,224,132,244,194,16,132,245,253,207,207,225,165,41,75,197,255,0,201,132,33,54,100,234,123,33,8,66,16,132,33,8,66,119,50,127,34,22,94,161,54,19,33,56,191,209,226,100,38,
206,33,51,198,194,113,50,118,253,139,219,120,185,75,148,190,152,82,248,41,68,255,0,130,100,230,16,132,38,126,115,74,84,94,33,9,171,238,46,151,132,241,50,137,227,201,176,75,95,165,50,148,165,226,241,243,
137,204,18,38,194,16,132,33,9,234,132,33,224,139,105,68,212,34,39,23,97,8,66,114,156,202,182,16,132,31,178,108,33,54,127,4,201,144,132,254,58,81,191,234,67,98,244,78,167,240,66,16,132,39,170,241,118,228,
254,185,147,139,233,165,47,162,151,47,166,234,230,16,132,33,61,47,209,120,155,25,9,139,248,41,75,175,110,188,158,181,176,240,85,147,33,59,131,92,220,165,46,82,241,122,132,39,174,148,186,138,167,43,39,
52,185,9,204,219,220,39,166,250,32,135,159,73,183,40,201,63,28,207,228,94,154,92,185,68,199,233,132,39,112,132,245,194,16,157,174,105,114,148,185,114,148,185,74,82,151,137,233,132,244,120,254,43,213,244,
210,148,188,120,46,82,148,165,41,75,219,230,255,0,4,217,232,184,139,211,226,16,132,33,25,8,76,132,234,178,247,8,143,7,129,165,196,33,57,241,137,83,224,181,125,28,20,25,80,218,200,136,136,77,165,41,70,
232,185,243,148,165,46,34,158,94,94,169,75,148,185,118,229,41,74,82,250,188,21,30,57,165,41,74,82,250,47,119,31,174,115,8,77,76,132,33,8,67,225,121,78,99,254,27,218,254,104,46,145,74,82,148,76,185,75,
169,148,165,202,82,151,209,74,82,136,121,125,147,170,82,237,41,74,82,151,41,74,82,148,165,46,210,151,154,82,247,127,134,117,123,132,230,16,132,33,53,108,246,194,115,50,33,206,102,78,105,74,82,148,167,
209,149,30,49,237,46,207,67,230,148,165,223,5,42,197,41,89,74,82,151,41,74,121,27,124,194,19,215,9,220,38,204,132,38,33,8,66,16,132,38,66,19,38,79,234,188,210,247,125,20,191,193,56,127,207,61,55,46,95,
92,226,148,165,41,114,245,121,188,223,252,185,237,132,33,9,179,136,76,132,200,66,122,225,8,76,132,244,206,167,182,16,156,194,16,132,33,50,127,13,47,245,199,176,155,8,66,16,132,196,33,8,76,52,145,225,111,
222,161,50,122,175,55,169,236,94,137,196,226,16,132,245,66,100,33,8,79,226,165,254,26,93,184,223,248,82,148,165,226,19,249,175,244,94,151,162,16,132,33,8,76,132,33,63,130,19,136,66,19,213,9,212,38,206,
225,8,66,122,161,8,66,100,33,8,66,16,130,88,104,126,214,137,144,92,194,99,201,176,130,30,44,93,49,115,75,205,69,234,16,156,66,115,115,199,119,139,212,223,28,34,148,190,144,10,197,41,94,121,230,16,132,
33,243,215,60,122,33,9,149,63,204,89,75,255,0,10,93,89,40,212,22,82,148,164,223,220,104,93,194,16,132,33,8,66,13,115,74,82,148,190,168,66,19,219,121,190,151,235,156,194,108,230,108,39,254,228,39,243,93,
187,121,188,47,124,238,159,73,139,175,164,23,209,226,84,106,50,112,251,165,219,205,152,98,151,139,196,234,245,74,121,207,156,204,143,124,148,165,41,74,82,226,151,185,196,215,59,140,153,50,151,23,219,58,
153,119,207,240,205,24,144,132,33,59,240,20,99,68,196,202,81,236,226,60,158,139,148,165,19,41,74,82,150,234,67,33,8,66,19,137,192,154,139,179,32,208,177,34,16,157,78,25,49,228,201,221,238,246,189,20,191,
249,244,191,195,61,212,165,226,117,125,23,41,118,148,165,41,74,82,148,165,41,74,81,50,148,108,165,41,74,82,137,148,163,240,54,54,82,151,214,250,190,154,95,93,254,27,147,102,66,19,33,55,199,163,193,227,
143,27,81,3,194,178,248,41,244,132,217,179,134,46,39,19,33,9,147,16,132,237,99,203,5,170,143,33,30,35,193,53,162,101,202,62,169,114,20,67,244,210,147,102,76,130,68,60,20,123,50,12,131,17,97,75,139,144,
132,33,8,76,156,194,16,132,244,189,165,216,66,9,108,39,170,255,0,76,254,41,233,132,31,242,166,95,92,254,73,234,153,54,19,212,248,126,114,115,74,94,159,244,223,92,226,16,158,201,212,244,66,16,132,33,58,
158,136,36,134,185,156,47,93,41,74,54,82,136,76,172,188,194,119,224,139,99,40,153,74,82,148,89,74,82,248,63,59,156,209,10,82,250,159,52,189,120,212,202,54,82,148,111,209,114,148,165,226,109,197,46,194,
16,155,8,77,132,212,136,65,161,139,31,182,255,0,21,229,237,246,165,234,92,76,79,231,132,244,194,117,8,66,19,217,125,40,124,207,93,254,185,144,132,230,19,248,60,21,21,77,157,76,155,61,47,60,30,61,147,133,
147,82,31,112,132,199,202,30,194,15,209,10,82,151,41,75,202,124,220,165,41,74,81,50,148,165,41,74,92,165,196,120,25,57,165,41,74,120,46,190,19,41,74,82,151,47,47,89,122,140,72,155,79,164,223,133,203,210,
46,82,242,245,114,101,41,96,223,240,66,19,139,255,0,144,137,232,163,238,16,157,206,103,240,82,148,165,46,66,16,155,59,157,76,156,66,113,50,123,161,61,147,82,246,92,111,33,8,79,76,33,61,87,217,120,157,
194,115,57,130,92,190,174,92,163,203,148,111,105,114,148,165,197,43,43,202,83,207,55,186,94,161,8,66,16,100,39,166,148,108,185,61,84,165,202,82,229,226,148,165,41,74,82,226,23,16,165,226,148,165,41,74,
82,250,214,222,41,70,203,180,165,41,125,168,127,248,8,127,248,144,153,56,191,195,57,165,41,125,19,184,66,117,127,240,111,11,97,49,34,108,26,39,20,185,71,136,107,170,93,171,151,213,41,75,151,33,56,157,
210,143,209,240,165,202,94,230,206,33,227,154,82,188,132,217,204,39,241,36,69,196,31,83,30,205,152,184,125,63,116,33,56,73,180,52,214,76,165,41,75,180,165,41,125,52,165,41,74,82,247,74,82,241,125,23,186,
82,148,165,245,205,241,252,55,220,250,132,33,8,93,185,75,148,165,47,172,5,195,22,210,148,165,47,83,170,94,41,75,148,187,74,82,151,221,63,134,16,132,33,8,66,109,41,74,82,148,165,41,121,165,234,229,133,
41,75,205,47,166,115,74,94,47,55,217,9,233,135,131,193,227,209,8,66,19,33,8,40,120,164,244,222,103,246,66,16,157,204,66,98,19,248,33,8,121,60,189,132,225,13,127,85,255,0,193,191,248,147,33,9,148,165,41,
116,162,99,99,124,223,100,254,138,82,148,165,234,245,75,221,202,82,151,103,115,32,253,119,41,74,81,178,141,148,163,101,202,82,244,150,94,97,20,39,165,38,198,39,177,231,157,185,114,245,61,104,159,248,18,
141,66,19,31,112,158,72,79,93,202,94,46,248,34,231,198,94,159,20,79,152,66,16,132,33,8,66,112,33,9,148,165,234,19,110,66,16,132,26,38,194,16,132,33,8,66,16,159,249,143,251,22,82,228,255,0,193,132,219,
176,159,213,57,158,165,180,165,41,79,7,140,191,215,63,130,122,34,26,19,63,54,16,153,8,136,66,122,191,231,116,190,152,79,85,201,158,72,78,169,95,115,35,21,207,39,146,179,233,8,120,227,207,44,72,132,213,
176,156,66,113,9,197,226,151,83,131,215,149,137,148,165,230,237,40,154,46,120,229,158,6,136,66,16,132,200,66,16,153,38,162,100,33,25,30,95,92,34,88,225,8,66,16,132,226,19,255,0,89,50,148,165,46,76,157,
207,252,8,76,153,8,78,33,61,116,188,82,151,152,66,115,6,35,199,84,185,114,237,47,173,30,50,16,131,60,113,50,123,235,41,74,242,248,41,74,82,148,165,41,122,132,245,79,85,46,94,102,67,230,211,239,105,15,
17,114,151,41,247,83,60,13,161,52,84,81,178,16,155,75,148,165,46,94,110,194,23,79,3,133,41,114,151,111,48,132,244,92,165,41,113,16,132,218,82,148,184,196,208,218,229,120,167,239,51,154,94,175,48,240,120,
60,108,196,71,132,125,32,196,38,146,115,8,79,236,75,250,231,43,249,161,63,178,16,106,108,245,194,122,89,125,115,38,194,122,231,84,163,238,16,139,221,227,39,178,113,9,205,247,92,72,153,50,229,203,179,24,
196,81,23,138,66,16,152,216,166,84,34,141,148,185,75,180,165,17,115,198,204,75,41,74,92,73,188,76,165,41,74,82,229,41,125,41,20,111,175,131,197,179,38,34,229,244,41,6,239,31,49,74,82,148,185,74,82,229,
226,122,238,61,162,101,41,114,148,165,210,151,138,95,228,94,149,253,9,16,104,153,125,51,255,0,10,151,214,132,55,120,111,153,194,26,201,234,127,248,111,19,47,20,189,207,69,230,19,33,54,115,127,142,226,
88,190,12,158,138,82,151,175,5,212,202,82,236,201,176,132,33,8,67,207,55,46,92,185,74,92,185,75,252,20,165,218,94,105,114,234,101,41,74,82,148,165,41,75,180,190,152,65,247,59,124,78,175,190,117,127,181,
106,216,66,122,153,6,137,136,66,19,16,104,241,235,165,238,229,244,210,151,249,47,87,105,121,67,218,92,82,148,165,41,75,168,127,199,63,149,23,47,116,165,41,74,95,85,47,20,165,244,222,105,122,156,82,143,
254,41,127,224,246,108,217,145,158,114,228,32,149,32,201,207,131,199,87,143,5,254,100,120,25,8,62,223,254,93,41,120,165,41,74,82,229,41,74,92,185,74,82,247,125,20,191,209,79,30,165,197,46,223,77,41,74,
83,224,184,167,130,204,249,139,222,94,139,252,107,249,33,9,196,26,33,8,66,113,63,142,255,0,98,68,60,30,61,243,219,9,196,244,95,84,39,156,130,68,38,79,93,41,244,132,32,209,16,217,79,61,249,201,144,132,
38,76,95,248,144,153,56,132,216,66,13,100,33,8,66,16,132,38,66,98,16,132,219,220,33,50,108,33,9,144,132,38,66,16,132,196,230,108,31,166,119,8,78,146,39,246,210,151,211,8,66,16,159,249,116,191,207,50,151,
47,166,241,61,9,151,47,174,16,155,54,16,132,38,76,132,33,8,66,19,137,183,248,233,121,184,145,8,66,100,244,65,34,19,20,42,72,101,41,120,187,123,186,90,126,83,193,79,186,223,112,135,143,210,162,162,162,
148,168,165,207,29,169,204,38,79,92,225,137,194,235,23,165,19,38,196,68,66,119,80,223,48,132,33,8,65,44,132,33,56,120,60,98,216,120,30,46,41,79,7,143,108,254,164,188,15,82,39,246,82,148,165,41,73,253,
83,97,63,245,175,166,122,97,9,215,129,162,16,108,165,40,215,141,165,27,41,74,95,125,230,100,33,8,66,16,153,61,16,155,8,66,16,153,102,41,224,168,111,184,79,76,201,196,245,210,151,33,54,16,132,244,46,87,
162,16,91,114,242,151,162,127,211,207,41,233,228,82,148,165,41,74,82,148,165,237,34,19,41,74,82,237,41,125,148,165,46,94,225,8,66,16,132,33,8,66,123,47,170,185,253,254,61,55,248,103,243,66,127,36,234,
16,132,226,16,132,254,8,50,16,132,201,197,47,55,41,75,143,107,46,66,19,137,204,244,67,230,66,17,19,16,155,114,13,228,226,148,241,233,68,226,82,34,98,16,141,144,132,33,53,144,133,26,201,145,30,54,101,200,
66,16,156,125,33,227,220,207,223,69,46,92,185,86,55,159,7,196,217,143,154,94,26,200,66,16,156,79,93,226,148,165,41,123,158,185,196,38,194,16,132,39,84,165,41,74,82,231,142,39,254,60,255,0,212,191,248,
20,165,219,148,165,238,16,132,233,127,226,94,159,84,168,180,176,84,82,137,151,170,81,175,68,245,82,149,9,162,246,248,132,33,50,19,33,9,50,15,46,66,8,176,165,69,240,40,55,125,141,229,230,115,75,235,130,
231,225,74,82,151,213,75,181,13,151,171,236,189,66,108,39,87,33,50,101,41,79,7,140,241,158,8,66,229,25,6,150,223,125,47,169,127,36,226,109,254,123,180,188,82,247,120,185,74,82,255,0,5,47,166,250,230,210,
237,202,82,151,30,82,151,223,61,115,187,197,219,180,94,95,129,248,69,233,236,32,214,92,248,139,192,184,162,126,70,198,241,50,148,165,41,75,170,20,76,167,136,125,30,252,43,8,82,137,151,82,202,61,167,210,
164,82,148,165,241,219,153,8,161,7,50,229,69,33,18,69,41,74,92,165,46,82,151,209,120,156,82,229,47,242,222,47,175,199,19,209,74,92,165,234,98,106,101,238,239,220,165,41,75,148,185,227,33,8,180,155,59,
132,234,100,38,206,33,9,204,33,50,19,33,49,250,239,246,33,255,0,233,191,227,190,54,229,41,74,82,148,165,41,113,166,191,157,54,138,63,127,210,19,38,66,16,157,36,63,30,139,149,151,168,37,120,72,179,43,46,
81,49,178,142,174,188,30,61,94,79,39,158,124,21,30,54,17,17,13,121,33,7,255,0,8,79,103,207,228,159,232,255,0,178,250,233,74,92,165,41,74,38,82,148,165,41,121,165,203,180,187,74,82,151,75,236,163,244,210,
151,139,143,148,53,205,218,60,156,204,94,153,139,153,204,38,194,9,15,16,255,0,244,223,48,158,155,252,23,138,54,223,244,76,132,39,9,19,39,240,222,33,8,66,19,217,16,222,166,191,79,254,7,219,203,183,184,
40,82,165,137,159,252,22,158,17,118,247,74,38,82,151,47,243,206,161,49,13,223,76,245,194,19,250,38,79,231,159,195,74,82,245,75,148,165,238,148,165,41,74,94,105,75,195,88,137,169,118,150,181,253,243,221,
63,154,16,132,246,78,167,244,78,103,240,249,226,61,165,254,139,219,123,8,66,19,137,207,141,171,187,215,158,169,80,223,244,223,108,33,9,176,66,70,137,72,73,70,146,27,71,129,194,100,32,244,66,50,16,126,
114,34,115,6,182,16,136,255,0,232,107,42,218,94,33,17,8,76,132,238,16,159,193,75,175,255,0,50,235,229,50,140,165,196,136,66,19,88,184,159,255,0,53,61,83,209,57,132,254,123,212,230,123,39,244,66,9,19,138,
94,225,8,70,71,254,17,144,143,213,8,66,16,132,217,147,184,66,108,226,122,160,150,33,24,155,67,240,242,51,74,155,20,253,103,135,248,125,68,116,141,22,63,35,161,105,81,81,74,95,111,130,143,97,8,66,122,127,
253,25,9,171,154,82,148,165,41,74,93,132,230,122,39,254,149,224,92,82,151,41,74,95,85,218,82,243,120,191,249,84,191,205,8,76,158,137,147,184,77,189,79,229,156,92,165,255,0,199,250,184,135,142,233,74,38,
92,82,151,96,217,81,81,81,224,240,68,66,98,84,156,44,68,201,204,33,23,224,252,63,68,234,31,10,65,224,221,227,232,95,30,114,16,125,112,157,254,122,82,141,255,0,226,66,16,132,33,4,33,56,26,39,247,207,239,
74,141,127,101,255,0,207,190,164,202,38,50,151,97,9,235,72,131,254,89,233,165,35,244,111,217,127,138,19,153,219,121,247,164,37,254,158,49,181,197,226,16,72,156,36,52,36,136,68,67,232,132,52,76,123,9,136,
153,255,0,72,76,243,151,41,79,5,69,91,68,231,209,177,141,222,126,199,4,148,248,87,43,249,111,190,117,59,157,66,115,56,165,46,93,165,41,70,246,16,158,232,47,252,152,38,55,239,191,213,8,66,115,8,77,132,
39,83,213,127,131,206,66,50,50,229,244,120,33,4,182,16,72,106,115,121,165,41,74,82,151,155,232,165,247,222,38,194,19,39,16,132,33,39,15,97,4,33,17,224,104,131,89,49,101,22,63,85,202,241,74,92,81,138,43,
152,165,69,84,165,41,74,82,148,165,41,125,16,94,10,125,100,143,232,221,254,5,191,73,202,247,206,161,61,77,148,184,160,225,8,66,19,110,94,41,75,234,67,196,136,52,66,16,152,253,15,105,75,176,132,39,246,
248,60,112,223,243,194,127,13,41,74,82,245,121,187,75,180,165,47,105,16,158,213,148,165,47,140,79,168,76,72,140,144,250,60,184,156,92,194,127,12,33,243,152,66,13,109,216,66,105,224,78,169,74,82,148,168,
184,90,41,75,191,70,134,178,19,41,75,151,95,16,154,182,13,16,132,38,26,32,214,33,25,8,121,33,25,8,66,100,237,122,124,13,242,153,81,113,191,226,190,143,30,200,66,117,57,191,193,125,16,132,196,26,234,151,
136,81,23,153,173,113,61,151,221,8,66,123,166,161,99,254,41,212,247,194,19,38,77,159,210,152,223,242,194,16,162,99,101,203,224,165,40,153,114,151,39,126,79,36,123,70,253,23,181,172,132,33,5,180,165,246,
174,38,210,151,20,80,38,125,39,138,84,176,218,40,196,94,34,34,33,8,65,255,0,194,9,69,231,42,41,113,74,82,237,66,66,161,186,85,138,92,82,148,189,82,151,255,0,21,36,242,70,26,158,155,151,170,82,245,75,239,
190,203,151,41,114,251,111,11,30,183,205,215,252,111,249,223,254,122,244,207,252,171,205,202,82,250,35,47,84,165,40,250,153,114,243,75,139,200,203,139,185,232,157,161,241,8,66,119,127,239,166,241,74,82,
234,209,148,165,231,206,70,127,211,34,223,210,79,164,226,100,234,60,66,16,76,243,101,23,200,66,16,159,240,136,105,13,63,227,162,103,224,217,127,174,151,110,95,69,47,162,148,241,180,165,46,82,251,150,82,
250,175,241,161,255,0,29,201,227,31,240,79,239,95,205,8,79,76,33,58,158,217,236,121,124,78,153,9,144,132,245,78,97,5,224,121,56,176,165,41,74,82,151,39,48,132,216,66,19,149,238,157,77,165,226,8,52,71,
254,19,41,74,45,94,79,57,5,227,31,166,148,165,41,113,49,112,237,155,43,40,79,74,83,224,109,178,255,0,50,177,36,71,250,127,244,68,76,37,147,219,63,241,214,40,140,140,156,174,169,120,191,220,135,233,68,
33,54,109,47,240,220,191,249,80,131,68,39,16,155,53,147,47,162,98,19,16,132,39,170,108,234,115,61,115,151,147,185,147,103,170,19,155,151,209,127,138,19,39,115,11,9,148,108,172,191,255,0,9,89,74,82,148,
167,131,199,49,17,16,158,255,0,4,226,108,39,16,130,88,132,33,229,162,66,31,10,84,84,120,99,201,195,69,136,66,49,167,183,255,0,5,33,234,233,50,151,152,66,108,39,169,255,0,109,202,82,229,47,107,41,125,119,
80,253,169,150,148,165,47,19,250,97,9,139,136,78,214,207,67,247,188,155,8,79,224,132,226,235,20,41,74,93,188,133,41,122,132,39,182,13,79,68,35,40,143,72,66,104,196,33,63,245,167,47,18,60,99,226,16,152,
242,229,41,102,45,27,27,27,46,214,138,86,87,59,3,66,137,148,154,81,9,253,171,254,141,255,0,19,219,253,15,250,167,87,171,148,165,41,74,82,226,240,135,180,188,210,151,248,47,240,82,228,32,214,82,148,167,
210,255,0,156,81,47,21,255,0,36,196,213,219,216,66,114,253,48,158,136,65,174,97,8,66,100,226,16,132,33,8,66,113,8,50,16,153,8,66,23,74,93,140,157,66,16,159,201,8,66,115,9,212,32,212,251,253,148,165,41,
75,232,243,205,215,235,88,47,244,52,60,15,255,0,62,243,75,252,207,249,33,9,171,31,112,94,233,221,46,210,241,74,81,50,137,148,185,8,79,124,229,188,165,41,70,251,68,33,9,6,239,232,182,141,243,74,120,207,
3,147,33,49,20,190,138,61,185,9,204,38,206,218,33,50,99,202,90,77,132,62,145,227,230,98,19,80,168,168,241,234,156,205,132,33,9,148,163,101,41,114,234,99,25,63,135,201,8,77,152,157,66,107,243,143,103,178,
151,184,79,229,72,132,33,63,247,175,254,245,47,182,250,161,8,76,158,148,82,140,162,219,204,33,9,148,191,197,74,82,229,238,141,147,143,57,9,190,79,36,33,9,234,69,245,126,108,38,177,16,132,198,66,19,40,
95,232,132,39,131,240,100,33,50,19,33,8,202,40,140,242,44,85,134,136,66,108,71,130,228,125,62,36,33,9,205,196,207,4,39,186,148,186,145,6,55,143,138,82,148,165,200,78,39,80,156,205,75,33,8,66,16,159,248,
176,132,33,8,66,16,133,20,66,177,9,136,36,52,52,79,232,162,217,179,255,0,74,19,217,63,241,111,41,237,254,25,236,77,33,188,165,207,205,165,41,120,162,199,18,41,81,224,240,57,8,66,44,185,74,82,250,96,188,
23,38,49,11,103,19,35,224,153,4,13,8,30,23,42,19,69,67,135,134,56,41,49,112,211,21,213,240,132,26,225,16,72,124,194,99,202,81,61,157,166,55,203,83,210,249,179,20,250,79,90,41,118,19,159,25,56,51,16,155,
114,148,240,66,108,33,8,76,157,34,151,215,8,66,19,149,137,98,30,248,28,19,75,20,163,98,101,41,74,95,255,0,149,132,39,83,215,54,113,8,66,19,209,9,239,190,58,155,63,169,122,111,9,194,249,226,150,20,79,193,
25,10,210,112,33,8,78,63,114,112,188,101,46,42,121,255,0,7,175,194,25,25,25,25,69,98,13,240,138,82,141,148,190,10,38,82,194,148,165,41,75,197,40,156,62,145,113,74,82,151,41,74,95,35,246,47,83,154,184,
132,18,244,65,162,19,181,205,202,82,178,204,82,148,190,138,82,250,33,8,66,97,175,77,217,204,32,214,36,78,41,74,82,141,151,248,231,80,158,168,66,127,227,194,19,155,197,41,127,138,100,225,122,111,48,155,
61,51,33,9,253,176,158,168,78,161,4,136,136,185,153,9,158,74,202,202,202,202,197,140,66,148,167,215,147,138,82,148,164,33,68,210,99,104,163,101,40,153,118,229,41,113,69,41,75,151,105,120,165,41,68,230,
40,217,74,82,178,148,191,214,189,52,165,226,16,155,51,243,33,9,238,163,202,82,139,209,8,79,69,19,19,71,142,32,209,54,16,107,81,68,169,39,47,47,174,11,248,161,56,93,207,101,254,105,237,191,197,120,185,
74,82,226,200,53,49,33,175,237,156,66,17,163,68,196,52,66,16,132,200,66,16,132,32,188,18,144,132,201,204,33,15,40,76,165,230,148,188,79,76,38,66,19,33,50,113,74,93,153,54,149,148,184,184,165,121,75,137,
151,209,63,169,17,249,221,16,72,230,66,108,39,174,100,245,78,41,74,82,148,165,41,68,198,255,0,206,147,233,151,97,9,220,26,33,8,60,132,217,143,138,207,39,157,72,98,101,230,16,175,41,74,55,148,190,248,46,
151,241,66,19,86,207,231,159,207,9,252,244,163,203,176,156,175,225,132,32,180,66,15,152,177,161,44,253,40,218,42,207,164,123,243,63,50,16,132,230,19,33,8,50,16,107,185,143,12,132,33,50,16,153,120,89,9,
196,33,8,66,103,140,132,39,162,19,92,255,0,202,153,224,165,255,0,7,179,41,74,68,66,16,132,201,159,167,140,185,227,47,16,155,9,196,33,50,115,75,213,41,75,205,47,52,76,165,41,74,82,148,184,246,16,132,226,
229,69,24,222,36,66,13,98,98,28,33,50,143,201,8,47,84,38,221,241,180,98,244,46,25,121,89,8,66,109,254,43,197,47,254,181,47,182,115,54,49,47,247,62,44,125,13,71,140,121,33,228,84,190,151,159,209,241,68,
241,74,55,137,229,27,40,153,74,120,42,41,115,225,255,0,192,197,211,119,105,74,82,178,177,179,41,74,82,148,162,249,179,18,34,32,209,225,20,132,23,9,158,15,163,68,33,25,8,66,19,209,250,61,132,33,9,204,33,
8,200,66,16,132,33,61,51,33,6,189,30,8,136,161,48,163,26,68,31,55,139,183,168,76,158,186,82,151,137,196,245,207,85,47,162,148,76,165,225,166,70,70,70,121,40,152,208,145,8,66,13,33,137,148,165,46,82,237,
41,74,82,148,163,40,189,16,132,213,196,33,8,66,98,122,87,186,16,159,201,114,151,187,233,132,33,9,176,132,39,80,132,33,63,146,229,41,113,188,33,104,162,40,40,85,143,34,16,255,0,130,80,126,68,33,229,31,
220,155,61,62,75,232,187,8,37,220,245,183,151,124,137,194,178,178,138,202,202,81,60,72,130,75,244,132,242,81,127,175,18,32,200,66,16,104,68,33,8,66,66,98,68,16,136,136,105,17,16,77,196,195,66,88,129,162,
13,50,12,132,33,5,31,253,232,31,6,44,132,33,4,136,78,63,117,177,146,227,192,132,33,8,66,9,16,76,66,19,208,201,144,152,150,38,32,207,36,244,206,38,78,33,17,8,70,140,66,44,158,212,120,60,20,165,67,242,88,
90,63,194,17,189,30,167,107,33,8,77,38,62,41,114,226,226,103,225,75,190,50,172,69,38,33,9,144,131,202,82,141,255,0,20,246,194,16,132,217,253,8,104,130,68,33,8,36,66,13,19,33,9,144,156,66,115,56,72,132,
68,68,103,150,66,13,111,209,46,217,93,31,16,132,247,76,132,33,54,158,10,83,193,224,168,168,171,97,50,16,132,240,66,16,156,76,243,196,194,9,18,117,68,203,148,163,123,9,137,16,135,233,4,188,194,78,42,152,
190,10,82,148,163,8,38,177,180,33,196,36,66,34,15,172,248,100,200,60,132,33,50,16,132,33,50,136,67,225,12,126,72,66,16,132,38,165,176,132,32,209,9,204,33,9,148,165,41,15,132,34,34,25,9,136,46,38,65,36,
61,165,41,74,92,126,234,82,148,165,101,101,47,48,132,212,137,147,23,52,165,41,74,82,151,221,120,140,248,121,202,121,19,212,34,19,135,145,145,144,140,158,185,204,39,162,122,41,75,232,132,245,210,226,148,
76,165,41,68,248,20,163,101,234,16,131,89,8,66,16,130,9,16,130,67,89,9,134,16,155,79,16,121,68,169,49,178,148,190,11,228,171,33,8,242,16,154,66,16,154,200,36,66,9,16,153,8,78,193,121,208,152,145,127,209,
33,162,107,33,8,66,16,74,147,32,169,12,66,21,9,148,163,101,47,19,41,228,172,175,9,143,15,51,255,0,131,243,193,97,112,152,221,94,70,203,254,100,35,35,35,202,39,144,72,76,121,197,225,125,60,253,98,116,126,
16,165,17,75,228,108,165,40,241,50,163,198,51,238,36,66,17,16,146,100,39,12,91,244,131,68,230,98,15,101,38,125,38,33,49,188,100,35,229,101,98,16,132,196,201,144,156,66,16,152,132,196,33,57,9,136,236,136,
66,19,176,33,8,200,200,81,8,66,113,8,66,16,132,33,8,76,132,230,100,230,16,72,72,104,104,136,136,132,18,32,150,209,188,100,33,56,100,226,16,156,95,239,132,23,162,117,8,66,99,91,121,187,74,92,92,32,197,
41,89,88,163,200,136,131,240,138,82,162,148,72,64,154,42,27,42,26,172,183,134,207,39,145,134,23,150,44,41,68,27,175,17,127,209,76,132,238,108,33,8,36,76,132,32,132,28,255,0,70,208,154,42,60,16,65,4,9,
9,36,130,162,148,65,177,20,108,165,46,27,41,68,29,50,148,185,75,11,136,184,165,202,143,185,50,16,68,36,136,124,16,132,38,120,60,114,200,36,65,144,153,8,52,52,36,70,67,206,35,35,63,233,144,132,27,41,124,
10,225,224,132,45,43,79,145,229,69,67,17,70,196,196,175,23,63,72,191,210,101,42,199,204,32,145,9,8,77,33,4,144,120,52,79,2,94,68,18,33,8,66,16,153,8,76,65,132,133,254,136,76,79,4,196,33,8,60,130,68,33,
25,31,23,127,118,112,144,201,136,66,16,132,232,83,216,54,223,16,211,16,152,144,132,32,146,33,16,135,228,123,224,240,81,178,226,225,251,67,165,40,152,218,41,74,94,222,78,161,56,132,216,66,16,75,16,107,
33,8,66,16,132,234,16,132,33,6,137,136,66,16,132,33,8,66,16,132,22,19,11,8,38,98,80,240,85,133,40,188,141,148,165,197,105,121,242,52,200,207,35,70,67,67,251,194,42,27,242,86,80,167,53,154,40,162,203,44,
162,139,44,161,127,178,73,218,138,27,178,188,165,101,101,226,243,122,13,148,165,43,41,74,82,148,165,41,75,151,154,92,189,10,178,152,108,91,40,178,202,40,70,219,205,99,228,161,65,101,98,138,232,233,8,22,
16,65,5,69,67,66,162,161,52,54,143,7,131,193,250,67,230,82,229,202,242,149,149,149,234,100,94,111,175,179,69,9,135,135,199,60,131,206,242,189,29,208,84,120,144,64,215,41,8,163,66,4,209,81,81,4,9,163,193,
224,162,60,82,162,162,162,162,172,253,23,156,124,30,20,165,46,84,55,89,224,169,21,30,41,227,60,21,30,15,4,9,8,32,130,10,138,138,138,138,138,81,225,26,65,72,32,130,72,36,146,6,154,59,231,158,108,176,184,
251,40,172,172,172,162,178,178,151,221,118,148,189,82,149,151,160,82,148,165,41,75,148,165,41,74,82,151,20,165,41,122,4,20,165,41,74,138,82,148,168,109,21,20,168,168,240,54,136,32,165,68,21,21,21,21,30,
49,4,17,55,73,135,69,141,139,169,146,62,251,83,152,79,198,23,210,212,65,175,252,104,65,162,127,252,58,248,53,233,165,219,148,183,255,0,62,229,226,151,43,43,41,69,21,254,149,149,137,145,127,233,101,102,
139,40,88,36,195,243,19,33,177,69,21,148,81,69,20,86,81,101,21,136,38,161,5,240,81,178,141,178,148,165,101,98,102,44,162,126,70,30,21,148,165,43,27,194,149,149,149,149,255,0,252,133,254,155,197,101,255,
0,204,97,184,55,255,0,138,177,49,188,252,196,135,255,0,240,105,194,248,255,0,250,234,86,86,86,94,211,130,197,179,43,43,255,0,252,211,63,255,0,85,231,254,44,33,9,239,158,183,255,0,169,249,255,0,250,0,144,
215,240,66,122,102,66,19,39,166,108,33,9,144,156,204,132,33,8,66,16,132,33,8,78,31,161,228,201,176,159,219,8,79,228,132,39,243,164,66,16,132,215,255,0,248,118,19,250,161,61,83,33,8,79,226,94,153,219,246,
13,98,196,184,78,103,166,16,131,92,195,244,132,32,201,204,245,161,250,89,8,66,100,32,200,71,136,66,99,39,73,13,19,249,223,16,157,66,16,154,151,16,132,26,254,24,66,30,10,82,235,234,117,56,159,211,63,246,
16,255,0,254,178,251,103,165,119,8,65,44,69,25,4,132,184,132,17,75,68,177,147,80,246,16,132,33,53,228,229,47,66,88,145,9,194,245,189,74,146,13,123,97,54,19,211,8,71,176,158,216,66,16,132,34,31,75,154,
94,96,215,80,72,104,135,140,132,33,59,125,204,132,33,9,252,115,38,161,250,111,254,52,247,206,33,8,79,253,138,95,84,230,16,155,9,220,254,89,252,208,132,38,194,16,130,172,248,16,131,226,19,18,230,19,103,
166,100,38,33,56,74,53,147,248,95,48,132,44,201,239,158,56,152,201,205,40,223,48,154,66,16,155,54,116,246,148,190,151,136,252,24,215,162,159,131,101,41,75,234,190,139,151,46,93,188,82,241,127,133,255,
0,28,33,8,66,16,132,26,217,239,132,33,8,66,47,84,33,8,66,16,132,39,115,249,103,51,219,8,66,100,33,9,147,33,9,136,60,132,33,8,65,169,220,32,215,83,153,147,215,8,53,233,132,200,77,125,34,229,199,205,47,
16,132,26,39,170,16,157,77,78,13,136,100,33,56,127,202,190,143,212,151,138,255,0,130,16,132,33,8,66,16,132,200,67,198,66,16,127,54,158,26,33,6,184,165,233,226,123,247,151,179,192,254,123,111,165,124,25,
127,137,45,191,193,50,16,132,33,8,65,162,19,33,57,165,218,55,168,107,97,8,66,16,132,33,49,12,101,41,74,94,32,209,8,79,116,254,251,233,132,217,169,19,135,136,186,95,75,33,8,65,34,19,30,66,16,72,100,39,
161,247,54,16,154,253,87,137,232,99,203,196,38,210,250,225,9,183,209,4,54,95,85,244,222,46,82,148,89,75,148,165,40,135,69,202,95,68,33,9,147,63,57,108,122,135,6,177,16,240,55,171,126,136,65,162,13,19,
186,95,30,152,50,19,62,189,119,26,244,222,225,8,66,16,132,33,8,66,107,68,33,8,52,66,16,132,226,122,166,194,16,132,203,151,184,66,11,103,19,136,66,15,137,176,156,95,225,132,33,6,137,255,0,141,9,194,201,
196,198,121,229,100,33,8,66,16,132,33,56,132,32,144,252,12,75,33,58,132,33,50,108,38,194,9,13,19,40,181,99,217,205,202,94,33,50,11,212,181,46,60,21,21,13,245,75,180,165,219,148,165,41,75,205,41,74,92,
165,41,125,16,132,33,8,76,66,16,130,89,60,19,102,190,111,20,148,240,76,66,17,14,23,17,9,159,4,202,60,124,65,162,16,132,199,252,83,33,63,158,250,144,245,127,20,32,151,181,228,33,9,204,33,9,141,8,126,150,
63,130,201,252,48,153,8,79,84,38,76,132,33,8,66,13,127,52,226,16,132,226,16,132,33,8,66,16,132,196,136,66,100,38,66,12,122,181,6,177,8,50,15,210,150,66,118,178,112,26,39,20,165,47,55,41,75,158,79,34,93,
46,37,100,18,33,49,191,240,242,70,65,34,162,151,91,226,151,132,202,82,251,33,8,66,16,132,32,145,17,8,76,107,23,16,153,8,179,225,75,211,80,67,99,249,234,135,146,148,165,226,151,133,175,88,214,76,66,16,
104,131,234,123,41,125,144,153,8,66,100,33,8,79,66,246,66,16,156,165,144,158,247,147,152,65,45,74,144,132,33,6,136,78,217,8,66,19,39,16,132,245,204,127,198,135,252,19,39,16,132,39,165,44,132,196,32,209,
8,66,16,155,57,165,24,111,80,144,144,220,41,114,16,120,249,121,53,121,33,9,144,156,38,34,244,253,169,9,8,65,162,113,4,136,78,110,177,161,20,163,40,135,148,185,122,131,68,32,215,105,16,76,243,8,200,196,
145,224,241,146,159,244,78,32,246,8,108,165,41,118,236,33,50,16,77,70,134,132,169,16,124,66,16,132,234,19,30,121,244,191,162,27,136,66,225,227,67,68,246,95,71,222,102,66,19,209,8,66,19,219,50,16,132,39,
48,132,33,50,16,132,230,19,133,195,93,194,104,135,205,112,168,168,110,250,90,242,79,68,39,242,78,225,58,156,206,97,9,179,133,171,134,66,16,157,33,226,254,9,144,131,68,33,8,34,143,21,19,41,113,144,132,
26,33,8,66,16,74,23,33,9,141,16,132,218,82,236,33,8,66,19,33,8,65,33,34,164,92,100,33,4,38,54,55,168,75,110,55,148,76,110,147,152,66,19,19,152,66,9,12,89,8,46,160,214,249,212,58,200,120,67,175,225,25,
8,66,13,19,62,100,32,131,68,39,40,112,139,150,31,71,169,9,100,62,9,227,223,209,139,38,193,41,233,132,153,5,191,163,99,67,244,66,16,104,132,237,16,132,18,200,34,16,132,33,8,66,19,135,169,19,81,8,66,112,
144,212,25,8,36,136,136,136,133,10,143,163,72,140,66,121,32,201,233,72,184,137,204,196,50,148,190,11,175,175,164,33,7,224,162,245,193,162,16,132,33,8,66,108,234,228,245,210,151,91,27,246,66,11,81,69,236,
101,202,39,196,225,173,188,209,99,68,212,44,100,196,238,100,30,66,99,18,38,183,190,9,196,226,9,100,200,66,16,132,22,178,114,145,240,184,246,148,165,121,227,27,41,118,16,132,33,57,71,210,106,18,30,194,
9,16,153,8,36,66,33,141,228,23,145,107,198,33,121,33,50,12,132,26,130,212,245,146,12,190,136,66,19,137,173,11,46,46,39,119,198,164,216,191,208,153,74,93,132,26,33,61,44,132,33,8,66,9,9,16,132,32,145,9,
136,178,101,244,77,67,120,209,49,151,82,185,137,33,191,34,85,97,134,218,21,43,202,92,81,104,188,66,99,234,12,78,98,210,12,75,26,26,49,252,198,250,153,53,107,31,146,11,102,189,191,207,125,112,132,199,147,
33,58,132,214,137,202,246,50,9,16,75,105,75,150,13,209,116,200,44,184,198,46,22,50,16,132,32,151,16,157,188,41,74,46,97,9,164,200,66,17,14,107,120,178,162,148,165,47,20,164,36,60,16,155,114,16,132,68,
68,60,30,7,204,201,210,216,66,19,63,69,159,79,158,130,27,19,108,72,132,68,17,75,141,229,27,197,197,41,70,60,72,130,194,16,131,88,177,116,250,121,114,194,140,165,46,44,88,218,184,111,26,216,78,225,48,209,
15,131,242,33,33,108,33,4,55,6,247,233,8,63,76,198,132,162,226,17,137,132,36,26,33,186,46,14,152,145,53,4,135,144,132,226,229,40,185,164,33,4,139,148,71,229,16,254,16,132,244,204,165,225,241,5,144,123,
74,94,95,241,76,132,33,58,125,222,104,203,195,246,222,16,135,143,165,220,62,20,127,53,243,49,177,50,243,70,242,236,229,144,132,32,144,151,129,236,230,240,178,241,49,100,33,8,77,100,196,177,188,163,217,
221,196,93,132,225,34,15,80,144,197,175,193,244,130,94,55,206,254,244,133,224,165,229,248,19,188,81,121,62,20,185,124,20,165,196,132,182,236,225,178,164,54,34,148,253,234,15,139,151,175,162,80,111,206,
66,20,73,137,100,88,107,192,241,100,199,144,130,68,225,147,22,166,84,85,148,111,87,156,165,245,93,103,224,196,33,33,184,50,205,19,41,98,217,70,207,188,53,143,152,222,210,151,97,54,19,185,159,224,69,41,
75,220,200,66,16,156,65,33,113,248,61,132,218,81,62,30,66,16,132,216,66,16,157,95,107,213,233,163,225,122,161,4,177,141,251,216,134,200,66,16,155,6,137,194,218,55,148,162,101,200,65,228,60,13,139,41,74,
82,151,23,41,229,202,94,81,68,76,121,70,199,224,82,158,79,36,33,244,132,33,9,212,39,80,72,131,240,92,107,18,18,131,218,49,12,76,183,27,133,41,247,136,65,7,224,76,165,17,74,125,22,66,97,8,36,53,16,216,
188,144,132,32,185,104,176,171,38,54,59,147,97,8,62,27,62,144,52,210,16,132,38,36,37,15,240,18,32,134,39,155,211,26,196,26,196,135,244,140,130,71,238,49,162,19,96,144,252,44,172,76,180,132,201,213,225,
162,16,132,225,139,203,32,138,55,176,157,45,98,229,161,175,66,200,77,124,164,65,248,217,179,187,148,165,41,118,148,165,225,13,114,177,231,205,165,17,224,240,120,34,34,26,71,140,165,197,197,41,75,234,153,
49,245,74,82,148,165,219,151,166,202,82,148,165,46,161,178,228,245,209,113,117,114,197,148,187,9,148,162,101,217,141,123,102,49,107,112,165,41,70,196,245,107,199,176,72,107,96,151,44,132,38,76,188,44,
66,198,176,252,30,49,99,250,126,19,99,18,225,161,240,150,165,143,132,51,233,5,203,20,163,54,127,208,150,125,196,219,193,40,34,151,33,6,76,124,226,19,33,56,123,8,52,32,146,67,9,9,99,250,36,67,119,8,50,
16,68,33,7,227,47,20,165,41,75,141,16,66,98,242,134,137,196,38,174,222,178,194,137,227,68,18,200,66,16,130,89,70,202,38,82,148,165,244,162,45,132,39,33,6,136,243,139,30,94,169,120,165,244,254,158,11,147,
30,222,94,82,178,148,172,172,162,148,126,125,180,165,218,93,191,208,252,137,100,225,34,13,127,4,226,151,182,50,229,19,46,47,161,49,132,198,246,148,188,162,8,125,60,44,36,201,136,167,145,151,37,196,18,
230,234,198,55,228,165,27,202,66,112,181,50,158,76,163,30,38,38,54,81,86,37,141,144,75,134,177,9,169,87,213,47,19,134,219,23,233,144,126,56,130,80,110,13,148,251,232,75,200,216,185,100,33,8,78,30,65,162,
17,144,72,152,201,231,206,47,154,197,225,240,157,121,70,207,251,194,199,147,16,135,145,45,24,143,168,37,224,88,242,151,62,144,130,67,101,47,166,13,137,243,8,65,47,91,226,236,200,65,109,46,19,209,188,104,
107,23,129,178,241,127,141,158,114,148,165,207,209,47,3,132,216,66,19,31,166,19,33,58,132,219,238,156,205,93,164,62,80,189,10,47,125,41,74,82,141,243,6,65,100,38,223,98,213,176,130,68,33,8,65,44,106,137,
12,132,22,49,226,103,145,241,118,240,207,210,16,156,82,226,86,68,178,45,18,225,26,39,140,74,147,17,11,30,82,147,94,66,16,72,152,199,141,31,252,32,182,226,19,192,158,49,252,225,35,224,219,240,73,228,38,
78,30,177,113,74,95,109,27,225,252,27,40,146,31,90,196,27,40,156,62,227,126,74,66,9,101,25,117,226,40,217,75,196,33,8,126,99,200,162,123,75,151,24,157,224,93,49,100,16,73,16,107,155,151,103,47,124,144,
130,18,32,146,60,21,20,165,203,180,120,152,216,217,118,19,38,78,33,8,66,16,132,32,145,240,165,41,113,228,212,76,130,24,216,190,113,120,121,122,132,33,8,66,16,120,250,89,4,137,194,33,8,66,19,25,9,202,24,
158,36,49,16,132,214,41,74,82,137,148,187,74,82,151,12,38,93,94,114,227,226,16,130,67,88,153,74,94,102,249,33,4,180,132,194,92,66,117,122,72,133,27,35,98,9,12,124,66,13,16,92,53,196,215,162,81,14,136,
158,121,131,255,0,8,37,148,98,198,55,139,41,70,253,12,108,172,250,36,78,60,31,68,55,224,188,12,125,33,8,67,252,31,9,195,225,226,89,9,140,81,229,158,69,232,242,66,16,240,84,47,44,110,44,160,212,152,104,
79,37,67,15,207,9,23,23,11,4,242,234,101,46,24,172,165,41,70,54,38,82,140,55,227,80,188,23,46,54,92,250,207,129,248,225,15,22,216,132,16,185,6,50,148,185,125,51,33,34,198,202,43,43,203,148,162,99,20,165,
40,223,52,165,226,109,41,120,188,61,72,98,88,144,214,82,226,249,27,195,121,124,20,165,18,26,241,220,201,204,33,9,172,132,33,8,66,112,241,137,139,32,242,151,152,82,226,24,144,138,55,201,177,134,199,233,
111,132,55,212,18,131,18,230,137,226,199,175,152,76,132,33,8,46,39,48,153,54,148,162,99,126,53,35,224,217,244,255,0,132,62,13,141,220,130,66,91,8,66,114,132,33,8,66,9,30,95,20,165,227,205,46,76,110,98,
228,98,79,97,22,198,147,166,137,159,130,243,177,148,37,158,63,69,6,143,7,142,39,146,122,41,75,147,89,4,33,227,27,19,42,41,68,198,156,31,34,95,185,224,53,225,75,148,174,148,67,214,27,103,156,132,196,252,
151,102,82,148,240,82,148,162,101,47,80,135,238,220,89,15,130,98,13,220,140,132,103,156,153,228,72,104,140,165,46,27,60,228,207,36,39,48,152,177,49,121,26,30,194,19,80,215,9,147,152,242,112,222,94,19,
201,143,185,183,95,80,132,33,8,124,25,8,66,16,158,133,175,87,75,182,36,66,99,201,151,212,178,227,23,70,248,132,38,66,16,125,60,92,44,156,194,16,75,27,62,144,132,33,8,76,191,204,217,113,16,98,18,13,221,
250,27,133,234,149,136,110,33,233,180,72,93,210,148,186,223,5,11,120,165,29,18,108,132,98,184,217,224,169,12,86,207,57,8,65,33,33,180,136,45,212,207,184,220,41,74,121,98,168,84,108,77,148,187,79,165,41,
75,143,60,30,10,139,163,8,51,69,116,175,23,209,41,141,226,68,164,33,240,108,165,27,228,163,120,132,92,165,216,36,73,140,165,199,198,45,26,196,136,62,215,87,18,30,33,73,172,68,225,241,57,109,75,199,16,
132,229,144,156,65,147,27,31,80,132,226,16,132,39,178,16,132,18,217,144,132,33,38,95,25,9,136,152,136,136,157,36,63,125,244,206,16,248,75,82,226,19,30,95,66,30,204,184,253,77,151,168,66,9,16,132,33,6,
49,16,132,33,8,77,38,34,16,154,248,88,241,123,40,223,9,195,242,66,19,30,206,145,6,132,136,37,136,163,187,89,228,165,40,155,44,27,23,82,242,127,240,66,98,99,101,196,38,82,151,201,70,92,154,222,209,50,142,
226,19,46,54,94,145,69,250,103,136,49,20,76,168,110,34,249,19,40,160,216,222,36,66,16,123,8,76,132,16,217,74,81,49,60,253,24,202,81,249,39,146,214,82,148,95,124,140,177,108,18,34,196,36,66,13,13,16,107,
16,65,162,65,124,31,140,127,121,101,27,47,73,143,32,133,144,144,165,230,245,70,66,108,196,242,108,197,196,23,243,193,178,139,133,179,211,4,134,136,66,13,9,100,62,4,137,141,141,229,16,135,253,75,149,204,
33,9,232,132,234,106,201,220,33,5,196,26,230,31,162,89,4,177,115,9,136,152,249,163,99,126,69,24,78,229,196,136,66,108,225,115,9,173,241,41,60,139,194,27,130,30,54,92,82,151,105,81,74,82,148,251,218,198,
203,177,113,8,66,16,154,120,72,111,166,83,192,180,165,41,68,202,94,46,37,211,212,82,226,104,126,5,44,197,41,70,233,6,225,66,202,148,165,197,226,98,68,198,241,143,16,144,73,142,50,240,165,40,197,76,106,
9,112,34,18,18,32,150,66,9,9,99,30,77,75,90,43,163,101,219,175,164,33,151,17,4,188,144,132,25,5,202,67,218,92,88,144,254,136,100,32,184,132,39,166,251,33,8,78,60,16,241,106,67,218,94,111,178,229,46,50,
16,132,237,229,203,235,98,214,78,96,209,9,180,165,41,74,82,247,57,153,50,16,132,31,48,132,33,49,151,136,77,155,68,242,114,178,242,241,161,161,161,114,93,194,11,20,189,62,38,44,74,203,6,197,218,248,75,
132,94,40,153,244,131,27,41,89,88,217,68,248,108,74,144,104,124,210,151,154,93,132,39,161,179,206,121,203,207,209,173,242,35,206,65,229,41,68,82,82,97,47,4,209,34,107,101,19,197,179,16,132,18,16,220,67,
173,141,108,65,249,120,18,254,144,127,128,188,60,146,40,37,53,161,139,233,8,66,99,225,143,97,5,195,67,72,126,184,72,82,241,241,159,156,62,33,49,231,145,229,196,93,92,78,40,222,50,151,153,195,23,105,228,
24,216,138,121,16,154,151,76,90,217,68,242,148,165,41,81,70,60,184,69,229,16,152,178,234,251,204,245,204,158,202,44,111,215,9,210,214,203,144,72,157,49,177,188,132,234,148,242,65,46,94,193,174,90,32,242,
98,16,152,191,129,108,33,6,200,197,184,200,65,137,16,132,32,249,89,118,136,120,91,213,46,44,89,56,131,103,146,50,50,78,32,145,9,196,34,26,68,18,25,40,132,33,7,139,33,56,132,39,8,66,121,32,145,4,137,134,
145,9,220,33,9,148,88,209,15,3,223,34,4,167,174,16,65,121,19,27,19,213,211,234,140,99,23,146,15,175,154,242,100,196,202,92,190,139,176,104,133,218,93,67,98,122,137,218,67,26,18,242,63,47,171,143,148,202,
50,107,120,214,33,15,91,47,161,61,127,113,81,234,93,82,148,162,101,47,48,156,66,19,152,77,158,165,204,26,38,194,19,39,182,141,245,121,165,198,36,37,219,103,145,34,98,229,178,137,239,239,79,233,9,137,140,
162,94,203,180,92,184,27,23,32,144,217,74,92,67,121,7,227,27,225,109,27,225,162,113,25,8,33,8,36,77,122,250,132,33,9,196,26,235,232,177,248,60,188,52,76,131,199,171,167,176,132,18,214,82,148,92,164,66,
106,89,121,66,94,5,236,248,45,152,253,13,151,39,23,23,206,52,52,33,141,19,152,65,33,171,136,76,36,145,224,98,240,49,23,155,147,150,66,100,33,55,244,74,16,135,207,226,154,241,98,38,66,112,62,48,150,66,
99,229,243,68,242,16,132,229,246,134,202,46,105,75,205,218,81,50,151,167,253,237,151,209,75,205,24,151,162,19,132,82,148,111,81,75,137,243,251,207,209,44,190,166,81,138,82,226,143,70,66,11,29,62,23,41,
69,244,163,20,111,132,136,65,224,180,70,66,9,13,109,40,181,118,214,82,137,139,139,212,201,148,132,217,175,134,36,65,162,16,155,114,16,155,68,41,114,16,75,26,33,9,219,101,41,114,9,123,154,23,142,158,45,
101,213,203,197,172,69,198,66,15,60,139,16,222,66,229,223,130,8,132,244,188,131,66,66,67,30,65,46,33,57,154,135,238,158,4,44,132,226,143,232,137,147,32,209,61,19,82,200,65,251,147,245,194,16,132,39,166,
151,149,197,237,108,39,162,141,226,68,244,165,141,151,87,166,226,233,162,9,19,39,20,165,203,172,190,69,147,171,172,188,164,66,16,131,22,82,158,5,196,169,52,108,52,200,70,79,34,193,8,52,44,130,89,49,246,
132,60,66,238,13,11,253,19,47,44,153,74,50,141,225,99,60,99,110,227,67,76,140,140,243,143,150,137,137,114,207,130,9,22,44,162,33,8,66,118,223,127,130,254,72,76,184,241,119,40,208,132,156,74,33,6,136,65,
121,16,113,23,200,152,200,66,16,132,16,190,14,13,172,82,151,206,92,162,101,41,250,92,72,108,100,36,41,102,138,182,19,217,57,127,117,99,101,23,32,184,253,47,210,150,81,12,132,33,61,13,248,41,249,196,33,
8,60,165,40,153,75,179,18,38,193,250,31,8,101,19,46,220,163,124,82,227,212,26,27,234,98,199,137,16,158,151,148,79,27,27,27,41,120,69,197,41,114,141,226,101,40,196,45,120,202,82,151,209,74,54,92,75,192,
241,228,16,74,101,46,165,144,132,245,188,152,209,49,9,229,60,137,13,148,172,242,92,82,140,38,125,216,76,131,248,82,236,32,158,79,220,132,214,225,122,189,62,225,8,61,66,92,222,158,194,98,84,153,60,148,
191,200,217,231,97,5,147,38,188,111,33,5,224,163,131,200,164,97,138,207,34,20,41,114,106,250,54,54,121,98,66,67,229,240,246,194,177,49,49,186,81,178,81,15,130,20,185,125,73,100,33,9,136,65,13,143,111,
11,17,121,250,36,65,163,235,16,140,152,130,198,46,225,58,125,82,240,246,9,101,41,74,81,50,148,165,47,30,54,141,151,105,125,179,40,222,33,49,188,75,155,180,165,41,74,82,148,165,41,113,188,66,40,216,245,
20,69,46,205,108,188,220,69,209,178,148,165,19,41,75,199,145,33,136,104,88,199,137,9,9,109,238,98,101,226,113,70,60,138,82,143,38,36,66,8,163,103,232,177,147,27,197,228,72,132,233,6,182,136,248,38,81,
190,18,139,193,71,244,88,196,188,251,168,198,136,200,36,46,89,75,195,249,202,92,252,9,148,80,157,247,82,151,166,39,148,129,240,15,10,93,40,216,158,33,34,9,34,42,56,139,162,194,149,148,184,181,252,38,216,
54,82,148,108,69,31,20,121,68,252,141,15,193,74,82,148,165,46,82,151,47,20,165,41,75,168,98,91,54,148,165,212,89,165,69,16,225,74,52,210,137,148,165,41,74,82,148,165,215,205,41,75,144,132,68,19,31,87,
24,158,79,85,234,151,111,161,122,27,41,74,82,151,102,178,148,188,221,165,40,217,120,130,32,182,151,13,220,69,41,118,141,148,165,216,65,45,18,212,36,65,164,200,134,38,82,11,69,212,230,122,160,217,191,7,
250,16,135,129,188,164,108,95,235,134,46,82,179,224,223,165,87,39,197,41,68,197,231,38,180,65,63,194,191,120,124,46,63,11,143,17,250,82,162,227,101,40,152,222,54,38,81,54,203,145,30,6,240,216,241,141,
228,225,104,178,148,184,222,54,144,195,244,64,124,13,233,118,151,60,137,8,132,18,195,18,16,66,203,163,196,94,38,162,233,70,203,169,107,101,41,69,196,33,60,138,10,53,123,74,47,99,33,8,65,162,16,132,31,
210,148,165,41,74,82,148,165,195,121,74,81,6,40,217,231,41,74,94,33,8,66,19,153,205,41,68,202,82,148,185,50,93,156,172,108,190,49,15,111,165,33,60,30,44,165,47,119,46,82,148,187,122,165,41,118,229,216,
65,241,58,76,108,165,230,19,97,8,66,16,132,240,42,99,202,8,76,132,30,37,211,67,240,47,184,215,25,9,197,246,223,68,32,208,148,226,228,164,33,4,178,19,32,151,12,75,206,81,221,141,147,32,200,33,49,122,233,
120,68,33,8,66,98,31,129,179,233,240,112,38,56,224,82,148,167,198,19,43,167,150,79,247,19,130,99,99,107,20,165,27,40,132,242,61,143,41,75,6,223,19,135,227,63,41,244,248,81,13,242,145,4,177,241,175,138,
54,121,35,35,35,26,18,240,126,136,124,44,126,71,243,41,123,124,82,151,19,16,134,27,47,9,8,190,133,148,165,46,84,85,137,15,192,248,126,20,165,226,17,144,140,132,229,234,26,237,49,134,203,143,152,66,16,
132,33,7,225,9,137,137,148,184,153,70,202,92,188,210,241,125,116,162,98,240,26,190,111,23,209,50,16,132,33,8,65,162,16,72,156,130,68,200,66,16,104,132,33,8,65,162,16,132,162,82,8,26,32,145,57,185,4,166,
66,117,8,33,137,187,173,159,68,18,9,68,146,47,52,165,40,189,112,132,29,23,182,107,215,144,75,91,32,134,202,138,55,137,30,7,16,252,233,3,131,133,22,20,165,46,210,140,168,168,165,47,20,165,224,55,73,69,
6,210,27,172,76,122,151,51,18,32,144,195,101,43,43,197,196,32,130,240,54,55,224,165,62,23,37,24,132,39,81,99,25,58,72,90,222,19,158,71,228,82,148,162,214,197,6,82,242,196,50,23,32,153,121,131,98,9,97,
206,33,4,181,228,33,4,132,63,15,41,74,92,187,70,202,54,82,179,201,75,228,176,110,247,15,140,66,103,230,190,103,80,132,33,6,188,101,41,74,94,41,74,93,90,198,34,151,41,74,93,165,63,58,188,82,148,190,183,
220,216,66,122,33,8,66,116,145,61,179,33,9,144,153,4,167,68,203,204,38,49,38,37,239,99,98,226,235,212,132,39,12,68,245,94,23,77,226,148,163,101,41,74,38,120,30,67,108,242,42,121,60,158,113,34,120,21,62,
158,17,74,241,190,17,75,254,98,176,216,162,99,119,41,74,38,81,15,201,25,250,63,36,31,140,167,135,5,176,135,151,10,81,178,225,186,76,187,60,19,133,244,165,24,241,49,178,148,163,126,10,82,250,97,25,8,66,
19,105,113,190,233,74,81,61,92,34,113,9,141,82,16,130,16,152,134,144,150,126,140,98,40,182,16,132,38,63,122,226,243,8,76,110,139,136,76,132,233,241,74,82,148,110,247,49,247,240,68,38,60,7,238,153,8,66,
16,132,33,50,16,132,200,77,132,26,23,12,155,75,237,124,174,223,161,62,103,161,143,40,191,150,227,99,101,41,120,92,162,151,148,94,233,120,132,202,64,157,198,136,108,76,189,66,15,193,245,137,16,132,33,8,
32,148,25,10,60,165,27,46,49,51,207,23,151,243,136,121,23,130,148,165,27,27,40,248,66,19,41,68,203,227,30,44,131,62,23,18,241,212,62,106,31,51,198,194,19,16,201,202,66,74,13,19,38,50,9,16,131,33,60,12,
162,84,88,52,67,227,19,168,46,82,63,114,15,18,244,81,15,103,31,156,193,161,174,23,112,132,18,214,66,16,132,38,50,16,72,155,75,195,198,202,82,19,134,136,65,240,145,8,66,16,132,33,4,181,170,36,77,100,33,
250,66,16,132,200,66,9,16,155,9,144,72,72,104,123,61,77,19,136,66,112,185,132,31,165,98,216,67,224,221,23,19,30,38,47,83,99,225,100,254,22,231,2,228,39,52,69,238,229,31,248,19,16,222,66,99,196,54,38,81,
178,148,80,169,16,53,196,92,82,148,165,198,126,139,170,132,240,197,202,92,104,153,8,36,144,216,245,33,112,246,19,24,165,41,118,16,72,144,132,214,66,17,147,96,168,132,38,144,107,89,68,61,155,30,74,66,104,
150,26,39,112,152,135,196,38,177,136,98,10,207,1,44,65,34,13,16,131,89,8,36,36,53,180,185,114,16,91,9,173,13,9,250,168,158,188,158,139,136,124,177,127,27,200,66,19,33,49,144,156,173,158,202,82,235,197,
144,132,33,9,175,211,113,242,253,151,91,226,118,253,40,98,234,115,118,82,65,117,74,94,88,144,151,163,207,166,225,249,196,32,149,32,246,250,110,210,228,18,229,141,151,206,95,24,177,177,188,242,26,37,195,
16,132,226,101,46,33,249,9,49,248,40,216,136,66,81,33,228,18,33,52,222,66,18,34,9,11,8,52,196,63,44,248,39,172,253,196,137,147,18,225,188,163,98,60,76,252,30,65,47,4,33,8,179,192,208,162,213,171,110,172,
240,94,24,199,176,155,247,164,181,178,248,23,19,192,131,71,194,143,254,138,8,165,241,197,22,209,231,231,11,94,81,50,148,108,165,46,82,137,221,153,74,33,106,209,228,33,6,32,214,78,23,112,132,39,161,226,
92,193,174,27,41,114,107,233,15,214,249,185,50,139,248,27,41,127,134,115,5,196,238,15,208,217,75,138,82,148,165,41,113,11,209,229,147,40,217,113,34,123,106,197,41,117,99,101,19,27,229,237,41,117,159,58,
76,186,197,27,41,74,81,100,32,198,27,229,162,16,153,74,55,137,16,80,81,186,76,65,34,99,226,226,67,24,92,66,9,99,18,207,248,16,107,134,76,63,25,8,78,47,8,66,82,19,39,109,141,142,137,9,120,38,177,20,165,
22,66,13,149,136,79,110,76,75,149,240,111,213,15,154,207,140,106,161,184,49,89,251,148,162,242,66,65,188,165,238,139,132,177,13,19,33,8,65,40,136,126,148,100,196,63,5,120,184,163,208,197,201,169,15,216,
249,130,66,39,8,120,134,61,90,245,34,16,152,255,0,142,19,46,223,91,67,196,251,152,145,8,66,16,132,33,8,95,123,98,233,177,226,66,93,50,151,18,196,92,64,194,226,16,132,32,151,20,165,40,159,45,226,91,7,173,
241,74,54,82,148,165,122,158,82,143,43,218,38,81,49,141,137,54,62,16,144,177,143,86,82,144,132,32,209,8,65,34,108,164,33,224,69,30,13,209,15,110,50,31,59,97,49,113,6,137,140,162,33,243,167,197,226,247,
9,176,130,8,100,38,66,98,38,182,66,113,60,9,13,114,135,228,75,39,20,180,98,88,216,159,11,132,40,235,32,208,245,132,252,99,251,137,113,9,194,27,219,207,210,19,150,188,143,192,134,65,33,175,36,68,71,129,
178,229,226,9,116,246,19,154,54,82,148,165,225,20,111,200,159,162,19,41,118,11,95,185,250,146,32,215,174,143,102,47,69,202,82,148,165,27,27,47,169,240,222,81,50,151,41,105,50,148,190,132,82,148,110,143,
18,243,197,41,114,237,16,194,121,68,143,152,98,178,141,234,148,108,250,200,62,167,162,81,40,126,15,233,8,36,66,13,11,68,25,4,60,132,22,44,110,160,151,112,157,54,49,162,16,132,225,98,89,60,141,16,157,49,
9,246,213,194,19,47,9,99,103,231,9,211,125,194,100,201,196,38,62,20,108,108,88,136,66,22,33,49,177,19,33,9,140,185,113,144,90,209,53,34,11,230,181,79,154,215,71,219,126,170,93,111,193,120,66,26,18,225,
178,158,79,36,199,176,72,156,34,122,105,118,120,33,8,66,113,114,99,202,95,74,226,148,186,223,84,165,229,118,158,181,143,17,8,66,107,216,76,72,157,220,123,74,81,178,250,223,166,9,97,178,234,101,218,55,
180,165,218,81,50,233,74,38,54,92,55,151,17,8,88,134,246,151,23,41,193,58,52,63,167,206,96,214,16,132,32,145,48,132,93,44,163,225,144,132,32,134,230,30,34,148,92,183,6,216,182,148,162,200,65,242,201,144,
75,169,179,152,44,168,191,195,224,168,140,177,68,196,246,16,154,185,68,39,119,18,33,15,133,40,143,3,207,150,202,81,189,126,207,133,47,15,41,244,107,192,168,80,197,140,81,50,9,12,111,97,244,65,172,72,107,
30,44,54,196,181,137,19,46,183,136,250,198,136,62,86,49,19,41,113,237,46,53,144,132,33,49,125,198,253,16,153,121,156,222,159,165,122,219,19,19,46,188,92,178,16,72,132,200,33,189,152,253,20,126,202,82,
148,165,41,113,44,76,252,228,145,50,243,8,76,250,200,65,101,230,240,242,8,163,99,197,194,93,81,64,197,24,68,240,66,100,18,39,11,41,125,116,189,61,132,24,134,136,46,40,216,157,32,246,98,90,252,186,6,93,
186,222,46,94,201,177,137,191,125,11,171,94,254,103,238,66,16,130,22,46,110,82,139,209,49,226,196,65,162,20,106,137,36,188,101,242,81,15,97,54,122,168,250,121,49,31,7,228,78,13,141,82,11,88,254,240,155,
54,13,8,67,26,18,41,69,18,33,98,235,100,38,44,131,68,38,172,98,101,27,23,112,72,123,9,143,105,117,227,40,177,100,203,238,125,175,103,224,241,50,137,137,159,73,195,200,66,101,254,90,66,108,33,61,148,162,
125,16,209,9,221,218,82,143,39,165,46,33,9,151,33,240,187,113,226,62,134,16,132,39,47,217,120,93,76,100,38,34,11,154,82,141,9,76,124,37,254,227,120,199,247,87,11,22,126,144,156,66,229,16,223,156,93,188,
54,196,182,148,165,199,144,132,39,43,170,81,188,250,124,244,194,19,166,49,11,66,92,33,178,228,197,232,98,39,77,236,202,83,247,97,54,16,130,40,249,2,120,251,240,27,50,177,8,179,129,228,126,245,8,66,16,
157,222,22,120,23,129,244,107,216,197,147,249,30,210,251,24,215,41,240,222,38,39,141,151,102,66,116,198,82,151,27,230,16,132,234,113,54,16,132,39,16,130,215,176,132,25,8,66,19,128,209,8,76,66,105,8,66,
16,131,37,194,240,92,75,102,60,66,245,194,123,103,23,17,121,104,75,166,66,19,18,216,66,108,196,120,132,198,136,66,9,112,185,100,233,63,74,19,33,9,218,68,33,243,30,165,196,212,171,33,9,236,132,229,226,
200,37,204,164,39,16,132,33,61,243,33,9,168,65,227,38,194,16,132,116,91,248,82,235,103,232,150,36,72,49,162,30,7,128,159,20,165,229,115,122,79,18,225,225,63,34,99,201,235,130,238,100,230,242,181,148,162,
196,136,66,122,166,210,136,82,159,113,49,49,229,19,225,141,116,201,195,18,30,223,100,226,250,238,205,67,226,16,132,214,174,38,66,19,38,78,95,150,36,50,19,17,9,144,107,88,97,127,12,39,161,234,88,221,18,
226,255,0,5,19,188,65,162,113,114,227,213,212,200,36,61,177,10,159,115,208,253,41,137,212,68,66,83,248,95,16,147,104,216,190,100,226,16,159,193,75,197,197,148,165,41,70,196,252,159,153,9,196,39,47,136,
65,9,31,6,52,65,6,174,144,249,162,33,15,206,225,53,112,254,31,66,250,46,175,247,188,76,162,101,246,210,228,199,113,114,241,20,165,214,92,65,162,99,216,55,194,230,227,229,228,18,38,206,86,78,167,162,148,
191,192,197,144,132,34,60,44,125,216,52,53,136,75,210,207,36,39,166,113,120,163,23,12,72,101,23,170,151,22,159,10,54,86,83,232,173,137,114,190,227,225,15,192,178,136,132,215,143,200,148,47,181,178,240,
159,243,177,178,148,91,75,211,225,11,211,74,82,151,110,82,245,50,12,248,54,44,43,197,19,39,129,162,117,56,98,38,53,137,8,66,9,30,16,223,145,100,18,214,137,144,132,245,61,76,164,32,151,15,225,244,33,107,
125,46,167,178,123,97,4,132,79,224,67,68,216,77,157,194,11,193,117,162,16,132,65,253,233,108,217,172,98,237,162,19,18,200,76,185,68,242,16,157,210,255,0,13,156,10,216,151,77,16,75,134,35,244,92,79,68,
245,62,44,210,227,120,153,125,82,144,156,36,76,76,124,209,190,16,197,225,19,209,8,66,122,27,41,117,241,4,133,221,197,17,75,233,163,37,32,151,140,108,165,22,79,74,41,112,153,71,141,188,92,191,77,40,153,
70,55,204,203,231,21,9,141,148,251,220,233,241,82,194,137,225,49,188,81,177,103,143,107,218,33,122,104,130,199,144,131,201,253,55,209,114,136,82,172,132,200,66,16,132,26,33,4,178,19,170,95,77,40,188,237,
40,216,253,51,138,92,132,244,181,137,107,24,139,139,208,217,74,81,49,20,165,47,173,139,176,75,210,202,94,15,1,189,179,152,76,243,137,107,68,32,134,70,66,9,122,33,8,66,100,39,51,72,53,218,23,75,41,75,183,
27,226,237,226,98,9,112,216,223,1,188,253,23,193,142,151,201,70,196,202,82,141,141,243,127,49,139,105,118,113,57,140,75,150,139,137,113,240,189,83,26,152,136,52,62,38,193,33,248,17,6,132,177,178,151,207,
47,193,75,224,165,46,49,182,199,168,163,108,165,40,153,74,93,188,82,240,245,172,76,110,161,56,127,199,8,66,16,158,139,195,216,121,41,68,202,94,41,75,232,108,162,200,66,19,111,16,154,188,23,96,215,172,
189,45,93,81,139,150,143,152,133,139,154,62,32,144,199,170,250,88,200,65,34,122,153,74,81,188,181,69,227,249,167,51,103,185,122,41,75,204,25,8,68,120,200,250,163,120,250,153,75,232,89,70,248,13,148,189,
38,65,226,62,228,32,145,7,196,33,50,15,138,54,95,68,38,54,82,151,138,92,163,226,226,242,244,196,92,55,136,165,212,134,226,24,76,162,122,241,116,154,241,120,45,32,216,178,241,8,66,16,157,78,39,51,94,166,
92,39,180,108,189,210,251,86,82,229,215,143,136,77,125,76,66,226,16,157,221,165,16,254,116,184,124,166,82,148,99,67,93,177,46,233,121,121,118,242,241,101,17,75,140,132,33,8,37,196,39,47,150,196,137,237,
104,104,153,53,61,52,165,41,74,94,124,139,40,178,148,165,23,161,251,94,78,225,9,197,212,76,98,198,33,38,76,67,226,241,53,12,92,75,38,22,254,144,130,71,193,177,146,144,75,41,74,92,94,214,248,89,40,150,
82,141,148,165,41,68,198,41,74,82,151,46,183,156,99,38,70,70,66,9,17,30,7,6,198,233,24,152,144,76,69,218,82,137,151,208,150,92,108,108,165,229,15,224,216,138,82,148,185,113,240,144,209,8,66,9,9,99,98,
99,247,190,222,82,237,47,16,158,150,126,243,117,11,62,182,151,166,38,38,95,5,41,117,109,200,207,56,246,227,123,61,47,47,55,19,198,198,197,139,208,178,15,19,19,47,241,61,158,120,165,203,233,132,62,98,94,
184,65,237,41,75,168,72,100,16,242,50,50,11,154,82,241,122,165,203,233,189,93,157,161,99,101,242,54,82,242,222,82,233,74,92,163,103,224,151,156,130,94,10,92,63,33,50,148,190,11,227,91,19,226,237,202,82,
244,169,13,76,68,202,54,50,141,237,47,15,105,68,203,194,16,189,37,159,17,107,27,87,199,11,25,50,148,163,108,79,23,19,27,47,145,12,124,15,10,92,156,188,130,67,47,8,153,251,220,33,15,152,124,169,75,148,
184,245,122,223,165,50,250,161,58,162,101,19,27,47,84,165,40,217,74,82,148,79,20,76,79,80,134,26,143,213,59,165,245,194,16,158,164,202,55,180,165,19,41,118,240,217,74,94,25,61,75,212,196,196,249,165,237,
227,201,136,137,252,16,131,229,234,251,147,169,183,16,223,51,38,46,191,114,15,211,6,252,101,199,139,18,26,23,40,165,241,194,66,67,218,38,92,89,114,148,111,46,92,92,76,50,19,63,79,220,104,104,132,33,228,
88,140,13,121,26,195,88,189,200,99,126,11,231,41,113,124,27,41,248,49,75,141,9,12,165,41,74,92,72,110,22,240,137,148,165,41,75,148,163,234,245,74,82,226,141,148,165,247,47,67,101,41,75,253,83,40,153,113,
62,97,9,176,156,65,46,74,33,50,159,93,62,233,74,82,243,114,240,187,165,229,151,25,75,139,133,195,41,125,11,27,47,161,20,165,218,93,104,104,76,94,120,121,69,195,123,50,148,184,181,115,8,136,179,198,82,
148,125,195,227,41,69,205,27,41,74,82,137,117,113,20,165,27,40,190,229,199,218,25,9,22,194,18,40,67,231,31,114,109,19,197,231,27,47,62,79,34,91,228,140,158,72,66,16,132,33,8,76,90,199,130,101,200,66,16,
93,181,71,175,4,200,50,16,133,200,36,124,62,177,34,12,132,32,242,248,30,33,98,30,78,155,30,161,9,144,98,68,38,144,132,38,210,148,165,40,153,74,54,82,151,88,202,82,148,184,185,165,203,148,76,167,238,161,
147,155,205,41,118,242,144,214,78,225,61,55,26,33,6,182,106,9,16,154,124,19,195,126,121,131,91,57,158,169,194,41,118,141,148,187,74,81,99,230,229,41,68,241,75,232,79,24,254,137,106,203,183,27,17,74,93,
165,30,45,163,101,196,62,33,8,61,66,248,81,178,148,165,41,74,82,151,201,79,58,197,197,19,202,50,148,76,162,22,178,16,74,35,232,69,230,114,144,241,139,97,8,66,16,132,230,34,19,82,26,22,16,132,32,151,138,
53,136,65,16,131,68,197,144,130,33,61,208,156,26,33,9,238,100,31,11,200,241,33,140,66,68,33,7,224,66,68,198,54,54,44,131,66,225,174,105,124,20,108,93,220,162,120,134,245,33,228,33,8,66,100,217,196,198,
50,19,170,81,245,9,210,245,66,16,132,39,104,99,226,119,8,78,215,16,132,214,130,193,10,55,224,111,201,74,82,151,83,62,141,31,4,255,0,138,151,25,75,205,199,197,31,8,75,132,244,78,86,65,250,158,94,155,200,
124,41,75,197,218,82,148,108,108,67,202,81,136,188,165,176,132,215,175,230,205,104,158,68,136,37,223,233,71,229,144,132,233,161,45,127,5,144,74,19,139,231,22,178,111,205,132,203,231,153,70,143,129,162,
16,130,92,49,136,89,63,129,114,200,66,16,152,187,111,200,159,45,9,19,23,48,66,201,141,9,11,94,161,50,16,92,76,152,245,33,34,118,254,98,251,151,20,165,199,202,68,33,8,76,132,39,16,132,33,54,148,165,40,
132,134,132,182,99,30,47,95,230,47,84,196,216,66,19,164,136,66,19,149,176,104,132,33,89,68,203,53,4,136,53,139,110,53,234,158,6,37,253,48,132,18,199,231,105,121,157,93,248,82,151,210,254,98,203,197,40,
250,188,221,100,200,201,197,196,65,34,16,124,177,178,236,33,7,194,85,144,158,167,137,229,230,148,184,201,139,150,66,19,94,221,79,95,16,154,95,75,203,120,223,145,189,90,253,44,165,46,81,62,159,51,42,233,
191,34,101,47,77,115,9,140,162,120,217,116,163,98,120,209,9,210,27,40,222,66,16,152,197,211,39,16,153,7,227,94,65,125,200,66,16,132,60,49,118,249,92,35,243,86,178,9,19,39,75,213,9,210,12,79,139,238,130,
40,159,80,132,33,9,144,132,26,229,62,18,33,8,66,19,39,16,132,39,80,156,66,107,196,182,148,187,53,122,105,118,151,133,235,165,41,124,15,22,66,20,188,221,185,8,66,16,132,33,8,33,45,108,165,19,19,40,216,
222,44,66,40,197,191,10,81,190,127,4,136,79,75,41,74,82,148,162,126,214,66,106,31,161,237,19,199,140,124,46,31,206,23,22,144,99,98,33,56,103,158,155,27,164,103,198,92,127,7,244,76,76,165,40,138,44,104,
157,60,163,99,110,148,243,199,193,50,137,148,98,90,251,158,137,183,149,180,111,154,47,190,134,169,8,66,19,16,99,238,16,69,47,48,100,215,143,133,203,225,115,68,203,134,246,141,250,46,95,77,40,135,180,76,
148,132,229,136,132,26,217,232,121,125,51,184,65,33,147,97,60,16,132,233,241,9,179,30,221,67,200,70,66,16,132,33,9,140,98,68,201,228,66,16,67,226,16,124,49,33,46,40,216,182,19,91,229,141,151,18,18,218,
121,99,202,81,139,33,8,66,16,132,244,209,189,93,206,232,153,75,196,225,148,95,11,148,188,65,44,120,249,162,214,248,162,198,132,166,49,137,137,223,91,9,221,89,251,140,107,18,20,76,154,182,148,165,41,75,
176,104,132,38,66,113,70,19,27,19,198,136,66,16,75,136,65,226,226,18,112,133,219,196,81,177,143,0,186,165,203,173,19,33,8,66,99,101,16,196,203,191,185,54,13,16,132,38,207,84,39,240,39,235,165,19,225,161,
106,16,131,198,45,104,156,191,226,132,212,182,235,197,195,234,16,132,237,236,33,53,47,100,26,17,8,124,225,178,227,240,48,158,204,98,68,214,203,213,17,112,78,242,200,78,166,94,63,114,8,98,247,66,16,132,
230,107,126,5,204,31,129,122,102,33,8,53,234,101,225,33,33,151,165,212,194,83,105,116,167,214,24,108,66,98,125,50,13,8,161,9,203,245,62,91,47,44,94,246,76,111,207,47,135,179,165,159,80,188,15,140,84,253,
46,92,120,157,219,159,164,202,49,74,93,163,126,154,82,228,200,52,34,19,137,252,173,119,8,76,99,197,143,33,10,81,11,143,127,56,159,203,120,162,16,200,66,108,226,151,143,188,46,102,194,122,11,150,248,156,
44,132,212,77,24,80,37,204,18,225,249,18,212,66,16,99,248,49,45,132,26,32,241,106,198,136,76,132,32,178,122,169,75,144,132,33,7,144,156,177,44,165,198,253,20,165,197,46,178,101,19,230,228,242,76,66,80,
165,25,50,50,50,101,244,50,143,149,247,30,36,65,9,245,9,204,38,180,66,100,33,9,179,26,244,66,108,229,190,227,38,62,158,194,98,201,144,132,32,203,224,79,193,60,159,6,202,81,50,144,103,194,244,198,196,66,
19,210,184,132,233,138,47,13,27,46,47,162,19,30,222,231,19,167,169,144,132,229,62,39,76,156,182,82,244,185,165,234,148,94,151,179,152,36,63,77,229,251,33,7,139,165,196,198,66,19,150,44,120,182,236,225,
112,200,66,12,94,149,199,239,161,234,244,82,151,166,76,184,186,108,108,253,225,118,150,81,12,120,153,49,23,210,198,23,250,16,186,177,178,151,60,21,11,201,75,171,138,82,148,165,41,75,232,132,38,190,38,
177,34,16,154,217,74,82,139,88,223,75,238,63,130,126,70,60,90,241,31,16,241,11,33,8,37,144,98,126,74,49,137,137,229,199,231,19,137,130,83,103,43,148,199,233,107,200,248,132,197,202,226,115,56,94,170,93,
72,72,104,110,49,58,60,131,225,12,91,54,141,139,134,66,11,211,74,94,155,196,44,189,70,66,19,135,137,148,190,122,111,153,232,165,41,75,204,33,4,136,76,157,182,81,50,240,217,244,153,8,77,153,50,100,18,214,
37,140,163,98,185,9,194,203,220,38,177,139,23,87,133,234,130,229,144,132,33,9,139,95,161,147,16,197,194,234,158,88,132,34,44,108,111,134,246,50,113,74,82,242,9,148,165,40,189,144,152,246,118,200,36,66,
11,89,54,106,199,240,74,62,225,4,134,76,37,233,106,158,5,40,217,68,245,35,243,39,162,19,152,66,122,231,51,9,19,33,8,36,62,147,244,44,165,19,41,120,187,74,81,178,159,98,12,76,163,200,77,165,40,134,41,113,
117,55,207,162,100,23,16,132,244,210,242,199,183,23,165,122,158,175,225,108,108,94,88,215,131,205,202,92,130,95,205,50,148,91,56,132,245,60,75,166,250,75,136,65,34,19,181,213,244,76,152,202,60,156,254,
147,167,147,16,65,111,193,8,65,250,33,54,16,132,196,33,8,34,193,224,158,93,189,62,94,190,24,152,159,48,132,33,8,66,107,201,205,47,119,41,117,110,16,132,18,39,19,33,51,244,94,137,219,203,205,230,16,68,
33,8,66,16,132,38,60,69,246,172,122,209,8,65,99,121,45,18,240,52,126,137,243,6,134,134,39,134,239,48,152,134,95,226,165,47,181,99,125,46,238,47,68,33,49,9,252,44,152,125,79,101,230,16,132,196,60,130,66,
254,57,253,175,170,33,178,250,103,75,210,248,163,101,19,40,197,41,70,36,79,98,244,50,16,76,79,136,126,240,217,110,62,33,8,136,52,65,8,33,20,185,74,82,137,158,51,247,166,242,234,202,82,151,208,177,170,
68,184,132,38,46,166,63,84,32,214,51,243,209,8,76,165,197,41,74,82,148,188,76,125,66,16,132,33,56,187,8,65,20,106,196,252,148,198,137,148,165,46,65,8,66,98,216,52,53,143,208,202,93,95,193,9,136,152,200,
66,19,217,75,233,165,203,205,47,162,229,201,176,104,75,219,117,122,105,74,81,11,170,83,206,95,226,132,39,51,27,41,75,148,165,27,41,75,213,44,45,22,210,148,188,62,16,153,74,82,247,117,226,198,65,34,113,
8,36,53,233,190,150,44,184,185,108,101,40,223,11,23,13,114,184,132,19,41,74,82,136,120,200,78,47,73,148,165,213,203,17,147,33,9,238,162,219,234,132,218,82,141,151,41,74,82,139,127,121,240,66,19,184,77,
165,212,196,241,144,140,97,4,225,246,200,66,113,118,13,13,19,154,81,190,175,162,226,198,81,109,41,75,194,199,139,41,114,19,181,203,25,113,74,82,148,162,41,75,137,243,9,252,48,132,33,58,108,162,125,34,
242,241,46,111,55,248,83,200,62,39,16,136,132,33,8,52,66,16,130,18,30,60,165,47,119,23,173,228,201,224,132,33,9,239,94,203,228,92,182,34,13,8,65,4,71,133,232,100,34,196,39,45,18,16,72,132,216,65,16,107,
213,125,6,188,225,44,126,133,196,39,51,41,74,82,241,50,19,167,143,208,138,95,77,218,82,136,32,240,108,165,225,50,137,226,68,32,150,52,65,174,66,16,132,18,26,135,150,200,65,114,131,15,9,221,246,34,148,
162,18,27,27,41,74,82,241,71,136,108,188,180,66,16,152,186,123,50,108,226,148,162,127,215,9,151,31,11,33,6,137,232,122,196,186,88,242,151,84,165,197,172,95,118,16,132,238,123,25,113,178,148,165,212,60,
88,182,151,215,123,163,16,189,16,155,61,105,148,188,182,81,50,148,165,230,151,15,128,98,148,185,75,144,147,152,66,100,33,8,66,16,107,186,39,228,79,25,74,82,148,162,27,41,74,82,229,203,211,200,66,16,75,
215,71,219,18,237,109,243,141,148,165,254,4,33,123,168,138,63,36,39,166,16,152,97,199,242,155,47,107,27,41,75,202,101,41,75,183,187,234,185,8,52,76,76,162,226,151,219,74,82,148,165,41,114,237,202,82,137,
141,151,111,47,154,82,151,41,121,104,132,245,92,188,92,165,40,184,98,101,40,153,112,157,26,202,92,188,172,132,227,206,26,33,9,147,87,52,187,74,81,22,98,148,165,41,74,82,229,41,125,49,144,165,46,178,137,
148,165,41,74,92,55,213,41,113,68,202,82,227,202,81,49,50,148,188,221,126,148,202,48,159,130,137,151,104,197,148,165,40,134,246,241,74,44,163,120,186,82,148,165,41,122,130,22,50,243,50,148,165,46,78,18,
22,13,13,16,132,32,144,176,153,75,132,203,151,105,74,38,54,54,126,137,141,151,79,162,250,224,208,195,143,122,218,92,74,143,199,84,111,219,74,93,91,121,165,229,112,200,76,75,27,41,117,114,202,82,151,91,
41,117,148,165,41,75,10,82,227,241,148,186,177,115,56,126,165,213,40,184,108,158,169,203,217,194,240,60,132,38,44,252,213,15,209,229,202,82,249,194,194,151,105,75,158,89,231,105,74,82,143,105,74,82,148,
67,244,33,162,19,181,140,107,223,50,19,154,82,240,196,196,203,148,188,11,165,41,75,148,165,40,153,120,165,41,75,148,165,40,134,241,49,188,67,101,41,74,39,224,184,217,75,213,41,75,204,225,228,200,78,38,
194,9,100,33,8,39,143,134,150,210,227,46,49,49,60,81,106,25,75,204,33,61,208,131,67,12,78,105,122,93,38,52,66,16,152,222,49,122,158,76,165,238,227,69,226,19,40,138,93,165,245,92,108,162,219,197,41,121,
165,225,13,50,16,153,8,79,83,25,8,66,113,56,132,24,223,73,204,197,140,165,41,70,252,97,178,151,17,224,241,148,165,27,46,82,148,165,197,41,74,81,242,136,36,36,50,148,68,18,33,49,174,17,8,52,66,16,153,4,
137,180,189,66,19,97,57,99,68,32,150,177,100,33,7,139,224,197,144,132,33,5,180,163,203,148,165,234,148,165,40,138,82,140,75,135,197,41,121,163,101,41,125,40,100,202,38,82,148,163,250,82,148,76,165,19,
47,13,237,215,136,121,4,185,185,74,92,120,152,223,174,148,66,27,40,152,222,222,41,117,255,0,4,30,12,78,41,118,229,47,31,164,39,43,203,60,61,16,132,38,78,87,161,162,98,92,164,82,226,25,75,180,190,134,65,
44,187,251,143,97,48,209,33,244,154,75,97,4,137,148,165,41,75,148,165,41,125,180,122,76,165,41,74,81,178,151,23,204,127,50,19,82,26,39,44,185,8,53,204,38,33,34,16,132,25,9,169,16,75,26,26,32,145,9,204,
200,37,204,200,77,108,165,212,202,82,246,199,183,102,174,145,8,52,65,16,155,8,66,122,105,74,94,161,50,113,61,19,210,202,82,229,41,74,82,229,202,92,120,153,68,245,190,41,74,81,50,241,75,136,185,8,66,115,
70,202,93,189,92,165,47,51,138,54,82,250,239,241,194,12,48,227,103,116,165,19,19,41,74,92,167,210,19,17,9,169,16,132,25,104,144,148,25,120,132,38,78,41,75,175,18,32,209,54,140,165,238,16,127,7,137,54,
207,154,144,246,148,250,127,198,49,38,82,242,20,163,15,225,74,81,178,151,91,43,225,148,165,47,32,241,4,184,132,33,50,16,132,62,20,111,184,66,100,198,132,136,76,100,32,151,13,19,19,210,185,131,66,88,178,
19,210,150,76,155,9,136,66,106,230,240,208,248,156,81,121,26,18,196,177,144,156,47,84,33,7,196,38,76,132,33,8,50,136,132,32,214,33,9,144,132,33,8,66,16,152,132,18,38,194,122,32,209,15,155,4,132,212,177,
34,115,8,78,23,108,99,245,95,91,202,93,165,41,74,82,151,155,253,104,66,100,39,9,148,185,75,139,17,9,141,249,213,136,99,66,19,26,32,184,67,216,65,144,132,32,134,133,173,16,132,32,208,145,8,66,109,41,80,
222,34,141,229,22,76,98,162,26,33,8,65,162,19,146,30,75,171,180,186,132,33,8,36,36,76,132,219,140,92,183,143,185,179,103,20,153,50,16,131,255,0,3,80,240,69,226,16,140,140,132,23,19,11,18,154,199,220,33,
61,109,16,132,33,49,229,203,196,207,163,68,198,39,6,202,81,50,141,148,184,145,61,47,136,52,52,66,8,44,31,144,212,196,61,152,153,56,132,26,26,100,119,72,66,16,131,89,8,65,124,32,215,84,162,120,145,8,36,
52,66,9,19,16,72,152,187,132,32,208,150,82,237,40,222,66,123,161,50,119,249,196,230,127,69,46,55,231,184,77,132,33,9,179,208,223,23,41,114,151,46,94,94,174,38,76,121,74,82,243,74,82,148,165,197,212,185,
72,130,89,6,33,54,99,32,144,208,217,68,248,75,154,94,111,162,16,130,89,75,232,93,194,16,153,9,252,141,23,133,212,233,163,224,159,158,24,214,76,152,178,151,249,23,16,132,226,16,100,38,144,131,90,132,135,
236,152,208,240,72,92,52,66,77,155,4,184,92,50,108,26,229,136,99,22,61,120,242,16,130,67,196,33,8,76,124,63,77,46,93,108,165,201,139,31,115,208,182,13,108,32,208,178,16,153,53,255,0,75,124,162,98,68,33,
8,66,100,33,8,65,34,9,16,153,8,66,100,201,141,19,33,8,78,233,74,82,148,187,8,79,76,33,7,144,132,38,65,228,33,50,99,101,47,23,83,198,65,115,9,144,132,33,50,19,136,66,9,106,67,101,27,22,178,100,219,139,
217,70,202,82,137,250,80,196,252,141,193,177,124,18,31,130,137,151,212,241,144,93,78,89,9,211,230,148,111,135,169,119,60,147,135,139,30,205,38,180,53,204,225,44,190,182,52,39,166,103,145,93,108,162,123,
49,7,173,31,49,34,13,97,134,132,181,34,15,134,178,13,31,163,67,17,50,16,157,82,244,145,9,252,215,33,5,169,212,33,9,196,254,70,53,168,72,158,231,139,181,174,114,241,101,219,204,33,9,147,97,50,100,31,170,
16,132,33,9,140,72,155,250,54,90,33,9,235,155,9,204,198,62,18,33,61,12,132,33,61,19,182,46,159,9,19,136,65,139,89,96,252,145,137,49,12,130,88,187,111,165,220,39,254,43,32,138,60,92,46,26,33,32,197,205,
131,9,148,92,82,227,27,245,210,245,8,78,154,32,145,8,34,198,202,39,141,16,130,94,117,240,214,44,131,33,8,79,107,200,33,143,97,50,16,156,207,75,196,249,131,233,12,122,132,191,149,161,162,9,9,19,32,241,
100,198,65,13,148,100,245,50,98,216,77,153,8,78,223,43,167,176,157,175,107,66,23,204,108,90,209,50,116,189,144,95,248,204,132,245,209,188,132,16,201,136,157,178,49,107,126,68,252,251,94,222,110,172,158,
168,46,219,47,80,248,39,143,183,211,26,33,8,37,196,18,217,151,217,74,92,165,202,81,50,242,216,216,152,216,217,68,244,163,101,19,27,242,33,178,148,162,22,82,140,190,248,36,66,100,26,39,170,241,122,121,
243,97,61,200,255,217,0,0};

const char* Mb1AudioProcessorEditor::woodtablebw_vsmall_jpg = (const char*) resource_Mb1AudioProcessorEditor_woodtablebw_vsmall_jpg;
const int Mb1AudioProcessorEditor::woodtablebw_vsmall_jpgSize = 159460;


//[EndFile] You can add extra defines here...
//[/EndFile]
