/*
  ==============================================================================

    DelayLine.cpp
    Created: 8 Apr 2014 8:19:44pm
    Author:  Jennifer Hsu

    simple delay line that reads and writes
  ==============================================================================
*/

#include "DelayLine.h"
#include <stdio.h>
#include <string.h>
#include <cstdlib>
#include <cmath>

#define MAX_DELAY 441000

DelayLine::DelayLine( )
{
    m_fs = 0.0f;
    m_delSamples = 0.0f;
    m_writer = 0;
    m_reader = 0;
    m_first = 0;
    m_last = 0;
    m_delBuffer = new float[MAX_DELAY];
    
    a = -0.33333333333333f;
	b = 0.0;
	c = 1.0f;
	d = 0.0;
}


DelayLine::DelayLine( float fs, float delSamples)
{
    m_fs = fs;
    m_delSamples = delSamples;
    m_writer = 0;
    m_reader = m_writer - m_delSamples;
    m_last = 0;
    m_first = m_last - m_delSamples;
    while( m_reader > MAX_DELAY )
    {
        m_reader -= MAX_DELAY;
    }
    while( m_reader < 0 )
    {
        m_reader += MAX_DELAY;
    }
    m_delBuffer = new float[MAX_DELAY];
    
    a = -0.33333333333333f;
	b = 0.0;
	c = 1.0f;
	d = 0.0;
}


DelayLine::~DelayLine( )
{
    delete m_delBuffer;
}


void DelayLine::setSampleRate( float fs )
{
    m_fs = fs;
    m_reader = m_writer - m_delSamples;
    while( m_reader >= MAX_DELAY )
    {
        m_reader -= MAX_DELAY;
    }
    while( m_reader < 0 )
    {
        m_reader += MAX_DELAY;
    }
}


void DelayLine::setDelaySamples( float delSamples )
{
    m_delSamples = delSamples;
    m_reader = m_writer - m_delSamples;
    while( m_reader >= MAX_DELAY )
    {
        m_reader -= MAX_DELAY;
    }
    while( m_reader < 0 )
    {
        m_reader += MAX_DELAY;
    }
}


float DelayLine::getSampleRate( )
{
    return m_fs;
}


float DelayLine::getDelaySamples( )
{
    return m_delSamples;
}


void DelayLine::tick(float * inSamp, float * outSamp, float fb, int fbNow)
{
    
    // write to delay buffer
    m_delBuffer[m_writer] = *inSamp;
    
    // do a fractional read
    float M = floor(m_delSamples);
    float frac = m_delSamples - M;
    int reader1 = floor(m_writer - M);
    int reader2 = floor(m_writer - (M+1));
    
    // wrap
    while( reader1 >= MAX_DELAY )
    {
        reader1 -= MAX_DELAY;
    }
    while( reader1 < 0 )
    {
        reader1 += MAX_DELAY;
    }
    while( reader2 >= MAX_DELAY )
    {
        reader2 -= MAX_DELAY;
    }
    while( reader2 < 0 )
    {
        reader2 += MAX_DELAY;
    }
    
    // fractional read
    *outSamp = (1.0f-frac)*m_delBuffer[reader1] + frac*m_delBuffer[reader2];
    
    // feedback
    if( fbNow == 1 )
    {
        m_delBuffer[m_writer] = (1.0f-fb)*m_delBuffer[m_writer] + (fb*(*outSamp));
        
        // easy saturation?
        if( m_delBuffer[m_writer] > 1.0f )
        {
            m_delBuffer[m_writer] = 1.0f;
        } else if(m_delBuffer[m_writer] <= -1.0f )
        {
            m_delBuffer[m_writer] = -1.0f;
        } else
        {
            m_delBuffer[m_writer] = a * (m_delBuffer[m_writer] * m_delBuffer[m_writer] * m_delBuffer[m_writer])  + b * (m_delBuffer[m_writer]  * m_delBuffer[m_writer])  + c * (m_delBuffer[m_writer]) + d;
        }
    }
    
    

    
    // increment writer
    m_writer++;
    while( m_writer >= MAX_DELAY )
    {
        m_writer -= MAX_DELAY;
    }
    while( m_writer < 0 )
    {
        m_writer += MAX_DELAY;
    }
    
    
    
/*
    m_delBuffer[m_writer] = *inSamp;
    *outSamp = m_delBuffer[m_reader];
    
    m_writer++;
    while( m_writer > MAX_DELAY )
    {
        m_writer -= MAX_DELAY;
    }
    while( m_writer < 0 )
    {
        m_writer += MAX_DELAY;
    }
    m_reader++;
    while( m_reader > MAX_DELAY )
    {
        m_reader -= MAX_DELAY;
    }
    while( m_reader < 0 )
    {
        m_reader += MAX_DELAY;
    }
 */
    
}

void DelayLine::addFbSample( float * fbSamp, float fbVal)
{
    // decrement writer
    m_writer--;
    while( m_writer < 0 )
    {
        m_writer += MAX_DELAY;
    }
    while( m_writer >= MAX_DELAY )
    {
        m_writer -= MAX_DELAY;
    }
    
    //m_delBuffer[m_writer] = (1.0f-fbVal)*m_delBuffer[m_writer] + (*(fbSamp) * fbVal);
    m_delBuffer[m_writer] += (*(fbSamp) * fbVal);
    
    // easy saturation?
    /*
    if( m_delBuffer[m_writer] > 1.0f )
    {
        m_delBuffer[m_writer] = 1.0f;
    } else if(m_delBuffer[m_writer] <= -1.0f )
    {
        m_delBuffer[m_writer] = -1.0f;
    } else
    {
        m_delBuffer[m_writer] = a * (m_delBuffer[m_writer] * m_delBuffer[m_writer] * m_delBuffer[m_writer])  + b * (m_delBuffer[m_writer]  * m_delBuffer[m_writer])  + c * (m_delBuffer[m_writer]) + d;
    }
    */
    // increment writer
    m_writer++;
    while( m_writer >= MAX_DELAY )
    {
        m_writer -= MAX_DELAY;
    }
    while( m_writer < 0 )
    {
        m_writer += MAX_DELAY;
    }
}


void DelayLine::clear( )
{
    memset(m_delBuffer, 0.0f, sizeof(float)*MAX_DELAY);
    m_writer = MAX_DELAY-1;
}
