/*
  ==============================================================================

    DelayLine.h
    Created: 8 Apr 2014 8:19:44pm
    Author:  Jennifer Hsu
 
    simple delay line (just reads and writes, nothing special)

  ==============================================================================
*/

#ifndef DELAYLINE_H_INCLUDED
#define DELAYLINE_H_INCLUDED

class DelayLine
{
    
public:
    
    // constructor/destructor
    DelayLine( );
    DelayLine( float fs, float delSamples );
    ~DelayLine( );
    
    // set/get
    void setSampleRate( float fs );
    void setDelaySamples( float delSamples );
    float getSampleRate( );
    float getDelaySamples( );
    
    // tick for samples
    void tick(float * inSamp, float * outSamp, float fb, int fbNow);
    void addFbSample( float * fbSamp, float fbVal);
    
    // clear out the delay line
    void clear( );
    
private:
    
    float * m_delBuffer;
    float m_fs;
    float m_delSamples;
    int m_writer;
    int m_reader;
    int m_first;
    int m_last;
    
    // soft clip coefficients
    float a, b, c, d;
    
};



#endif  // DELAYLINE_H_INCLUDED

