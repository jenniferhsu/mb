/*
  ==============================================================================

    This file was auto-generated!

    It contains the basic startup code for a Juce application.

  ==============================================================================
*/

#include "PluginProcessor.h"
#include "PluginEditor.h"


//==============================================================================
Mb1AudioProcessor::Mb1AudioProcessor()
{
    
    // HARDCODED!!!! SAMPLE RATE!!!
    fs = 44100.0f;
    fsLast = fs;
    
    ampCurrentVal = 0.0f; // push VST default to effect
    
    freqCutoff0Val = 100.0f;
    freqCutoff1Val = 300.0f;
    
    freqCutoffB = log10(20)/log10(2);
    freqCutoffM = (log10(22050)/log10(2)) - freqCutoffB;
    
    delTime0Inc = 0.0f;
    delTime0Target = 0.0f;
    delTime0Current = 0.0f;
    
    amp0ValCurrent = amp1ValCurrent = amp2ValCurrent = 1.0f;
    amp0ValTarget = amp1ValTarget = amp2ValTarget = 1.0f;
    amp0ValInc = amp1ValInc = amp2ValInc = 0.0f;
    
//    fb0Val = fb1Val = fb2Val = 0.0f;
    fb0ValCurrent = fb1ValCurrent = fb2ValCurrent = 0.0f;
    fb0ValTarget = fb1ValTarget = fb2ValTarget = 0.0f;
    fb0ValInc = fb1ValInc = fb2ValInc = 0.0f;
    
    delTime1Inc = 0.0f;
    delTime1Target = 0.0f;
    delTime1Current = 0.0f;
    
    delTime2Inc = 0.0f;
    delTime2Target = 0.0f;
    delTime2Current = 0.0f;
    
    //UserParams[ampCurrent] = ampCurrentVal; // default amplitude
    float expCutoff = log10(freqCutoff0Val)/log10(2);
    UserParams[freqCutoff0] = (expCutoff - freqCutoffB) / freqCutoffM;
    expCutoff = log10(freqCutoff1Val)/log10(2);
    UserParams[freqCutoff1] = (expCutoff - freqCutoffB) / freqCutoffM;
    UserParams[delTime0] = delTime0Current;
    UserParams[delTime1] = delTime1Current;
    UserParams[delTime2] = delTime2Current;
    UserParams[amp0] = amp0ValCurrent;
    UserParams[amp1] = amp1ValCurrent;
    UserParams[amp2] = amp2ValCurrent;
    UserParams[fb0] = fb0ValCurrent;
    UserParams[fb1] = fb1ValCurrent;
    UserParams[fb2] = fb2ValCurrent;
    
    blockCntr = 0;
    UIUpdateFlag = true; // request UI update
    
    // init parameters
    pi = 4.0f * atanf(1.0f);
	twoPi = 8.0f * atanf(1.0f);
    
    // hardcode for now!
    //blockSz = 1024;
    blockSz = 256;
    blockSzLast = blockSz;
    sizeFFT = 4*blockSz;
    halfSizeFFT = ( float )sizeFFT/2.0f;
    
    log2n = (long)log2f((float)sizeFFT);
    setupReal = create_fftsetup(log2n, FFT_RADIX2);
    
    // FFT parameter initialization
    setupReal = vDSP_create_fftsetup(log2n, FFT_RADIX2);
    if (setupReal == NULL)
    {
        fprintf(stderr, "Error, vDSP_create_fftsetup failed.\n");
        exit (EXIT_FAILURE);
    }
    
    
    float *ObservedMemory = (float *)malloc(sizeFFT * sizeof(*ObservedMemory));
    
    if (ObservedMemory == NULL )// || ifftOut == NULL)
    {
        fprintf(stderr, "Error, failed to allocate memory.\n");
        exit(EXIT_FAILURE);
    }
    
    // assign half of ObservedMemory to reals and half to imaginaries.
    Observed = { ObservedMemory, ObservedMemory + halfSizeFFT };
    
    input0 = new float[sizeFFT];
    input1 = new float[sizeFFT];
    fftBuffer0 = new float[sizeFFT];
    fftBuffer1 = new float[sizeFFT];
    ifftOut0 = new float[sizeFFT];
    ifftOut1 = new float[sizeFFT];
    olaBlock0 = new float[sizeFFT];
    olaBlock1 = new float[sizeFFT];
    analWindow = new float[sizeFFT];
    synthWindow = new float[sizeFFT];
    memset(input0, 0, sizeFFT*sizeof(float));
    memset(input1, 0, sizeFFT*sizeof(float));
    memset(fftBuffer0, 0, sizeFFT*sizeof(float));
    memset(fftBuffer1, 0, sizeFFT*sizeof(float));
    memset(ifftOut0, 0, sizeFFT*sizeof(float));
    memset(ifftOut1, 0, sizeFFT*sizeof(float));
    memset(olaBlock0, 0, sizeFFT*sizeof(float));
    memset(olaBlock1, 0, sizeFFT*sizeof(float));
    memset(analWindow, 0, sizeFFT*sizeof(float));
    memset(synthWindow, 0, sizeFFT*sizeof(float));
    
    output0 = new float[sizeFFT];
    output1 = new float[sizeFFT];
    memset(output0, 0, sizeFFT*sizeof(float));
    memset(output1, 0, sizeFFT*sizeof(float));
    
    output00 = new float[sizeFFT];
    output01 = new float[sizeFFT];
    memset(output00, 0, sizeFFT*sizeof(float));
    memset(output01, 0, sizeFFT*sizeof(float));
    
    output10 = new float[sizeFFT];
    output11 = new float[sizeFFT];
    memset(output10, 0, sizeFFT*sizeof(float));
    memset(output11, 0, sizeFFT*sizeof(float));
    
    inShift0 = new float[sizeFFT];
    inShift1 = new float[sizeFFT];
    memset(inShift0, 0, sizeFFT*sizeof(float));
    memset(inShift1, 0, sizeFFT*sizeof(float));
    
    bufferPosition0 = bufferPosition1 = 0;
    
    // make the hann window
    makeHannWindow(sizeFFT, analWindow);
    makeHannWindow(sizeFFT, synthWindow);
    scaleWindows( );
    
    // == MULTIBAND TEST!!! ==
    // set up structures for multiband FFT
    // band 1
    float *ObservedMemory0 = (float *)malloc(sizeFFT * sizeof(float *));
    if (ObservedMemory0 == NULL )// || ifftOut == NULL)
    {
        fprintf(stderr, "Error, failed to allocate memory for ObservedMemory0.\n");
        exit(EXIT_FAILURE);
    }
    Observed0 = { ObservedMemory0, ObservedMemory0 + halfSizeFFT };
    ifftOut00 = new float[sizeFFT];
    ifftOut01 = new float[sizeFFT];
    olaBlock00 = new float[sizeFFT];
    olaBlock01 = new float[sizeFFT];
    memset(ifftOut00, 0, sizeFFT*sizeof(float));
    memset(ifftOut01, 0, sizeFFT*sizeof(float));
    memset(olaBlock00, 0, sizeFFT*sizeof(float));
    memset(olaBlock01, 0, sizeFFT*sizeof(float));
    binCutoff0 = 2;
    // band 2
    float *ObservedMemory1 = (float *)malloc(sizeFFT * sizeof(float *));
    if (ObservedMemory1 == NULL )// || ifftOut == NULL)
    {
        fprintf(stderr, "Error, failed to allocate memory for ObservedMemory0.\n");
        exit(EXIT_FAILURE);
    }
    Observed1 = { ObservedMemory1, ObservedMemory1 + halfSizeFFT };
    ifftOut10 = new float[sizeFFT];
    ifftOut11 = new float[sizeFFT];
    olaBlock10 = new float[sizeFFT];
    olaBlock11 = new float[sizeFFT];
    memset(ifftOut10, 0, sizeFFT*sizeof(float));
    memset(ifftOut11, 0, sizeFFT*sizeof(float));
    memset(olaBlock10, 0, sizeFFT*sizeof(float));
    memset(olaBlock11, 0, sizeFFT*sizeof(float));
    binCutoff1 = 7;
    // ==============
    
    // hardcode for now
    delLine00 = new DelayLine(fs, ( int )delTime0Current);
    delLine01 = new DelayLine(fs, ( int )delTime0Current);
    delLine10 = new DelayLine(fs, ( int )delTime1Current);
    delLine11 = new DelayLine(fs, ( int )delTime1Current);
    delLine20 = new DelayLine(fs, ( int )delTime2Current);
    delLine21 = new DelayLine(fs, ( int )delTime2Current);
    delLine00->clear( );
    delLine01->clear( );
    delLine10->clear( );
    delLine11->clear( );
    delLine20->clear( );
    delLine21->clear( );
    
    // cross fade delay lines
    delLineLowFade0 = new DelayLine(fs, ( int )delTime0Current);
    delLineLowFade1 = new DelayLine(fs, ( int )delTime0Current);
    delLineMidFade0 = new DelayLine(fs, ( int )delTime1Current);
    delLineMidFade1 = new DelayLine(fs, ( int )delTime1Current);
    delLineHighFade0 = new DelayLine(fs, ( int )delTime2Current);
    delLineHighFade1 = new DelayLine(fs, ( int )delTime2Current);
    delLineMidFade0->clear( );
    delLineMidFade1->clear( );
    delLineHighFade0->clear( );
    delLineHighFade1->clear( );
    
    lowDelFade0 = lowDelFade1 = 1.0f;
    lowDelFadeInc0 = lowDelFadeInc1 = 0.0f;
    origOrFadeLow = 0;
    transOnLow = 0;
    lowDelFadeCntr0 = lowDelFadeCntr1 = 0;
    
    midDelFade0 = midDelFade1 = 1.0f;
    midDelFadeInc0 = midDelFadeInc1 = 0.0f;
    origOrFadeMid = 0;
    transOnMid = 0;
    midDelFadeCntr0 = midDelFadeCntr1 = 0;
    
    highDelFade0 = highDelFade1 = 1.0f;
    highDelFadeInc0 = highDelFadeInc1 = 0.0f;
    origOrFadeHigh = 0;
    transOnHigh = 0;
    highDelFadeCntr0 = highDelFadeCntr1 = 0;
    
    ampRunningSum = 0.0f;
    
    // tell the host the latency from the FFT overlap add operation
    setLatencySamples(blockSz*3);
    
    // window curve
    useFreqWindows = 0;
    winCurveBins = 4;
    
    // delay time increments
    numDelTimeIncs = floor( 44100.0f / 30.0f );
    delTimeIncCntr = 0;
    
    // delay line modulation to test taht the fractional delay is working
    /*
    m0 = 40.0f;
    A = 20.0f;
    f0 = 20.0f;
    delLineModCnt = 0;
    delLineMod = new float[44100];
    for( int i = 0; i < 44100; i++ )
    {
        delLineMod[i] = m0*(1+A*sin(2*M_PI*f0*i));
    }
     */
    
    // frequency scaling
    log2toOneOver24 = logf(powf(2.0f,(1.0f/24.0f)));
    
    
    lastPosInfo.resetToDefault();
    snapMode[LOW] = snapMode[MID] = snapMode[HIGH] = false;
    snapValue[LOW] = snapValue[MID] = snapValue[HIGH] = 0.0f;
    lastBeat = -1;
    lastBar = -1;
    beat = 1;
    bar = 1;
    bpm = lastPosInfo.bpm; // default value
    
    delTimesQuant[LOW] = delTimesQuant[MID] = delTimesQuant[HIGH] = 0.0f;
    
    quantValStrings.add("0");
    quantValStrings.add("1/64");
    quantValStrings.add("1/32");
    quantValStrings.add("1/24");
    quantValStrings.add("1/16");
    quantValStrings.add("1/12");
    quantValStrings.add("1/8");
    quantValStrings.add("1/6");
    quantValStrings.add("1/4");
    quantValStrings.add("1/3");
    quantValStrings.add("1/2");
    quantValStrings.add("1");
    
    lfoCntr[LOW] = lfoCntr[MID] = lfoCntr[HIGH] = 0;
    lfoSpeed[LOW] = lfoSpeed[MID] = lfoSpeed[HIGH] = 0.5f;
    lfoSwing[LOW] = lfoSwing[MID] = lfoSwing[HIGH] = 0.7f;
    lfoM0[LOW] = lfoM0[MID] = lfoM0[HIGH] = 10.0f*(fs/1000.0f); // 10.0 is 10 milliseconds
    
    lfoLength[LOW] = lfoLength[MID] = lfoLength[HIGH] = floor(fs);
    lfoMnHigh = new float[lfoLength[HIGH]];
    
    for( int i = 0; i < lfoLength[HIGH]; i++ )
    {
        // TEMP
        lfoMnHigh[i] = lfoM0[HIGH]*(1.0f + lfoSwing[HIGH]*sin(2*M_PI*lfoSpeed[HIGH]*(( float )i/lfoLength[HIGH])));
    }
}

Mb1AudioProcessor::~Mb1AudioProcessor()
{
    
    delete [] input0;
    delete [] input1;
    delete [] fftBuffer0;
    delete [] fftBuffer1;
    delete [] ifftOut0;
    delete [] ifftOut1;
    delete [] olaBlock0;
    delete [] olaBlock1;
    delete [] analWindow;
    delete [] synthWindow;
    
    delete [] output0;
    delete [] output1;
    delete [] output00;
    delete [] output01;
    delete [] output10;
    delete [] output11;
    delete [] inShift0;
    delete [] inShift1;
    
    delete delLine00;
    delete delLine01;
    delete delLine10;
    delete delLine11;
    delete delLine20;
    delete delLine21;
    
    delete delLineHighFade0;
    delete delLineHighFade1;
    
    destroy_fftsetup(setupReal);
    free(&Observed);
    free(&Observed0);

}

//==============================================================================
const String Mb1AudioProcessor::getName() const
{
    return JucePlugin_Name;
}

int Mb1AudioProcessor::getNumParameters()
{
    return totalNumParam;
}

// Called by the host to find out the value of one of the filter's parameters.
float Mb1AudioProcessor::getParameter (int index)
{
    float expCutoff, m, b, ampValDB;
    switch(index)
    {
        /*case ampCurrent: //example update from internal
            UserParams[ampCurrent] = ampCurrentVal;
            return UserParams[ampCurrent];*/
        case freqCutoff0:
            expCutoff = log10(freqCutoff0Val)/log10(2);
            UserParams[freqCutoff0] = (expCutoff - freqCutoffB) / freqCutoffM;
            return UserParams[freqCutoff0];
        case freqCutoff1:
            expCutoff = log10(freqCutoff1Val)/log10(2);
            UserParams[freqCutoff1] = (expCutoff - freqCutoffB) / freqCutoffM;
            return UserParams[freqCutoff1];
            /*
             UserParams[freqCutoff1] = freqCutoff1Val;
             return UserParams[freqCutoff1];
             */
        case delTime0:
            if(!snapMode[LOW])
                UserParams[delTime0] = delTime0Target/3.0f;
            else
                UserParams[delTime0] = delTimesQuant[LOW];
            return UserParams[delTime0];
        case delTime1:
            if(!snapMode[MID])
                UserParams[delTime1] = delTime1Target/3.0f;
            else
                UserParams[delTime1] = delTimesQuant[MID];
            return UserParams[delTime1];
        case delTime2:
            if(!snapMode[HIGH])
                UserParams[delTime2] = delTime2Target/3.0f;
            else
                UserParams[delTime2] = delTimesQuant[HIGH];
            return UserParams[delTime2];
        case amp0:
            UserParams[amp0] = amp0ValTarget;
            return UserParams[amp0];
        case amp1:
            UserParams[amp1] = amp1ValTarget;
            return UserParams[amp1];
        case amp2:
            UserParams[amp2] = amp2ValTarget;
            return UserParams[amp2];
        case fb0:
            UserParams[fb0] = fb0ValTarget;
            return UserParams[fb0];
        case fb1:
            UserParams[fb1] = fb1ValTarget;
            return UserParams[fb1];
        case fb2:
            UserParams[fb2] = fb2ValTarget;
            return UserParams[fb2];
        /*case freqWind:
            UserParams[freqWind] = useFreqWindows;
            return UserParams[freqWind];*/
        case syncOnLow:
            UserParams[syncOnLow] = snapMode[LOW];
            return UserParams[syncOnLow];
        case syncOnMid:
            UserParams[syncOnMid] = snapMode[MID];
            return UserParams[syncOnMid];
        case syncOnHigh:
            UserParams[syncOnHigh] = snapMode[HIGH];
            return UserParams[syncOnHigh];
        default: return 0.0f; //invalid index
    }
}

// The host will call this method to change the value of one of the filter's parameters.
void Mb1AudioProcessor::setParameter (int index, float newValue)
{
    float cutoffExp, ampValDB;
    switch(index)
    {
        /*case ampCurrent:
            UserParams[ampCurrent] = newValue;
            ampCurrentVal = UserParams[ampCurrent];
            break;*/
        case freqCutoff0:
            UserParams[freqCutoff0] = newValue;
            /*
            freqCutoff0Val = UserParams[freqCutoff0];
            // hardcoded sample rate right now...
            binCutoff0 = ( int )floor((freqCutoff0Val * sizeFFT)/fs);
             */
            // value coming in is from 0 to 1
            // need to transform to Hz and then to bins
            cutoffExp = UserParams[freqCutoff0]*freqCutoffM + freqCutoffB;
            freqCutoff0Val = powf(2.0f, cutoffExp);
            binCutoff0 = ( int )floor((freqCutoff0Val * sizeFFT)/fs);
            break;
        case delTime0:
            UserParams[delTime0] = newValue;
            if( !snapMode[LOW] )
                delTime0Target = 3.0f*UserParams[delTime0];
            else
                snapDelTime(delTime0, LOW, &delTimesQuant[LOW], &delTime0Target);
            break;
        case freqCutoff1:
            UserParams[freqCutoff1] = newValue;
            cutoffExp = UserParams[freqCutoff1]*freqCutoffM + freqCutoffB;
            freqCutoff1Val = powf(2.0f, cutoffExp);
            binCutoff1 = ( int )floor((freqCutoff1Val * sizeFFT)/fs);
            break;
            /*
            UserParams[freqCutoff1] = newValue;
            freqCutoff1Val = UserParams[freqCutoff1];
            // hardcoded sample rate right now...
            binCutoff1 = ( int )floor((freqCutoff1Val * sizeFFT)/fs);
            break;
             */
        case delTime1:
            UserParams[delTime1] = newValue;
            if( !snapMode[MID] )
                delTime1Target = 3.0f*UserParams[delTime1];
            else
                snapDelTime(delTime1, MID, &delTimesQuant[MID], &delTime1Target);
            break;
        case delTime2:
            UserParams[delTime2] = newValue;
            if( !snapMode[HIGH] )
                delTime2Target = 3.0f*UserParams[delTime2];
            else
                snapDelTime(delTime2, HIGH, &delTimesQuant[HIGH], &delTime2Target);
            break;
        case amp0:
            UserParams[amp0] = newValue;
            amp0ValTarget = UserParams[amp0];
            break;
        case amp1:
            UserParams[amp1] = newValue;
            amp1ValTarget = UserParams[amp1];
            break;
        case amp2:
            UserParams[amp2] = newValue;
            amp2ValTarget = UserParams[amp2];
            break;
        case fb0:
            UserParams[fb0] = newValue;
            fb0ValTarget = UserParams[fb0];
            break;
        case fb1:
            UserParams[fb1] = newValue;
            fb1ValTarget = UserParams[fb1];
            break;
        case fb2:
            UserParams[fb2] = newValue;
            fb2ValTarget = UserParams[fb2];
            break;
        /*case freqWind:
            UserParams[freqWind] = newValue;
            if( UserParams[freqWind] == true )
                useFreqWindows = 1;
            else
                useFreqWindows = 0;
            break;*/
        case syncOnLow:
            UserParams[syncOnLow] = newValue;
            if( UserParams[syncOnLow] == true )
            {
                snapMode[LOW] = true;
                snapDelTime(delTime0, HIGH, &delTimesQuant[LOW], &delTime0Target);
            } else
            {
                snapMode[LOW] = false;
            }
            break;
        case syncOnMid:
            UserParams[syncOnMid] = newValue;
            if( UserParams[syncOnMid] == true )
            {
                snapMode[MID] = true;
                snapDelTime(delTime1, MID, &delTimesQuant[MID], &delTime1Target);
            } else
            {
                snapMode[MID] = false;
            }
            break;
        case syncOnHigh:
            UserParams[syncOnHigh] = newValue;
            if( UserParams[syncOnHigh] == true )
            {
                snapMode[HIGH] = true;
                snapDelTime(delTime2, HIGH, &delTimesQuant[HIGH], &delTime2Target);
            } else
            {
                snapMode[HIGH] = false;
            }
            break;
        default: return;
            
    }
    RequestUIUpdate( );
}

const String Mb1AudioProcessor::getParameterName (int index)
{
    switch(index)
    {
        //case ampCurrent: return "Current amplitude value";
        case freqCutoff0: return "low frequency cutoff";
        case delTime0: return "delay time (low)";
        case freqCutoff1: return "high frequency cutoff";
        case delTime1: return "delay time (mid)";
        case delTime2: return "delay time (high)";
        case amp0: return "low freq amplitude";
        case amp1: return "mid freq amplitude";
        case amp2: return "high freq amplitude";
        case fb0: return "low freq feedback";
        case fb1: return "mid freq feedback";
        case fb2: return "high freq feedback";
        //case freqWind: return "use frequency windows";
        case syncOnLow: return "sync low";
        case syncOnMid: return "sync mid";
        case syncOnHigh: return "sync high";
        default:return String::empty;
    }
}

// called when the host wants the value of the parameter to be displayed
const String Mb1AudioProcessor::getParameterText (int index)
{
    /*
    if(index>=0 && index<totalNumParam)
        return String(UserParams[index]); //return parameter value as string
    else return String::empty;
     */
    String freqCutoffString, delTimeString, fbString, ampString;
    switch(index)
    {
        //case ampCurrent:
        //    return String(ampCurrentVal);
        case freqCutoff0:
            freqCutoffString = String(freqCutoff0Val, 2);
            freqCutoffString.append(" Hz", 3);
            return freqCutoffString;
        case delTime0:
            if( !snapMode[LOW] )
            {
                delTimeString = String(delTime0Target, 2);
                delTimeString.append(" s", 2);
            } else
            {
                // get quantization text
                delTimeString = quantValStrings[( int )(delTimesQuant[LOW]*11.0f)];
            }
            return delTimeString;
        case freqCutoff1:
            freqCutoffString = String(freqCutoff1Val, 2);
            freqCutoffString.append(" Hz", 3);
            return freqCutoffString;
        case delTime1:
            if( !snapMode[MID] )
            {
                delTimeString = String(delTime1Target, 2);
                delTimeString.append(" s", 2);
            } else
            {
                // get quantization text
                delTimeString = quantValStrings[( int )(delTimesQuant[MID]*11.0f)];
            }
            return delTimeString;
        case delTime2:
            if( !snapMode[HIGH] )
            {
                delTimeString = String(delTime2Target, 2);
                delTimeString.append(" s", 2);
            } else
            {
                // get quantization text
                delTimeString = quantValStrings[( int )(delTimesQuant[HIGH]*11.0f)];
            }
            return delTimeString;
        case amp0:
            if( UserParams[amp0] != 0.0f )
                ampString = String(20.0f*log10(UserParams[amp0]));
            else
                ampString = String("-inf");
            ampString.append(" dB", 3);
            return ampString;
        case amp1:
            if( UserParams[amp1] != 0.0f )
                ampString = String(20.0f*log10(UserParams[amp1]));
            else
                ampString = String("-inf");
            ampString.append(" dB", 3);
            return ampString;
        case amp2:
            if( UserParams[amp2] != 0.0f )
                ampString = String(20.0f*log10(UserParams[amp2]));
            else
                ampString = String("-inf");
            ampString.append(" dB", 3);
            return ampString;
        case fb0:
            fbString = String(fb0ValTarget*100.0f, 0);
            fbString.append(" %", 2);
            return fbString;
        case fb1:
            fbString = String(fb1ValTarget*100.0f, 0);
            fbString.append(" %", 2);
            return fbString;
        case fb2:
            fbString = String(fb2ValTarget*100.0f, 0);
            fbString.append(" %", 2);
            return fbString;
        //case freqWind:
        //    return "use frequency windows";
        case syncOnLow:
            return String(UserParams[syncOnLow]);
        case syncOnMid:
            return String(UserParams[syncOnMid]);
        case syncOnHigh:
            return String(UserParams[syncOnHigh]);
        default:
            return String::empty;
    }
}

const String Mb1AudioProcessor::getInputChannelName (int channelIndex) const
{
    return String (channelIndex + 1);
}

const String Mb1AudioProcessor::getOutputChannelName (int channelIndex) const
{
    return String (channelIndex + 1);
}

bool Mb1AudioProcessor::isInputChannelStereoPair (int index) const
{
    return true;
}

bool Mb1AudioProcessor::isOutputChannelStereoPair (int index) const
{
    return true;
}

bool Mb1AudioProcessor::acceptsMidi() const
{
   #if JucePlugin_WantsMidiInput
    return true;
   #else
    return false;
   #endif
}

bool Mb1AudioProcessor::producesMidi() const
{
   #if JucePlugin_ProducesMidiOutput
    return true;
   #else
    return false;
   #endif
}

bool Mb1AudioProcessor::silenceInProducesSilenceOut() const
{
    return false;
}

double Mb1AudioProcessor::getTailLengthSeconds() const
{
    return 0.0;
}

int Mb1AudioProcessor::getNumPrograms()
{
    return 0;
}

int Mb1AudioProcessor::getCurrentProgram()
{
    return 0;
}

void Mb1AudioProcessor::setCurrentProgram (int index)
{
}


const String Mb1AudioProcessor::getProgramName (int index)
{
    return String::empty;
}

void Mb1AudioProcessor::changeProgramName (int index, const String& newName)
{
}

//==============================================================================
void Mb1AudioProcessor::prepareToPlay (double sampleRate, int samplesPerBlock)
{
    // Use this method as the place to do any pre-playback
    
    // initialisation that you need..
    blockSz = samplesPerBlock;
    fs = sampleRate;
    // update fft parameters based on block size and sample rate
    if( blockSzLast != blockSz || fsLast != fs)
    {
        blockSizeUpdate( );
    }
    blockSzLast = blockSz;
    fsLast = fs;
    
    // maybe don't put this here?
    // anyway to clear it all only when the player head is moved?
    // or do we not want to clear it.... don't know
    delLine00->clear( );
    delLine01->clear( );
    delLine10->clear( );
    delLine11->clear( );
    delLine20->clear( );
    delLine21->clear( );
    
    
}

void Mb1AudioProcessor::setPlayHead( AudioPlayHead * newPlayHead )
{
    ap = newPlayHead;   // this does get called...maybe let's check it
}

AudioPlayHead * Mb1AudioProcessor::getPlayHead( ) const noexcept
{
    return ap;
}

void Mb1AudioProcessor::releaseResources()
{
    // When playback stops, you can use this as an opportunity to free up any
    // spare memory, etc.
}

void Mb1AudioProcessor::processBlock (AudioSampleBuffer& buffer, MidiBuffer& midiMessages)
{
    
    // ******** PPQ STUFF ********* //

    if (ap != nullptr )
    {
        bool success = ap->getCurrentPosition(currPos);
        if( success )
        {
            // Successfully got the current time from the host..
            lastPosInfo = currPos;
        }
        else
        {
            // If the host fails to fill-in the current time, we'll just clear it to a default..
            lastPosInfo.resetToDefault();
        }
    } else
    {
        lastPosInfo.resetToDefault();
    }
    double ppq = lastPosInfo.ppqPosition;
    // tells you position at beginning of block
    // what note is it?
    // when numerator hits 1
    // with an LFO
    double beats;
    int numerator = lastPosInfo.timeSigNumerator;
    int denominator = lastPosInfo.timeSigDenominator;
    int ppqPerBar;
    if (numerator == 0 || denominator == 0)
    {
        bar = 1;
        beat = 1;
    } else
    {
        ppqPerBar = (numerator * 4 / denominator);
        // if quarter note:
        // const double beats  = (fmod (ppq, ppqPerBar) / ppqPerBar) * numerator * 1.0f;
        // if 1/6:
        //const double beats  = (fmod (ppq, ppqPerBar) / ppqPerBar) * numerator * 1.5f;
        // if eigth note:
        // const double beats  = (fmod (ppq, ppqPerBar) / ppqPerBar) * numerator * 2.0f;
        // if 1/12 note:
        //const double beats  = (fmod (ppq, ppqPerBar) / ppqPerBar) * numerator * 3.0f;
        // if 16th note:
        //const double beats  = (fmod (ppq, ppqPerBar) / ppqPerBar) * numerator * 4.0f;
        // if 1/24:
        //const double beats  = (fmod (ppq, ppqPerBar) / ppqPerBar) * numerator * 6.0f;
        // if 1/32:
        beats  = (fmod (ppq, ppqPerBar) / ppqPerBar) * numerator * snapValue[LOW];
        bar = ((int) ppq) / ppqPerBar + 1;
        beat   = ((int) beats) + 1;
        float newBpm = lastPosInfo.bpm;
        if( newBpm != bpm )
        {
            bpm = newBpm;
            // update delay times with new bpm
            snapDelTime(delTime0, LOW, &delTimesQuant[LOW], &delTime0Target);
            snapDelTime(delTime1, MID, &delTimesQuant[MID], &delTime1Target);
            snapDelTime(delTime2, HIGH, &delTimesQuant[HIGH], &delTime2Target);
        }
        
    }
    

    // **** END OF PPQ STUFF ****** //
    
    int bufferSamps = buffer.getNumSamples( );
    blockSz = getBlockSize( );
    fs = getSampleRate( );
    
    // FFT variables
    int sampsLeftInCD0, sampsLeftInCD1;
    sampsLeftInCD0 = sampsLeftInCD1 = bufferSamps;  // number of samples left to copy in from channel data
    int processSamps0, processSamps1;
    int inPtr0, inPtr1, outPtr0, outPtr1;
    inPtr0 = inPtr1 = outPtr0 = outPtr1 = 0;

    // parameter incrementing variables
    float oneOverBlockSize = 1.0f/( float )bufferSamps;
    float numIncs = fs/25.0f;
    float oneOverNumIncs = 1.0f/numIncs;
    
    //delTime0Inc = ( delTime0Target - delTime0Current ) * oneOverBlockSize;
    delTime0Inc = ( delTime0Target - delTime0Current ) * oneOverNumIncs;
    float delTime0CurrentOld = delTime0Current;
    
    //delTime1Target0 = 0.9f*delTime1Current + 0.1f*delTime1Target;
    //delTime1Inc = ( delTime1Target - delTime1Current ) * oneOverBlockSize;
    delTime1Inc = ( delTime1Target - delTime1Current ) * oneOverNumIncs;
    float delTime1CurrentOld = delTime1Current;
    
    delTime2Inc = ( delTime2Target - delTime2Current ) * oneOverNumIncs;
    float delTime2CurrentOld = delTime2Current;
    //float delTime2IncOld = delTime2Inc;
    //int delTimeIncCntrOld = delTimeIncCntr;
    
    // interpolate amplitude
    amp0ValInc = ( amp0ValTarget - amp0ValCurrent ) * oneOverBlockSize;
    float amp0ValCurrentOld = amp0ValCurrent;
    amp1ValInc = ( amp1ValTarget - amp1ValCurrent ) * oneOverBlockSize;
    float amp1ValCurrentOld = amp1ValCurrent;
    amp2ValInc = ( amp2ValTarget - amp2ValCurrent ) * oneOverBlockSize;
    float amp2ValCurrentOld = amp2ValCurrent;
    
    // interpolate feedback
    fb0ValInc = ( fb0ValTarget - fb0ValCurrent ) * oneOverBlockSize;
    float fb0ValCurrentOld = fb0ValCurrent;
    fb1ValInc = ( fb1ValTarget - fb1ValCurrent ) * oneOverBlockSize;
    float fb1ValCurrentOld = fb1ValCurrent;
    fb2ValInc = ( fb2ValTarget - fb2ValCurrent ) * oneOverBlockSize;
    float fb2ValCurrentOld = fb2ValCurrent;

   
    // cross fading changing delay times
    // lows
    if( origOrFadeLow == 1 )
    {
        // using the fade delay line currently
        if( abs( ( float )delLineLowFade0->getDelaySamples( ) - (delTime0Target*fs) ) >= 0.0005 && !transOnLow )
        {
            // if we are not at the correct delay time
            // set up the other delay lines to the target value
            delLine00->setDelaySamples(delTime0Target*fs);
            delLine01->setDelaySamples(delTime0Target*fs);
            // adjust variables for the crossfade
            lowDelFadeCntr0 = 0;
            lowDelFadeCntr1 = 0;
            lowDelFade0 = 0.0f;
            lowDelFade1 = 0.0f;
            lowDelFadeInc0 = oneOverNumIncs;
            lowDelFadeInc1 = oneOverNumIncs;
            origOrFadeLow = 0;
            transOnLow = 1;
        }
    } else if( origOrFadeLow == 0 )
    {
        // using the original delay lines currently
        if( abs( ( float )delLine00->getDelaySamples( ) - (delTime0Target*fs) ) >= 0.0005 && !transOnLow)
        {
            // if we are not at the correct delay time
            // set up the other delay lines to the target value
            delLineLowFade0->setDelaySamples(delTime0Target*fs);
            delLineLowFade1->setDelaySamples(delTime0Target*fs);
            // adjust variables for crossfade
            lowDelFadeCntr0 = 1;
            lowDelFadeCntr1 = 1;
            lowDelFade0 = 1.0f;
            lowDelFade1 = 1.0f;
            lowDelFadeInc0 = -oneOverNumIncs;
            lowDelFadeInc1 = -oneOverNumIncs;
            origOrFadeLow = 1;
            transOnLow = 1;
        }
    }
    // mids
    if( origOrFadeMid == 1 )
    {
        // using the fade delay line currently
        if( abs( ( float )delLineMidFade0->getDelaySamples( ) - (delTime1Target*fs) ) >= 0.0005 && !transOnMid )
        {
            // if we are not at the correct delay time
            // set up the other delay lines to the target value
            delLine10->setDelaySamples(delTime1Target*fs);
            delLine11->setDelaySamples(delTime1Target*fs);
            // adjust variables for the crossfade
            midDelFadeCntr0 = 0;
            midDelFadeCntr1 = 0;
            midDelFade0 = 0.0f;
            midDelFade1 = 0.0f;
            midDelFadeInc0 = oneOverNumIncs;
            midDelFadeInc1 = oneOverNumIncs;
            origOrFadeMid = 0;
            transOnMid = 1;
        }
    } else if( origOrFadeMid == 0 )
    {
        // using the original delay lines currently
        if( abs( ( float )delLine10->getDelaySamples( ) - (delTime1Target*fs) ) >= 0.0005 && !transOnMid)
        {
            // if we are not at the correct delay time
            // set up the other delay lines to the target value
            delLineMidFade0->setDelaySamples(delTime1Target*fs);
            delLineMidFade1->setDelaySamples(delTime1Target*fs);
            // adjust variables for crossfade
            midDelFadeCntr0 = 1;
            midDelFadeCntr1 = 1;
            midDelFade0 = 1.0f;
            midDelFade1 = 1.0f;
            midDelFadeInc0 = -oneOverNumIncs;
            midDelFadeInc1 = -oneOverNumIncs;
            origOrFadeMid = 1;
            transOnMid = 1;
        }
    }
    
    // highs
    if( origOrFadeHigh == 1 )
    {
        // using the fade delay line currently
        if( abs( ( float )delLineHighFade1->getDelaySamples( ) - (delTime2Target*fs) ) >= 0.0005 && !transOnHigh )
        {
            // if we are not at the correct delay time
            // set up the other delay lines to the target value
            delLine20->setDelaySamples(delTime2Target*fs);
            delLine21->setDelaySamples(delTime2Target*fs);
            // adjust variables for the crossfade
            highDelFadeCntr0 = 0;
            highDelFadeCntr1 = 0;
            highDelFade0 = 0.0f;
            highDelFade1 = 0.0f;
            highDelFadeInc0 = oneOverNumIncs;
            highDelFadeInc1 = oneOverNumIncs;
            origOrFadeHigh = 0;
            transOnHigh = 1;
        }
    } else if( origOrFadeHigh == 0 )
    {
        // using the original delay lines currently
        if( abs( ( float )delLine21->getDelaySamples( ) - (delTime2Target*fs) ) >= 0.0005 && !transOnHigh)
        {
            // if we are not at the correct delay time
            // set up the other delay lines to the target value
            delLineHighFade0->setDelaySamples(delTime2Target*fs);
            delLineHighFade1->setDelaySamples(delTime2Target*fs);
            // adjust variables for crossfade
            highDelFadeCntr0 = 1;
            highDelFadeCntr1 = 1;
            highDelFade0 = 1.0f;
            highDelFade1 = 1.0f;
            highDelFadeInc0 = -oneOverNumIncs;
            highDelFadeInc1 = -oneOverNumIncs;
            origOrFadeHigh = 1;
            transOnHigh = 1;
        }
    }
    
    
    for (int channel = 0; channel < getNumInputChannels(); ++channel)
    {
        
        float* channelData = buffer.getSampleData(channel);
        
        // zero out Observed variables
        for( int i = 0; i < halfSizeFFT; i++ )
        {
            Observed.realp[i] = 0.0f;
            Observed.imagp[i] = 0.0f;
            Observed0.realp[i] = 0.0f;
            Observed0.imagp[i] = 0.0f;
            Observed1.realp[i] = 0.0f;
            Observed1.imagp[i] = 0.0f;
        }
        
        // sanity check !!!!
        if( blockSz != bufferSamps )
        {
            float poop = 0.3f;
        }
        
        
        
        // ************* TRYING FFT STUFF! ******************
        // trying to handle different bufferSamps and blockSz
        
    
        if( channel == 0 )
        {
            while( sampsLeftInCD0 > 0 )
            {
                // calculate how many samples we can process right now
                if( sampsLeftInCD0 + bufferPosition0 < blockSz)
                    processSamps0 = sampsLeftInCD0;
                else
                    processSamps0 = blockSz - bufferPosition0;
            
                // shift values in input by samples left in channelData
                //memcpy(input0+bufferPosition0, input0+sampsLeftInCD0, processSamps0 * sizeof(float));
                // copy in new samples into input buffer
                memcpy(input0+bufferPosition0, channelData+inPtr0, processSamps0 * sizeof(float));
                    
                // copy over output -- this is where we put our shit into the delayline
                float outSamp0, outSamp0Fade, outSamp1, outSamp1Fade, outSamp2, outSamp2Fade;
                for( int i = 0; i < sampsLeftInCD0; i++ )
                {
                    // increment parameters
                    delTime0Current += delTime0Inc;
                    delTime1Current += delTime1Inc;
                    delTime2Current += delTime2Inc;
                    
                    amp0ValCurrent += amp0ValInc;
                    amp1ValCurrent += amp1ValInc;
                    amp2ValCurrent += amp2ValInc;
                    
                    fb0ValCurrent += fb0ValInc;
                    fb1ValCurrent += fb1ValInc;
                    fb2ValCurrent += fb2ValInc;
                    
                    lowDelFade0 += lowDelFadeInc0;
                    midDelFade0 += midDelFadeInc0;
                    highDelFade0 += highDelFadeInc0;
                    
                    // feed into delay lines
                    delLine00->tick(&output00[i+bufferPosition0], &outSamp0, fb0ValCurrent, false);
                    delLine10->tick(&output10[i+bufferPosition0], &outSamp1, fb1ValCurrent, false);
                    delLine20->tick(&output0[i+bufferPosition0], &outSamp2, fb2ValCurrent, false);
                    
                    // feed into crossfade delay lines
                    delLineLowFade0->tick(&output00[i+bufferPosition0], &outSamp0Fade, fb0ValCurrent, false);
                    delLineMidFade0->tick(&output10[i+bufferPosition0], &outSamp1Fade, fb1ValCurrent, false);
                    delLineHighFade0->tick(&output0[i+bufferPosition0], &outSamp2Fade, fb2ValCurrent, false);
                    
                    // add feedback into all delay lines
                    outSamp0 = lowDelFade0*outSamp0 + (1.0f - lowDelFade0)*outSamp0Fade;
                    delLine00->addFbSample(&outSamp0, fb0ValCurrent);
                    delLineLowFade0->addFbSample(&outSamp0, fb0ValCurrent);
                    
                    outSamp1 = midDelFade0*outSamp1 + (1.0f - midDelFade0)*outSamp1Fade;
                    delLine10->addFbSample(&outSamp1, fb1ValCurrent);
                    delLineMidFade0->addFbSample(&outSamp1, fb1ValCurrent);
                    
                    outSamp2 = highDelFade0*outSamp2 + (1.0f - highDelFade0)*outSamp2Fade;
                    delLine20->addFbSample(&outSamp2, fb2ValCurrent);
                    delLineHighFade0->addFbSample(&outSamp2, fb2ValCurrent);
                    
                    lowDelFadeCntr0++;
                    midDelFadeCntr0++;
                    highDelFadeCntr0++;
                    
                    if( lowDelFadeCntr0 >= numIncs )
                    {
                        transOnLow = 0;
                        lowDelFadeCntr0 = numIncs - 1;
                        lowDelFadeInc0 = 0.0f;
                    }
                    if( midDelFadeCntr0 >= numIncs )
                    {
                        transOnMid = 0;
                        midDelFadeCntr0 = numIncs - 1;
                        midDelFadeInc0 = 0.0f;
                    }
                    if( highDelFadeCntr0 >= numIncs )
                    {
                        transOnHigh = 0;
                        highDelFadeCntr0 = numIncs - 1;
                        highDelFadeInc0 = 0.0f;
                    }
                    
                    // output to dac
                    channelData[i+outPtr0] = amp0ValCurrent*outSamp0 + amp1ValCurrent*outSamp1 + amp2ValCurrent*outSamp2;
                    
                    if(channelData[i] > 1.0f)
                        channelData[i] = 1.0f;
                    if(channelData[i] < -1.0f)
                        channelData[i] = -1.0f;
                    
                    //channelData[i+outPtr0] = output0[i+bufferPosition0];
                }
                
                bufferPosition0 += processSamps0;
                
                // if we filled a block, we can do the FFT!
                if( bufferPosition0 >= blockSz )
                {
                    bufferPosition0 = 0;
                    
                    // shift values in fft buffer by one block size (shift out oldest data)
                    memcpy(inShift0, inShift0+blockSz, (sizeFFT - blockSz) * sizeof(float));
                    // copy in new block into fft buffer (one block size)
                    memcpy(inShift0+(sizeFFT-blockSz), input0, blockSz * sizeof(float));
                    
                    // analysis window
                    for(int i = 0; i < sizeFFT; i++)
                    {
                        fftBuffer0[i] = inShift0[i] * analWindow[i];
                    }
                    
                    // FFT/IFFT
                    // put real input into split complex vector Observed
                    vDSP_ctoz((DSPComplex *) fftBuffer0, 2*stride, &Observed, 1, halfSizeFFT);
                    // fft
                    vDSP_fft_zrip(setupReal, &Observed, 1, log2n, FFT_FORWARD);
                    
                    
                    /* SEPARATE INTO SPECTRAL BANDS !*/
                    splitIntoSpectralBands( );
                    // now the information will be in
                    // Observed (highs), Observed0 (lows), Observed1 (mids)
                    
                    // === LOW BAND ===
                    vDSP_fft_zrip(setupReal, &Observed0, 1, log2n, FFT_INVERSE);
                    vDSP_ztoc(&Observed0, 1, (COMPLEX *) ifftOut00, 2, halfSizeFFT);
                    // synthesis window
                    for(int i = 0; i < sizeFFT; i++)
                    {
                        ifftOut00[i] = ifftOut00[i] * synthWindow[i];
                    }
                    // overlap add with saved output
                    for(int i = 0; i < sizeFFT; i++)
                    {
                        olaBlock00[i] = olaBlock00[i] + ifftOut00[i];
                    }
                    // shift out the block we just output
                    memcpy(output00, output00+blockSz, (sizeFFT - blockSz) * sizeof(float));
                    // copy ola stuff to output
                    memcpy(output00+(sizeFFT-blockSz), olaBlock00, blockSz*sizeof(float));
                    
                    // shift out the block we just output
                    memcpy(olaBlock00, olaBlock00+blockSz, (sizeFFT - blockSz) * sizeof(float));
                    // zero out the end of olaBlock0
                    memset(olaBlock00+(sizeFFT-blockSz), 0, blockSz*sizeof(float));
                    // === end of low band ===
                    
                    
                    // === MID BAND ===
                    vDSP_fft_zrip(setupReal, &Observed1, 1, log2n, FFT_INVERSE);
                    vDSP_ztoc(&Observed1, 1, (COMPLEX *) ifftOut10, 2, halfSizeFFT);
                    // synthesis window
                    for(int i = 0; i < sizeFFT; i++)
                    {
                        ifftOut10[i] = ifftOut10[i] * synthWindow[i];
                    }
                    // overlap add with saved output
                    for(int i = 0; i < sizeFFT; i++)
                    {
                        olaBlock10[i] = olaBlock10[i] + ifftOut10[i];
                    }
                    // shift out the block we just output
                    memcpy(output10, output10+blockSz, (sizeFFT - blockSz) * sizeof(float));
                    // copy ola stuff to output
                    memcpy(output10+(sizeFFT-blockSz), olaBlock10, blockSz*sizeof(float));
                    
                    // shift out the block we just output
                    memcpy(olaBlock10, olaBlock10+blockSz, (sizeFFT - blockSz) * sizeof(float));
                    // zero out the end of olaBlock0
                    memset(olaBlock10+(sizeFFT-blockSz), 0, blockSz*sizeof(float));
                    // === end of mid band ===
                    
                    
                    // === HIGH BAND ===
                    // ifft
                    vDSP_fft_zrip(setupReal, &Observed, 1, log2n, FFT_INVERSE);
                    // convert to split real vector
                    vDSP_ztoc(&Observed, 1, (COMPLEX *) ifftOut0, 2, halfSizeFFT);
                    
                    // synthesis window
                    for(int i = 0; i < sizeFFT; i++)
                    {
                        ifftOut0[i] = ifftOut0[i] * synthWindow[i];
                    }
                    // overlap add with saved output
                    for(int i = 0; i < sizeFFT; i++)
                    {
                        olaBlock0[i] = olaBlock0[i] + ifftOut0[i];
                    }
                    
                    // shift out the block we just output
                    memcpy(output0, output0+blockSz, (sizeFFT - blockSz) * sizeof(float));
                    // copy ola stuff to output
                    memcpy(output0+(sizeFFT-blockSz), olaBlock0, blockSz*sizeof(float));
                    
                    // shift out the block we just output
                    memcpy(olaBlock0, olaBlock0+blockSz, (sizeFFT - blockSz) * sizeof(float));
                    // zero out the end of olaBlock0
                    memset(olaBlock0+(sizeFFT-blockSz), 0, blockSz*sizeof(float));
                    
                    
                }
                inPtr0 += processSamps0;
                outPtr0 += processSamps0;
                sampsLeftInCD0 -= processSamps0;
            }
            
        } else
        {
            while( sampsLeftInCD1 > 0 )
            {
                // calculate how many samples we can process right now
                if( sampsLeftInCD1 + bufferPosition1 < blockSz)
                    processSamps1 = sampsLeftInCD1;
                else
                    processSamps1 = blockSz - bufferPosition1;
                
                // shift values in input by samples left in channelData
                // copy in new samples into input buffer
                memcpy(input1+bufferPosition1, channelData+inPtr1, processSamps1 * sizeof(float));
                
                // copy over output
                float outSamp0, outSamp0Fade, outSamp1, outSamp1Fade, outSamp2, outSamp2Fade;
                for( int i = 0; i < sampsLeftInCD1; i++ )
                {
                    // increment parameters
                    delTime0CurrentOld += delTime0Inc;
                    delTime1CurrentOld += delTime1Inc;
                    delTime2CurrentOld += delTime2Inc;
                    
                    amp0ValCurrentOld += amp0ValInc;
                    amp1ValCurrentOld += amp1ValInc;
                    amp2ValCurrentOld += amp2ValInc;
                    
                    fb0ValCurrentOld += fb0ValInc;
                    fb1ValCurrentOld += fb1ValInc;
                    fb2ValCurrentOld += fb2ValInc;
                    
                    lowDelFade1 += lowDelFadeInc1;
                    midDelFade1 += midDelFadeInc1;
                    highDelFade1 += highDelFadeInc1;
                    
                    // feed into delay lines
                    delLine01->tick(&output01[i+bufferPosition1], &outSamp0, fb0ValCurrentOld, false);
                    delLine11->tick(&output11[i+bufferPosition1], &outSamp1, fb1ValCurrentOld, false);
                    delLine21->tick(&output1[i+bufferPosition1], &outSamp2, fb2ValCurrentOld, false);
                    
                    delLineLowFade1->tick(&output01[i+bufferPosition1], &outSamp0Fade, fb0ValCurrentOld, false);
                    delLineMidFade1->tick(&output11[i+bufferPosition1], &outSamp1Fade, fb1ValCurrentOld, false);
                    delLineHighFade1->tick(&output1[i+bufferPosition1], &outSamp2Fade, fb2ValCurrentOld, false);
                    
                    
                    outSamp0 = lowDelFade1*outSamp0 + (1.0f - lowDelFade1)*outSamp0Fade;
                    outSamp1 = midDelFade1*outSamp1 + (1.0f - midDelFade1)*outSamp1Fade;
                    outSamp2 = highDelFade1*outSamp2 + (1.0f - highDelFade1)*outSamp2Fade;
                    
                    // add feedback samples to delay lines
                    delLine01->addFbSample(&outSamp0, fb0ValCurrentOld);
                    delLineLowFade1->addFbSample(&outSamp0, fb0ValCurrentOld);
                    
                    delLine11->addFbSample(&outSamp1, fb1ValCurrentOld);
                    delLineMidFade1->addFbSample(&outSamp1, fb1ValCurrentOld);
                    
                    delLine21->addFbSample(&outSamp2, fb2ValCurrentOld);
                    delLineHighFade1->addFbSample(&outSamp2, fb2ValCurrentOld);
                    
                    lowDelFadeCntr1++;
                    midDelFadeCntr1++;
                    highDelFadeCntr1++;
                    
                    if( lowDelFadeCntr1 >= numIncs )
                    {
                        transOnLow = 0;
                        lowDelFadeCntr1 = numIncs - 1;
                        lowDelFadeInc1 = 0.0f;
                    }
                    if( midDelFadeCntr1 >= numIncs )
                    {
                        transOnMid = 0;
                        midDelFadeCntr1 = numIncs - 1;
                        midDelFadeInc1 = 0.0f;
                    }
                    if( highDelFadeCntr1 >= numIncs )
                    {
                        transOnHigh = 0;
                        highDelFadeCntr1 = numIncs - 1;
                        highDelFadeInc1 = 0.0f;
                    }
                    
                    // output to dac
                    channelData[i+outPtr1] = amp0ValCurrentOld*outSamp0 + amp1ValCurrentOld*outSamp1 + amp2ValCurrentOld*outSamp2;
                    
                    if(channelData[i] > 1.0f)
                        channelData[i] = 1.0f;
                    if(channelData[i] < -1.0f)
                        channelData[i] = -1.0f;
                    
                    //channelData[i+outPtr1] = amp0ValCurrentOld*output1[i+bufferPosition1];
                }
                
                bufferPosition1 += processSamps1;
                
                // if we filled a block, we can do the FFT!
                if( bufferPosition1 >= blockSz )
                {
                    bufferPosition1 = 0;

                    // shift values in fft buffer by one block size (shift out oldest data)
                    memcpy(inShift1, inShift1+blockSz, (sizeFFT - blockSz) * sizeof(float));
                    // copy in new block into fft buffer (one block size)
                    memcpy(inShift1+(sizeFFT-blockSz), input1, blockSz * sizeof(float));
                    
                    // analysis window
                    for(int i = 0; i < sizeFFT; i++)
                    {
                        fftBuffer1[i] = inShift1[i] * analWindow[i];
                    }
                    
                    // FFT/IFFT
                    // put real input into split complex vector Observed
                    vDSP_ctoz((DSPComplex *) fftBuffer1, 2*stride, &Observed, 1, halfSizeFFT);
                    // fft
                    vDSP_fft_zrip(setupReal, &Observed, 1, log2n, FFT_FORWARD);
                    
                    
                    /* SEPARATE INTO SPECTRAL BANDS !*/
                    splitIntoSpectralBands( );
                    // now the information will be in
                    // Observed (highs), Observed0 (lows), Observed1 (mids)

                    
                    // === LOW BAND ===
                    vDSP_fft_zrip(setupReal, &Observed0, 1, log2n, FFT_INVERSE);
                    vDSP_ztoc(&Observed0, 1, (COMPLEX *) ifftOut01, 2, halfSizeFFT);
                    // synthesis window
                    for(int i = 0; i < sizeFFT; i++)
                    {
                        ifftOut01[i] = ifftOut01[i] * synthWindow[i];
                    }
                    // overlap add with saved output
                    for(int i = 0; i < sizeFFT; i++)
                    {
                        olaBlock01[i] = olaBlock01[i] + ifftOut01[i];
                    }
                    // shift out the block we just output
                    memcpy(output01, output01+blockSz, (sizeFFT - blockSz) * sizeof(float));
                    // copy ola stuff to output
                    memcpy(output01+(sizeFFT-blockSz), olaBlock01, blockSz*sizeof(float));
                    
                    // shift out the block we just output
                    memcpy(olaBlock01, olaBlock01+blockSz, (sizeFFT - blockSz) * sizeof(float));
                    // zero out the end of olaBlock0
                    memset(olaBlock01+(sizeFFT-blockSz), 0, blockSz*sizeof(float));
                    // === end of low band ===
                    
                    // === MID BAND ===
                    vDSP_fft_zrip(setupReal, &Observed1, 1, log2n, FFT_INVERSE);
                    vDSP_ztoc(&Observed1, 1, (COMPLEX *) ifftOut11, 2, halfSizeFFT);
                    // synthesis window
                    for(int i = 0; i < sizeFFT; i++)
                    {
                        ifftOut11[i] = ifftOut11[i] * synthWindow[i];
                    }
                    // overlap add with saved output
                    for(int i = 0; i < sizeFFT; i++)
                    {
                        olaBlock11[i] = olaBlock11[i] + ifftOut11[i];
                    }
                    // shift out the block we just output
                    memcpy(output11, output11+blockSz, (sizeFFT - blockSz) * sizeof(float));
                    // copy ola stuff to output
                    memcpy(output11+(sizeFFT-blockSz), olaBlock11, blockSz*sizeof(float));
                    
                    // shift out the block we just output
                    memcpy(olaBlock11, olaBlock11+blockSz, (sizeFFT - blockSz) * sizeof(float));
                    // zero out the end of olaBlock0
                    memset(olaBlock11+(sizeFFT-blockSz), 0, blockSz*sizeof(float));
                    // === end of mid band ===
                    
                    
                    // === HIGH BAND ===
                    // ifft
                    vDSP_fft_zrip(setupReal, &Observed, 1, log2n, FFT_INVERSE);
                    // convert to split real vector
                    vDSP_ztoc(&Observed, 1, (COMPLEX *) ifftOut1, 2, halfSizeFFT);
                    
                    // synthesis window
                    for(int i = 0; i < sizeFFT; i++)
                    {
                        ifftOut1[i] = ifftOut1[i] * synthWindow[i];
                    }
                    // overlap add with saved output
                    for(int i = 0; i < sizeFFT; i++)
                    {
                        olaBlock1[i] = olaBlock1[i] + ifftOut1[i];
                    }
                    
                    // shift out the block we just output
                    memcpy(output1, output1+blockSz, (sizeFFT - blockSz) * sizeof(float));
                    // copy ola stuff to output
                    memcpy(output1+(sizeFFT-blockSz), olaBlock1, blockSz*sizeof(float));
                    
                    // shift out the block we just output
                    memcpy(olaBlock1, olaBlock1+blockSz, (sizeFFT - blockSz) * sizeof(float));
                    // zero out the end of olaBlock0
                    memset(olaBlock1+(sizeFFT-blockSz), 0, blockSz*sizeof(float));
                    // === end of high band ===
                    
                }
                inPtr1 += processSamps1;
                outPtr1 += processSamps1;
                sampsLeftInCD1 -= processSamps1;
            }
            
        }
        continue;   // skip everything I wrote before
        
        // ********* END TRYING FFT STUFF! *******************
        
        
        
        if( channel == 0 )
        {
            // left channel
            
            // shift values in fft buffer by one block size (shift out oldest data)
            memcpy(input0, input0+blockSz, (sizeFFT - blockSz) * sizeof(float));
            // copy in new block into fft buffer
            memcpy(input0+(sizeFFT-blockSz), channelData, blockSz * sizeof(float));
            
            // analysis window
            for(int i = 0; i < sizeFFT; i++)
            {
                fftBuffer0[i] = input0[i] * analWindow[i];
            }
            
            // FFT/IFFT
            // put real input into split complex vector Observed
            vDSP_ctoz((DSPComplex *) fftBuffer0, 2*stride, &Observed, 1, halfSizeFFT);
            // fft
            vDSP_fft_zrip(setupReal, &Observed, 1, log2n, FFT_FORWARD);
           
            /* ****** */
            
            // ******************************
            // MULTIBAND TEST
            // grab the correct bins
            float m = -1.0f/( float )(2.0f*winCurveBins);  // decreasing curve
            float bLow = ( float )-m*(binCutoff0 + winCurveBins);
            float bMidDec = ( float )-m*(binCutoff1 + winCurveBins);
            float bMidInc = ( float )m*(binCutoff0 - winCurveBins);
            float bHigh = ( float )m*(binCutoff1 - winCurveBins);
            for( int i = 0; i < halfSizeFFT; i++ )
            {
                if(i < binCutoff0)
                {
                    // fill lower bins
                    Observed0.realp[i] = Observed.realp[i];
                    Observed0.imagp[i] = Observed.imagp[i];
                    
                    if( !useFreqWindows )
                    {
                        
                         Observed1.realp[i] = 0.0f;
                         Observed1.imagp[i] = 0.0f;
                         Observed.realp[i] = 0.0f;
                         Observed.imagp[i] = 0.0f;
                    } else
                    {
                        // mids curve ascent and lower bins descent
                        if( i >= (binCutoff0 - winCurveBins) )
                        {
                            float yMid = ( float )(-m * i + bMidInc);
                            Observed1.realp[i] = Observed.realp[i]*yMid;
                            Observed1.imagp[i] = Observed.imagp[i]*yMid;
                        
                            float yLow = ( float )(m * i + bLow);
                            Observed0.realp[i] = Observed.realp[i]*yLow;
                            Observed0.imagp[i] = Observed.imagp[i]*yLow;;
                        
                        } else
                        {
                            Observed1.realp[i] = 0.0f;
                            Observed1.imagp[i] = 0.0f;
                        }
                    
                        // highs curve (increasing)
                        if( i >= (binCutoff1 - winCurveBins) )
                        {
                            float yHigh = ( float )(-m * i + bHigh);
                            Observed.realp[i] = Observed.realp[i]*yHigh;
                            Observed.imagp[i] = Observed.imagp[i]*yHigh;
                        } else
                        {
                            Observed.realp[i] = 0.0f;
                            Observed.imagp[i] = 0.0f;
                        }
                    
                    }
                    
                } else if( i >= binCutoff0 && i < binCutoff1 )
                {
                    // fill mids bins
                    Observed1.realp[i] = Observed.realp[i];
                    Observed1.imagp[i] = Observed.imagp[i];
                    
                    if( !useFreqWindows )
                    {
                        
                         Observed0.realp[i] = 0.0f;
                         Observed0.imagp[i] = 0.0f;
                         Observed.realp[i] = 0.0f;
                         Observed.imagp[i] = 0.0f;
                        
                    } else
                    {
                        // lower bins descent and mid bins ascent
                        if( i <= (binCutoff0 + winCurveBins) )
                        {
                            float yLow = ( float )(m * i + bLow);
                            Observed0.realp[i] = Observed.realp[i]*yLow;
                            Observed0.imagp[i] = Observed.imagp[i]*yLow;;
                        
                            float yMid = ( float )(-m * i + bMidInc);
                            Observed1.realp[i] = Observed.realp[i]*yMid;
                            Observed1.imagp[i] = Observed.imagp[i]*yMid;
                        
                        } else
                        {
                            Observed0.realp[i] = 0.0f;
                            Observed0.imagp[i] = 0.0f;
                        }
                    
                        // highs curve ascent and mid bins descent
                        if( i >= (binCutoff1 - winCurveBins) )
                        {
                            float yHigh = ( float )(-m * i + bHigh);
                            Observed.realp[i] = Observed.realp[i]*yHigh;
                            Observed.imagp[i] = Observed.imagp[i]*yHigh;
                        
                            float yMid = ( float )(m * i + bMidDec);
                            Observed1.realp[i] = Observed.realp[i]*yMid;
                            Observed1.imagp[i] = Observed.imagp[i]*yMid;
                        
                        } else
                        {
                            Observed.realp[i] = 0.0f;
                            Observed.imagp[i] = 0.0f;
                        }
                    }
                    
                    
                } else
                {
                    // fill highs bins
                    if( !useFreqWindows )
                    {
                        
                         Observed0.realp[i] = 0.0f;
                         Observed0.imagp[i] = 0.0f;
                         Observed1.realp[i] = 0.0f;
                         Observed1.imagp[i] = 0.0f;
                        
                    } else
                    {
                        // lows curve descent and highs ascent
                        if( i <= (binCutoff0 + winCurveBins) )
                        {
                            float yLow = ( float )(m * i + bLow);
                            Observed0.realp[i] = Observed.realp[i]*yLow;
                            Observed0.imagp[i] = Observed.imagp[i]*yLow;
                        
                            float yHigh = ( float )(-m * i + bHigh);
                            Observed.realp[i] = Observed.realp[i]*yHigh;
                            Observed.imagp[i] = Observed.imagp[i]*yHigh;
                        } else
                        {
                            Observed0.realp[i] = 0.0f;
                            Observed0.imagp[i] = 0.0f;
                        }

                        // mids curve descent and highs ascent
                        if( i <= ( binCutoff1 + winCurveBins) )
                        {
                            float yMid = ( float )(m * i + bMidDec);
                            Observed1.realp[i] = Observed.realp[i]*yMid;
                            Observed1.imagp[i] = Observed.imagp[i]*yMid;
                        
                            float yHigh = ( float )(-m * i + bHigh);
                            Observed.realp[i] = Observed.realp[i]*yHigh;
                            Observed.imagp[i] = Observed.imagp[i]*yHigh;
                        
                        } else
                        {
                            Observed1.realp[i] = 0.0f;
                            Observed1.imagp[i] = 0.0f;
                        }
                    }
                    
                }
            }
            // ******************************
            
            // *** display the amplitude ***
            blockCntr++;
            float amp = sqrt(Observed.realp[3]*Observed.realp[3] + Observed.imagp[3]*Observed.imagp[3]);
            ampRunningSum = ampRunningSum + amp;
            if( blockCntr >= 43 )
            {
                ampCurrentVal = ampRunningSum / blockCntr;
                ampRunningSum = 0.0f;
                RequestUIUpdate( );
                blockCntr = 0;
            }
            
            // ifft
            vDSP_fft_zrip(setupReal, &Observed, 1, log2n, FFT_INVERSE);
            
            // convert to split real vector
            vDSP_ztoc(&Observed, 1, (COMPLEX *) ifftOut0, 2, halfSizeFFT);
            
            // synthesis window
            for(int i = 0; i < sizeFFT; i++)
            {
                ifftOut0[i] = ifftOut0[i] * synthWindow[i];
            }
            
            // overlap add with saved output
            for(int i = 0; i < sizeFFT; i++)
            {
                olaBlock0[i] = olaBlock0[i] + ifftOut0[i];
            }
           
            
            
            // ******************************
            // MULTIBAND TEST
            
            // === BAND 1 ===
            vDSP_fft_zrip(setupReal, &Observed0, 1, log2n, FFT_INVERSE);
            vDSP_ztoc(&Observed0, 1, (COMPLEX *) ifftOut00, 2, halfSizeFFT);
            // synthesis window
            for(int i = 0; i < sizeFFT; i++)
            {
                ifftOut00[i] = ifftOut00[i] * synthWindow[i];
            }
            // overlap add with saved output
            for(int i = 0; i < sizeFFT; i++)
            {
                olaBlock00[i] = olaBlock00[i] + ifftOut00[i];
            }
            // take oldest block in accumlated output buffer as output
            //memcpy(channelData, olaBlock00, bufferSamps * sizeof(float));
            /*
            // copy oldest block into delay buffer and add delayed low freq output to
            // audio output
            float outSamp;
            for( int i = 0; i < bufferSamps; i++ )
            {
                // adjust delay line
                delTime0Current += delTime0Inc; // only increment once
                delLine00->setDelaySamples( ( int )(delTime0Current * fs) );
                
                delLine00->tick(&(olaBlock00[i]), &outSamp);
                channelData[i] = channelData[i] + outSamp;
            }
            
            // shift out the block we just output
            memcpy(olaBlock00, olaBlock00+blockSz, (sizeFFT - blockSz) * sizeof(float));
            // zero out the end of olaBlock0
            memset(olaBlock00+(sizeFFT-blockSz), 0, blockSz*sizeof(float));
            */
            // === end of band 1 ===
            
            
            // === BAND 2 ===
            vDSP_fft_zrip(setupReal, &Observed1, 1, log2n, FFT_INVERSE);
            vDSP_ztoc(&Observed1, 1, (COMPLEX *) ifftOut10, 2, halfSizeFFT);
            // synthesis window
            for(int i = 0; i < sizeFFT; i++)
            {
                ifftOut10[i] = ifftOut10[i] * synthWindow[i];
            }
            // overlap add with saved output
            for(int i = 0; i < sizeFFT; i++)
            {
                olaBlock10[i] = olaBlock10[i] + ifftOut10[i];
            }
            // === end of band 2 ===
            
            
            // copy oldest block into delay buffer and add multiband delayed output to
            // audio output
            float outSamp0, outSamp1, outSamp2, outSamp0Fade, outSamp1Fade, outSamp2Fade;
            for( int i = 0; i < bufferSamps; i++ )
            {
                
                // adjust delay lines
                delTime0Current += delTime0Inc; // only increment once
                //delLine00->setDelaySamples( delTime0Current * fs);
                delTime1Current += delTime1Inc;
                //delLine10->setDelaySamples( delTime1Current * fs );
                delTime2Current += delTime2Inc;
                //delLine20->setDelaySamples( delTime2Current * fs );
                
                // interpolate feedback
                fb0ValCurrent += fb0ValInc;
                fb1ValCurrent += fb1ValInc;
                fb2ValCurrent += fb2ValInc;
                
                delLine00->tick(&(olaBlock00[i]), &outSamp0, fb0ValCurrent, 0);
                delLine10->tick(&(olaBlock10[i]), &outSamp1, fb1ValCurrent, 0);
                delLine20->tick(&(olaBlock0[i]), &outSamp2, fb2ValCurrent, 0);
                
                delLineLowFade0->tick(&(olaBlock00[i]), &outSamp0Fade, fb0ValCurrent, 0);
                delLineMidFade0->tick(&(olaBlock10[i]), &outSamp1Fade, fb1ValCurrent, 0);
                delLineHighFade0->tick(&(olaBlock0[i]), &outSamp2Fade, fb2ValCurrent, 0);

                lowDelFade0 += lowDelFadeInc0;
                midDelFade0 += midDelFadeInc0;
                highDelFade0 += highDelFadeInc0;
                
                outSamp0 = lowDelFade0*outSamp0 + (1.0f - lowDelFade0)*outSamp0Fade;
                delLine00->addFbSample(&outSamp0, fb0ValCurrent);
                delLineLowFade0->addFbSample(&outSamp0, fb0ValCurrent);
                
                outSamp1 = midDelFade0*outSamp1 + (1.0f - midDelFade0)*outSamp1Fade;
                delLine10->addFbSample(&outSamp1, fb1ValCurrent);
                delLineMidFade0->addFbSample(&outSamp1, fb1ValCurrent);
                
                //outSamp2 = (outSamp2 * cos(highDelFade0*1.5708)) + (outSamp2Fade * sin(highDelFade0*1.5708));
                outSamp2 = highDelFade0*outSamp2 + (1.0f - highDelFade0)*outSamp2Fade;
                delLine20->addFbSample(&outSamp2, fb2ValCurrent);
                delLineHighFade0->addFbSample(&outSamp2, fb2ValCurrent);
                
                lowDelFadeCntr0++;
                midDelFadeCntr0++;
                highDelFadeCntr0++;
                
                if( lowDelFadeCntr0 >= numIncs )
                {
                    transOnLow = 0;
                    lowDelFadeCntr0 = numIncs - 1;
                    lowDelFadeInc0 = 0.0f;
                }
                if( midDelFadeCntr0 >= numIncs )
                {
                    transOnMid = 0;
                    midDelFadeCntr0 = numIncs - 1;
                    midDelFadeInc0 = 0.0f;
                }
                if( highDelFadeCntr0 >= numIncs )
                {
                    transOnHigh = 0;
                    highDelFadeCntr0 = numIncs - 1;
                    highDelFadeInc0 = 0.0f;
                }
                
                
                
                // interpolate amplitudes
                amp0ValCurrent += amp0ValInc;
                amp1ValCurrent += amp1ValInc;
                amp2ValCurrent += amp2ValInc;
                
                channelData[i] = amp2ValCurrent*outSamp2 + amp0ValCurrent*outSamp0 + amp1ValCurrent*outSamp1;
                
                if(channelData[i] > 1.0f)
                    channelData[i] = 1.0f;
                if(channelData[i] < -1.0f)
                    channelData[i] = -1.0f;
                
                
                /*
                if(snapMode == true && lastBeat != beat && lastBar != bar)
                {
                    channelData[i] = 1.0f;
                } else if(snapMode == true && lastBeat != beat)
                {
                    channelData[i] = 0.5f;
                }
                 */
            }
            
            // === band 1 ===
            // shift out the block we just output
            memcpy(olaBlock00, olaBlock00+blockSz, (sizeFFT - blockSz) * sizeof(float));
            // zero out the end of olaBlock0
            memset(olaBlock00+(sizeFFT-blockSz), 0, blockSz*sizeof(float));
            // ===============
            // === band 2 ===
            // shift out the block we just output
            memcpy(olaBlock10, olaBlock10+blockSz, (sizeFFT - blockSz) * sizeof(float));
            // zero out the end of olaBlock1
            memset(olaBlock10+(sizeFFT-blockSz), 0, blockSz*sizeof(float));
            // ===============
            /* THIS OUTPUTS THE HIGH FREQ PART */
            // take oldest block in accumlated output buffer as output
            //memcpy(channelData, olaBlock0, bufferSamps * sizeof(float));
            // shift out the block we just output
            memcpy(olaBlock0, olaBlock0+blockSz, (sizeFFT - blockSz) * sizeof(float));
            // zero out the end of olaBlock0
            memset(olaBlock0+(sizeFFT-blockSz), 0, blockSz*sizeof(float));
       
            // ******************************

            
        } else
        {
            // right channel
            
            // shift values in fft buffer by one block size (shift out oldest data)
            memcpy(input1, input1+blockSz, (sizeFFT - blockSz) * sizeof(float));
            // copy in new block into fft buffer
            memcpy(input1+(sizeFFT-blockSz), channelData, blockSz * sizeof(float));
            
            // analysis window
            for(int i = 0; i < sizeFFT; i++)
            {
                fftBuffer1[i] = input1[i] * analWindow[i];
            }
            
            // FFT/IFFT
            // put real input into split complex vector Observed
            vDSP_ctoz((DSPComplex *) fftBuffer1, 2*stride, &Observed, 1, halfSizeFFT);
            // fft
            vDSP_fft_zrip(setupReal, &Observed, 1, log2n, FFT_FORWARD);
            
            /* PROCESSING */
            
            /* ****** */
            
            // ******************************
            // MULTIBAND TEST
            // grab the lower bins
            float m = -1.0f/( float )(2.0f*winCurveBins);  // decreasing curve
            float bLow = ( float )-m*(binCutoff0 + winCurveBins);
            float bMidDec = ( float )-m*(binCutoff1 + winCurveBins);
            float bMidInc = ( float )m*(binCutoff0 - winCurveBins);
            float bHigh = ( float )m*(binCutoff1 - winCurveBins);
            for( int i = 0; i < halfSizeFFT; i++ )
            {
                if(i < binCutoff0)
                {
                    // fill lower bins
                    Observed0.realp[i] = Observed.realp[i];
                    Observed0.imagp[i] = Observed.imagp[i];
                    
                    if( !useFreqWindows )
                    {
                        
                        Observed1.realp[i] = 0.0f;
                        Observed1.imagp[i] = 0.0f;
                        Observed.realp[i] = 0.0f;
                        Observed.imagp[i] = 0.0f;
                    } else
                    {
                        // mids curve ascent and lower bins descent
                        if( i >= (binCutoff0 - winCurveBins) )
                        {
                            float yMid = ( float )(-m * i + bMidInc);
                            Observed1.realp[i] = Observed.realp[i]*yMid;
                            Observed1.imagp[i] = Observed.imagp[i]*yMid;
                            
                            float yLow = ( float )(m * i + bLow);
                            Observed0.realp[i] = Observed.realp[i]*yLow;
                            Observed0.imagp[i] = Observed.imagp[i]*yLow;;
                            
                        } else
                        {
                            Observed1.realp[i] = 0.0f;
                            Observed1.imagp[i] = 0.0f;
                        }
                        
                        // highs curve (increasing)
                        if( i >= (binCutoff1 - winCurveBins) )
                        {
                            float yHigh = ( float )(-m * i + bHigh);
                            Observed.realp[i] = Observed.realp[i]*yHigh;
                            Observed.imagp[i] = Observed.imagp[i]*yHigh;
                        } else
                        {
                            Observed.realp[i] = 0.0f;
                            Observed.imagp[i] = 0.0f;
                        }
                        
                    }
                    
                } else if( i >= binCutoff0 && i < binCutoff1 )
                {
                    // fill mids bins
                    Observed1.realp[i] = Observed.realp[i];
                    Observed1.imagp[i] = Observed.imagp[i];
                    
                    if( !useFreqWindows )
                    {
                        
                        Observed0.realp[i] = 0.0f;
                        Observed0.imagp[i] = 0.0f;
                        Observed.realp[i] = 0.0f;
                        Observed.imagp[i] = 0.0f;
                        
                    } else
                    {
                        // lower bins descent and mid bins ascent
                        if( i <= (binCutoff0 + winCurveBins) )
                        {
                            float yLow = ( float )(m * i + bLow);
                            Observed0.realp[i] = Observed.realp[i]*yLow;
                            Observed0.imagp[i] = Observed.imagp[i]*yLow;;
                            
                            float yMid = ( float )(-m * i + bMidInc);
                            Observed1.realp[i] = Observed.realp[i]*yMid;
                            Observed1.imagp[i] = Observed.imagp[i]*yMid;
                            
                        } else
                        {
                            Observed0.realp[i] = 0.0f;
                            Observed0.imagp[i] = 0.0f;
                        }
                        
                        // highs curve ascent and mid bins descent
                        if( i >= (binCutoff1 - winCurveBins) )
                        {
                            float yHigh = ( float )(-m * i + bHigh);
                            Observed.realp[i] = Observed.realp[i]*yHigh;
                            Observed.imagp[i] = Observed.imagp[i]*yHigh;
                            
                            float yMid = ( float )(m * i + bMidDec);
                            Observed1.realp[i] = Observed.realp[i]*yMid;
                            Observed1.imagp[i] = Observed.imagp[i]*yMid;
                            
                        } else
                        {
                            Observed.realp[i] = 0.0f;
                            Observed.imagp[i] = 0.0f;
                        }
                    }
                    
                    
                } else
                {
                    // fill highs bins
                    if( !useFreqWindows )
                    {
                        
                        Observed0.realp[i] = 0.0f;
                        Observed0.imagp[i] = 0.0f;
                        Observed1.realp[i] = 0.0f;
                        Observed1.imagp[i] = 0.0f;
                        
                    } else
                    {
                        // lows curve descent and highs ascent
                        if( i <= (binCutoff0 + winCurveBins) )
                        {
                            float yLow = ( float )(m * i + bLow);
                            Observed0.realp[i] = Observed.realp[i]*yLow;
                            Observed0.imagp[i] = Observed.imagp[i]*yLow;
                            
                            float yHigh = ( float )(-m * i + bHigh);
                            Observed.realp[i] = Observed.realp[i]*yHigh;
                            Observed.imagp[i] = Observed.imagp[i]*yHigh;
                        } else
                        {
                            Observed0.realp[i] = 0.0f;
                            Observed0.imagp[i] = 0.0f;
                        }
                        
                        // mids curve descent and highs ascent
                        if( i <= ( binCutoff1 + winCurveBins) )
                        {
                            float yMid = ( float )(m * i + bMidDec);
                            Observed1.realp[i] = Observed.realp[i]*yMid;
                            Observed1.imagp[i] = Observed.imagp[i]*yMid;
                            
                            float yHigh = ( float )(-m * i + bHigh);
                            Observed.realp[i] = Observed.realp[i]*yHigh;
                            Observed.imagp[i] = Observed.imagp[i]*yHigh;
                            
                        } else
                        {
                            Observed1.realp[i] = 0.0f;
                            Observed1.imagp[i] = 0.0f;
                        }
                    }
                    
                }
            }
            // ******************************
            
            
            // ifft
            vDSP_fft_zrip(setupReal, &Observed, 1, log2n, FFT_INVERSE);
            
            // convert to split real vector
            vDSP_ztoc(&Observed, 1, (COMPLEX *) ifftOut1, 2, halfSizeFFT);
            
            // synthesis window
            for(int i = 0; i < sizeFFT; i++)
            {
                ifftOut1[i] = ifftOut1[i] * synthWindow[i];
            }
             
            // overlap add with saved output
            for(int i = 0; i < sizeFFT; i++)
            {
                olaBlock1[i] = olaBlock1[i] + ifftOut1[i];
            }
            
   
            
            // ******************************
            // MULTIBAND TEST
            
            // === BAND 1 ===
            vDSP_fft_zrip(setupReal, &Observed0, 1, log2n, FFT_INVERSE);
            vDSP_ztoc(&Observed0, 1, (COMPLEX *) ifftOut01, 2, halfSizeFFT);
            // synthesis window
            for(int i = 0; i < sizeFFT; i++)
            {
                ifftOut01[i] = ifftOut01[i] * synthWindow[i];
            }
            // overlap add with saved output
            for(int i = 0; i < sizeFFT; i++)
            {
                olaBlock01[i] = olaBlock01[i] + ifftOut01[i];
            }
            // === end of band 1 ===
            
            // === BAND 2 ===
            vDSP_fft_zrip(setupReal, &Observed1, 1, log2n, FFT_INVERSE);
            vDSP_ztoc(&Observed1, 1, (COMPLEX *) ifftOut11, 2, halfSizeFFT);
            // synthesis window
            for(int i = 0; i < sizeFFT; i++)
            {
                ifftOut11[i] = ifftOut11[i] * synthWindow[i];
            }
            // overlap add with saved output
            for(int i = 0; i < sizeFFT; i++)
            {
                olaBlock11[i] = olaBlock11[i] + ifftOut11[i];
            }
            // === end of band 2 ===
            
            // take oldest block in accumlated output buffer as output
            //memcpy(channelData, olaBlock01, bufferSamps * sizeof(float));
            
            // put low freq data into delay buffer and add the delayed multiband data to the audio output
            float outSamp0, outSamp1, outSamp2, outSamp0Fade, outSamp1Fade, outSamp2Fade;
            for( int i = 0; i < bufferSamps; i++ )
            {
                // adjust delay lines
                delTime0CurrentOld += delTime0Inc;
                //delLine01->setDelaySamples( delTime0CurrentOld * fs );
                delTime1CurrentOld += delTime1Inc;
                //delLine11->setDelaySamples( delTime1CurrentOld * fs );
                delTime2CurrentOld += delTime2Inc;
                //delLine21->setDelaySamples( delTime2CurrentOld * fs );
                
                // interpolate feedback
                fb0ValCurrentOld += fb0ValInc;
                fb1ValCurrentOld += fb1ValInc;
                fb2ValCurrentOld += fb2ValInc;

                delLine01->tick(&(olaBlock01[i]), &outSamp0, fb0ValCurrentOld, 0);
                delLine11->tick(&(olaBlock11[i]), &outSamp1, fb1ValCurrentOld, 0);
                delLine21->tick(&(olaBlock1[i]), &outSamp2, fb2ValCurrentOld, 0);
                
                delLineLowFade1->tick(&(olaBlock01[i]), &outSamp0Fade, fb0ValCurrentOld, 0);
                delLineMidFade1->tick(&(olaBlock11[i]), &outSamp1Fade, fb1ValCurrentOld, 0);
                delLineHighFade1->tick(&(olaBlock1[i]), &outSamp2Fade, fb2ValCurrentOld, 0);
                 
                lowDelFade1 += lowDelFadeInc1;
                midDelFade1 += midDelFadeInc1;
                highDelFade1 += highDelFadeInc1;
                
                outSamp0 = lowDelFade1*outSamp0 + (1.0f - lowDelFade1)*outSamp0Fade;
                delLine01->addFbSample(&outSamp0, fb0ValCurrentOld);
                delLineLowFade1->addFbSample(&outSamp0, fb0ValCurrentOld);
                
                outSamp1 = midDelFade1*outSamp1 + (1.0f - midDelFade1)*outSamp1Fade;
                delLine11->addFbSample(&outSamp1, fb1ValCurrentOld);
                delLineMidFade1->addFbSample(&outSamp1, fb1ValCurrentOld);
                
                //outSamp2 = (outSamp2 * cos(highDelFade1*1.5708)) + (outSamp2Fade * sin(highDelFade1*1.5708));
                outSamp2 = highDelFade1*outSamp2 + (1.0f-highDelFade1)*outSamp2Fade;
                delLine21->addFbSample(&outSamp2, fb2ValCurrentOld);
                delLineHighFade1->addFbSample(&outSamp2, fb2ValCurrentOld);
                
                lowDelFadeCntr1++;
                midDelFadeCntr1++;
                highDelFadeCntr1++;
                
                if( lowDelFadeCntr1 >= numIncs )
                {
                    transOnLow = 0;
                    lowDelFadeCntr1 = numIncs - 1;
                    lowDelFadeInc1 = 0.0f;
                }
                if( midDelFadeCntr1 >= numIncs )
                {
                    transOnMid = 0;
                    midDelFadeCntr1 = numIncs - 1;
                    midDelFadeInc1 = 0.0f;
                }
                if( highDelFadeCntr1 >= numIncs )
                {
                    transOnHigh = 0;
                    highDelFadeCntr1 = numIncs - 1;
                    highDelFadeInc1 = 0.0f;
                }
                
                
                // interpolate amplitudes
                amp0ValCurrentOld += amp0ValInc;
                amp1ValCurrentOld += amp1ValInc;
                amp2ValCurrentOld += amp2ValInc;
                
                channelData[i] = amp2ValCurrentOld*outSamp2 + amp0ValCurrentOld*outSamp0 + amp1ValCurrentOld*outSamp1;
                
                if(channelData[i] > 1.0f)
                    channelData[i] = 1.0f;
                if(channelData[i] < -1.0f)
                    channelData[i] = -1.0f;

            }
            // === band 1
            // shift out the block we just output
            memcpy(olaBlock01, olaBlock01+blockSz, (sizeFFT - blockSz) * sizeof(float));
            // zero out the end of olaBlock0
            memset(olaBlock01+(sizeFFT-blockSz), 0, blockSz*sizeof(float));
            // === band 2
            // shift out the block we just output
            memcpy(olaBlock11, olaBlock11+blockSz, (sizeFFT - blockSz) * sizeof(float));
            // zero out the end of olaBlock0
            memset(olaBlock11+(sizeFFT-blockSz), 0, blockSz*sizeof(float));
            // ******************************

            /* COPY HIGH FREQ DATA INTO AUDIO OUTPUT */
            // take oldest block in accumlated output buffer as output
            //memcpy(channelData, olaBlock1, bufferSamps * sizeof(float));
            // shift out the block we just output
            memcpy(olaBlock1, olaBlock1+blockSz, (sizeFFT - blockSz) * sizeof(float));
            // zero out the end of olaBlock1
            memset(olaBlock1+(sizeFFT-blockSz), 0, blockSz*sizeof(float));
        }
        
      /* *************** */

    }
    
    // In case we have more outputs than inputs, we'll clear any output
    // channels that didn't contain input data, (because these aren't
    // guaranteed to be empty - they may contain garbage).
    for (int i = getNumInputChannels(); i < getNumOutputChannels(); ++i)
    {
        buffer.clear (i, 0, buffer.getNumSamples());
    }
}

//==============================================================================
bool Mb1AudioProcessor::hasEditor() const
{
    return true; // (change this to false if you choose to not supply an editor)
}

AudioProcessorEditor* Mb1AudioProcessor::createEditor()
{
    return new Mb1AudioProcessorEditor (this);
}

//==============================================================================
void Mb1AudioProcessor::getStateInformation (MemoryBlock& destData)
{
    // You should use this method to store your parameters in the memory block.
    // You could do that either as raw data, or use the XML or ValueTree classes
    // as intermediaries to make it easy to save and load complex data.
    //Save UserParams/Data to file
    XmlElement root("Root");
    XmlElement *el;
    //el = root.createNewChildElement("ampCurrent");
    //el->addTextElement(String(UserParams[ampCurrent]));
    el = root.createNewChildElement("freqCutoff0");
    el->addTextElement(String(UserParams[freqCutoff0]));
    el = root.createNewChildElement("delTime0");
    el->addTextElement(String(UserParams[delTime0]));
    el = root.createNewChildElement("freqCutoff1");
    el->addTextElement(String(UserParams[freqCutoff1]));
    el = root.createNewChildElement("delTime1");
    el->addTextElement(String(UserParams[delTime1]));
    el = root.createNewChildElement("delTime2");
    el->addTextElement(String(UserParams[delTime2]));
    el = root.createNewChildElement("amp0");
    el->addTextElement(String(UserParams[amp0]));
    el = root.createNewChildElement("amp1");
    el->addTextElement(String(UserParams[amp1]));
    el = root.createNewChildElement("amp2");
    el->addTextElement(String(UserParams[amp2]));
    el = root.createNewChildElement("fb0");
    el->addTextElement(String(UserParams[fb0]));
    el = root.createNewChildElement("fb1");
    el->addTextElement(String(UserParams[fb1]));
    el = root.createNewChildElement("fb2");
    el->addTextElement(String(UserParams[fb2]));
    //el = root.createNewChildElement("freqWind");
    //el->addTextElement(String(UserParams[freqWind]));
    el = root.createNewChildElement("syncOnLow");
    el->addTextElement(String(UserParams[syncOnLow]));
    el = root.createNewChildElement("syncOnMid");
    el->addTextElement(String(UserParams[syncOnMid]));
    el = root.createNewChildElement("syncOnHigh");
    el->addTextElement(String(UserParams[syncOnHigh]));
    copyXmlToBinary(root,destData);
}

void Mb1AudioProcessor::setStateInformation (const void* data, int sizeInBytes)
{
    // You should use this method to restore your parameters from this memory block,
    // whose contents will have been created by the getStateInformation() call.
    //Load UserParams/Data from file
    XmlElement* pRoot = getXmlFromBinary(data,sizeInBytes);
    if(pRoot!=NULL)
    {
        forEachXmlChildElement((*pRoot),pChild)
        {
            /*if(pChild->hasTagName("ampCurrent"))
            {
                String text = pChild->getAllSubText();
                setParameter(ampCurrent, text.getFloatValue());
            } else */if(pChild->hasTagName("freqCutoff0"))
            {
                String text = pChild->getAllSubText();
                setParameter(freqCutoff0, text.getFloatValue());
            } else if(pChild->hasTagName("delTime0"))
            {
                String text = pChild->getAllSubText();
                setParameter(delTime0, text.getFloatValue());
            } else if(pChild->hasTagName("freqCutoff1"))
            {
                String text = pChild->getAllSubText();
                setParameter(freqCutoff1, text.getFloatValue());
            } else if(pChild->hasTagName("delTime1"))
            {
                String text = pChild->getAllSubText();
                setParameter(delTime1, text.getFloatValue());
            } else if(pChild->hasTagName("delTime2"))
            {
                String text = pChild->getAllSubText();
                setParameter(delTime2, text.getFloatValue());
            } else if(pChild->hasTagName("amp0"))
            {
                String text = pChild->getAllSubText();
                setParameter(amp0, text.getFloatValue());
            } else if(pChild->hasTagName("amp1"))
            {
                String text = pChild->getAllSubText();
                setParameter(amp1, text.getFloatValue());
            } else if(pChild->hasTagName("amp2"))
            {
                String text = pChild->getAllSubText();
                setParameter(amp2, text.getFloatValue());
            } else if(pChild->hasTagName("fb0"))
            {
                String text = pChild->getAllSubText();
                setParameter(fb0, text.getFloatValue());
            } else if(pChild->hasTagName("fb1"))
            {
                String text = pChild->getAllSubText();
                setParameter(fb1, text.getFloatValue());
            } else if(pChild->hasTagName("fb2"))
            {
                String text = pChild->getAllSubText();
                setParameter(fb2, text.getFloatValue());
            } /*else if(pChild->hasTagName("freqWind"))
            {
                String text = pChild->getAllSubText();
                setParameter(freqWind, text.getFloatValue());
            } */else if(pChild->hasTagName("syncOnLow"))
            {
                String text = pChild->getAllSubText();
                setParameter(syncOnLow, text.getFloatValue());
            } else if(pChild->hasTagName("syncOnMid"))
            {
                String text = pChild->getAllSubText();
                setParameter(syncOnMid, text.getFloatValue());
            } else if(pChild->hasTagName("syncOnHigh"))
            {
                String text = pChild->getAllSubText();
                setParameter(syncOnHigh, text.getFloatValue());
            }
        }
        delete pRoot;
        UIUpdateFlag=true;//Request UI update
    }
}

//==============================================================================
// This creates new instances of the plugin..
AudioProcessor* JUCE_CALLTYPE createPluginFilter()
{
    return new Mb1AudioProcessor();
}


// == Jennifer utility functions ==
//===============================================================================
// Create a Hann window
void Mb1AudioProcessor::makeHannWindow(int winLength, float * win)
{
    for( int n = 0; n < winLength; n++ )
    {
        win[n] = 0.5f * (1.0f - cos((2.0f*M_PI*n)/(winLength-1)));
    }
}

// adapted from Tom Erbe's scaleWindows( ) function
void Mb1AudioProcessor::scaleWindows(void)
{
    
    long ind;
    float sum, analFactor, synthFactor;
    
    sum = 0.0f;
    for( ind = 0; ind < sizeFFT; ind++ )
    {
        sum += analWindow[ind];
    }
    
    synthFactor = 2.0f/sum;
    analFactor = 2.0f/sum;
    
    for( ind = 0; ind < sizeFFT; ind++ )
    {
        analWindow[ind] *= analFactor;
        synthWindow[ind] *= analFactor;
    }
    
    sum = 0.0f;
    for( ind = 0; ind < sizeFFT; ind += blockSz )
    {
        sum += synthWindow[ind] * synthWindow[ind];
    }
    
    // scale for appleveclib
    sum = 1.0f/(sum*sizeFFT*2.0f);
    for( ind = 0; ind < sizeFFT; ind++ )
    {
        synthWindow[ind] *= sum;
    }
    
}

// sample rate and block size update
void Mb1AudioProcessor::blockSizeUpdate( )
{
    sizeFFT = 4*blockSz;
    halfSizeFFT = ( float )sizeFFT/2.0f;
    
    log2n = (long)log2f((float)sizeFFT);
    setupReal = create_fftsetup(log2n, FFT_RADIX2);
    
    // free the fft structures
    destroy_fftsetup(setupReal);
    free(&Observed);
    
    // reassign fft structure memory
    setupReal = vDSP_create_fftsetup(log2n, FFT_RADIX2);
    if (setupReal == NULL)
    {
        fprintf(stderr, "Error, vDSP_create_fftsetup failed.\n");
        exit (EXIT_FAILURE);
    }
    
    float *ObservedMemory = (float *)malloc(sizeFFT * sizeof(*ObservedMemory));
    
    if (ObservedMemory == NULL )
    {
        fprintf(stderr, "Error, failed to allocate memory.\n");
        exit(EXIT_FAILURE);
    }
    
    // assign half of ObservedMemory to reals and half to imaginaries.
    Observed = { ObservedMemory, ObservedMemory + halfSizeFFT };
    
    // free up memory
    delete [] input0;
    delete [] input1;
    delete [] fftBuffer0;
    delete [] fftBuffer1;
    delete [] ifftOut0;
    delete [] ifftOut1;
    delete [] olaBlock0;
    delete [] olaBlock1;
    delete [] analWindow;
    delete [] synthWindow;
    
    delete [] output0;
    delete [] output1;

    delete [] output00;
    delete [] output10;

    delete [] output10;
    delete [] output11;

    delete [] inShift0;
    delete [] inShift1;
    
    // reassign memory
    input0 = new float[sizeFFT];
    input1 = new float[sizeFFT];
    fftBuffer0 = new float[sizeFFT];
    fftBuffer1 = new float[sizeFFT];
    ifftOut0 = new float[sizeFFT];
    ifftOut1 = new float[sizeFFT];
    olaBlock0 = new float[sizeFFT];
    olaBlock1 = new float[sizeFFT];
    analWindow = new float[sizeFFT];
    synthWindow = new float[sizeFFT];
    
    output0 = new float[sizeFFT];
    output1 = new float[sizeFFT];
    output00 = new float[sizeFFT];
    output01 = new float[sizeFFT];
    output10 = new float[sizeFFT];
    output11 = new float[sizeFFT];
    
    inShift0 = new float[sizeFFT];
    inShift1 = new float[sizeFFT];
    
    // set everything to 0
    memset(input0, 0, sizeFFT*sizeof(float));
    memset(input1, 0, sizeFFT*sizeof(float));
    memset(fftBuffer0, 0, sizeFFT*sizeof(float));
    memset(fftBuffer1, 0, sizeFFT*sizeof(float));
    memset(ifftOut0, 0, sizeFFT*sizeof(float));
    memset(ifftOut1, 0, sizeFFT*sizeof(float));
    memset(olaBlock0, 0, sizeFFT*sizeof(float));
    memset(olaBlock1, 0, sizeFFT*sizeof(float));
    memset(analWindow, 0, sizeFFT*sizeof(float));
    memset(synthWindow, 0, sizeFFT*sizeof(float));
    
    memset(output0, 0, sizeFFT*sizeof(float));
    memset(output1, 0, sizeFFT*sizeof(float));
    
    memset(output00, 0, sizeFFT*sizeof(float));
    memset(output01, 0, sizeFFT*sizeof(float));
    memset(output10, 0, sizeFFT*sizeof(float));
    memset(output11, 0, sizeFFT*sizeof(float));
    
    memset(inShift0, 0, sizeFFT*sizeof(float));
    memset(inShift1, 0, sizeFFT*sizeof(float));
    
    
    // make the hann windows
    makeHannWindow(sizeFFT, analWindow);
    makeHannWindow(sizeFFT, synthWindow);
    // scale the windows
    scaleWindows( );
    
    // == MULTIBAND TEST!!! ==
    // band 1
    // free memory
    free(&Observed0);
    delete [] ifftOut00;
    delete [] ifftOut01;
    delete [] olaBlock00;
    delete [] olaBlock01;
    // set up structures for multiband FFT
    float *ObservedMemory0 = (float *)malloc(sizeFFT * sizeof(float *));
    if (ObservedMemory0 == NULL )// || ifftOut == NULL)
    {
        fprintf(stderr, "Error, failed to allocate memory for ObservedMemory0.\n");
        exit(EXIT_FAILURE);
    }
    Observed0 = { ObservedMemory0, ObservedMemory0 + halfSizeFFT };
    ifftOut00 = new float[sizeFFT];
    ifftOut01 = new float[sizeFFT];
    olaBlock00 = new float[sizeFFT];
    olaBlock01 = new float[sizeFFT];
    memset(ifftOut00, 0, sizeFFT*sizeof(float));
    memset(ifftOut01, 0, sizeFFT*sizeof(float));
    memset(olaBlock00, 0, sizeFFT*sizeof(float));
    memset(olaBlock01, 0, sizeFFT*sizeof(float));
    //binCutoff0 = 10;
    
    // band 2
    // free memory
    free(&Observed1);
    delete [] ifftOut10;
    delete [] ifftOut11;
    delete [] olaBlock10;
    delete [] olaBlock11;
    float *ObservedMemory1 = (float *)malloc(sizeFFT * sizeof(float *));
    if (ObservedMemory1 == NULL )// || ifftOut == NULL)
    {
        fprintf(stderr, "Error, failed to allocate memory for ObservedMemory0.\n");
        exit(EXIT_FAILURE);
    }
    Observed1 = { ObservedMemory1, ObservedMemory1 + halfSizeFFT };
    ifftOut10 = new float[sizeFFT];
    ifftOut11 = new float[sizeFFT];
    olaBlock10 = new float[sizeFFT];
    olaBlock11 = new float[sizeFFT];
    memset(ifftOut10, 0, sizeFFT*sizeof(float));
    memset(ifftOut11, 0, sizeFFT*sizeof(float));
    memset(olaBlock10, 0, sizeFFT*sizeof(float));
    memset(olaBlock11, 0, sizeFFT*sizeof(float));
    
    
     binCutoff0 = ( int )floor((freqCutoff0Val * sizeFFT)/fs);
     binCutoff1 = ( int )floor((freqCutoff1Val * sizeFFT)/fs);
    
    
    //binCutoff1 = 20;
    // ==============

    ampRunningSum = 0.0f;
    
    // tell the host the latency from the FFT overlap add operation
    setLatencySamples(blockSz*3);
}

float Mb1AudioProcessor::getFreqCutoff(int index)
{
    if( index == 0)
    {
        return freqCutoff0Val;
    } else if( index == 1 )
    {
        return freqCutoff1Val;
    } else
    {
        return 0.0f;
    }
}


// splitIntoSpectralBands( ) takes the FFT of the input signal (stored in Observed)
// and based on the bin cutoff frequencies in binCutoff0 and binCutoff1,
// will split the band by putting the FFTs of each band into:
// Observed0: the lows
// Observed1: the mids
// Observed: the highs
// if useFreqWindows is set to true, it will have overlap, otherwise,
// this will just split the spectrum using bin numbers
// using the windows actually makes it sound worse
void Mb1AudioProcessor::splitIntoSpectralBands( )
{
    // grab the correct bins
    float m = -1.0f/( float )(2.0f*winCurveBins);  // decreasing curve
    float bLow = ( float )-m*(binCutoff0 + winCurveBins);
    float bMidDec = ( float )-m*(binCutoff1 + winCurveBins);
    float bMidInc = ( float )m*(binCutoff0 - winCurveBins);
    float bHigh = ( float )m*(binCutoff1 - winCurveBins);
    for( int i = 0; i < halfSizeFFT; i++ )
    {
        if(i < binCutoff0)
        {
            // fill lower bins
            Observed0.realp[i] = Observed.realp[i];
            Observed0.imagp[i] = Observed.imagp[i];
            
            if( !useFreqWindows )
            {
                
                Observed1.realp[i] = 0.0f;
                Observed1.imagp[i] = 0.0f;
                Observed.realp[i] = 0.0f;
                Observed.imagp[i] = 0.0f;
            } else
            {
                // mids curve ascent and lower bins descent
                if( i >= (binCutoff0 - winCurveBins) )
                {
                    float yMid = ( float )(-m * i + bMidInc);
                    Observed1.realp[i] = Observed.realp[i]*yMid;
                    Observed1.imagp[i] = Observed.imagp[i]*yMid;
                    
                    float yLow = ( float )(m * i + bLow);
                    Observed0.realp[i] = Observed.realp[i]*yLow;
                    Observed0.imagp[i] = Observed.imagp[i]*yLow;;
                    
                } else
                {
                    Observed1.realp[i] = 0.0f;
                    Observed1.imagp[i] = 0.0f;
                }
                
                // highs curve (increasing)
                if( i >= (binCutoff1 - winCurveBins) )
                {
                    float yHigh = ( float )(-m * i + bHigh);
                    Observed.realp[i] = Observed.realp[i]*yHigh;
                    Observed.imagp[i] = Observed.imagp[i]*yHigh;
                } else
                {
                    Observed.realp[i] = 0.0f;
                    Observed.imagp[i] = 0.0f;
                }
                
            }
            
        } else if( i >= binCutoff0 && i < binCutoff1 )
        {
            // fill mids bins
            Observed1.realp[i] = Observed.realp[i];
            Observed1.imagp[i] = Observed.imagp[i];
            
            if( !useFreqWindows )
            {
                
                Observed0.realp[i] = 0.0f;
                Observed0.imagp[i] = 0.0f;
                Observed.realp[i] = 0.0f;
                Observed.imagp[i] = 0.0f;
                
            } else
            {
                // lower bins descent and mid bins ascent
                if( i <= (binCutoff0 + winCurveBins) )
                {
                    float yLow = ( float )(m * i + bLow);
                    Observed0.realp[i] = Observed.realp[i]*yLow;
                    Observed0.imagp[i] = Observed.imagp[i]*yLow;;
                    
                    float yMid = ( float )(-m * i + bMidInc);
                    Observed1.realp[i] = Observed.realp[i]*yMid;
                    Observed1.imagp[i] = Observed.imagp[i]*yMid;
                    
                } else
                {
                    Observed0.realp[i] = 0.0f;
                    Observed0.imagp[i] = 0.0f;
                }
                
                // highs curve ascent and mid bins descent
                if( i >= (binCutoff1 - winCurveBins) )
                {
                    float yHigh = ( float )(-m * i + bHigh);
                    Observed.realp[i] = Observed.realp[i]*yHigh;
                    Observed.imagp[i] = Observed.imagp[i]*yHigh;
                    
                    float yMid = ( float )(m * i + bMidDec);
                    Observed1.realp[i] = Observed.realp[i]*yMid;
                    Observed1.imagp[i] = Observed.imagp[i]*yMid;
                    
                } else
                {
                    Observed.realp[i] = 0.0f;
                    Observed.imagp[i] = 0.0f;
                }
            }
            
            
        } else
        {
            // fill highs bins
            if( !useFreqWindows )
            {
                
                Observed0.realp[i] = 0.0f;
                Observed0.imagp[i] = 0.0f;
                Observed1.realp[i] = 0.0f;
                Observed1.imagp[i] = 0.0f;
                
            } else
            {
                // lows curve descent and highs ascent
                if( i <= (binCutoff0 + winCurveBins) )
                {
                    float yLow = ( float )(m * i + bLow);
                    Observed0.realp[i] = Observed.realp[i]*yLow;
                    Observed0.imagp[i] = Observed.imagp[i]*yLow;
                    
                    float yHigh = ( float )(-m * i + bHigh);
                    Observed.realp[i] = Observed.realp[i]*yHigh;
                    Observed.imagp[i] = Observed.imagp[i]*yHigh;
                } else
                {
                    Observed0.realp[i] = 0.0f;
                    Observed0.imagp[i] = 0.0f;
                }
                
                // mids curve descent and highs ascent
                if( i <= ( binCutoff1 + winCurveBins) )
                {
                    float yMid = ( float )(m * i + bMidDec);
                    Observed1.realp[i] = Observed.realp[i]*yMid;
                    Observed1.imagp[i] = Observed.imagp[i]*yMid;
                    
                    float yHigh = ( float )(-m * i + bHigh);
                    Observed.realp[i] = Observed.realp[i]*yHigh;
                    Observed.imagp[i] = Observed.imagp[i]*yHigh;
                    
                } else
                {
                    Observed1.realp[i] = 0.0f;
                    Observed1.imagp[i] = 0.0f;
                }
            }
            
        }
    }
}

// snapDelTime fills in some class variables when we are in snapMode
// paramInd: the index of the variable in UserParams\
// svInd: the snapValue index (LOW, MID, or HIGH)
// delTimeQuant: the delay time that will be used to set the UserParams[paramInd] variable during snapMode
// delTimeTarget: delay time used to set UserParams[paramInd] when snapMode is off and the actually time
//  target that the delay line will try to delay by
void Mb1AudioProcessor::snapDelTime( int paramInd, int svInd, float * delTimeQuant, float * delTimeTarget  )
{
    // get index in quantization values
    int quantValInd = ( int )floor(UserParams[paramInd]*11.0f);
    
    // save this value for display
    *delTimeQuant = UserParams[paramInd];
    
    if(*delTimeQuant != 0.0f)
    {
        // calculate snapValue
        snapValue[svInd] = (1.0f / quantVals[quantValInd]) / 4.0f;
    
        // calculate delTimeTarget
        *delTimeTarget = (60.0f/bpm)/snapValue[svInd];
    } else
    {
        *delTimeTarget = 0.0f;
    }
    
}