/*
  ==============================================================================

    This file was auto-generated!

    It contains the basic startup code for a Juce application.

  ==============================================================================
*/

#ifndef PLUGINPROCESSOR_H_INCLUDED
#define PLUGINPROCESSOR_H_INCLUDED

#include "../JuceLibraryCode/JuceHeader.h"
#include <Accelerate/Accelerate.h>
#include "DelayLine.h"

// some constants for myself
#define LOW 0
#define MID 1
#define HIGH 2


//==============================================================================
/**
*/
class Mb1AudioProcessor  : public AudioProcessor
{
public:
    //==============================================================================
    Mb1AudioProcessor();
    ~Mb1AudioProcessor();

    //==============================================================================
    void prepareToPlay (double sampleRate, int samplesPerBlock);
    void releaseResources();

    void processBlock (AudioSampleBuffer& buffer, MidiBuffer& midiMessages);

    //==============================================================================
    AudioProcessorEditor* createEditor();
    bool hasEditor() const;

    //==============================================================================
    const String getName() const;

    int getNumParameters();

    float getParameter (int index);
    void setParameter (int index, float newValue);

    const String getParameterName (int index);
    const String getParameterText (int index);

    const String getInputChannelName (int channelIndex) const;
    const String getOutputChannelName (int channelIndex) const;
    bool isInputChannelStereoPair (int index) const;
    bool isOutputChannelStereoPair (int index) const;

    bool acceptsMidi() const;
    bool producesMidi() const;
    bool silenceInProducesSilenceOut() const;
    double getTailLengthSeconds() const;

    //==============================================================================
    int getNumPrograms();
    int getCurrentProgram();
    void setCurrentProgram (int index);
    const String getProgramName (int index);
    void changeProgramName (int index, const String& newName);

    //==============================================================================
    void getStateInformation (MemoryBlock& destData);
    void setStateInformation (const void* data, int sizeInBytes);
    
    // jennifer functions
    void makeHannWindow(int winLength, float * win);
    void scaleWindows(void);
    void blockSizeUpdate( );
    float getFreqCutoff(int index);
    void splitIntoSpectralBands( );
    void snapDelTime( int paramInd, int svInd, float * delTimeQuant, float * delTimeTarget  );
    
    enum Parameters{
        //ampCurrent = 0,
        freqCutoff0 = 0,
        freqCutoff1,
        delTime0,
        delTime1,
        delTime2,
        amp0,
        amp1,
        amp2,
        fb0,
        fb1,
        fb2,
        //freqWind,
        syncOnLow,
        syncOnMid,
        syncOnHigh,
        totalNumParam
    };
    bool NeedsUIUpdate(){ return UIUpdateFlag; };
    void RequestUIUpdate(){ UIUpdateFlag=true; };
    void ClearUIUpdateFlag(){ UIUpdateFlag=false; };
    void setPlayHead( AudioPlayHead * newPlayHead );
    AudioPlayHead * getPlayHead( ) const noexcept;
    
    StringArray quantValStrings;

private:
    //==============================================================================
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (Mb1AudioProcessor)
    
    float UserParams[totalNumParam];
    float ampCurrentVal;
    
    float freqCutoff0Val;
    float freqCutoff1Val;
    float freqCutoffM, freqCutoffB;
    
    float amp0ValInc, amp0ValCurrent, amp0ValTarget;
    float amp1ValInc, amp1ValCurrent, amp1ValTarget;
    float amp2ValInc, amp2ValCurrent, amp2ValTarget;
    
    //float fb0Val, fb1Val, fb2Val;
    float fb0ValInc, fb0ValCurrent, fb0ValTarget;
    float fb1ValInc, fb1ValCurrent, fb1ValTarget;
    float fb2ValInc, fb2ValCurrent, fb2ValTarget;
    
    float delTime0Inc, delTime0Current, delTime0Target;
    float delTime1Inc, delTime1Current, delTime1Target;
    float delTime2Inc, delTime2Current, delTime2Target;

    float delTimesQuant[3];
    
    int delTimeIncCntr;
    int numDelTimeIncs;
    
    int lfoCntr[3];
    float lfoSpeed[3];
    float lfoSwing[3];
    float lfoM0[3];
    int lfoLength[3];
    float * lfoMnHigh;
    
    int blockCntr;
    bool UIUpdateFlag;
    
    // jennifer parameters
    long halfSizeFFT, sizeFFT, log2n, blockSz, blockSzLast;
    float pi, twoPi;
    // vDSP parameters
    FFTSetup setupReal;
	COMPLEX_SPLIT split;
    const vDSP_Stride stride = 1;
    DSPSplitComplex Observed;
    
    // ----------------------------
    // MULTIBAND TEST
    // band 0
    DSPSplitComplex Observed0;
    float * ifftOut00;
    float * ifftOut01;
    float * olaBlock00;
    float * olaBlock01;
    int binCutoff0;
    // band 1
    DSPSplitComplex Observed1;
    float * ifftOut10;
    float * ifftOut11;
    float * olaBlock10;
    float * olaBlock11;
    int binCutoff1;
    // ----------------------------
    
    float * ifftOut0;
    float * ifftOut1;
    float * olaBlock0;
    float * olaBlock1;
    
    float * input0;
    float * input1;
    float * output0;
    float * output1;
    float * output00;
    float * output01;
    float * output10;
    float * output11;
    
    int bufferPosition0, bufferPosition1;
    float * inShift0;
    float * inShift1;
    
    float * fftBuffer0;
    float * fftBuffer1;
    
    float * analWindow;
    float * synthWindow;
    
    DelayLine * delLine00;
    DelayLine * delLine01;
    DelayLine * delLine10;
    DelayLine * delLine11;
    DelayLine * delLine20;
    DelayLine * delLine21;
    
    // cross fading delay lines
    DelayLine * delLineLowFade0;
    DelayLine * delLineLowFade1;
    DelayLine * delLineMidFade0;
    DelayLine * delLineMidFade1;
    DelayLine * delLineHighFade0;
    DelayLine * delLineHighFade1;
    
    
    // cross fading delay line variables
    // mids
    float lowDelFade0, lowDelFadeInc0, lowDelFade1, lowDelFadeInc1;
    int origOrFadeLow, transOnLow, lowDelFadeCntr0, lowDelFadeCntr1;
    // mids
    float midDelFade0, midDelFadeInc0, midDelFade1, midDelFadeInc1;
    int origOrFadeMid, transOnMid, midDelFadeCntr0, midDelFadeCntr1;
    // highs
    float highDelFade0, highDelFadeInc0, highDelFade1, highDelFadeInc1;
    int origOrFadeHigh, transOnHigh, highDelFadeCntr0, highDelFadeCntr1;
    
    float * testFilter;
    float ampRunningSum;
    
    float fs, fsLast;
    
    
    // window curve
    int useFreqWindows;
    int winCurveBins;
    
    // delay line modulation
    float m0, A, f0;
    float * delLineMod;
    int delLineModCnt;
    
    // constant-q frequency scaling
    float log2toOneOver24;
    
    // this keeps a copy of the last set of time info that was acquired during an audio
    // callback - the UI component will read this and display it.
    AudioPlayHead * ap;
    AudioPlayHead::CurrentPositionInfo lastPosInfo;
    AudioPlayHead::CurrentPositionInfo currPos;
    bool snapMode[3];
    float snapValue[3];
    int lastBar, lastBeat;
    int bar, beat;
    float bpm;
    
    float quantVals[12] =
    {
        0.0f,
        1.0f/64.0f,
        1.0f/32.0f,
        1.0f/24.0f,
        1.0f/16.0f,
        1.0f/12.0f,
        1.0f/8.0f,
        1.0f/6.0f,
        1.0f/4.0f,
        1.0f/3.0f,
        1.0f/2.0f,
        1.0f
    };

    
};

#endif  // PLUGINPROCESSOR_H_INCLUDED
