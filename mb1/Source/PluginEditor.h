/*
  ==============================================================================

  This is an automatically generated GUI class created by the Introjucer!

  Be careful when adding custom code to these files, as only the code within
  the "//[xyz]" and "//[/xyz]" sections will be retained when the file is loaded
  and re-saved.

  Created with Introjucer version: 3.1.0

  ------------------------------------------------------------------------------

  The Introjucer is part of the JUCE library - "Jules' Utility Class Extensions"
  Copyright 2004-13 by Raw Material Software Ltd.

  ==============================================================================
*/

#ifndef __JUCE_HEADER_D34281729F7E6EBB__
#define __JUCE_HEADER_D34281729F7E6EBB__

//[Headers]     -- You can add your own extra header files here --
#include "JuceHeader.h"
#include "PluginProcessor.h"
#include "mbLookAndFeel.h"
//[/Headers]



//==============================================================================
/**
                                                                    //[Comments]
    An auto-generated component, created by the Introjucer.

    Describe your class and how it works here!
                                                                    //[/Comments]
*/
class Mb1AudioProcessorEditor  : public AudioProcessorEditor,
                                 public Timer,
                                 public SliderListener,
                                 public ButtonListener
{
public:
    //==============================================================================
    Mb1AudioProcessorEditor (Mb1AudioProcessor* ownerFilter);
    ~Mb1AudioProcessorEditor();

    //==============================================================================
    //[UserMethods]     -- You can add your own custom methods in this section.
    void timerCallback( );
    Mb1AudioProcessor * getProcessor( ) const
    {
        return static_cast<Mb1AudioProcessor *>( getAudioProcessor( ) );
    }

    //[/UserMethods]

    void paint (Graphics& g);
    void resized();
    void sliderValueChanged (Slider* sliderThatWasMoved);
    void buttonClicked (Button* buttonThatWasClicked);

    // Binary resources:
    static const char* woodtablebw_vsmall_jpg;
    static const int woodtablebw_vsmall_jpgSize;


private:
    //[UserVariables]   -- You can add your own custom variables in this section.
    mbLookAndFeel myLookAndFeel;
    //[/UserVariables]

    //==============================================================================
    ScopedPointer<Slider> lowFreqSld;
    ScopedPointer<Label> freqCutoffLbl;
    ScopedPointer<Slider> delTimeSld;
    ScopedPointer<Label> delayTimeLbl;
    ScopedPointer<Slider> delTimeSld2;
    ScopedPointer<Label> lowFreqLbl;
    ScopedPointer<Slider> delTimeSld3;
    ScopedPointer<Label> midFreqLbl;
    ScopedPointer<Slider> ampLowSld;
    ScopedPointer<Slider> ampMidSld;
    ScopedPointer<Slider> ampHighSld;
    ScopedPointer<Label> ampLbl;
    ScopedPointer<Label> highFreqLbl2;
    ScopedPointer<Slider> fbLowSld;
    ScopedPointer<Label> fbLbl;
    ScopedPointer<Slider> fbMidSld;
    ScopedPointer<Slider> fbHighSld;
    ScopedPointer<Label> lowFreqValLbl;
    ScopedPointer<Label> lowDelTimeLbl;
    ScopedPointer<Label> midDelTimeLbl;
    ScopedPointer<Label> highDelTimeLbl;
    ScopedPointer<Label> highFreqValLbl;
    ScopedPointer<Label> lowHzLbl;
    ScopedPointer<Label> highHzLbl;
    ScopedPointer<ToggleButton> syncOnHighTglBtn;
    ScopedPointer<ToggleButton> syncOnMidTglBtn;
    ScopedPointer<ToggleButton> syncOnLowTglBtn;
    ScopedPointer<Label> lowAmpLbl;
    ScopedPointer<Label> midAmpLbl;
    ScopedPointer<Label> highAmpLbl;
    ScopedPointer<Label> lowFbLbl;
    ScopedPointer<Label> midFbLbl;
    ScopedPointer<Label> highFbLbl;
    Image cachedImage_woodtablebw_vsmall_jpg;


    //==============================================================================
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (Mb1AudioProcessorEditor)
};

//[EndFile] You can add extra defines here...
//[/EndFile]

#endif   // __JUCE_HEADER_D34281729F7E6EBB__
